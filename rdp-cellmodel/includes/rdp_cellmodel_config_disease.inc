<?php

/**
 * @file
 * Provide functions to put the configuration page of the Cell Model Catalogue
 *   together.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Generate the form field for the Cell Model Catalogue configuration page.
 */
function rdp_cellmodel_config_disease($form, &$form_state) {
  $form = [];

  if (isset($_GET['id'])) {
    $disease = CellLineDiseaseRepository::findById($_GET['id']);
  }
  else {
    $disease = new CellLineDisease();
  }

  $form_state['id'] = $disease->getId();

  $form ['disease_name'] = [
    '#type' => 'textfield',
    '#title' => 'Name of Disease',
    '#required' => TRUE,
    '#default_value' => $disease->getDiseaseName(),
  ];

  $form ['disease_icd'] = [
    '#type' => 'textfield',
    '#title' => 'ICD of Disease',
    '#required' => TRUE,
    '#default_value' => $disease->getIcd(),
  ];

  // Cancel button. Returns to the default landing page.
  $form['back'] = [
    '#type' => 'button',
    '#submit' => ['rdp_cellmodel_config_disease_back'],
    '#value' => '<i class="	glyphicon glyphicon-arrow-left"></i> ' . t('Back to Config'),
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => [],
    '#attributes' => ['class' => ['btn-primary btn-sm']],
  ];

  $form['disease_submit'] = [
    '#type' => 'submit',
    '#value' => ' Save',
    '#attributes' => ['class' => ['btn-success btn-sm']],
  ];

  return $form;
}

/**
 * Process what is done, when the Submit button is pressed.
 */
function rdp_cellmodel_config_disease_submit($form, &$form_state) {
  $disease = new CellLineDisease();
  // Get the unchanged disease to compare it with the changed disease.
  // If there were changes made, apply them to all diagnoses with the old values.
  $old_disease = CellLineDiseaseRepository::findById($form_state['id']);

  $disease->setId($form_state['id']);

  $disease->setDiseaseName($form_state['values']['disease_name']);
  // Apply changes of disease name (if any) to all diagnoses.
  if ($old_disease->getDiseaseName() !== $disease->getDiseaseName()) {
    $condition = db_and();
    $condition->condition('disease', $old_disease->getDiseaseName(), '=');
    $diagnoses = CellLineDiagnosisRepository::findBy($condition);
    foreach ($diagnoses as $diagnosis) {
      $diagnosis->setDisease($disease->getDiseaseName());
      $diagnosis->save();
    }
  }
  // Apply changes of icd (if any) to all diagnoses.
  $disease->setIcd($form_state['values']['disease_icd']);
  if ($old_disease->getIcd() !== $disease->getIcd()) {
    $condition = db_and();
    $condition->condition('icd', $old_disease->getIcd(), '=');
    $diagnoses = CellLineDiagnosisRepository::findBy($condition);
    foreach ($diagnoses as $diagnosis) {
      $diagnosis->setIcd($disease->getIcd());
      $diagnosis->save();
    }
  }

  if (!empty($disease->getDiseaseName()) and !empty($disease->getIcd())) {
    $disease->save();
    $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_DEFAULT);
    drupal_set_message('Disease with name <i>' . $disease->getDiseaseName() . '</i> and ICD <i>' .
      $disease->getIcd() . '</i> saved successfully.');
  }
  else {
    drupal_set_message('Could not save disease. Disease name or ICD are empty.', 'error');
  }
}

/**
 * Submit function for the back button.
 */
function rdp_cellmodel_config_disease_back($form, &$form_state) {

  // Redirect back to the Edit Cell Line form.
  $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_DEFAULT);
}
