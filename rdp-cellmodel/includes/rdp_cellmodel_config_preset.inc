<?php

/**
 * @file
 * Provide functions to put the configuration page of the Cell Model Catalogue Preset part
 *   together.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Generate the form field for the Cell Model Catalogue configuration page.
 */
function rdp_cellmodel_config_preset($form, &$form_state) {
  $form = [];

  $cell_line= new CellLine();

  if (isset($_GET['id'])) {
    $preset = CellLinePresetRepository::findById($_GET['id']);
  }
  else {
    $preset = new CellLinePreset();
  }

  $form_state['id'] = $preset->getId();

  $form ['preset_name'] = [
    '#type' => 'textfield',
    '#title' => 'Name of Preset',
    '#required' => TRUE,
    '#default_value' => $preset->getName(),
  ];

  $form['ethics_preset_1'] = $cell_line->getFormFieldEthics1($preset->getEthicsPreset1());
  $form['ethics_preset_2'] = $cell_line->getFormFieldEthics2($preset->getEthicsPreset2());
  $form['ethics_preset_3'] = $cell_line->getFormFieldEthics3($preset->getEthicsPreset3());
  $form['ethics_preset_4'] = $cell_line->getFormFieldEthics4($preset->getEthicsPreset4());
  $form['ethics_preset_5'] = $cell_line->getFormFieldEthics5($preset->getEthicsPreset5());
  $form['ethics_preset_6'] = $cell_line->getFormFieldEthics6($preset->getEthicsPreset6());
  $form['ethics_preset_7'] = $cell_line->getFormFieldEthics7($preset->getEthicsPreset7());
  $form['ethics_preset_8'] = $cell_line->getFormFieldEthics8($preset->getEthicsPreset8());
  $form['ethics_preset_9'] = $cell_line->getFormFieldEthics9($preset->getEthicsPreset9());
  $form['ethics_preset_10'] = $cell_line->getFormFieldEthics10($preset->getEthicsPreset10());
  $form['ethics_preset_11'] = $cell_line->getFormFieldEthics11($preset->getEthicsPreset11());
  $form['ethics_preset_12'] = $cell_line->getFormFieldEthics12($preset->getEthicsPreset12());
  $form['ethics_preset_13'] = $cell_line->getFormFieldEthics13($preset->getEthicsPreset13());
  $form['ethics_preset_14'] = $cell_line->getFormFieldEthics14($preset->getEthicsPreset14());
  $form['ethics_preset_15'] = $cell_line->getFormFieldEthics15($preset->getEthicsPreset15());
  $form['ethics_preset_16'] = $cell_line->getFormFieldEthics16($preset->getEthicsPreset16());
  $form['ethics_preset_17'] = $cell_line->getFormFieldEthics17($preset->getEthicsPreset17());
  $form['ethics_preset_18'] = $cell_line->getFormFieldEthics18($preset->getEthicsPreset18());
  $form['ethics_preset_19'] = $cell_line->getFormFieldEthics19($preset->getEthicsPreset19());
  $form['ethics_preset_20'] = $cell_line->getFormFieldEthics20($preset->getEthicsPreset20());
  $form['ethics_preset_21'] = $cell_line->getFormFieldEthics21($preset->getEthicsPreset21());
  $form['ethics_preset_22'] = $cell_line->getFormFieldEthics22($preset->getEthicsPreset22());
  $form['ethics_preset_23'] = $cell_line->getFormFieldEthics23($preset->getEthicsPreset23());
  $form['ethics_preset_24'] = $cell_line->getFormFieldEthics24($preset->getEthicsPreset24());
  $form['ethics_preset_25'] = $cell_line->getFormFieldEthics25($preset->getEthicsPreset25());
  $form['ethics_preset_26'] = $cell_line->getFormFieldEthics26($preset->getEthicsPreset26());
  $form['ethics_preset_27'] = $cell_line->getFormFieldEthics27($preset->getEthicsPreset27());

  // Cancel button. Returns to config page.
  $form['back'] = [
    '#type' => 'button',
    '#submit' => ['rdp_cellmodel_config_preset_back'],
    '#value' => '<i class="	glyphicon glyphicon-arrow-left"></i> ' . t('Back to Config'),
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => [],
    '#attributes' => ['class' => ['btn-primary btn-sm']],
  ];

  $form['preset_submit'] = [
    '#type' => 'submit',
    '#value' => ' Save',
    '#attributes' => ['class' => ['btn-success btn-sm']],
  ];

  return $form;
}

/**
 * Process what is done, when the Submit button is pressed.
 */
function rdp_cellmodel_config_preset_submit($form, &$form_state) {
  $preset = new CellLinePreset();
  $preset->setId($form_state['id']);

  $preset->setName($form_state['values']['preset_name']);
  $preset->setEthicsPreset1($form_state['values']['ethics_preset_1']);
  $preset->setEthicsPreset2($form_state['values']['ethics_preset_2']);
  $preset->setEthicsPreset3($form_state['values']['ethics_preset_3']);
  $preset->setEthicsPreset4($form_state['values']['ethics_preset_4']);
  $preset->setEthicsPreset5($form_state['values']['ethics_preset_5']);
  $preset->setEthicsPreset6($form_state['values']['ethics_preset_6']);
  $preset->setEthicsPreset7($form_state['values']['ethics_preset_7']);
  $preset->setEthicsPreset8($form_state['values']['ethics_preset_8']);
  $preset->setEthicsPreset9($form_state['values']['ethics_preset_9']);
  $preset->setEthicsPreset10($form_state['values']['ethics_preset_10']);
  $preset->setEthicsPreset11($form_state['values']['ethics_preset_11']);
  $preset->setEthicsPreset12($form_state['values']['ethics_preset_12']);
  $preset->setEthicsPreset13($form_state['values']['ethics_preset_13']);
  $preset->setEthicsPreset14($form_state['values']['ethics_preset_14']);
  $preset->setEthicsPreset15($form_state['values']['ethics_preset_15']);
  $preset->setEthicsPreset16($form_state['values']['ethics_preset_16']);
  $preset->setEthicsPreset17($form_state['values']['ethics_preset_17']);
  $preset->setEthicsPreset18($form_state['values']['ethics_preset_18']);
  $preset->setEthicsPreset19($form_state['values']['ethics_preset_19']);
  $preset->setEthicsPreset20($form_state['values']['ethics_preset_20']);
  $preset->setEthicsPreset21($form_state['values']['ethics_preset_21']);
  $preset->setEthicsPreset22($form_state['values']['ethics_preset_22']);
  $preset->setEthicsPreset23($form_state['values']['ethics_preset_23']);
  $preset->setEthicsPreset24($form_state['values']['ethics_preset_24']);
  $preset->setEthicsPreset25($form_state['values']['ethics_preset_25']);
  $preset->setEthicsPreset26($form_state['values']['ethics_preset_26']);
  $preset->setEthicsPreset27($form_state['values']['ethics_preset_27']);

  if (!empty($preset->getName())) {
    $preset->save();
    $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_DEFAULT);
    drupal_set_message('Preset with name <i>' . $preset->getName() . '</i> was saved successfully.');
  }
  else {
    drupal_set_message('Could not save preset. Preset name empty.', 'error');
  }
}

/**
 * Submit function for the back button.
 */
function rdp_cellmodel_config_preset_back($form, &$form_state) {

  // Redirect back to the Edit Cell Line form.
  $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_DEFAULT);
}
