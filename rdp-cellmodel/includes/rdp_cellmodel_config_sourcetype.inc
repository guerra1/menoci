<?php
function rdp_cellmodel_config_sourcetype($form, &$form_state){
    $form = [];

    if(!isset($_GET["action"]))
        drupal_set_message("Undefined request", 'error');
    else if($_GET["action"] == 'delete' && isset($_GET["s"])){
        CellLineSourceRepository::delete($_GET["s"]);
        drupal_Set_message("Source Type \"".$_GET["s"]."\" deleted");
    }
    else if($_GET["action"] == 'add'){
        $form['sourcetype'] = [
          '#type' => 'textfield',
          '#title' => 'Name of new Source Type',
          '#required' => TRUE,
        ];
        $form['sourcetype_submit'] = [
          '#type' => 'submit',
          '#value' => 'Save',
          '#attributes' => ['class' => ['btn-success btn-sm']]
        ];
    }
    else{
        drupal_set_message("Undefined Action", 'error');
    }

    $form['back'] = [
        '#type' => 'button',
        '#submit' => ['rdp_cellmodel_config_sourcetype_back'],
        '#value' => 'Back',
        '#executes_submit_callback' => TRUE,
        '#limit_validation_errors' => [],
        '#attributes' => ['class' => ['btn-primary btn-sm']],
    ];
    return $form;
}

function rdp_cellmodel_config_sourcetype_submit($form, &$form_state){
    $new_sourcetype = $form_state['values']['sourcetype'];
    $query = CellLineSourceRepository::save($new_sourcetype);
    if(!$query) drupal_set_message('Could not add this Source Type; Check for duplicates', 'error');
    else drupal_set_message('New Source Type added');
}

function rdp_cellmodel_config_sourcetype_back($form, &$form_state) {
    $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_DEFAULT);
}