<?php

/**
 * @return string
 * @throws \Exception
 */
function rdp_cellmodel_clone_starlims_details() {

  if (!isset($_GET['line']) or !user_access(RDP_CELLMODEL_PERMISSION_MANAGE)) {
    drupal_access_denied();
  }

  $cell_line = CellLineRepository::findById($_GET['line']);

  $btn_back = l('<i class="	glyphicon glyphicon-arrow-left"></i> ' . t('Cell Line Page'),
    rdp_cellmodel_url(RDP_CELLMODEL_URL_CELL_LINE_VIEW) . '?tab=' . $cell_line->getTabType() . '&line=' . $cell_line->getId(),
    [
      'html' => TRUE,
      'attributes' => [
        'class' => ['btn btn-info btn-sm'],
        'style' => 'margin-left: 5px;',
      ],
    ]
  );

  $starlims_clones_table = $btn_back;

  $tbl_clones_header = [
    CellmodelConstants::getFieldTitlesStarlimsClones()['clone_name'],
    ['data' => CellmodelConstants::getFieldTitlesStarlimsClones()['starlims_id'], 'field' => 'a.starlims_id'],
    ['data' => CellmodelConstants::getFieldTitlesStarlimsClones()['starlims_lab_id'], 'field' => 'a.starlims_lab_id'],
    CellmodelConstants::getFieldTitles()['project'],
    CellmodelConstants::getFieldTitles()['biosafety'],
    ['data' => CellmodelConstants::getFieldTitlesStarlimsClones()['tubes'], 'field' => 'a.tubes'],
  ];

  $clones = CellLineStarLimsCloneRepository::findAll($tbl_clones_header);
  $tbl_clones_rows = [];
  foreach ($clones as $clone) {
    $cell_line = CellLineRepository::findById($clone->getCellLine());
    $starlims_id_explode = explode("_", $clone->getCloneStarlimsId());
    $clone_number = (int)$starlims_id_explode[4];
    $tbl_clones_rows [] = [
      $clone->getCloneStarlimsLabId() . " clone " . $clone_number,
      $clone->getCloneStarlimsId(),
      $clone->getCloneStarlimsLabId(),
      $cell_line->getProject(),
      $cell_line->getBiosafety(),
      $clone->getCloneStarlimsTubes(),
    ];
  }

  if (!empty($tbl_clones_rows)) {
    $starlims_clones_table .= theme('table', [
      'header' => $tbl_clones_header,
      'rows' => $tbl_clones_rows,
    ]);
  }

  return $starlims_clones_table;

}
