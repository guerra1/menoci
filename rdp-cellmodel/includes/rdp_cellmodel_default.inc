<?php

/**
 * @file
 * Provide functions to put the default page of the Cell Model Catalogue
 *   together.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Show the default page of the Cell Model Catalogue.
 *
 * The main page of the Cell Model Catalogue contains a table showing all
 * CellLines in the database separated by different tabs. It also
 * provides the user with buttons to create and edit CellLines, import and
 * export CellLines via Excel spreadsheets and navigating to a statistics page.
 * The page also features a search and filter bar.
 *
 * @return string
 *   The string for rendering the default page.
 * @throws \Exception
 */
function rdp_cellmodel_default() {

  // Set the title for this page.
  drupal_set_title('Cell Model Catalogue');

  // Check if all CellLines have a tab type assigned.
  // Set to 'internal' if no tab type is set.
  // Necessary so no CellLines is hidden.
  $cell_lines_to_check = CellLineRepository::findAll();
  foreach ($cell_lines_to_check as $cell_line_to_check) {
    if (!$cell_line_to_check->getTabType()) {
      $cell_line_to_check->setTabType('internal');
      $cell_line_to_check->save();
      drupal_set_message(t('<u>Notice:</u> The cell line <i><a href="@url?tab=@tab_type&line=@id">@lab_id</a>
    </i> had no assigned tab type. <br>
      The tab type of this cell line was automatically set to <i>Internal</i>.', [
        '@url' => rdp_cellmodel_url(RDP_CELLMODEL_URL_CELL_LINE_VIEW),
        '@tab_type' => $cell_line_to_check->getTabType(),
        '@id' => $cell_line_to_check->getId(),
        '@lab_id' => $cell_line_to_check->getLabId(),
      ]), 'warning');
    }
  }

  // Define the current tab type and string.
  if (isset($_GET['tab'])) {
    $tab_type = $_GET['tab'];
    $tab_string = CellmodelConstants::getOptionsTabType() [$_GET['tab']];
  }
  else {
    $tab_type = 'internal';
    $tab_string = 'Internal';
  }

  // Generate the 'Show All Cell Lines' button.
  $var_string = '?show_all=TRUE';
  if (isset($_GET['tab'])) {
    $var_string .= '&tab=' . $_GET['tab'];
  }
  $btn_show_all = l('<span class="glyphicon glyphicon-zoom-in"></span>',
    rdp_cellmodel_url(RDP_CELLMODEL_URL_DEFAULT) . $var_string, [
      'html' => TRUE,
      'attributes' => [
        'data-toggle' => 'tooltip',
        'data-placement' => 'right',
        'title' => "Show All Cell Lines",
        'class' => ['btn btn-default btn-xs'],
        'style' => 'position: relative;',
      ],
    ]);

  // Set the correct ID Label and define the header of the cell line table.
  // The empty fields are for the buttons (edit, duplicate and view) and the visibility and warning symbols.
  if ($tab_type == 'gen_mod') {
    $id_label = t('Editing ID');
    if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE)) {
      $table_header = [
        ['data' => ''],
        ['data' => ''],
        ['data' => ''],
        ['data' => ''],
        ['data' => $id_label, 'field' => 't.lab_id'],
        ['data' => t('hPSCreg Name'), 'field' => 't.hpscreg_name'],
        ['data' => t('Cell Type'), 'field' => 't.cell_type'],
        ['data' => t('Editing Strategy'), 'field' => 'm.editing_strategy'],
        ['data' => t('Targeted Gene'), 'field' => 'm.editing_targeted_gene'],
        ['data' => t('Editing Method'), 'field' => 'm.editing_method1'],
        ['data' => t('Grade'), 'field' => 't.grade'],
        ['data' => ''],
      ];
    }
    elseif (user_access(RDP_CELLMODEL_PERMISSION_VIEW)) {
      $table_header = [
        ['data' => ''],
        ['data' => ''],
        ['data' => $id_label, 'field' => 't.lab_id'],
        ['data' => t('hPSCreg Name'), 'field' => 't.hpscreg_name'],
        ['data' => t('Cell Type'), 'field' => 't.cell_type'],
        ['data' => t('Editing Strategy'), 'field' => 'm.editing_strategy'],
        ['data' => t('Targeted Gene'), 'field' => 'm.editing_targeted_gene'],
        ['data' => t('Editing Method'), 'field' => 'm.editing_method1'],
        ['data' => t('Grade'), 'field' => 't.grade'],
        ['data' => $btn_show_all],
      ];
    }
    else {
      $table_header = [
        ['data' => ''],
        ['data' => t('hPSCreg Name'), 'field' => 't.hpscreg_name'],
        ['data' => t('Cell Type'), 'field' => 't.cell_type'],
        ['data' => t('Editing Strategy'), 'field' => 'm.editing_strategy'],
        ['data' => t('Targeted Gene'), 'field' => 'm.editing_targeted_gene'],
        ['data' => t('Editing Method'), 'field' => 'm.editing_method1'],
        ['data' => t('Grade'), 'field' => 't.grade'],
        ['data' => $btn_show_all],
      ];
    }
  }
  else {
    $id_label = t('Lab ID');
    if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE)) {
      $table_header = [
        ['data' => ''],
        ['data' => ''],
        ['data' => ''],
        ['data' => ''],
        ['data' => $id_label, 'field' => 't.lab_id'],
        ['data' => t('iPSC ID'), 'field' => 't.ipsc_id'],
        ['data' => t('hPSCreg Name'), 'field' => 't.hpscreg_name'],
        ['data' => t('Cell Type'), 'field' => 't.cell_type'],
        ['data' => t('ICD Code'), 'field' => 'n.icd'],
        ['data' => t('Disease'), 'field' => 'n.disease'],
        ['data' => t('Source'), 'field' => 't.source_type'],
        ['data' => t('Reprogramming'), 'field' => 't.reprogramming_method'],
        ['data' => t('Grade'), 'field' => 't.grade'],
        ['data' => ''],
      ];
    }
    elseif (user_access(RDP_CELLMODEL_PERMISSION_VIEW)) {
      $table_header = [
        ['data' => ''],
        ['data' => ''],
        ['data' => t('Lab ID'), 'field' => 't.lab_id'],
        ['data' => t('iPSC ID'), 'field' => 't.ipsc_id'],
        ['data' => t('hPSCreg Name'), 'field' => 't.hpscreg_name'],
        ['data' => t('Cell Type'), 'field' => 't.cell_type'],
        ['data' => t('ICD Code'), 'field' => 'n.icd'],
        ['data' => t('Disease'), 'field' => 'n.disease'],
        ['data' => t('Source'), 'field' => 't.source_type'],
        ['data' => t('Reprogramming'), 'field' => 't.reprogramming_method'],
        ['data' => t('Grade'), 'field' => 't.grade'],
        ['data' => $btn_show_all],
      ];
    }
    else {
      $table_header = [
        ['data' => ''],
        ['data' => t('hPSCreg Name'), 'field' => 't.hpscreg_name'],
        ['data' => t('Cell Type'), 'field' => 't.cell_type'],
        ['data' => t('ICD Code'), 'field' => 'n.icd'],
        ['data' => t('Disease'), 'field' => 'n.disease'],
        ['data' => t('Source'), 'field' => 't.source_type'],
        ['data' => t('Reprogramming'), 'field' => 't.reprogramming_method'],
        ['data' => t('Grade'), 'field' => 't.grade'],
        ['data' => $btn_show_all],
      ];
    }
  }

  // --------------------------- <<< SEARCH QUERY >>>---------------------------

  // If the user filtered for a cell line, deal with the filter query.
  $filtered = FALSE;
  $db_condition_filter = db_and();
  $filter_params = [];
  if (isset($_GET['filter_type'])) {
    $db_condition_filter->condition('cell_type', $_GET['filter_type'], '=');
    $filter_params ['Cell Type'] = $_GET['filter_type'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_icd'])) {
    $db_condition_filter->condition('icd', $_GET['filter_icd'], '=');
    $filter_params ['ICD'] = $_GET['filter_icd'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_disease'])) {
    $db_condition_filter->condition('disease', $_GET['filter_disease'], '=');
    $filter_params ['Disease'] = $_GET['filter_disease'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_source'])) {
    $db_condition_filter->condition('source_type', $_GET['filter_source'], '=');
    $filter_params ['Source'] = $_GET['filter_source'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_rep_method'])) {
    $db_condition_filter->condition('reprogramming_method', $_GET['filter_rep_method'], '=');
    $filter_params ['Reprogramming'] = $_GET['filter_rep_method'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_grade'])) {
    $db_condition_filter->condition('grade', $_GET['filter_grade'], '=');
    $filter_params ['Grade'] = $_GET['filter_grade'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_gender'])) {
    $db_condition_filter->condition('donor_gender', $_GET['filter_gender'], '=');
    $filter_params ['Gender'] = $_GET['filter_gender'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_age'])) {
    if ($_GET['filter_age'] == '<= 80') {
      $min = 80;
      $max = 150;
    }
    else {
      $min_max = explode('-', $_GET['filter_age']);
      $min = $min_max[0];
      $max = $min_max[1];
    }

    $db_condition_filter->condition('donor_age', $min, '>=');
    $db_condition_filter->condition('donor_age', $max, '<=');
    $filter_params ['Age'] = $_GET['filter_age'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_editing_strategy'])) {
    $db_condition_filter->condition('editing_strategy', $_GET['filter_editing_strategy'], '=');
    $filter_params ['Editing Strategy'] = $_GET['filter_editing_strategy'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_editing_method1'])) {
    $db_condition_filter->condition('editing_method1', $_GET['filter_editing_method1'], '=');
    $filter_params ['Editing Method (1/3)'] = $_GET['filter_editing_method1'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_editing_method2'])) {
    $db_condition_filter->condition('editing_method2', $_GET['filter_editing_method2'], '=');
    $filter_params ['Editing Method (2/3)'] = $_GET['filter_editing_method2'];
    $filtered = TRUE;
  }
  if (isset($_GET['filter_editing_method3'])) {
    $db_condition_filter->condition('editing_method3', $_GET['filter_editing_method3'], '=');
    $filter_params ['Editing Method (3/3)'] = $_GET['filter_editing_method3'];
    $filtered = TRUE;
  }

  // If the user searched for a cell line, deal with the search query.
  $searched = FALSE;
  $structured_params = ['string' => [],];
  $db_condition_search = db_or();
  $cell_lines_searched = [];
  if (isset ($_GET['search_query'])) {
    $searched = TRUE;
    $search_query = $_GET['search_query'];
    $search_params = [];

    // look for quotation marks as they mark the beginning and end of a connected phrase
    if (preg_match_all('/"(.*?)"/i', $search_query, $matches)) {
      // remove all matches from the search query which gets exploded later
      foreach ($matches[0] as $match) {
        $search_query = str_replace($match, '', $search_query);
      }
      // add all matches without the quotation marks into an array for later merging
      foreach ($matches[1] as $match) {
        $search_params [] = $match;
      }
    }

    // look for any duplicate whitespaces
    $search_query = preg_replace('/\s\s+/', ' ', $search_query);

    // look if the start or ending of the string is a whitespace
    if (substr($search_query, 0, 1) == ' ') {
      $search_query = substr($search_query, 1);
    }
    if (substr($search_query, -1) == ' ') {
      $search_query = substr($search_query, 0, -1);

    }

    // if the remaining search query is not empty, explode it
    $expl_query = NULL;
    if (!empty($search_query) and $search_query !== ' ') {
      $expl_query = explode(' ', $search_query);
    }

    // if the remaining search query got exploded, merge the two arrays for searching in the db
    if ($expl_query !== NULL) {
      $search_params = array_merge($search_params, $expl_query);
    }

    if (empty($search_params)) {
      $search_params = [''];
    }

    // Go through each search phrase and search for matches in the database.
    for ($i = 0; $i < count($search_params); $i++) {
      $param = $search_params[$i];

      $db_condition_search->condition('cell_type', '%' . db_like($param) . '%', 'LIKE');
      $db_condition_search->condition('lab_id', '%' . db_like($param) . '%', 'LIKE');
      $db_condition_search->condition('ipsc_id', '%' . db_like($param) . '%', 'LIKE');
      $db_condition_search->condition('hpscreg_name', '%' . db_like
        ($param) . '%', 'LIKE');
      $db_condition_search->condition('source_type', '%' . db_like($param) . '%',
        'LIKE');
      $db_condition_search->condition('reprogramming_method', '%' . db_like($param) . '%', 'LIKE');
      $db_condition_search->condition('grade', '%' . db_like($param) . '%',
        'LIKE');
      $db_condition_search->condition('disease', '%' . db_like($param) . '%', 'LIKE');
      $db_condition_search->condition('icd', '%' . db_like($param) . '%', 'LIKE');
      $db_condition_search->condition('genetic_predisposition_spec', '%' . db_like($param) . '%', 'LIKE');
      $structured_params['string'][] = $param;
    }
  }

  // Print a text when filtered or searched
  $search_result_text = '';
  if ($filtered or $searched) {
    if ($searched) {
      $search_result_text .= t('Searched for: ');
      foreach ($structured_params['string'] as $search_str) {
        $search_result_text .= '<code>' . check_plain($search_str) . '</code>, ';
      }
      $search_result_text = substr($search_result_text, 0, -2);
    }

    if ($filtered) {
      $filter_str = 'Filtered for: ';
      if ($searched) {
        $search_result_text .= ' | ' . $filter_str;
      }
      else {
        $search_result_text .= $filter_str;
      }

      // Add the search phrases to response text.
      foreach ($filter_params as $filter_param) {
        $search_result_text .= '<code>' . $filter_param . '</code>, ';
      }
      $search_result_text = substr($search_result_text, 0, -2);
    }

    // Get all cell lines.
    if ($filtered and !$searched) {
      $cell_lines = CellLineRepository::findByUseTableSort($table_header, $db_condition_filter, $tab_type);
    }
    elseif (!$filtered and $searched) {
      $cell_lines = CellLineRepository::findByUseTableSort($table_header, $db_condition_search, $tab_type);
    }
    else {
      $cell_lines = CellLineRepository::findByTwoUseTableSort($table_header, $db_condition_search, $db_condition_filter, $tab_type);
    }

    // Get searched cell lines for filter form fields
    if ($searched) {
      $cell_lines_searched = CellLineRepository::findByUseTableSort($table_header, $db_condition_search, $tab_type);
    }

    // If a non manager/viewer user searches remove private lines from $cell_lines.
    $non_manager_lines = [];
    if (!user_access(RDP_CELLMODEL_PERMISSION_MANAGE) and !user_access(RDP_CELLMODEL_PERMISSION_VIEW)) {
      foreach ($cell_lines as $cell_line) {
        if ($cell_line->getVisibility() == 'Public') {
          $non_manager_lines [] = $cell_line;
        }
      }
      $cell_lines = $non_manager_lines;
    }

    // Count cell lines.
    $count = count($cell_lines);

    // Print the rest of the search response text.
    $search_result_text .= t(' | Found <strong>@count</strong> matches in <i>@tab_str</i>.', [
      '@count' => $count,
      '@tab_str' => $tab_string,
    ]);
  }

  // Add the filter_params to structured_params
  foreach ($filter_params as $filter_param) {
    $structured_params['string'][] = $filter_param;
  }

  // Get all cell lines if no search was done.
  if (!$searched and
    !$filtered and
    user_access(RDP_CELLMODEL_PERMISSION_MANAGE)) {
    $cell_lines = CellLineRepository::findAllUseTableSortAndUsePagerDefault($table_header, 1000, $tab_type);
  }
  elseif (!$searched and
    !$filtered and
    (user_access(RDP_CELLMODEL_PERMISSION_VIEW))) {

    if (isset($_GET['show_all']) and (bool) $_GET['show_all'] == TRUE) {
      $cell_lines = CellLineRepository::findAllUseTableSortAndUsePagerDefault($table_header, 1000, $tab_type);
    }
    else {
      $cell_lines = CellLineRepository::findAllUseTableSortAndUsePagerDefault($table_header, 25, $tab_type);
    }
  }
  elseif (!$searched and
    !$filtered and
    !user_access(RDP_CELLMODEL_PERMISSION_MANAGE) and
    !user_access(RDP_CELLMODEL_PERMISSION_VIEW)) {

    if (isset($_GET['show_all']) and (bool) $_GET['show_all'] == TRUE) {
      $cell_lines = CellLineRepository::findAllPublicUseTableSortAndUsePagerDefault($table_header, 1000, $tab_type);
    }
    else {
      $cell_lines = CellLineRepository::findAllPublicUseTableSortAndUsePagerDefault($table_header, 20, $tab_type);
    }
  }

  // --------------------------- <<< PAGE HEADER >>>---------------------------

  // Prepare page header.
  $page_header = '';

  // Print the tabs.
  $tabs_form = drupal_get_form('rdp_cellmodel_form_tabs');
  $page_header .= drupal_render($tabs_form);

  $btn_new_cell_line = l(t('<span class="glyphicon glyphicon-plus"></span> New'), new_edit_url($tab_type, 'new'), [
    'html' => TRUE,
    'attributes' => [
      'data-toggle' => 'tooltip',
      'data-placement' => 'bottom',
      'title' => "Create New Cell Line (" . CellmodelConstants::getOptionsTabType()[$tab_type] . ").",
      'class' => ['btn btn-info btn-sm'],
    ],
  ]);

  $btn_import = l(t('<span class="glyphicon glyphicon-upload"></span> Import'),
    rdp_cellmodel_url(RDP_CELLMODEL_URL_UPLOAD_CELL_LINES) . '?tab=' . $tab_type, [
      'html' => TRUE,
      'attributes' => [
        'data-toggle' => 'tooltip',
        'data-placement' => 'bottom',
        'title' => "Import new Cell Lines.",
        'class' => ['btn btn-info btn-sm'],
        'style' => 'margin-left: 5px;',
      ],
    ]);

  $btn_export = l(t('<span class="glyphicon glyphicon-download"></span> Export'),
    rdp_cellmodel_url(RDP_CELLMODEL_URL_DOWNLOAD_CELL_LINES) . '?tab=' . $tab_type, [
      'html' => TRUE,
      'attributes' => [
        'data-toggle' => 'tooltip',
        'data-placement' => 'bottom',
        'title' => "Export all Cell Lines as Excel Sheet.",
        'class' => ['btn btn-info btn-sm'],
        'style' => 'margin-left: 5px;',
      ],
    ]);

  $btn_stats = l(t('<span class="glyphicon glyphicon-stats"></span> Stats'),
    rdp_cellmodel_url(RDP_CELLMODEL_URL_STATS) . '?tab=' . $tab_type, [
      'html' => TRUE,
      'attributes' => [
        'data-toggle' => 'tooltip',
        'data-placement' => 'bottom',
        'title' => "Show statistics and recent activities.",
        'class' => ['btn btn-info btn-sm'],
        'style' => 'margin-left: 5px;',
      ],
    ]);

  // Determine which buttons to show.
  if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE)) {
    $buttons = $btn_new_cell_line . $btn_import . $btn_export . $btn_stats;
  }
  elseif (user_access(RDP_CELLMODEL_PERMISSION_VIEW)) {
    $buttons = $btn_export . $btn_stats;
  }
  else {
    $buttons = '';
  }

  // Print the buttons.
  if (!empty($buttons)) {
    $page_header .= '
  <div class="row">
     <div class="col-sm-4 text-right">
     ' . $buttons . '
     </div>&nbsp;&nbsp;
  </div>';
  }

  // Print the filter/search box for everybody.
  $search_form = drupal_get_form('rdp_cellmodel_form_filter', $cell_lines_searched);
  $page_header .= drupal_render($search_form);

  // Print the search response text only if a search or filter was done by the user.
  if ($searched or $filtered) {
    $page_header .= $search_result_text . '<div>&nbsp;</div>';
  }

  if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE)) {
    $btn_config = l('<span class="glyphicon glyphicon-cog"></span>',
      rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_DEFAULT), [
        'html' => TRUE,
        'attributes' => [
          'data-toggle' => 'tooltip',
          'data-placement' => 'left',
          'title' => "Configure aspects of the cell model catalogue.",
          'class' => ['btn btn-default btn-xs'],
          'style' => 'position: absolute;',
        ],
      ]);
  }
  else {
    $btn_config = '';
  }

  $page_header .= $btn_config;
  // ---------------------------- <<< PAGE BODY >>>----------------------------

  // Define the rows (individual cell lines) of the cell line table.
  $table_rows = [];

  foreach ($cell_lines as $cell_line) {

    // Generate the buttons.
    $edit = '<a href="' . new_edit_url($tab_type, 'edit', $cell_line->getId()) .
      '"><div style="text-align: center" title="' . t('Edit this Cell Line') .
      '"><i class="glyphicon glyphicon-pencil" </i></div></a>';

    $duplicate = '<a href="' . new_edit_url($tab_type, 'duplicate', $cell_line->getId()) .
      '"><div style="text-align: center" title="' . t('Create a new Cell Line based on this Cell Line') .
      '"><i class="glyphicon glyphicon-plus" </i></div></a>';

    $view = '<a href="' . rdp_cellmodel_url(RDP_CELLMODEL_URL_CELL_LINE_VIEW) .
      '?tab=' . $tab_type . '&line=' . $cell_line->getId() . '"><div style="text-align: center" title="' .
      t('View this Cell Line') . '"><i class="glyphicon glyphicon-eye-open" </i></div></a>';

    // Show warning symbol if ...
    // ... an only primary culture cell line is public.
    if ($cell_line->getCellType() == 'only primary culture' and $cell_line->getVisibility() == 'Public') {
      $notification = '<span style="color:#a9a9a9"><div style="text-align: center;" title = "' .
        t('This cell line of type ONLY PRIMARY CULTURE should probably be PRIVATE.') . '"><i
class="glyphicon glyphicon-exclamation-sign" </i></div></span>';
    }
    // ... a no sample line is public.
    elseif ($cell_line->getCellType() == 'no sample' and $cell_line->getVisibility() == 'Public') {
      $notification = '<span style="color:#000000"><div style="text-align: center;" title = "' .
        t('This cell line of type NO SAMPLE should probably be PRIVATE.') . '"><i
class="glyphicon glyphicon-exclamation-sign" </i></div></span>';
    }
    // ... a public iPSC cell lines is not registered at hPSCreg.
    elseif ($cell_line->getCellType() == 'iPSC' and $cell_line->getVisibility() == 'Public' and
      empty($cell_line->getHpscregName())) {
      $notification = '<span style="color:#ff0101"><div style="text-align: center;" title = "' .
        t('This PUBLIC cell line of type IPSC should probably be submitted at HPSCREG.') . '"><i
class="glyphicon glyphicon-exclamation-sign" </i></div></span>';
    }
    else {
      $notification = '';
    }

    if (checkIfNoTubesForCellline($cell_line)) {
      $notification = '<span style="color:#ff3333"><div style="text-align: center;" title = "' .
        t('This cell line has clones in the UMG Biobank with no tubes.') . '"><i
class="glyphicon glyphicon-flag" </i></div></span>';
    }
    else if (checkIfNoTubesForCellline($cell_line, 2)) {
      $notification = '<span style="color:#ffdb27"><div style="text-align: center;" title = "' .
        t('This cell line has clones in the UMG Biobank with only one tube.') . '"><i
class="glyphicon glyphicon-flag" </i></div></span>';
    }

    // Generate visibility symbol.
    $visibility = '';
    if ($cell_line->getVisibility() == 'Private') {
      $visibility = '<div style="text-align: center;" title = "' . t('This cell line is PRIVATE.') .
        '"><i class="glyphicon glyphicon-lock"></i>';
    }
    elseif ($cell_line->getVisibility() == 'Public') {
      $visibility = '<div style="text-align: center;" title = "' . t('This cell line is PUBLIC.') .
        '"><i class="glyphicon glyphicon-globe"></i>';
    }

    // Fill the table rows depending of the permission level and tab type.
    // Gen Mod Lines:
    if ($tab_type == 'gen_mod') {
      $editing = CellLineEditingRepository::findByCellLineId($cell_line->getId());
      if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE)) {
        $table_rows[] = [
          $visibility,
          $edit,
          $duplicate,
          $view,
          search_mark_found_string($structured_params,
            $cell_line->getLabId()),
          search_mark_found_string($structured_params, $cell_line->getHpscregName()),
          search_mark_found_string($structured_params,
            $cell_line->getCellType()),
          search_mark_found_string($structured_params,
            $editing->getEditingStrategy()),
          search_mark_found_string($structured_params,
            $editing->getEditingTargetedGene()),
          search_mark_found_string($structured_params,
            combine_editing_method($editing)),
          search_mark_found_string($structured_params, $cell_line->getGrade()),
          $notification,
        ];
      }
      elseif (user_access(RDP_CELLMODEL_PERMISSION_VIEW)) {
        $table_rows[] = [
          $visibility,
          $view,
          search_mark_found_string($structured_params,
            $cell_line->getLabId()),
          search_mark_found_string($structured_params, $cell_line->getHpscregName()),
          search_mark_found_string($structured_params,
            $cell_line->getCellType()),
          search_mark_found_string($structured_params,
            $editing->getEditingStrategy()),
          search_mark_found_string($structured_params,
            $editing->getEditingTargetedGene()),
          search_mark_found_string($structured_params,
            combine_editing_method($editing)),
          search_mark_found_string($structured_params, $cell_line->getGrade()),
          '',
        ];
      }
      else {
        $table_rows[] = [
          $view,
          $cell_line->getHpscregName() ? search_mark_found_string($structured_params, $cell_line->getHpscregName()) : '<i>unassigned</i>',
          search_mark_found_string($structured_params,
            $cell_line->getCellType()),
          search_mark_found_string($structured_params,
            $editing->getEditingStrategy()),
          search_mark_found_string($structured_params,
            $editing->getEditingTargetedGene()),
          search_mark_found_string($structured_params,
            combine_editing_method($editing)),
          search_mark_found_string($structured_params, $cell_line->getGrade()),
          '',
        ];
      }
    }
    // Non GenMod Lines:
    else {
      $diagnosis = CellLineDiagnosisRepository::findByCellLineIdAndPrimary($cell_line->getId());
      if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE)) {
        $table_rows[] = [
          $visibility,
          $edit,
          $duplicate,
          $view,
          search_mark_found_string($structured_params,
            $cell_line->getLabId()),
          search_mark_found_string($structured_params,
            $cell_line->getIpscId()),
          search_mark_found_string($structured_params, $cell_line->getHpscregName()),
          search_mark_found_string($structured_params,
            $cell_line->getCellType()),
          search_mark_found_string($structured_params,
            $diagnosis
              ->getIcd()),
          search_mark_found_string($structured_params,
            $diagnosis
              ->getDisease()),
          search_mark_found_string($structured_params,
            $cell_line->getSourceType()),
          search_mark_found_string($structured_params, $cell_line->getReprogrammingMethod()),
          search_mark_found_string($structured_params, $cell_line->getGrade()),
          $notification,
        ];
      }
      elseif (user_access(RDP_CELLMODEL_PERMISSION_VIEW)) {
        $table_rows[] = [
          $visibility,
          $view,
          search_mark_found_string($structured_params,
            $cell_line->getLabId()),
          search_mark_found_string($structured_params,
            $cell_line->getIpscId()),
          search_mark_found_string($structured_params, $cell_line->getHpscregName()),
          search_mark_found_string($structured_params,
            $cell_line->getCellType()),
          search_mark_found_string($structured_params,
            $diagnosis
              ->getIcd()),
          search_mark_found_string($structured_params,
            $diagnosis
              ->getDisease()),
          search_mark_found_string($structured_params,
            $cell_line->getSourceType()),
          search_mark_found_string($structured_params, $cell_line->getReprogrammingMethod()),
          search_mark_found_string($structured_params, $cell_line->getGrade()),
          '',
        ];
      }
      else {
        $table_rows[] = [
          $view,
          $cell_line->getHpscregName() ? search_mark_found_string($structured_params, $cell_line->getHpscregName()) : '<i>unassigned</i>',
          search_mark_found_string($structured_params,
            $cell_line->getCellType()),
          search_mark_found_string($structured_params,
            $diagnosis
              ->getIcd()),
          search_mark_found_string($structured_params,
            $diagnosis
              ->getDisease()),
          search_mark_found_string($structured_params,
            $cell_line->getSourceType()),
          search_mark_found_string($structured_params,
            $cell_line->getReprogrammingMethod()),
          search_mark_found_string($structured_params,
            $cell_line->getGrade()),
          '',
        ];
      }
    }

  }

  if (count($table_rows) == 0) {
    $table_rows = [
      'data' => [
        [
          'data' => t('No results.'),
          'colspan' => 20,
        ],
      ],
    ];
  }

  // Theme the table.
  $page_body = theme('table', [
      'header' => $table_header,
      'rows' => $table_rows,
    ]) . theme(
      'pager', ['tags' => []]
    );

  // The output.
  $output = $page_header . $page_body;
  return $output;
}

/**
 * Mark the search/filter phrases of the user in the rows of the table.
 *
 * @param string[] $structured_params
 *   The search phrases.
 * @param string   $string
 *   The text in which the phrases are to be marked.
 *
 * @return string
 *   The string with the marked phrases.
 */
function search_mark_found_string($structured_params, $string) {

  // Mark all matching free text search phrases in the string.
  // (?=[^<>]*(?:<|$))/i   ==> makes sure that html tags are ignored
  // $0                    ==> makes sure that the original text is highlighted
  //                           and not the entered query
  foreach ($structured_params['string'] as $query) {
    $string = preg_replace("/({$query})(?=[^<>]*(?:<|$))/i",
      '<span style="color: #c7254e;background-color: #f9f2f4">$0</span>', $string);
  }
  return $string;
}
