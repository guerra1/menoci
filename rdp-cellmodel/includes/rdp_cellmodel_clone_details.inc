<?php

/**
 * @return string
 * @throws \Exception
 */
function rdp_cellmodel_clone_details() {

  if (!isset($_GET['line']) or !user_access(RDP_CELLMODEL_PERMISSION_MANAGE)) {
    drupal_access_denied();
  }

  if (isset($_GET['show_all']) and $_GET['show_all']) {
    $show_all = TRUE;
  }
  else {
    $show_all = FALSE;
  }

  $cell_line = CellLineRepository::findById($_GET['line']);

  if ($show_all) {
    $btn_back = l('<i class="	glyphicon glyphicon-arrow-left"></i> ' . t('Clone Details'),
      rdp_cellmodel_url(RDP_CELLMODEL_URL_CLONE_DETAILS) . '?tab=' . $cell_line->getTabType() . '&line=' . $cell_line->getId(),
      [
        'html' => TRUE,
        'attributes' => [
          'class' => ['btn btn-info btn-sm'],
          'style' => 'margin-left: 5px;',
        ],
      ]
    );
  }
  else {
    $btn_back = l('<i class="	glyphicon glyphicon-arrow-left"></i> ' . t('Cell Line Page'),
      rdp_cellmodel_url(RDP_CELLMODEL_URL_CELL_LINE_VIEW) . '?tab=' . $cell_line->getTabType() . '&line=' . $cell_line->getId(),
      [
        'html' => TRUE,
        'attributes' => [
          'class' => ['btn btn-info btn-sm'],
          'style' => 'margin-left: 5px;',
        ],
      ]
    );
  }

  // "Edit" button for editing clones.
  if ($show_all) {
    $btn_edit = '';
  }
  else {
    $btn_edit = l('<i class="glyphicon glyphicon-pencil"></i> ' . t('Edit'),
      new_edit_url($cell_line->getTabType(), 'edit', $cell_line->getId()) . '#edit-vertical-tabs-clones',
      [
        'html' => TRUE,
        'attributes' => [
          'class' => ['btn btn-info btn-sm'],
          'style' => 'margin-left: 5px;',
        ],
      ]
    );
  }

  if ($show_all) {
    $btn_show_all = '';
  }
  else {
    $btn_show_all = l('<i class="	glyphicon glyphicon-list-alt"></i> ' . t('List All Clones'),
      rdp_cellmodel_url(RDP_CELLMODEL_URL_CLONE_DETAILS) .
      '?tab=' . $cell_line->getTabType() . '&line=' . $cell_line->getId() . '&show_all=' . TRUE,
      [
        'html' => TRUE,
        'attributes' => [
          'class' => ['btn btn-info btn-sm'],
          'style' => 'margin-left: 5px;',
        ],
      ]
    );
  }

  if ($show_all) {
    $btn_export = l('<i class="	glyphicon glyphicon-download"></i> ' . t('Export'),
      rdp_cellmodel_url(RDP_CELLMODEL_URL_DOWNLOAD_CLONES),
      [
        'html' => TRUE,
        'attributes' => [
          'class' => ['btn btn-info btn-sm'],
          'style' => 'margin-left: 5px;',
        ],
      ]
    );
  }
  else {
    $btn_export = '';
  }

  $left_buttons = '
    <div class="inline">
      <div class="col-xs-8">' . $btn_back . $btn_edit . '</div></div>';

  // Print the buttons.

  $right_buttons = '
  <div class="row">
     <div class="col-sm-4 text-right">
     ' . $btn_show_all . $btn_export . '
     </div>&nbsp;&nbsp;
  </div>';

  $legend = '<b>Characterization:</b><br><span class="glyphicon glyphicon-unchecked"></span> - Not Performed |
                   <span class="glyphicon glyphicon-expand"></span> - In Progess |
                   <span class="glyphicon glyphicon-check"></span> - Completed <br><span>&nbsp;</span>';

  if ($cell_line->getTabType() !== 'gen_mod' and !$show_all) {
    $show_editing = FALSE;
    $show_mutation = TRUE;
  }
  elseif ($cell_line->getTabType() == 'gen_mod' and !$show_all) {
    $show_editing = TRUE;
    $show_mutation = FALSE;
  }
  else {
    $show_editing = TRUE;
    $show_mutation = TRUE;
  }

  // Define the header for the table.

  $header_pre = [
    [
      'data' => '<span class="glyphicon glyphicon-unchecked"></span>',
      'field' => 't.characterization',
    ],
    [
      'data' => CellmodelConstants::getFieldTitles()['clone_number'],
      'field' => 't.clone_number',
    ],
  ];

  if (!$show_all and $cell_line->getTabType() == 'gen_mod') {
    $header_pre = array_merge($header_pre, [
      [
        'data' => CellmodelConstants::getFieldTitles()['genomic_modification'],
        'field' => 't.genomic_modification',
      ],
      [
        'data' => CellmodelConstants::getFieldTitles()['zygosity'],
        'field' => 't.zygosity',
      ],
      [
        'data' => CellmodelConstants::getFieldTitles()['specification'],
        'field' => 't.specification',
      ],
    ]);
  }
  $header = [
    [
      'data' => CellmodelConstants::getFieldTitles()['start_date'],
      'field' => 't.start_date',
    ],
    [
      'data' => CellmodelConstants::getFieldTitles()['responsible'],
      'field' => 't.responsible',
    ],
    [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'Morph.' :
        CellmodelConstants::getFieldTitles()['morphology'],
      'field' => 't.morphology',
    ],
    [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'Plur. PCR' :
        CellmodelConstants::getFieldTitles()['pluriotency_pcr'],
      'field' => 't.pluriotency_pcr',
    ],
    [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'Plur. Imm.' :
        CellmodelConstants::getFieldTitles()['pluriotency_immuno'],
      'field' => 't.pluriotency_immuno',
    ],
    [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'Plur. FACS' :
        CellmodelConstants::getFieldTitles()['pluriotency_facs'],
      'field' => 't.pluriotency_facs',
    ],
    [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'Diff. Imm.' :
        CellmodelConstants::getFieldTitles()['differentiation_immuno'],
      'field' => 't.differentiation_immuno',
    ],
  ];

  $header = array_merge($header_pre, $header);

  if ($show_mutation) {
    $header [] = [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'Mut. conf.' :
        CellmodelConstants::getFieldTitles()['mutation_analyzed'],
      'field' => 't.mutation_analyzed',
    ];
  }
  if ($show_editing) {
    $header [] = [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'Edit. conf.' :
        CellmodelConstants::getFieldTitles()['editing_analyzed'],
      'field' => 't.editing_analyzed',
    ];
  }

  $header_remain = [
    [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'HLA anal.' :
        CellmodelConstants::getFieldTitles()['hla_analyzed'],
      'field' => 't.hla_analyzed',
    ],
    [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'STR anal.' :
        CellmodelConstants::getFieldTitles()['str_analyzed'],
      'field' => 't.str_analyzed',
    ],
    [
      'data' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER) ? 'Karyo. anal.' :
        CellmodelConstants::getFieldTitles()['karyo_analyzed'],
      'field' => 't.karyo_analyzed',
    ],
    [
      'data' => CellmodelConstants::getFieldTitles()['karyotype'],
      'field' => 't.karyotype',
    ],
  ];

  $header = array_merge($header, $header_remain);

  if ($show_all) {
    $clones = CellLineCloneRepository::findAll($header);
  }
  else {
    $clones = CellLineCloneRepository::findByCellLineId($_GET['line'], $header);
  }

  // Assemble the content of the table.
  $rows = [];
  foreach ($clones as $clone) {
    $row_pre = [
      getCharacterizationIcon($clone->getCharacterization()),
      l($clone->getCloneNumber(), rdp_cellmodel_url(RDP_CELLMODEL_URL_CELL_LINE_VIEW) .
        '?line=' . $clone->getCellLine() . '&tab=' . CellLineRepository::findById($clone->getCellLine())
          ->getTabType()),
    ];

    if (!$show_all and $cell_line->getTabType() == 'gen_mod') {
      $row_pre = array_merge($row_pre, [
        $clone->getGenomicModification(),
        $clone->getZygosity(),
        $clone->getSpecification(),
      ]);
    }

    $row = [
      $clone->getStartDate(),
      $clone->getResponsible(),
      getCrossTick($clone->getMorphology()),
      getCrossTick($clone->getPluriotencyPcr()),
      getCrossTick($clone->getPluriotencyImmuno()),
      getCrossTick($clone->getPluriotencyFacs()),
      getCrossTick($clone->getDifferentiationImmuno()),
    ];
    $tab_type = CellLineRepository::findById($clone->getCellLine())
      ->getTabType();
    if ($show_mutation) {
      $row [] = ($tab_type == 'gen_mod') ? '' : getCrossTick($clone->getMutationAnalyzed());
    }
    if ($show_editing) {
      $row [] = ($tab_type == 'gen_mod') ? getCrossTick($clone->getEditingAnalyzed()) : '';
    }
    $row_remain = [
      getCrossTick($clone->getHlaAnalyzed()),
      getCrossTick($clone->getStrAnalyzed()),
      getCrossTick($clone->getKaryoAnalyzed()),
      $clone->getKaryotype(),
    ];
    $row = array_merge($row_pre, $row, $row_remain);
    $rows [] = $row;
  }

  // Generate the table.
  $page_body = theme('table', [
    'header' => $header,
    'rows' => $rows,
  ]);
  return $left_buttons . $right_buttons . $legend . $page_body;
}

/**
 * Set the title of the page when viewing clone details.
 *
 * @return string
 */
function rdp_cellmodel_clone_details_title_callback() {
  $cell_line = CellLineRepository::findById($_GET['line']);
  if (isset($_GET['show_all']) and $_GET['show_all']) {
    return "View: All Clones from All Cell Lines";
  }
  else {
    return 'Clones: ' . $cell_line->getLabId() . ' (' . CellmodelConstants::getOptionsTabType()[$_GET['tab']] . ')';
  }

}

/**
 * Returns a cross or a tick symbol depending on the given boolean.
 *
 * @param $true_false
 *   TRUE or FALSE.
 *
 * @return string
 *   Red cross or green tick symbol
 */
function getCrossTick($true_false) {
  if ($true_false == TRUE) {
    return '<i style="color: green;" class="glyphicon glyphicon-ok"></i>';
  }
  else {
    return '<i style="color: red;" class="glyphicon glyphicon-remove"></i>';
  }
}
