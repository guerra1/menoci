<?php

/**
 * @file
 * Process the downloading of files from the Cell Model Catalogue module.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 * SPDX-License-Identifier: GPL-3.0
 */

// Load the PHPExcel library.
module_load_include('php', 'sfb_commons', 'lib/phpexcel_1.8.0/PHPExcel');

// Set the sheet indices of the template and export file sheets.
const PATIENT_DERIVED_SHEET_INDEX = 0;
const GEN_MOD_SHEET_INDEX = 1;
const EXTERNAL_SHEET_INDEX = 2;
const AS_PROJECT_SHEET_INDEX = 3;
const DATA_SHEET_INDEX = 4;

/**
 * Act as a router to the correct function depending on the current URL.
 */
function rdp_cellmodel_download() {

  if (!(user_access(RDP_CELLMODEL_PERMISSION_MANAGE) or user_access(RDP_CELLMODEL_PERMISSION_VIEW))) {
    drupal_access_denied();
  }

  if (arg(1) != NULL) {
    if (arg(1) == 'download') {
      if (arg(2) == 'template') {
        return rdp_cellmodel_download_template();

      }
      elseif (arg(2) == 'cell-lines') {
        return rdp_cellmodel_download_cell_lines();
      }
      elseif (arg(2) == 'clones') {
        return rdp_cellmodel_download_clones();
      }
    }
  }
  drupal_not_found();

  return NULL;
}

/**
 * Process the request for downloading the excel sheet summarizing all
 * registered cell lines.
 */
function rdp_cellmodel_download_cell_lines() {
  try {
    // Load the template file.
    $objPHPExcel = PHPExcel_IOFactory::load(RDP_CELLMODEL_PATH_EXCEL_EXPORT);

    // Fill the template file with the data.
    $tab_count = 0;
    foreach (CellmodelConstants::getOptionsTabType() as $tab_key => $tab_value) {
      $first_content_row = 3;
      $cell_lines = CellLineRepository::findByTabType($tab_key);
      $num_cell_lines = count($cell_lines);
      for ($i = 0; $i < $num_cell_lines; $i++) {
        $cell_line = $cell_lines[$i];

        // Get all the data from the dynamic fields.
        $prim_diag = CellLineDiagnosisRepository::findByCellLineIdAndPrimary($cell_line->getId());
        $sec_diags = CellLineDiagnosisRepository::findByCellLineIdAndSecondary($cell_line->getId());
        $relatives = CellLineDonorRelativeRepository::findByCellLineId($cell_line->getId());
        $rep_dates = CellLineReprogrammingDateRepository::findByCellLineId($cell_line->getId());
        $editing = CellLineEditingRepository::findByCellLineId($cell_line->getId());
        $edit_dates = CellLineEditingDateRepository::findByCellLineId($cell_line->getId());

        $start_column = 'A';

        if ($tab_key == 'gen_mod') {
          $objPHPExcel->setActiveSheetIndex($tab_count)
            // General (A-O)
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getLabId())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHpscregName())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getAltId())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getProvider())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getLocation())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getProject())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getGrade())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getUnderlyingcellLine())
            ->setCellValue($start_column++ . ($first_content_row + $i), $editing->getCloneUsed())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getObtainable())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricAreas())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricResearch())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricClinical())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricCommercial())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricAdditional())
            // Editing (P-W)
            ->setCellValue($start_column++ . ($first_content_row + $i), $editing->getEditingStrategy())
            ->setCellValue($start_column++ . ($first_content_row + $i), $editing->getEditingTargetedGene())
            ->setCellValue($start_column++ . ($first_content_row + $i), $editing->getEditingMethod1())
            ->setCellValue($start_column++ . ($first_content_row + $i), $editing->getEditingMethod2())
            ->setCellValue($start_column++ . ($first_content_row + $i), $editing->getEditingMethod3())
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(0, $edit_dates) ? transformIntoExcelDate($edit_dates[0]->getDate()) : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(1, $edit_dates) ? transformIntoExcelDate($edit_dates[1]->getDate()) : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(2, $edit_dates) ? transformIntoExcelDate($edit_dates[2]->getDate()) : '')
            // Miscellaneous (X-AD)
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getContactEMail())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDistribution())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getCorrespondence())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getLinesInBiobank())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getComments())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDOIs())
            ->setCellValue($start_column . ($first_content_row + $i), $cell_line->getVisibility());
        }
        else {
          $objPHPExcel->setActiveSheetIndex($tab_count)
            // General (A-P)
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getLabId())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHpscregName())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getAltId())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getProvider())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getLocation())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getProject())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getCellType())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getIpscId())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getGrade())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getUnderlyingcellLine())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getObtainable())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricAreas())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricResearch())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricClinical())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricCommercial())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getRestricAdditional())
            // Diagnosis (Q-W)
            ->setCellValue($start_column++ . ($first_content_row + $i), $prim_diag ? $prim_diag->getDisease() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(0, $sec_diags) ? $sec_diags[0]->getDisease() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(1, $sec_diags) ? $sec_diags[1]->getDisease() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(2, $sec_diags) ? $sec_diags[2]->getDisease() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getGeneticPredisposition())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getGeneticPredispositionSpec())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getVisibilityPredispositionSpec())
            // Donor (X-AR)
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDonorGender())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDonorRace())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDonorAge())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHlaA())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHlaB())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHlaDrb1())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDonorSecutrialId())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDonorPseudonym())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDonorStarlimsId())
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(0, $relatives) ? $relatives[0]->getType() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(0, $relatives) ? $relatives[0]->getStarlimsId() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(0, $relatives) ? $relatives[0]->getPseudonym() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(1, $relatives) ? $relatives[1]->getType() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(1, $relatives) ? $relatives[1]->getStarlimsId() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(1, $relatives) ? $relatives[1]->getPseudonym() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(2, $relatives) ? $relatives[2]->getType() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(2, $relatives) ? $relatives[2]->getStarlimsId() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(2, $relatives) ? $relatives[2]->getPseudonym() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(3, $relatives) ? $relatives[3]->getType() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(3, $relatives) ? $relatives[3]->getStarlimsId() : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(3, $relatives) ? $relatives[3]->getPseudonym() : '')
            // Reprogramming (AS-BA)
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getSourceType())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getSampleOrigin())
            ->setCellValue($start_column++ . ($first_content_row + $i), transformIntoExcelDate($cell_line->getSamplingDate()))
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getPrimaryCulture())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getReprogrammingMethod())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getReprogrammingVector())
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(0, $rep_dates) ? transformIntoExcelDate($rep_dates[0]->getDate()) : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(1, $rep_dates) ? transformIntoExcelDate($rep_dates[1]->getDate()) : '')
            ->setCellValue($start_column++ . ($first_content_row + $i),
              array_key_exists(2, $rep_dates) ? transformIntoExcelDate($rep_dates[2]->getDate()) : '')
            // Virus Testing (BB-BJ)
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHivResult())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHivMaterial())
            ->setCellValue($start_column++ . ($first_content_row + $i), transformIntoExcelDate($cell_line->getHivDate()))
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHepB())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHepBMaterial())
            ->setCellValue($start_column++ . ($first_content_row + $i), transformIntoExcelDate($cell_line->getHepBDate()))
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHepC())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getHepCMaterial())
            ->setCellValue($start_column++ . ($first_content_row + $i), transformIntoExcelDate($cell_line->getHepCDate()))
            // Miscellaneous (BK-BQ)
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getContactEMail())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDistribution())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getCorrespondence())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getLinesInBiobank())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getComments())
            ->setCellValue($start_column++ . ($first_content_row + $i), $cell_line->getDOIs())
            ->setCellValue($start_column . ($first_content_row + $i), $cell_line->getVisibility());
        }
      }
      $objPHPExcel->setActiveSheetIndex($tab_count);
      foreach (excelColumnRange('A', $objPHPExcel->getActiveSheet()
        ->getHighestColumn()) as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
          ->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->calculateColumnWidths();
        $colwidth = $objPHPExcel->getActiveSheet()
          ->getColumnDimension($columnID)
          ->getWidth();
        if ($colwidth > RDP_CELLMODEL_COLUMN_AUTO_SIZE_MAX) {
          $objPHPExcel->getActiveSheet()
            ->getColumnDimension($columnID)
            ->setAutoSize(FALSE);
          $objPHPExcel->getActiveSheet()
            ->getColumnDimension($columnID)
            ->setWidth(RDP_CELLMODEL_COLUMN_AUTO_SIZE_MAX);
        }
      }
      $tab_count++;
    }

    // Set the basic file properties.
    $objPHPExcel->getProperties()->setCreator("Research Data Platform")
      ->setLastModifiedBy('Research Data Platform')
      ->setTitle("Export - Cell Model Catalogue");
    $objPHPExcel->setActiveSheetIndex(0);

    // Create writer and output the file.
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    // Set the headers to force download and set file properties.
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . RDP_CELLMODEL_EXPORT_FILE_NAME . '"');
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');
  } catch (Exception $exception) {
    watchdog('rdp_cellmodel', $exception);
  }
}

/**
 * Transform a date into the format Excel is using.
 *
 * @param string $date_raw
 *   A string date in YYYY-MM-DD format.
 *
 * @return string
 *  A string date in DD.MM.YYYY format.
 * @throws \Exception
 */
function transformIntoExcelDate($date_raw) {
  if (!empty($date_raw)) {
    $date = new DateTime($date_raw);
    $date_excel = PHPExcel_Shared_Date::PHPToExcel($date);
    return $date_excel;
  }
  else {
    return '';
  }
}

/**
 * Process the request for downloading the excel template acting as a base file
 * for uploading cell lines.
 */
function rdp_cellmodel_download_template() {
  try {
    // Load the template file.
    $objPHPExcel = PHPExcel_IOFactory::load(RDP_CELLMODEL_PATH_EXCEL_TEMPLATE);

    // Write select values for disease and relative type into a separate sheet.
    // Reason: Length restrictions of the excel data validation field of 256 chars.
    $objPHPExcel->setActiveSheetIndex(DATA_SHEET_INDEX);

    $column_count = 'A';
    $diseases = CellmodelConstants::getOptionsDiseaseNames();
    $count = 0;
    foreach ($diseases as $disease) {
      $objPHPExcel->getActiveSheet()
        ->setCellValue($column_count . ($count + 1), $disease);
      $count++;
    }
    $disease_area = 'data!A1:A' . sizeof($diseases);

    $column_count++;
    $relative_types = array_values(CellmodelConstants::getOptionsRelativeType());
    for ($i = 0; $i < count($relative_types); $i++) {
      $objPHPExcel->getActiveSheet()
        ->setCellValue($column_count . ($i + 1), $relative_types[$i]);
    }
    $relative_type_area = 'data!B1:B' . sizeof($relative_types);

    // Prepare the array for setting the select values.
    // This first subarray is for internal, external and as_lines
    // The second subarray is for genomic modified lines
    $select_value_by_column =
      [
        [
          ['D', generateString(CellmodelConstants::getOptionsLocation())],
          ['F', generateString(CellmodelConstants::getOptionsCellType())],
          ['H', generateString(CellmodelConstants::getOptionsGrade())],
          ['J', generateString(CellmodelConstants::getOptionsYesNo())],
          ['L', generateString(CellmodelConstants::getOptionsYesNo())],
          ['M', generateString(CellmodelConstants::getOptionsYesNo())],
          ['N', generateString(CellmodelConstants::getOptionsYesNo())],
          ['P', 'disease'],
          ['Q', 'disease'],
          ['R', 'disease'],
          ['S', 'disease'],
          ['T', generateString(CellmodelConstants::getOptionsKnownUnknown())],
          ['V', generateString(CellmodelConstants::getOptionsVisibility())],
          ['W', generateString(CellmodelConstants::getOptionsGender())],
          ['X', generateString(CellmodelConstants::getOptionsRace())],
          ['AF', 'relative'],
          ['AI', 'relative'],
          ['AL', 'relative'],
          ['AO', 'relative'],
          ['AR', generateString(CellmodelConstants::getOptionsSource())],
          ['AT', 'date'],
          ['AU', generateString(CellmodelConstants::getOptionsYesNo())],
          ['AV', generateString(CellmodelConstants::getOptionsRepMethod())],
          ['AX', 'date'],
          ['AY', 'date'],
          ['AZ', 'date'],
          [
            'BA',
            generateString(CellmodelConstants::getOptionsTestingResults()),
          ],
          [
            'BB',
            generateString(CellmodelConstants::getOptionsTestingMaterial()),
          ],
          ['BC', 'date'],
          [
            'BD',
            generateString(CellmodelConstants::getOptionsTestingResults()),
          ],
          [
            'BE',
            generateString(CellmodelConstants::getOptionsTestingMaterial()),
          ],
          ['BF', 'date'],
          [
            'BG',
            generateString(CellmodelConstants::getOptionsTestingResults()),
          ],
          [
            'BH',
            generateString(CellmodelConstants::getOptionsTestingMaterial()),
          ],
          ['BI', 'date'],
          ['BK', generateString(CellmodelConstants::getOptionsDistribution())],
          ['BO', generateString(CellmodelConstants::getOptionsVisibility())],
        ],
        [
          ['C', generateString(CellmodelConstants::getOptionsLocation())],
          ['E', generateString(CellmodelConstants::getOptionsGrade())],
          ['I', generateString(CellmodelConstants::getOptionsYesNo())],
          ['K', generateString(CellmodelConstants::getOptionsYesNo())],
          ['L', generateString(CellmodelConstants::getOptionsYesNo())],
          ['M', generateString(CellmodelConstants::getOptionsYesNo())],
          [
            'O',
            generateString(CellmodelConstants::getOptionsEditingStrategy()),
          ],
          [
            'Q',
            generateString(CellmodelConstants::getOptionsEditingMethod1()),
          ],
          [
            'R',
            generateString(CellmodelConstants::getOptionsEditingMethod2()),
          ],
          [
            'S',
            generateString(CellmodelConstants::getOptionsEditingMethod3()),
          ],
          ['T', 'date'],
          ['U', 'date'],
          ['V', 'date'],
          ['X', generateString(CellmodelConstants::getOptionsDistribution())],
          ['AB', generateString(CellmodelConstants::getOptionsVisibility())],

        ],
      ];

    // Set the select values for all select fields.
    $first_content_row = 3;
    $last_validation_row = 300;
    $last_sheet = GEN_MOD_SHEET_INDEX;
    for ($i = 0; $i <= $last_sheet; $i++) {
      $objPHPExcel->setActiveSheetIndex($i);
      if ($i !== 1) {
        $column_value_index = 0;
      }
      else {
        $column_value_index = 1;
      }
      foreach ($select_value_by_column[$column_value_index] as $column) {
        for ($k = $first_content_row; $k <= $last_validation_row; $k++) {
          if ($column[1] == 'date') {
            $objValidation = $objPHPExcel->getActiveSheet()
              ->getCell($column[0] . $k)
              ->getDataValidation();
            $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_DATE);
            $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_STOP);
            $objValidation->setAllowBlank(FALSE);
            $objValidation->setShowErrorMessage(TRUE);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('The value you entered is not a date.');
          }
          else {
            $objValidation = $objPHPExcel->getActiveSheet()
              ->getCell($column[0] . $k)
              ->getDataValidation();
            $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_STOP);
            $objValidation->setAllowBlank(FALSE);
            $objValidation->setShowInputMessage(TRUE);
            $objValidation->setShowErrorMessage(TRUE);
            $objValidation->setShowDropDown(TRUE);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('The value you entered is not in the list of allowed options.
            Please pick a value from the drop-down list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            if ($column[1] == 'disease') {
              $objValidation->setFormula1($disease_area);
            }
            elseif ($column[1] == 'relative') {
              $objValidation->setFormula1($relative_type_area);
            }
            else {
              $objValidation->setFormula1('"' . $column[1] . '"');
            }
          }
        }
      }
    }

    // Automatically set the size of the columns for better readability, but
    // limit column width to a maximum.
    for ($i = 0; $i < $objPHPExcel->getSheetCount(); $i++) {
      $objPHPExcel->setActiveSheetIndex($i);
      foreach (excelColumnRange('A', $objPHPExcel->getActiveSheet()
        ->getHighestColumn()) as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
          ->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->calculateColumnWidths();
        $colwidth = $objPHPExcel->getActiveSheet()
          ->getColumnDimension($columnID)
          ->getWidth();
        if ($colwidth > RDP_CELLMODEL_COLUMN_AUTO_SIZE_MAX) {
          $objPHPExcel->getActiveSheet()
            ->getColumnDimension($columnID)
            ->setAutoSize(FALSE);
          $objPHPExcel->getActiveSheet()
            ->getColumnDimension($columnID)
            ->setWidth(RDP_CELLMODEL_COLUMN_AUTO_SIZE_MAX);
        }
      }
    }

    // Set the basic file properties.
    $objPHPExcel->getProperties()->setCreator("Research Data Platform")
      ->setLastModifiedBy('Research Data Platform')
      ->setTitle("Template - Cell Model Catalogue")
      ->setCustomProperty("Version", RDP_CELLMODEL_TEMPLATE_FILE_VERSION_NUMBER);
    $objPHPExcel->setActiveSheetIndex(0);

    // Create writer and output the file.
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    // Set the headers to force download and set file properties.
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . RDP_CELLMODEL_TEMPLATE_FILE_NAME . '"');
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');
  } catch (Exception $exception) {
    watchdog('rdp_cellmodel', $exception);
  }
}

/**
 * Go through the range from lower to upper. Used for cycling over a range fom
 * e.g. A to ZZ.
 *
 * @param string $lower
 *   Start value e.g. A.
 * @param string $upper
 *  End value e.g. BT.
 *
 * @return \Generator
 *   Range.
 */
function excelColumnRange($lower, $upper) {
  ++$upper;
  for ($i = $lower; $i !== $upper; ++$i) {
    yield $i;
  }
}

/**
 * Converts a one dimensional array into a string by linking the individual
 * cell values together. The values are separated by a comma.
 *
 * @param string[] $array
 *   The one-dimnesional array to be converted into a string.
 *
 * @return bool|string|null
 *   The generated string.
 */
function generateString($array) {
  if (!$array) {
    return NULL;
  }
  $string = '';
  foreach ($array as $element) {
    $string .= $element . ',';
  }
  return substr($string, 0, -1);
}

/**
 *
 */
function rdp_cellmodel_download_clones() {
  try {
    // Load the template file.
    $objPHPExcel = PHPExcel_IOFactory::load(RDP_CELLMODEL_PATH_EXCEL_EXPORT_CLONES);
    $first_content_row = 2;
    $clones = CellLineCloneRepository::findAll();
    $num_clones = count($clones);
    for ($i = 0; $i < $num_clones; $i++) {
      $clone = $clones[$i];
      $start_column = 'A';

      $objPHPExcel->setActiveSheetIndex()
        // General (A-O)
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getCloneNumber())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getCloneNumberStarlims())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getGenomicModification())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getZygosity())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getHdrNhej())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getSpecification())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getVisibilityClone())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getVisibility())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getCultureMedium())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getCharacterization())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getStartDate())
        ->setCellValue($start_column++ . ($first_content_row + $i), $clone->getResponsible())
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getMorphology()))
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getPluriotencyPcr()))
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getPluriotencyImmuno()))
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getPluriotencyFacs()))
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getDifferentiationImmuno()))
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getEditingAnalyzed()))
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getMutationAnalyzed()))
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getHlaAnalyzed()))
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getStrAnalyzed()))
        ->setCellValue($start_column++ . ($first_content_row + $i), setCharacterizationValue($clone->getKaryoAnalyzed()))
        ->setCellValue($start_column . ($first_content_row + $i), $clone->getKaryotype());
    }
    $objPHPExcel->getActiveSheetIndex();
    foreach (excelColumnRange('A', $objPHPExcel->getActiveSheet()
      ->getHighestColumn()) as $columnID) {
      $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(TRUE);
      $objPHPExcel->getActiveSheet()->calculateColumnWidths();
      $colwidth = $objPHPExcel->getActiveSheet()
        ->getColumnDimension($columnID)
        ->getWidth();
      if ($colwidth > RDP_CELLMODEL_COLUMN_AUTO_SIZE_MAX) {
        $objPHPExcel->getActiveSheet()
          ->getColumnDimension($columnID)
          ->setAutoSize(FALSE);
        $objPHPExcel->getActiveSheet()
          ->getColumnDimension($columnID)
          ->setWidth(RDP_CELLMODEL_COLUMN_AUTO_SIZE_MAX);
      }
    }

    // Set the basic file properties.
    $objPHPExcel->getProperties()->setCreator("Research Data Platform")
      ->setLastModifiedBy('Research Data Platform')
      ->setTitle("Export - Cell Model Catalogue - Clones");
    $objPHPExcel->setActiveSheetIndex(0);

    // Create writer and output the file.
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    // Set the headers to force download and set file properties.
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . RDP_CELLMODEL_EXPORT_CLONES_FILE_NAME . '"');
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');
  } catch (Exception $e) {
    watchdog('error', $e);
  }
}

/**
 * @param $true_false
 *
 * @return string
 */
function setCharacterizationValue($true_false) {
  if ($true_false) {
    return '✔';
  }
  else {
    return '';
  }
}
