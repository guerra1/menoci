<?php

/**
 * @file
 * Provide the forms and functions for importing cell line data via
 * PHPExcel.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Generate the page for importing cell lines. The user has to enter
 * credentials if the cell lines are to be registered at hPSCreg.
 *
 * @return array
 *   Assembled form field.
 */
function rdp_cellmodel_upload($form, &$form_state) {

  // Generate all form fields.
  $form['info'] = [
    '#markup' => t('<i class="glyphicon glyphicon-exclamation-sign">
     </i> The import does <b>not overwite or change</b> existing cell lines. Existing cell lines
     are identified by the Lab/Editing ID.<div>&nbsp;</div>
     You must use the <b>most recent version</b> of the template file. You can download it via the "Download Template" button.<div>&nbsp;</div>
     If you want to automatically register <b>all public iPSC</b> lines at hPSCreg,
     please enter your hPSCreg credentials. Otherwise keep the fields empty. The hPSCreg names are generated based on the order in the Excel
     spreadsheet.<div>&nbsp;</div>
     The import can take several minutes. Please <b>do not close or leave</b> this browser tab until
     redirected to the main page. <div>&nbsp;</div>'),
    '#prefix' => '
    <div class="inline">
      <div class="col-xs-10">',
    '#suffix' => '</div>
    </div>',
  ];

  $form['template'] = [
    '#type' => 'submit',
    '#value' => t('Download Template (v@version)', ['@version' => RDP_CELLMODEL_TEMPLATE_FILE_VERSION_NUMBER]),
    '#attributes' => ['class' => ['btn-primary btn-sm']],
    '#submit' => ['rdp_cellmodel_upload_submit_template'],
    '#prefix' => '
    <div class="row">
      <div class="col-xs-2 text-right">',
    '#suffix' => '&nbsp;</div>
    </div>',
  ];

  $form['file'] = [
    '#type' => 'file',
    '#title' => t('Choose an xlsx-file based on the most recent template version.'),
  ];

  $form['hpscreg_username'] = [
    '#type' => 'textfield',
    '#title' => t('hPSCreg Username'),
    '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
  ];

  $form['hpscreg_password'] = [
    '#type' => 'password',
    '#title' => t('hPSCreg Password'),
    '#attributes' => ['autocomplete' => 'new-password'],
    '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
  ];

  $form['cancel'] = [
    '#type' => 'submit',
    '#submit' => ['rdp_cellmodel_upload_submit_cancel'],
    '#value' => t('Cancel'),
    '#attributes' => ['class' => ['btn-danger btn-sm']],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Import'),
    '#attributes' => ['class' => ['btn-success btn-sm']],
  ];

  return $form;
}

/**
 * Submit function for tab buttons in the upload page. Import and register the
 * cell lines to hPSCreg.
 *
 * @throws \PHPExcel_Exception
 * @throws \PHPExcel_Reader_Exception
 */
function rdp_cellmodel_upload_submit($form, &$form_state) {
  // Process the file provided by the user.
  $file_destination = 'public://';

  $validators = ['file_validate_extensions' => ['xlsx']];

  $file = file_save_upload('file', $validators, $file_destination, FILE_EXISTS_REPLACE);

  if ($wrapper = file_stream_wrapper_get_instance_by_uri('public://')) {
    $file_path = $wrapper->realpath() . '/';
  }
  else {
    $file_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . 'sites/default/files/';
  }

  $file_name = $file->filename;
  $import_file = $file_path . $file_name;

  // Load the library and initialize.
  module_load_include('php', 'sfb_commons', 'lib/phpexcel_1.8.0/PHPExcel');

  // Load the import file.
  $objPHPExcel = PHPExcel_IOFactory::load($import_file);

  $tab_type = (($_GET['tab']) ? $_GET['tab'] : 'internal');
  $version_number = $objPHPExcel->getProperties()
    ->getCustomPropertyValue("Version");
  if (!$version_number) {
    $version_number = '0';
  }

  // Check if the most recent version of the template was used.
  if ($version_number !== RDP_CELLMODEL_TEMPLATE_FILE_VERSION_NUMBER) {
    file_delete($file);
    $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_UPLOAD_CELL_LINES) . '?tab=' . $tab_type;
    drupal_set_message(t('The cell line import was canceled since you are not using the the most recent version of the
    import template. <br>
    You are using version @version_current but the most recent version is @version_recent.', [
      '@version_current' => $version_number,
      '@version_recent' => RDP_CELLMODEL_TEMPLATE_FILE_VERSION_NUMBER,
    ]), 'error');
    return NULL;
  }

  // Get the Cell Line objects.
  global $user;
  $first_content_row = 3;
  $sheetCount = 4;

  // Loop through each data row of the worksheet.
  for ($i = 0; $i < $sheetCount; $i++) {
    $sheet = $objPHPExcel->setActiveSheetIndex($i);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    for ($row = $first_content_row; $row <= $highestRow; $row++) {
      // Read a row of data into an array.
      $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE, TRUE);
      // Create new Cell Line object.
      $cell_line_db_lab = CellLineRepository::findByLabId($rowData[$row]['A']);
      $cell_line_db_ipsc = CellLineRepository::findByIpscId($rowData[$row]['F']);
      $gen_mod = FALSE;
      // Save the data into the object.
      if (!empty($rowData[$row]['A']) and isset($rowData[$row]['A']) and $cell_line_db_lab == NULL and $cell_line_db_ipsc == NULL) {
        $cell_line = new CellLine();
        $diagnoses = [];
        $relatives = [];
        $repDates = [];
        $editing = new CellLineEditing();
        $edit_dates = [];
        $clones = [];

        $cell_line->setCreatedDate(date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT));
        $cell_line->setCreatedBy($user->uid);
        $cell_line->setLastModifiedDate(date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT));
        $cell_line->setLastModifiedBy($user->uid);

        $first_content_column = 'A';
        if ($i !== 1) {
          // General (A-O);
          $cell_line->setLabId($rowData[$row][$first_content_column++]);
          $cell_line->setAltId($rowData[$row][$first_content_column++]);
          if ($i == 2) {
            $cell_line->setProvider($rowData[$row][$first_content_column++]);
          }
          else {
            $cell_line->setProvider(RDP_CELLMODEL_DEFAULT_PROVIDER[0]);
            $first_content_column++;
          }

          $cell_line->setLocation($rowData[$row][$first_content_column++]);
          $cell_line->setProject($rowData[$row][$first_content_column++]);
          $cell_line->setCellType($rowData[$row][$first_content_column++]);
          $cell_line->setIpscId($rowData[$row][$first_content_column++]);
          $cell_line->setGrade($rowData[$row][$first_content_column++]);
          if (!empty($rowData[$row][$first_content_column])) {
            if ($underlying_cell_line = CellLineRepository::findByLabId($rowData[$row][$first_content_column])) {
              $cell_line->setSameDonor('Yes');
              $cell_line->setUnderlyingcellLine($underlying_cell_line->getId());
            }
          }
          else {
            $cell_line->setSameDonor('No');
          }
          $first_content_column++;
          $cell_line->setObtainable($rowData[$row][$first_content_column++]);
          $cell_line->setRestricAreas($rowData[$row][$first_content_column++]);
          $cell_line->setRestricResearch($rowData[$row][$first_content_column++]);
          $cell_line->setRestricClinical($rowData[$row][$first_content_column++]);
          $cell_line->setRestricCommercial($rowData[$row][$first_content_column++]);
          $cell_line->setRestricAdditional($rowData[$row][$first_content_column++]);
          // Diagnosis (P-V)
          for ($k = 0; $k < 4; $k++) {
            if (!empty($rowData[$row][$first_content_column])) {
              $diagnosis = new CellLineDiagnosis();
              if ($k == 0) {
                $diagnosis->setType('primary');
              }
              else {
                $diagnosis->setType('secondary');
              }
              $diagnosis->setDisease($rowData[$row][$first_content_column]);
              $diagnosis->setIcd(CellLineDiseaseRepository::findByDiseaseName($diagnosis->getDisease())
                ->getIcd());
              $diagnoses[] = $diagnosis;
            }
            $first_content_column++;
          }
          $cell_line->setDiagnoses($diagnoses);
          $cell_line->setGeneticPredisposition($rowData[$row][$first_content_column++]);
          $cell_line->setGeneticPredispositionSpec($rowData[$row][$first_content_column++]);
          $cell_line->setVisibilityPredispositionSpec($rowData[$row][$first_content_column++]);
          // Donor (W-AQ)
          $cell_line->setDonorGender($rowData[$row][$first_content_column++]);
          $cell_line->setDonorRace($rowData[$row][$first_content_column++]);
          $cell_line->setDonorAge($rowData[$row][$first_content_column++]);
          $cell_line->setHlaA($rowData[$row][$first_content_column++]);
          $cell_line->setHlaB($rowData[$row][$first_content_column++]);
          $cell_line->setHlaDrb1($rowData[$row][$first_content_column++]);
          $cell_line->setDonorSecutrialId($rowData[$row][$first_content_column++]);
          $cell_line->setDonorPseudonym($rowData[$row][$first_content_column++]);
          $cell_line->setDonorStarlimsId($rowData[$row][$first_content_column++]);
          for ($k = 0; $k < 4; $k++) {
            $curr_row = $first_content_column;
            if (!empty($rowData[$row][++$curr_row]) or
              !empty($rowData[$row][++$curr_row])) {
              $relative = new CellLineDonorRelative();
              $relative->setType($rowData[$row][$first_content_column++]);
              $relative->setStarlimsId($rowData[$row][$first_content_column++]);
              $relative->setPseudonym($rowData[$row][$first_content_column++]);
              $relatives[] = $relative;
            }
            else {
              $first_content_column++;
              $first_content_column++;
              $first_content_column++;
            }
          }
          $cell_line->setDonorRelatives($relatives);
          // Reprogramming (AR-AZ)
          $cell_line->setSourceType($rowData[$row][$first_content_column++]);
          $cell_line->setSampleOrigin($rowData[$row][$first_content_column++]);
          if (!empty($rowData[$row][$first_content_column])) {
            $date = date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row][$first_content_column]));
          }
          else {
            $date = NULL;
          }
          $first_content_column++;
          $cell_line->setSamplingDate($date);
          $cell_line->setPrimaryCulture($rowData[$row][$first_content_column++]);
          $cell_line->setReprogrammingMethod($rowData[$row][$first_content_column++]);
          $cell_line->setReprogrammingVector($rowData[$row][$first_content_column++]);
          for ($k = 0; $k < 3; $k++) {
            if (!empty($rowData[$row][$first_content_column])) {
              $date = date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row][$first_content_column]));
              $repDate = new CellLineReprogrammingDate();
              $repDate->setDate($date);
              $repDates[] = $repDate;
            }
            $first_content_column++;
          }
          $cell_line->setReprogrammingDates($repDates);
          // Virus Testing (BA-BI)
          $cell_line->setHivResult($rowData[$row][$first_content_column++]);
          $cell_line->setHivMaterial($rowData[$row][$first_content_column++]);
          if (!empty($rowData[$row][$first_content_column])) {
            $date = date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row][$first_content_column]));
          }
          else {
            $date = NULL;
          }
          $first_content_column++;
          $cell_line->setHivDate($date);
          $cell_line->setHepB($rowData[$row][$first_content_column++]);
          $cell_line->setHepBMaterial($rowData[$row][$first_content_column++]);
          if (!empty($rowData[$row][$first_content_column])) {
            $date = date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row][$first_content_column]));
          }
          else {
            $date = NULL;
          }
          $first_content_column++;
          $cell_line->setHepBDate($date);
          $cell_line->setHepC($rowData[$row][$first_content_column++]);
          $cell_line->setHepCMaterial($rowData[$row][$first_content_column++]);
          if (!empty($rowData[$row][$first_content_column])) {
            $date = date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row][$first_content_column]));
          }
          else {
            $date = NULL;
          }
          $first_content_column++;
          $cell_line->setHepCDate($date);
          // Miscellaneous (BJ-BO)
          $cell_line->setContactEMail($rowData[$row][$first_content_column++]);
          $cell_line->setDistribution($rowData[$row][$first_content_column++]);
          $cell_line->setCorrespondence($rowData[$row][$first_content_column++]);
          $cell_line->setLinesInBiobank($rowData[$row][$first_content_column++]);
          $cell_line->setComments($rowData[$row][$first_content_column++]);
          $cell_line->setVisibility($rowData[$row][$first_content_column]);
          switch ($i) {
            case 0:
              $cell_line->setTabType('internal');
              break;
            case 2:
              $cell_line->setTabType('external');
              break;
            case 3:
              $cell_line->setTabType('as_project');
              break;
          }
        }
        else {
          $gen_mod = TRUE;
          // Genetic Modification
          // General (A-N);
          $cell_line->setLabId($rowData[$row][$first_content_column++]);
          $cell_line->setProvider(RDP_CELLMODEL_DEFAULT_PROVIDER[0]);
          $cell_line->setAltId($rowData[$row][$first_content_column++]);
          $cell_line->setLocation($rowData[$row][$first_content_column++]);
          $cell_line->setProject($rowData[$row][$first_content_column++]);
          // It is always iPSC for imported GenMod lines.
          $cell_line->setCellType('iPSC');
          $cell_line->setGrade($rowData[$row][$first_content_column++]);
          $cell_line->setDonorStarlimsId($rowData[$row][$first_content_column++]);
          if (!empty($rowData[$row][$first_content_column])) {
            if ($underlying_cell_line = CellLineRepository::findByLabId($rowData[$row][$first_content_column])) {
              $cell_line->setSameDonor('Yes');
              $cell_line->setUnderlyingcellLine($underlying_cell_line->getId());
            }
          }
          $first_content_column++;
          $editing->setCloneUsed($rowData[$row][$first_content_column++]);
          $cell_line->setObtainable($rowData[$row][$first_content_column++]);
          $cell_line->setRestricAreas($rowData[$row][$first_content_column++]);
          $cell_line->setRestricResearch($rowData[$row][$first_content_column++]);
          $cell_line->setRestricClinical($rowData[$row][$first_content_column++]);
          $cell_line->setRestricCommercial($rowData[$row][$first_content_column++]);
          $cell_line->setRestricAdditional($rowData[$row][$first_content_column++]);
          // Editing (O - V)
          $editing->setEditingStrategy($rowData[$row][$first_content_column++]);
          $editing->setEditingTargetedGene($rowData[$row][$first_content_column++]);
          $editing->setEditingMethod1($rowData[$row][$first_content_column++]);
          $editing->setEditingMethod2($rowData[$row][$first_content_column++]);
          $editing->setEditingMethod3($rowData[$row][$first_content_column++]);

          for ($l = 0; $l < 3; $l++) {
            if (!empty($rowData[$row][$first_content_column])) {
              $date = date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row][$first_content_column]));
              $editDate = new CellLineEditingDate();
              $editDate->setDate($date);
              $edit_dates[] = $editDate;
            }
            $first_content_column++;
          }
          $editing->setEditingDates($edit_dates);

          // Misc (W-AB)
          $cell_line->setContactEMail($rowData[$row][$first_content_column++]);
          $cell_line->setCorrespondence($rowData[$row][$first_content_column++]);
          $cell_line->setCorrespondence($rowData[$row][$first_content_column++]);
          $cell_line->setLinesInBiobank($rowData[$row][$first_content_column++]);
          $cell_line->setComments($rowData[$row][$first_content_column++]);
          $cell_line->setVisibility($rowData[$row][$first_content_column]);
          $cell_line->setTabType('gen_mod');
        }

        $params_array = compare_cell_lines($cell_line, $editing);

        // Prepare dynamic fields comparison.
        $add_count = [
          sizeof($cell_line->getDiagnoses()),
          sizeof($cell_line->getDonorRelatives()),
          sizeof($cell_line->getReprogrammingDates()),
          sizeof($editing->getEditingDates()),
          sizeof($cell_line->getClones()),
        ];
        $dynamic_field_comparison = generate_dynamic_field_comparison_string($add_count);

        $action = '<b>The cell line was imported.</b><div></div><u>Added parameters:</u> ' . $params_array[1];
        $action .= $dynamic_field_comparison;

        $cell_line->save();
        if ($gen_mod) {
          $editing->setCellLine(CellLineRepository::findByLabId($cell_line->getLabId())
            ->getId());
          $editing->save();
        }

        $log = new CellmodelLog();
        $log->setAction($action);
        $log->setCellLine(CellLineRepository::findByLabId($cell_line->getLabId())
          ->getId());
        $log->save();

        // TODO Add Alt Names and Comments to hPSCreg upload?
        if ($cell_line->getCellType() == 'iPSC' and
          $cell_line->getVisibility() == 'Public' and
          $cell_line->getTabType() !== 'external' and
          !empty ($form_state['values']['hpscreg_username']) and
          !empty ($form_state['values']['hpscreg_password']) and
          !($cell_line->getSameDonor() == 'Yes' and
            (empty($cell_line->getUnderlyingcellLine())))) {
          $result = api_hpscreg($cell_line->getUnderlyingcellLine(),
            $form_state['values']['hpscreg_username'],
            $form_state['values']['hpscreg_password'],
            '', '', TRUE, $cell_line->getTabType());

          if (preg_match('/^[A-Z]{2,6}[i,e]\d{3}-[A-Z]{1,2}(-\d{1,2})?$/i',
            $result)) {
            $cell_line = CellLineRepository::findByLabId($cell_line->getLabId());
            $cell_line->setHpscregName($result);

            $log = new CellmodelLog();
            $log->setAction('<b>The cell line was registered at hPSCreg.</b><div></div><u>Generated name:</u> ' . $result);
            $log->setCellLine($cell_line->getId());
            $log->save();

            $cell_line->save();
          }
          else {
            drupal_set_message(t('The cell line <i><a href="@url?tab=@tab_type&line=@id">@lab_id </a></i>
                                        could not be registered at hPSCreg. <br>@result',
              [
                '@url' => rdp_cellmodel_url(RDP_CELLMODEL_URL_CELL_LINE_VIEW),
                '@tab_type' => $cell_line->getTabType(),
                '@id' => $cell_line->getId(),
                '@lab_id' => $cell_line->getLabId(),
                '@result' => $result,
              ]
            ), 'warning');
          }
        }
      }
      if ($cell_line_db_lab !== NULL) {
        drupal_set_message('The cell line with the Lab ID ' . $cell_line_db_lab->getLabId() .
          'could not be imported. A cell line with that Lab ID already exists.', 'warning');
      }
      elseif ($cell_line_db_ipsc !== NULL) {
        drupal_set_message('The cell line with the iPSC ID' . $cell_line_db_ipsc->getIpscId() .
          'could not be imported. A cell line with that iPSC ID already exists.', 'warning');
      }
    }
  }
  // Delete the file and redirect the user.
  file_delete($file);
  $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_DEFAULT) . '?tab=' . $tab_type;
  drupal_set_message(t('The cell line import has been completed.'));
  return NULL;
}

/**
 * Submit function for the cancel button.
 */
function rdp_cellmodel_upload_submit_cancel($form, &$form_state) {
  $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_DEFAULT) . '?tab=' . (($_GET['tab']) ? $_GET['tab'] : 'internal');
}

/**
 * * Submit function for the download template button.
 */
function rdp_cellmodel_upload_submit_template($form, &$form_state) {
  $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_DOWNLOAD_TEMPLATE);
}
