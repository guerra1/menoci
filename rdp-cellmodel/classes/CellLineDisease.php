<?php

/**
 * @file
 * Provide class for CellLineDisease objects.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class CellLineDisease {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   Database ID of disease type.
   */
  private $id;

  /**
   * @var string
   *   Name of the disease.
   */
  private $disease_name;

  /**
   * @var string
   *   ICD of the disease.
   */
  private $icd;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getDiseaseName() {
    return $this->disease_name;
  }

  /**
   * @param string $disease_name
   */
  public function setDiseaseName($disease_name) {
    $this->disease_name = $disease_name;
  }

  /**
   * @return string
   */
  public function getIcd() {
    return $this->icd;
  }

  /**
   * @param string $icd
   */
  public function setIcd($icd) {
    $this->icd = $icd;
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Save the data of this CellLineDisease into the database.
   */
  public function save() {
    try {
      CellLineDiseaseRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}
