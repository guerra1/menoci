<?php

/**
 * @file
 * Provides the database layer for @see \CellLine.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineRepository
 */
class CellLineRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLine.
   */
  static $tableName = 'cellmodel_cell_line';

  /**
   * @var array
   *   All database fields for CellLine.
   */
  static $databaseFields = [
    'id',
    'created_date',
    'created_by',
    'last_modified_date',
    'last_modified_by',
    'tab_type',
    'lab_id',
    'alt_id',
    'provider',
    'location',
    'project',
    'cell_type',
    'grade',
    'ipsc_id',
    'same_donor',
    'same_donor_line',
    'obtainable',
    'restric_areas',
    'restric_research',
    'restric_clinical',
    'restric_commercial',
    'restric_additional',
    'hpscreg_name',
    'genetic_predisposition',
    'genetic_predisposition_spec',
    'genetic_predisposition_spec_visibility',
    'donor_gender',
    'donor_race',
    'donor_age',
    'donor_secutrial_id',
    'donor_pseudonym',
    'donor_starlims_id',
    'source_type',
    'sample_origin',
    'sampling_date',
    'primary_culture',
    'reprogramming_method',
    'reprogramming_vector',
    'hiv',
    'hiv_material',
    'hiv_date',
    'hep_b',
    'hep_b_material',
    'hep_b_date',
    'hep_c',
    'hep_c_material',
    'hep_c_date',
    'hla_a',
    'hla_b',
    'hla_drb1',
    'ethics1',
    'ethics2',
    'ethics3',
    'ethics4',
    'ethics4_copy',
    'ethics5',
    'ethics5_copy',
    'ethics6',
    'ethics7',
    'ethics8',
    'ethics9',
    'ethics10',
    'ethics11',
    'ethics11_spec_name',
    'ethics11_spec_number',
    'ethics12',
    'ethics12_spec_name',
    'ethics12_spec_number',
    'ethics13',
    'ethics14',
    'ethics15',
    'ethics16',
    'ethics17',
    'ethics18',
    'ethics19',
    'ethics20',
    'ethics21',
    'ethics22',
    'ethics23',
    'ethics24',
    'ethics25',
    'ethics26',
    'ethics27',
    'contact',
    'correspondence',
    'cost_coverage',
    'distribution',
    'lines_in_biobank',
    'biosafety',
    'comments',
    'visibility',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLine into the database.
   *
   * @param \CellLine $cell_line
   *   CellLine object to be saved.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($cell_line) {
    db_merge(self::$tableName)
      ->key(['id' => $cell_line->getId()])
      ->fields([
        'created_date' => $cell_line->getCreatedDate(),
        'created_by' => $cell_line->getCreatedBy(),
        'last_modified_date' => $cell_line->getLastModifiedDate(),
        'last_modified_by' => $cell_line->getLastModifiedBy(),
        'tab_type' => $cell_line->getTabType(),
        'lab_id' => $cell_line->getLabId(),
        'alt_id' => $cell_line->getAltId(),
        'provider' => $cell_line->getProvider(),
        'location' => $cell_line->getLocation(),
        'project' => $cell_line->getProject(),
        'cell_type' => $cell_line->getCellType(),
        'grade' => $cell_line->getGrade(),
        'ipsc_id' => $cell_line->getIpscId(),
        'same_donor' => $cell_line->getSameDonor(),
        'same_donor_line' => $cell_line->getUnderlyingcellLine(),
        'obtainable' => $cell_line->getObtainable(),
        'restric_areas' => $cell_line->getRestricAreas(),
        'restric_research' => $cell_line->getRestricResearch(),
        'restric_clinical' => $cell_line->getRestricClinical(),
        'restric_commercial' => $cell_line->getRestricCommercial(),
        'restric_additional' => $cell_line->getRestricAdditional(),
        'hpscreg_name' => $cell_line->getHpscregName(),
        'genetic_predisposition' => $cell_line->getGeneticPredisposition(),
        'genetic_predisposition_spec' =>
          $cell_line->getGeneticPredispositionSpec(),
        'genetic_predisposition_spec_visibility' =>
          $cell_line->getVisibilityPredispositionSpec(),
        'donor_gender' => $cell_line->getDonorGender(),
        'donor_race' => $cell_line->getDonorRace(),
        'donor_age' => $cell_line->getDonorAge(),
        'donor_secutrial_id' => $cell_line->getDonorSecutrialId(),
        'donor_pseudonym' => $cell_line->getDonorPseudonym(),
        'donor_starlims_id' => $cell_line->getDonorStarlimsId(),
        'source_type' => $cell_line->getSourceType(),
        'sample_origin' => $cell_line->getSampleOrigin(),
        'sampling_date' => $cell_line->getSamplingDate(),
        'primary_culture' => $cell_line->getPrimaryCulture(),
        'reprogramming_method' => $cell_line->getReprogrammingMethod(),
        'reprogramming_vector' => $cell_line->getReprogrammingVector(),
        'hiv' => $cell_line->getHivResult(),
        'hiv_material' => $cell_line->getHivMaterial(),
        'hiv_date' => $cell_line->getHivDate(),
        'hep_b' => $cell_line->getHepB(),
        'hep_b_material' => $cell_line->getHepBMaterial(),
        'hep_b_date' => $cell_line->getHepBDate(),
        'hep_c' => $cell_line->getHepC(),
        'hep_c_material' => $cell_line->getHepCMaterial(),
        'hep_c_date' => $cell_line->getHepCDate(),
        'hla_a' => $cell_line->getHlaA(),
        'hla_b' => $cell_line->getHlaB(),
        'hla_drb1' => $cell_line->getHlaDrb1(),
        'ethics1' => $cell_line->getEthics1(),
        'ethics2' => $cell_line->getEthics2(),
        'ethics3' => $cell_line->getEthics3(),
        'ethics4' => $cell_line->getEthics4(),
        'ethics4_copy' => $cell_line->getEthics4Contact(),
        'ethics5' => $cell_line->getEthics5(),
        'ethics5_copy' => $cell_line->getEthics5Contact(),
        'ethics6' => $cell_line->getEthics6(),
        'ethics7' => $cell_line->getEthics7(),
        'ethics8' => $cell_line->getEthics8(),
        'ethics9' => $cell_line->getEthics9(),
        'ethics10' => $cell_line->getEthics10(),
        'ethics11' => $cell_line->getEthics11(),
        'ethics11_spec_name' => $cell_line->getEthics11SpecName(),
        'ethics11_spec_number' => $cell_line->getEthics11SpecNumber(),
        'ethics12' => $cell_line->getEthics12(),
        'ethics12_spec_name' => $cell_line->getEthics12SpecName(),
        'ethics12_spec_number' => $cell_line->getEthics12SpecNumber(),
        'ethics13' => $cell_line->getEthics13(),
        'ethics14' => $cell_line->getEthics14(),
        'ethics15' => $cell_line->getEthics15(),
        'ethics16' => $cell_line->getEthics16(),
        'ethics17' => $cell_line->getEthics17(),
        'ethics18' => $cell_line->getEthics18(),
        'ethics19' => $cell_line->getEthics19(),
        'ethics20' => $cell_line->getEthics20(),
        'ethics21' => $cell_line->getEthics21(),
        'ethics22' => $cell_line->getEthics22(),
        'ethics23' => $cell_line->getEthics23(),
        'ethics24' => $cell_line->getEthics24(),
        'ethics25' => $cell_line->getEthics25(),
        'ethics26' => $cell_line->getEthics26(),
        'ethics27' => $cell_line->getEthics27(),
        'contact' => $cell_line->getContactEMail(),
        'correspondence' => $cell_line->getCorrespondence(),
        'cost_coverage' => $cell_line->getCostCoverage(),
        'distribution' => $cell_line->getDistribution(),
        'lines_in_biobank' => $cell_line->getLinesInBiobank(),
        'biosafety' => $cell_line->getBiosafety(),
        'comments' => $cell_line->getComments(),
        'visibility' => $cell_line->getVisibility(),
      ])
      ->execute();

    $cell_line_id = self::findByLabId($cell_line->getLabId())->getId();

    $obj_diags = $cell_line->getDiagnoses();
    $db_diags = CellLineDiagnosisRepository::findByCellLineId($cell_line_id);
    if ($obj_diags !== NULL) {
      foreach ($obj_diags as $diag) {
        $diag->setCellLine($cell_line_id);
        $diag->save();
      }

      foreach ($db_diags as $db_diag) {
        $diag_exists = FALSE;
        foreach ($obj_diags as $obj_diag) {
          if ($db_diag->getId() == $obj_diag->getId()) {
            $diag_exists = TRUE;
            break;
          }
        }
        if (!$diag_exists){
          CellLineDiagnosisRepository::delete($db_diag->getId());
        }
      }
    }

    $obj_rels = $cell_line->getDonorRelatives();
    $db_rels = CellLineDonorRelativeRepository::findByCellLineId($cell_line_id);
    if ($obj_rels !== NULL) {
      foreach ($obj_rels as $rel) {
        $rel->setCellLine($cell_line_id);
        $rel->save();
      }

      foreach ($db_rels as $db_rel) {
        $rel_exists = FALSE;
        foreach ($obj_rels as $obj_rel) {
          if ($db_rel->getId() == $obj_rel->getId()) {
            $rel_exists = TRUE;
            break;
          }
        }
        if (!$rel_exists){
          CellLineDonorRelativeRepository::delete($db_rel->getId());
        }
      }
    }

    $obj_rep_dates = $cell_line->getReprogrammingDates();
    $db_rep_dates= CellLineReprogrammingDateRepository::findByCellLineId($cell_line_id);
    if ($obj_rep_dates !== NULL) {
      foreach ($obj_rep_dates as $rep) {
        $rep->setCellLine($cell_line_id);
        $rep->save();
      }

      foreach ($db_rep_dates as $db_rep_date) {
        $rep_date_exists = FALSE;
        foreach ($obj_rep_dates as $obj_rep_date) {
          if ($db_rep_date->getId() == $obj_rep_date->getId()) {
            $rep_date_exists = TRUE;
            break;
          }
        }
        if (!$rep_date_exists){
          CellLineReprogrammingDateRepository::delete($db_rep_date->getId());
        }
      }
    }

    $obj_clones = $cell_line->getClones();
    $db_clones = CellLineCloneRepository::findByCellLineId($cell_line_id);
    if ($obj_clones !== NULL) {
      foreach ($obj_clones as $clone) {
        $clone->setCellLine($cell_line_id);
        $clone->save();
      }

      foreach ($db_clones as $db_clone) {
        $clone_exists = FALSE;
        foreach ($obj_clones as $obj_clone) {
          if ($db_clone->getId() == $obj_clone->getId()) {
            $clone_exists = TRUE;
            break;
          }
        }
        if (!$clone_exists){
          CellLineCloneRepository::delete($db_clone->getId());
        }
      }
    }
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLine object.
   *
   * @param $result
   *   Database result of a finder function.
   *
   * @return \CellLine
   *   New CellLine object.
   */
  public static function databaseResultsToCellLine($result) {
    $cell_line = new CellLine();

    if (empty($result)) {
      return NULL;
    }

    // Set the variables.
    $cell_line->setId($result->id);
    $cell_line->setCreatedDate($result->created_date);
    $cell_line->setCreatedBy($result->created_by);
    $cell_line->setLastModifiedDate($result->last_modified_date);
    $cell_line->setLastModifiedBy($result->last_modified_by);
    $cell_line->setTabType($result->tab_type);
    $cell_line->setLabId($result->lab_id);
    $cell_line->setAltId($result->alt_id);
    $cell_line->setProvider($result->provider);
    $cell_line->setLocation($result->location);
    $cell_line->setProject($result->project);
    $cell_line->setCellType($result->cell_type);
    $cell_line->setGrade($result->grade);
    $cell_line->setIpscId($result->ipsc_id);
    $cell_line->setSameDonor($result->same_donor);
    $cell_line->setUnderlyingcellLine($result->same_donor_line);
    $cell_line->setObtainable($result->obtainable);
    $cell_line->setRestricAreas($result->restric_areas);
    $cell_line->setRestricResearch($result->restric_research);
    $cell_line->setRestricClinical($result->restric_clinical);
    $cell_line->setRestricCommercial($result->restric_commercial);
    $cell_line->setRestricAdditional($result->restric_additional);
    $cell_line->setHpscregName($result->hpscreg_name);
    $cell_line->setGeneticPredisposition($result->genetic_predisposition);
    $cell_line->setGeneticPredispositionSpec
    ($result->genetic_predisposition_spec);
    $cell_line->setVisibilityPredispositionSpec
    ($result->genetic_predisposition_spec_visibility);
    $cell_line->setDonorGender($result->donor_gender);
    $cell_line->setDonorRace($result->donor_race);
    $cell_line->setDonorAge($result->donor_age);
    $cell_line->setDonorSecutrialId($result->donor_secutrial_id);
    $cell_line->setDonorPseudonym($result->donor_pseudonym);
    $cell_line->setDonorStarlimsId($result->donor_starlims_id);
    $cell_line->setSourceType($result->source_type);
    $cell_line->setSampleOrigin($result->sample_origin);
    $cell_line->setSamplingDate($result->sampling_date);
    $cell_line->setPrimaryCulture($result->primary_culture);
    $cell_line->setReprogrammingMethod($result->reprogramming_method);
    $cell_line->setReprogrammingVector($result->reprogramming_vector);
    $cell_line->setHivResult($result->hiv);
    $cell_line->setHivMaterial($result->hiv_material);
    $cell_line->setHivDate($result->hiv_date);
    $cell_line->setHepB($result->hep_b);
    $cell_line->setHepBMaterial($result->hep_b_material);
    $cell_line->setHepBDate($result->hep_b_date);
    $cell_line->setHepC($result->hep_c);
    $cell_line->setHepCMaterial($result->hep_c_material);
    $cell_line->setHepCDate($result->hep_c_date);
    $cell_line->setHlaA($result->hla_a);
    $cell_line->setHlaB($result->hla_b);
    $cell_line->setHlaDrb1($result->hla_drb1);
    $cell_line->setEthics1($result->ethics1);
    $cell_line->setEthics2($result->ethics2);
    $cell_line->setEthics3($result->ethics3);
    $cell_line->setEthics4($result->ethics4);
    $cell_line->setEthics4Contact($result->ethics4_copy);
    $cell_line->setEthics5($result->ethics5);
    $cell_line->setEthics5Contact($result->ethics5_copy);
    $cell_line->setEthics6($result->ethics6);
    $cell_line->setEthics7($result->ethics7);
    $cell_line->setEthics8($result->ethics8);
    $cell_line->setEthics9($result->ethics9);
    $cell_line->setEthics10($result->ethics10);
    $cell_line->setEthics11($result->ethics11);
    $cell_line->setEthics11SpecName($result->ethics11_spec_name);
    $cell_line->setEthics11SpecNumber($result->ethics11_spec_number);
    $cell_line->setEthics12($result->ethics12);
    $cell_line->setEthics12SpecName($result->ethics12_spec_name);
    $cell_line->setEthics12SpecNumber($result->ethics12_spec_number);
    $cell_line->setEthics13($result->ethics13);
    $cell_line->setEthics14($result->ethics14);
    $cell_line->setEthics15($result->ethics15);
    $cell_line->setEthics16($result->ethics16);
    $cell_line->setEthics17($result->ethics17);
    $cell_line->setEthics18($result->ethics18);
    $cell_line->setEthics19($result->ethics19);
    $cell_line->setEthics20($result->ethics20);
    $cell_line->setEthics21($result->ethics21);
    $cell_line->setEthics22($result->ethics22);
    $cell_line->setEthics23($result->ethics23);
    $cell_line->setEthics24($result->ethics24);
    $cell_line->setEthics25($result->ethics25);
    $cell_line->setEthics26($result->ethics26);
    $cell_line->setEthics27($result->ethics27);
    $cell_line->setContactEMail($result->contact);
    $cell_line->setCorrespondence($result->correspondence);
    $cell_line->setCostCoverage($result->cost_coverage);
    $cell_line->setDistribution($result->distribution);
    $cell_line->setLinesInBiobank($result->lines_in_biobank);
    $cell_line->setBiosafety($result->biosafety);
    $cell_line->setComments($result->comments);
    $cell_line->setVisibility($result->visibility);

    return $cell_line;
  }

  /**
   * Read database results and create an array with CellLine objects.
   *
   * @param $results
   *   Database result of a finder function.
   *
   * @return \CellLine[]
   *   New CellLine objects.
   */
  public static function databaseResultsToCellLines($results) {
    $cell_lines = [];
    foreach ($results as $result) {
      $cell_lines[] = self::databaseResultsToCellLine($result);
    }

    return $cell_lines;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Return CellLine of given Lab ID.
   *
   * @param string $lab_id
   *   Lab ID of the given CellLine.
   *
   * @return \CellLine
   *   Found CellLine object.
   */
  public static function findByLabId($lab_id) {
    $result = db_select(self::$tableName, 'a')
      ->condition('lab_id', $lab_id, '=')
      ->fields('a', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToCellLine($result);
  }

  /**
   * Return CellLine of given iPSC ID.
   *
   * @param string $ipsc_id
   *   iPSC ID of the given CellLine.
   *
   * @return \CellLine
   *   Found CellLine object.
   */
  public static function findByIpscId($ipsc_id) {
    $result = db_select(self::$tableName, 'a')
      ->condition('ipsc_id', $ipsc_id, '=')
      ->fields('a', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToCellLine($result);
  }

  /**
   * Return CellLine of given tab_type.
   *
   * @param string $tab_type
   *   Tab type of the given CellLine.
   *
   * @return \CellLine[]
   *   Found CellLine objects.
   */
  public static function findByTabType($tab_type) {
    $result = db_select(self::$tableName, 'a')
      ->condition('tab_type', $tab_type, '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToCellLines($result);
  }

  /**
   * Return CellLine of a given database ID.
   *
   * @param string $id
   *   Database ID of the given CellLine.
   *
   * @return \CellLine
   *   Found CellLine object.
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 'a')
      ->condition('id', $id, '=')
      ->fields('a', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToCellLine($result);
  }

  /**
   * Return CellLine of a given hPSCreg name.
   *
   * @param string $hpscreg
   *   HPSCreg name of the fiven CellLine.
   *
   * @return \CellLine
   *   Found CellLine object.
   */
  public static function findByHpscreg($hpscreg) {
    $result = db_select(self::$tableName, 'a')
      ->condition('hpscreg_name', $hpscreg, '=')
      ->fields('a', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToCellLine($result);
  }

  /**
   * Execute a search query on the CellLine database with a given condition.
   * TableSort is used.
   *
   * @param       $condition
   *   Database condition.
   * @param array $header
   *   Table header.
   * @param       $tab_type
   *   Tab type of the CellLines.
   *
   * @return \CellLine[]
   *   Found CellLine objects.
   */
  public static function findByUseTableSort($header, $condition, $tab_type) {
    $query = db_select(self::$tableName, 't');
    $query->leftJoin('cellmodel_cell_line_diagnosis', 'n', "t.id = n.cell_line AND n.type = 'primary'");
    $query->leftJoin('cellmodel_cell_line_editing', 'm', "t.id = m.cell_line");

    $results = $query
      ->condition($condition)
      ->condition('tab_type', $tab_type, '=')
      ->fields('t', self::$databaseFields)
      ->fields('n', ['icd', 'disease'])
      ->fields('m', [
        'editing_strategy',
        'editing_method1',
        'editing_method2',
        'editing_method3',
      ])
      ->extend('TableSort')
      ->orderByHeader($header)
      ->orderBy('id')
      ->execute();

    return self::databaseResultsToCellLines($results);
  }

  /**
   * Execute a search query on the CellLine database with a given condition.
   *
   * @param $condition
   *   Database condition.
   *
   * @return \CellLine[]
   *   Found CellLine objects.
   */
  public static function findBy($condition) {
    $query = db_select(self::$tableName, 't');
    $query->leftJoin('cellmodel_cell_line_diagnosis', 'n', "t.id = n.cell_line AND n.type = 'primary'");

    $results = $query
      ->condition($condition)
      ->fields('t', self::$databaseFields)
      ->fields('n', ['icd', 'disease'])
      ->execute();

    return self::databaseResultsToCellLines($results);
  }

  /**
   * Execute a search query on the CellLine database with a two types of
   * given conditions.
   *
   * @param $header
   *   Table header.
   * @param $condition1
   *   Database condition 1.
   * @param $condition2
   *   Database condition 2.
   * @param $tab_type
   *   Tab type of the CellLines.
   *
   * @return \CellLine[]
   *   The respective cell line.
   */
  public static function findByTwoUseTableSort($header, $condition1, $condition2, $tab_type) {
    $query = db_select(self::$tableName, 't');
    $query->leftJoin('cellmodel_cell_line_diagnosis', 'n', "t.id = n.cell_line AND n.type = 'primary'");
    $query->leftJoin('cellmodel_cell_line_editing', 'm', "t.id = m.cell_line");
    $results = $query
      ->condition($condition1)
      ->condition($condition2)
      ->condition('tab_type', $tab_type, '=')
      ->fields('t', self::$databaseFields)
      ->fields('n', ['icd', 'disease'])
      ->fields('m', [
        'editing_strategy',
        'editing_method1',
        'editing_method2',
        'editing_method3',
      ])
      ->extend('TableSort')
      ->orderByHeader($header)
      ->orderBy('id')
      ->execute();

    return self::databaseResultsToCellLines($results);
  }

  /**
   * Execute a search query on the CellLine database with a given condition.
   * The CellLines have to be public.
   *
   * @param $condition
   *   Database condition.
   *
   * @return \CellLine[].
   *  Found CellLine objects.
   */
  public static function findByPublic($condition) {
    $results = db_select(self::$tableName, 'a')
      ->condition('visibility', 'Public', '=')
      //->condition('hpscreg_name', '', '<>')
      ->condition($condition)
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToCellLines($results);
  }

  /**
   * Find all CellLines with the same underlying cell line.
   *
   * @param $underlying_cell_line
   *   Database ID of underlying cell line.
   *
   * @return \CellLine[].
   *  Found CellLine objects.
   */
  public static function findByRelated($underlying_cell_line) {
    $results = db_select(self::$tableName, 'a')
      ->condition('same_donor_line', $underlying_cell_line, '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToCellLines($results);
  }

  /**
   * Return all CellLines.
   *
   * @return \CellLine[]
   *   Found CellLine objects.
   */
  public static function findAll() {
    $results = db_select(self::$tableName, 'a')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToCellLines($results);
  }

  /**
   * Return IDs of all Celllines.
   *
   * @return array IDs found
   */
  public static function getAllIds(){
    $results = db_select(self::$tableName,'id')
      ->fields('id',['id'])
      ->execute()
      ->fetchCol();
    return $results;
  }


  /**
   * Return all CellLines. Use TableSort.
   *
   * @param     $header
   *   Table header.
   * @param int $limit
   *   Maximum number of cell lines per page.
   * @param     $tab_type
   *   Tab type of the CellLines.
   *
   * @return \CellLine[]
   *   Found CellLine objects.
   */
  public static function findAllUseTableSortAndUsePagerDefault($header, $limit, $tab_type = 'internal') {

    $query = db_select(self::$tableName, 't');
    $query->leftJoin('cellmodel_cell_line_diagnosis', 'n', "t.id = n.cell_line AND n.type = 'primary'");
    $query->leftJoin('cellmodel_cell_line_editing', 'm', "t.id = m.cell_line");

    $results = $query
      ->extend('TableSort')
      ->fields('t', self::$databaseFields)
      ->fields('n', ['icd', 'disease'])
      ->fields('m', [
        'editing_strategy',
        'editing_targeted_gene',
        'editing_method1',
        'editing_method2',
        'editing_method3',
      ])
      ->condition('tab_type', $tab_type, '=')
      ->extend('PagerDefault')
      ->limit($limit)
      ->orderByHeader($header)
      ->orderBy('id')
      ->execute();
    return self::databaseResultsToCellLines($results);
  }

  /**
   * Return all CellLines. Use TableSort. The CellLines have to be public.
   *
   * @param     $header
   *   Table header.
   * @param int $limit
   *   Maximum number of cell lines per page.
   *
   * @return \CellLine[]
   *   Found CellLine objects.
   */
  public static function findAllPublicUseTableSortAndUsePagerDefault($header, $limit, $tab_type = 'internal') {

    $query = db_select(self::$tableName, 't');
    $query->leftJoin('cellmodel_cell_line_diagnosis', 'n', "t.id = n.cell_line AND n.type = 'primary'");
    $query->leftJoin('cellmodel_cell_line_editing', 'm', "t.id = m.cell_line");

    $results = $query
      ->extend('TableSort')
      ->fields('t', self::$databaseFields)
      ->fields('n', ['icd', 'disease'])
      ->fields('m', [
        'editing_strategy',
        'editing_targeted_gene',
        'editing_method1',
        'editing_method2',
        'editing_method3',
      ])
      ->extend('PagerDefault')
      ->condition('tab_type', $tab_type, '=')
      ->condition('visibility', 'Public', '=')
      ->limit($limit)
      ->orderByHeader($header)
      ->orderBy('id')
      ->execute();
    return self::databaseResultsToCellLines($results);
  }

  /**
   * Return CellLines with specific statistics.
   *
   * @param $tab_type
   *   Tab type of the CellLines.
   *
   * @return array
   *   Found CellLine objects with statistics.
   */
  public static function statistics($tab_type) {
    // Get cell type stats
    $query = db_select(self::$tableName, 't');
    if ($tab_type == 'total') {
      $query
        ->fields('t', ['cell_type'])
        ->groupBy('t.cell_type')
        ->addExpression('COUNT(t.cell_type)', 'n');
    }
    else {
      $query
        ->fields('t', ['cell_type'])
        ->condition('tab_type', $tab_type, '=')
        ->groupBy('t.cell_type')
        ->addExpression('COUNT(t.cell_type)', 'n');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $cell_type = [];
    foreach ($result as $row) {
      $cell_type['type'][] = $row->cell_type;
      $cell_type['n'][] = $row->n;
    }

    // Get source type stats
    $query = db_select(self::$tableName, 't');
    if ($tab_type == 'total') {
      $query
        ->fields('t', ['source_type'])
        ->groupBy('t.source_type')
        ->addExpression('COUNT(*)', 'n');
    }
    else {
      $query
        ->fields('t', ['source_type'])
        ->condition('tab_type', $tab_type, '=')
        ->groupBy('t.source_type')
        ->addExpression('COUNT(*)', 'n');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $source_type = [];
    $none = 0;
    foreach ($result as $row) {
      if ($row->source_type) {
        $source_type['type'][] = $row->source_type;
        $source_type['n'][] = $row->n;
      }
      else {
        $none += $row->n;
      }
    }

    if ($none) {
      $source_type['type'][] = 'None';
      $source_type['n'][] = $none;
    }

    // Get reprogramming method stats
    $query = db_select(self::$tableName, 't');
    if ($tab_type == 'total') {
      $query
        ->fields('t', ['reprogramming_method'])
        ->groupBy('t.reprogramming_method')
        ->addExpression('COUNT(*)', 'n');
    }
    else {
      $query
        ->fields('t', ['reprogramming_method'])
        ->condition('tab_type', $tab_type, '=')
        ->groupBy('t.reprogramming_method')
        ->addExpression('COUNT(*)', 'n');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $rep_method = [];
    $none = 0;
    foreach ($result as $row) {
      if ($row->reprogramming_method) {
        $rep_method['method'][] = $row->reprogramming_method;
        $rep_method['n'][] = $row->n;
      }
      else {
        $none += $row->n;
      }
    }

    if ($none) {
      $rep_method['method'][] = 'None';
      $rep_method['n'][] = $none;
    }

    // Get donor gender stats
    $query = db_select(self::$tableName, 't');
    if ($tab_type == 'total') {
      $query
        ->fields('t', ['donor_gender'])
        ->groupBy('t.donor_gender')
        ->addExpression('COUNT(*)', 'n');
    }
    else {
      $query
        ->fields('t', ['donor_gender'])
        ->condition('tab_type', $tab_type, '=')
        ->groupBy('t.donor_gender')
        ->addExpression('COUNT(*)', 'n');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $gender = [];
    $none = 0;
    foreach ($result as $row) {
      if ($row->donor_gender) {
        $gender['gender'][] = $row->donor_gender;
        $gender['n'][] = $row->n;
      }
      else {
        $none += $row->n;
      }
    }

    if ($none) {
      $gender['gender'][] = 'None';
      $gender['n'][] = $none;
    }

    // Get primary disease stats
    $diagnosis_threshold = 3;

    $query = db_select(self::$tableName, 't');
    $query->leftJoin('cellmodel_cell_line_diagnosis', 'n', "t.id = n.cell_line AND n.type = 'primary'");
    if ($tab_type == 'total') {
      $query
        ->fields('n', ['disease'])
        ->groupBy('n.disease')
        ->addExpression('COUNT(*)', 'n');
    }
    else {
      $query
        ->fields('n', ['disease'])
        ->condition('t.tab_type', $tab_type, '=')
        ->groupBy('n.disease')
        ->addExpression('COUNT(*)', 'n');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $disease_prim = [];
    $others = 0;
    foreach ($result as $row) {
      if ($row->disease) {
        if ($row->n >= $diagnosis_threshold) {
          $disease_prim['disease'][] = $row->disease;
          $disease_prim['n'][] = $row->n;
        }
        else {
          $others += $row->n;
        }
      }
    }

    if ($others) {
      $disease_prim['disease'][] = 'others';
      $disease_prim['n'][] = $others;
    }

    // Get secondary disease stats
    $query = db_select(self::$tableName, 't');
    $query->leftJoin('cellmodel_cell_line_diagnosis', 'n', "t.id = n.cell_line AND n.type = 'secondary'");
    if ($tab_type == 'total') {
      $query
        ->fields('n', ['disease'])
        ->groupBy('n.disease')
        ->addExpression('COUNT(*)', 'n');
    }
    else {
      $query
        ->fields('n', ['disease'])
        ->condition('t.tab_type', $tab_type, '=')
        ->groupBy('n.disease')
        ->addExpression('COUNT(*)', 'n');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $disease_sec = [];
    $others = 0;
    foreach ($result as $row) {
      if ($row->disease) {
        if ($row->n > $diagnosis_threshold) {
          $disease_sec['disease'][] = $row->disease;
          $disease_sec['n'][] = $row->n;
        }
        else {
          $others += $row->n;
        }
      }
    }

    if ($others) {
      $disease_sec['disease'][] = 'others';
      $disease_sec['n'][] = $others;
    }

    // Get sampling date
    $query = db_select(self::$tableName, 't');
    if ($tab_type == 'total') {
      $query
        ->fields('t', ['sampling_date'])
        ->groupBy('t.sampling_date')
        ->addExpression('COUNT(*)', 'n');
    }
    else {
      $query
        ->fields('t', ['sampling_date'])
        ->condition('t.tab_type', $tab_type, '=')
        ->groupBy('t.sampling_date')
        ->addExpression('COUNT(*)', 'n');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $samp_date = [];
    $none = 0;
    foreach ($result as $row) {
      if ($row->sampling_date) {
        $samp_date['date'][] = $row->sampling_date;
        $samp_date['n'][] = $row->n;
      }
      else {
        $none += $row->n;
      }
    }

    if ($none) {
      $samp_date['date'][] = 'None';
      $samp_date['n'][] = $none;
    }

    // Return
    return [
      'cell_type' => $cell_type,
      'source_type' => $source_type,
      'rep_method' => $rep_method,
      'gender' => $gender,
      'disease_prim' => $disease_prim,
      'disease_sec' => $disease_sec,
      'samp_date' => $samp_date,
    ];
  }
}
