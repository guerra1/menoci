<?php

/**
 * @file
 * Provide database layer for @see \CellLineEditingDate.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineEditingDateRepository
 */
class CellLineEditingDateRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLineEditingDate.
   */
  static $tableName = 'cellmodel_cell_line_editing_date';

  /**
   * @var array
   *   All database fields for CellLineEditingDate.
   */
  static $databaseFields = [
    'id',
    'cell_line',
    'date',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLineEditingDate into the database.
   *
   * @param \CellLineEditingDate $date
   *   CellLineEditingDate object to be saved.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($date) {
    db_merge(self::$tableName)
      ->key(['id' => $date->getId()])
      ->fields([
        'cell_line' => $date->getCellLine(),
        'date' => $date->getDate(),
      ])
      ->execute();
  }

  /**
   * Remove a CellLineEditingDate entry from the database.
   *
   * @param int $date_id
   *   ID of the respective CellLineEditingDate object.
   */
  public static function delete($date_id) {
    db_delete(self::$tableName)
      ->condition('id', $date_id, '=')
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLineEditingDate object.
   *
   * @param \stdClass $result
   *   Database result of a finder function.
   *
   * @return \CellLineEditingDate
   *   New CellLineEditingDate object.
   */
  public static function databaseResultsToEditingDate($result) {
    $date = new CellLineEditingDate();

    if (empty($result)) {
      return $date;
    }

    // Set the variables.
    $date->setId($result->id);
    $date->setCellLine($result->cell_line);
    $date->setDate($result->date);

    return $date;
  }

  /**
   * Read database results and create an array with CellLineEditingDate objects.
   *
   * @param \DatabaseStatementInterface $results
   *   Database result of a finder function.
   *
   * @return \CellLineEditingDate[]
   *   New CellLineEditingDate objects.
   */
  public static function databaseResultsToEditingDates($results) {
    $diagnoses = [];
    foreach ($results as $result) {
      $diagnoses[] = self::databaseResultsToEditingDate($result);
    }

    return $diagnoses;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Return CellLineEditingDate of given CellLine (Database ID).
   *
   * @param int $cell_line_id
   *   The ID of the given CellLine.
   *
   * @return \CellLineEditingDate[]
   *  Found CellLineEditingDate objects.
   */
  public static function findByCellLineId($cell_line_id) {
    $result = db_select(self::$tableName, 'a')
      ->condition('cell_line', $cell_line_id, '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToEditingDates($result);
  }
}
