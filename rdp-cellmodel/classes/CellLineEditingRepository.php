<?php

/**
 * @file
 * Provide database layer for @see \CellLineEditing.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineEditingRepository
 */
class CellLineEditingRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLineEditing.
   */
  static $tableName = 'cellmodel_cell_line_editing';

  /**
   * @var array
   *   All database fields for CellLineEditing.
   */
  static $databaseFields = [
    'id',
    'cell_line',
    'clone_used',
    'editing_strategy',
    'editing_targeted_gene',
    'editing_targeted_gene',
    'editing_targeted_gene_visibility',
    'editing_method1',
    'editing_method2',
    'editing_method3',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLineEditing into the database.
   *
   * @param \CellLineEditing $editing
   *   CellLineEditing object to be saved.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($editing) {
    db_merge(self::$tableName)
      ->key(['id' => $editing->getId()])
      ->fields([
        'cell_line' => $editing->getCellLine(),
        'clone_used' => $editing->getCloneUsed(),
        'editing_strategy' => $editing->getEditingStrategy(),
        'editing_targeted_gene' => $editing->getEditingTargetedGene(),
        'editing_targeted_gene_visibility' => $editing->getEditingTargetedGeneVisibility(),
        'editing_method1' => $editing->getEditingMethod1(),
        'editing_method2' => $editing->getEditingMethod2(),
        'editing_method3' => $editing->getEditingMethod3(),
      ])
      ->execute();

    $cell_line_id = $editing->getCellLine();

    $obj_edit_dates = $editing->getEditingDates();
    $db_edit_dates= CellLineEditingDateRepository::findByCellLineId($cell_line_id);
    if ($obj_edit_dates !== NULL) {
      foreach ($obj_edit_dates as $date) {
        $date->setCellLine($cell_line_id);
        $date->save();
      }
    }

    foreach ($db_edit_dates as $db_edit_date) {
      $edit_date_exists = FALSE;
      foreach ($obj_edit_dates as $obj_edit_date) {
        if ($db_edit_date->getId() == $obj_edit_date->getId()) {
          $edit_date_exists = TRUE;
          break;
        }
      }
      if (!$edit_date_exists){
        CellLineEditingDateRepository::delete($db_edit_date->getId());
      }
    }
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLineEditing object.
   *
   * @param \stdClass $result
   *   Database result of a finder function.
   *
   * @return \CellLineEditing
   *   New CellLineEditing object.
   */
  public static function databaseResultsToEditing($result) {
    $editing = new CellLineEditing();

    if (empty($result)) {
      return $editing;
    }

    // Set the variables.
    $editing->setId($result->id);
    $editing->setCellLine($result->cell_line);
    $editing->setCloneUsed($result->clone_used);
    $editing->setEditingStrategy($result->editing_strategy);
    $editing->setEditingTargetedGene($result->editing_targeted_gene);
    $editing->setEditingTargetedGeneVisibility($result->editing_targeted_gene_visibility);
    $editing->setEditingMethod1($result->editing_method1);
    $editing->setEditingMethod2($result->editing_method2);
    $editing->setEditingMethod3($result->editing_method3);

    return $editing;
  }

  /**
   * Read database results and create an array with CellLineEditing objects.
   *
   * @param \DatabaseStatementInterface $results
   *   Database result of a finder function.
   *
   * @return \CellLineEditing[]
   *   New CellLineEditing objects.
   */
  public static function databaseResultsToEditings($results) {
    $diagnoses = [];
    foreach ($results as $result) {
      $diagnoses[] = self::databaseResultsToEditing($result);
    }

    return $diagnoses;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Return CellLineEditing of given CellLine (Database ID).
   *
   * @param int $cell_line
   *   The ID of the given CellLine.
   *
   * @return \CellLineEditing
   *  Found CellLineEditing object.
   */
  public static function findByCellLineId($cell_line) {
    $result = db_select(self::$tableName, 'a')
      ->condition('cell_line', $cell_line, '=')
      ->fields('a', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToEditing($result);
  }
}
