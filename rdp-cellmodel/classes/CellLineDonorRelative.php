<?php

/**
 * @file
 * Provide class for the CellLineDonorRelative objects. Provide form fields
 * for each variable when building @see \rdp_cellmodel_cell_line_new_edit().
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineDonorRelative
 */
class CellLineDonorRelative {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   Database ID of the relative.
   */
  private $id;

  /**
   * @var int
   *   Respective cell line of the relative.
   */
  private $cell_line;

  /**
   * @var string
   *   Relative type.
   */
  private $type;

  /**
   * @var string
   *   STARLIMS ID of the relative.
   */
  private $starlims_id;

  /**
   * @var string
   *   Pseudonym of the relative.
   */
  private $pseudonym;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return mixed $id
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return mixed $cell_line
   */
  public function getCellLine() {
    return $this->cell_line;
  }

  /**
   * @param mixed $cell_line
   */
  public function setCellLine($cell_line) {
    $this->cell_line = $cell_line;
  }

  /**
   * @return string $pseudonym
   */
  public function getPseudonym() {
    return $this->pseudonym;
  }

  /**
   * @param string $pseudonym
   */
  public function setPseudonym($pseudonym) {
    $this->pseudonym = $pseudonym;
  }

  /**
   * @return mixed $starlims_id
   */
  public function getStarlimsId() {
    return $this->starlims_id;
  }

  /**
   * @param mixed $starlims_id
   */
  public function setStarlimsId($starlims_id) {
    $this->starlims_id = $starlims_id;
  }

  /**
   * @return mixed $type
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param mixed $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  // ---------------------------<<< FORM FIELDS >>>-----------------------------

  /**
   * @param int $count
   *   Current count of relatives.
   *
   * @return array
   *   Relative fieldset.
   */
  public function getFormFieldRelFieldset($count) {
    $count++;
    $title_prefix = generate_counting_string($count);
    return [
      '#type' => 'fieldset',
      '#title' => $title_prefix . t(' Relative of Donor'),
    ];
  }

  /**
   * @return array
   *   Relative STARLIMS ID form field.
   */
  public function getFormFieldRelStarID() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['relative_starlims_id'] . ' (*)',
      '#maxlength' => 16,
      '#default_value' => $this->getStarlimsId(),
      '#attributes' => [
        'placeholder' => t('e.g. 8716299125'),
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @return array
   *   Relative type form field.
   */
  public function getFormFieldRelType() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['relative_type'],
      '#default_value' => $this->getType(),
      '#options' => CellmodelConstants::getOptionsRelativeType(),
      '#empty_value' => '',
    ];
  }

  /**
   * @return array
   *   Relative pseudonym form field.
   */
  public function getFormFieldRelPseudonym() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['relative_pseudonym'] . ' (*)',
      '#default_value' => $this->pseudonym,
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
      '#attributes' => [
        'placeholder' => t('e.g. 17B0061'),
        'autocomplete' => 'off',
      ],
    ];
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellLineDonorRelative into the database.
   */
  public function save() {
    try {
      CellLineDonorRelativeRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}
