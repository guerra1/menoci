<?php

/**
 * @file
 * Provide class for the CellLineClone objects. Provide form fields
 * for each variable when building @see \rdp_cellmodel_cell_line_new_edit().
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineClone
 */
class CellLineClone {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   Database ID of the clone.
   */
  private $id;

  /**
   * @var int
   *   ID of the respective cell line.
   */
  private $cell_line;

  /**
   * @var string
   */
  private $clone_number;

  /**
   * @var string
   */
  private $clone_number_starlims;

  /**
   * @var string
   */
  private $culture_medium;

  /**
   * @var string
   */
  private $characterization;

  /**
   * @var string
   */
  private $start_date;

  /**
   * @var string
   */
  private $responsible;

  /**
   * @var string
   */
  private $morphology;

  /**
   * @var string
   */
  private $pluriotency_pcr;

  /**
   * @var string
   */
  private $pluriotency_immuno;

  /**
   * @var string
   */
  private $pluriotency_facs;

  /**
   * @var string
   */
  private $differentiation_immuno;

  /**
   * @var string
   */
  private $hla_analyzed;

  /**
   * @var string
   */
  private $mutation_analyzed;

  /**
   * @var string
   */
  private $editing_analyzed;

  /**
   * @var string
   */
  private $str_analyzed;

  /**
   * @var string
   */
  private $karyo_analyzed;

  /**
   * @var string
   */
  private $karyotype;

  /**
   * @var string
   */
  private $genomic_modification;

  /**
   * @var
   */
  private $hdr_nhej;

  /**
   * @var string
   */
  private $zygosity;

  /**
   * @var string
   */
  private $specification;

  /**
   * @var string
   */
  private $visibility;

  /**
   * @var string
   */
  private $visibility_clone;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getCellLine() {
    return $this->cell_line;
  }

  /**
   * @param int $cell_line
   */
  public function setCellLine($cell_line) {
    $this->cell_line = $cell_line;
  }

  /**
   * @return mixed
   */
  public function getHdrNhej() {
    return $this->hdr_nhej;
  }

  /**
   * @param mixed $hdr_nhej
   */
  public function setHdrNhej($hdr_nhej) {
    $this->hdr_nhej = $hdr_nhej;
  }

  /**
   * @return string
   */
  public function getCloneNumber() {
    return $this->clone_number;
  }

  /**
   * @param string $clone_number
   */
  public function setCloneNumber($clone_number) {
    $this->clone_number = $clone_number;
  }

  /**
   * @return string
   */
  public function getGenomicModification() {
    return $this->genomic_modification;
  }

  /**
   * @param string $genomic_modification
   */
  public function setGenomicModification($genomic_modification) {
    $this->genomic_modification = $genomic_modification;
  }

  /**
   * @return string
   */
  public function getZygosity() {
    return $this->zygosity;
  }

  /**
   * @param string $zygosity
   */
  public function setZygosity($zygosity) {
    $this->zygosity = $zygosity;
  }

  /**
   * @return string
   */
  public function getSpecification() {
    return $this->specification;
  }

  /**
   * @param string $specification
   */
  public function setSpecification($specification) {
    $this->specification = $specification;
  }

  /**
   * @return string
   */
  public function getVisibility() {
    return $this->visibility;
  }

  /**
   * @param string $visibility
   */
  public function setVisibility($visibility) {
    $this->visibility = $visibility;
  }

  /**
   * @return string
   */
  public function getVisibilityClone() {
    return $this->visibility_clone;
  }

  /**
   * @param string $visibility_clone
   */
  public function setVisibilityClone($visibility_clone) {
    $this->visibility_clone = $visibility_clone;
  }

  /**
   * @return string
   */
  public function getCloneNumberStarlims() {
    return $this->clone_number_starlims;
  }

  /**
   * @param string $clone_number_starlims
   */
  public function setCloneNumberStarlims($clone_number_starlims) {
    $this->clone_number_starlims = $clone_number_starlims;
  }

  /**
   * @return string
   */
  public function getCultureMedium() {
    return $this->culture_medium;
  }

  /**
   * @param string $culture_medium
   */
  public function setCultureMedium($culture_medium) {
    $this->culture_medium = $culture_medium;
  }

  /**
   * @return string
   */
  public function getCharacterization() {
    return $this->characterization;
  }

  /**
   * @param string $characterization
   */
  public function setCharacterization($characterization) {
    $this->characterization = $characterization;
  }

  /**
   * @return string
   */
  public function getStartDate() {
    return $this->start_date;
  }

  /**
   * @param string $start_date
   */
  public function setStartDate($start_date) {
    $this->start_date = $start_date;
  }

  /**
   * @return string
   */
  public function getResponsible() {
    return $this->responsible;
  }

  /**
   * @param string $responsible
   */
  public function setResponsible($responsible) {
    $this->responsible = $responsible;
  }

  /**
   * @return string
   */
  public function getMorphology() {
    return $this->morphology;
  }

  /**
   * @param string $morphology
   */
  public function setMorphology($morphology) {
    $this->morphology = $morphology;
  }

  /**
   * @return string
   */
  public function getPluriotencyPcr() {
    return $this->pluriotency_pcr;
  }

  /**
   * @param string $pluriotency_pcr
   */
  public function setPluriotencyPcr($pluriotency_pcr) {
    $this->pluriotency_pcr = $pluriotency_pcr;
  }

  /**
   * @return string
   */
  public function getPluriotencyImmuno() {
    return $this->pluriotency_immuno;
  }

  /**
   * @param string $pluriotency_immuno
   */
  public function setPluriotencyImmuno($pluriotency_immuno) {
    $this->pluriotency_immuno = $pluriotency_immuno;
  }

  /**
   * @return string
   */
  public function getPluriotencyFacs() {
    return $this->pluriotency_facs;
  }

  /**
   * @param string $pluriotency_facs
   */
  public function setPluriotencyFacs($pluriotency_facs) {
    $this->pluriotency_facs = $pluriotency_facs;
  }

  /**
   * @return string
   */
  public function getDifferentiationImmuno() {
    return $this->differentiation_immuno;
  }

  /**
   * @param string $differentiation_immuno
   */
  public function setDifferentiationImmuno($differentiation_immuno) {
    $this->differentiation_immuno = $differentiation_immuno;
  }

  /**
   * @return string
   */
  public function getHlaAnalyzed() {
    return $this->hla_analyzed;
  }

  /**
   * @param string $hla_analyzed
   */
  public function setHlaAnalyzed($hla_analyzed) {
    $this->hla_analyzed = $hla_analyzed;
  }

  /**
   * @return string
   */
  public function getMutationAnalyzed() {
    return $this->mutation_analyzed;
  }

  /**
   * @param string $mutation_analyzed
   */
  public function setMutationAnalyzed($mutation_analyzed) {
    $this->mutation_analyzed = $mutation_analyzed;
  }

  /**
   * @return string
   */
  public function getEditingAnalyzed() {
    return $this->editing_analyzed;
  }

  /**
   * @param string $editing_analyzed
   */
  public function setEditingAnalyzed($editing_analyzed) {
    $this->editing_analyzed = $editing_analyzed;
  }

  /**
   * @return string
   */
  public function getStrAnalyzed() {
    return $this->str_analyzed;
  }

  /**
   * @param string $str_analyzed
   */
  public function setStrAnalyzed($str_analyzed) {
    $this->str_analyzed = $str_analyzed;
  }

  /**
   * @return string
   */
  public function getKaryoAnalyzed() {
    return $this->karyo_analyzed;
  }

  /**
   * @param string $karyo_analyzed
   */
  public function setKaryoAnalyzed($karyo_analyzed) {
    $this->karyo_analyzed = $karyo_analyzed;
  }

  /**
   * @return string
   */
  public function getKaryotype() {
    return $this->karyotype;
  }

  /**
   * @param string $karyotype
   */
  public function setKaryotype($karyotype) {
    $this->karyotype = $karyotype;
  }



  // ---------------------------<<< FORM FIELDS >>>-----------------------------

  /**
   * @param int $count
   *   Current count of Clones.
   *
   * @return array
   *   Clone fieldset.
   */
  public function getFormFieldCloneFieldset($count, $saved_count) {
    $count++;
    $title = t('<i>New Clone</i>');

    if ($count > $saved_count) {
      $collapsed = FALSE;
    }
    else {
      $collapsed = TRUE;
    }

    $icon = getCharacterizationIcon($this->getCharacterization());
    if (!empty($this->getCloneNumber())) {
      $title = $icon . ' ' . $this->getCloneNumber();

      if (!empty($this->getGenomicModification())) {
        $title .= ' | ' . $this->getGenomicModification();
      }
      if (!empty($this->getHdrNhej())) {
        $title .= ' | ' . $this->getHdrNhej();
      }
      if (!empty($this->getZygosity())) {
        $title .= ' | ' . $this->getZygosity();
      }
      if (!empty($this->getSpecification())) {
        $title .= ' | ' . $this->getSpecification();
      }
    }

    return [
      '#type' => 'fieldset',
      '#title' => $title,
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
    ];
  }

  /**
   * @return array
   *   Genomic Modification form field.
   */
  public function getFormFieldGenomicModification($genetic_editing) {
    if ($genetic_editing) {
      return [
        '#type' => 'select',
        '#title' => CellmodelConstants::getFieldTitles()['genomic_modification'],
        '#default_value' => $this->genomic_modification,
        '#empty_value' => '',
        '#options' => CellmodelConstants::getOptionsGenomicModification(),
      ];
    }
    else {
      return [
        '#type' => 'hidden',
      ];
    }
  }

  /**
   * @return array
   *   HDR/NHEJ form field.
   */
  public function getFormFieldHdrNhej($genetic_editing) {
    if ($genetic_editing) {
      return [
        '#type' => 'select',
        '#title' => CellmodelConstants::getFieldTitles()['hdr_nhej'],
        '#default_value' => $this->hdr_nhej,
        '#empty_value' => '',
        '#options' => CellmodelConstants::getOptionsHdrNhej(),
      ];
    }
    else {
      return [
        '#type' => 'hidden',
      ];
    }
  }

  /**
   * @return array
   *   Zygosity form field.
   */
  public function getFormFieldZygosity($genetic_editing) {
    if ($genetic_editing) {
      return [
        '#type' => 'select',
        '#title' => CellmodelConstants::getFieldTitles()['zygosity'],
        '#default_value' => $this->zygosity,
        '#empty_value' => '',
        '#options' => CellmodelConstants::getOptionsZygosity(),
      ];
    }
    else {
      return [
        '#type' => 'hidden',
      ];
    }
  }

  /**
   * @return array
   *   Visibility form field.
   */
  public function getFormFieldVisibilityClone() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['clone_visibility'] . ' *',
      '#default_value' => $this->visibility_clone,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsVisibility(),
    ];
  }

  /**
   * @return array
   *   Visibility form field.
   */
  public function getFormFieldVisibility($i, $genetic_editing) {
    if ($genetic_editing) {
      return [
        '#type' => 'radios',
        '#title' => CellmodelConstants::getFieldTitles()['spec_visibility'] . ' *',
        '#default_value' => $this->visibility,
        '#empty_value' => '',
        '#options' => CellmodelConstants::getOptionsVisibility(),
        '#states' => [
          'visible' => [
            ':input[name*="clone_visible_' . $i . '"]' => [
              ['value' => 'Public'],
            ],
          ],
        ],
      ];
    }
    else {
      return [
        '#type' => 'hidden',
      ];
    }
  }

  /**
   * @return array
   *   Clone Number form field.
   */
  public function getFormFieldCloneNumber() {
    // Disable the field when editing, since it can act as an UID.
    if (!empty($this->getCloneNumber())) {
      $attributes = [
        'disabled' => 'disabled',
      ];
    }
    else {
      $attributes = [
        'placeholder' => t('e.g. ipWT1-LZTR1-KO.1'),
        'autocomplete' => 'off',
      ];
    }

    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['clone_number'] . ' *',
      '#default_value' => $this->clone_number,
      '#attributes' => [
        'placeholder' => $attributes,
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @return array
   *   Specification form field.
   */
  public function getFormFieldSpecification($genetic_editing) {
    if ($genetic_editing) {
      $type = 'textfield';
    }
    else {
      $type = 'hidden';
    }
    return [
      '#type' => $type,
      '#title' => CellmodelConstants::getFieldTitles()['specification'],
      '#default_value' => $this->specification,
      '#attributes' => [
        'placeholder' => t('e.g. p.G12V + KO, compound hetero'),
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @return array
   *   Clone Number in StarLIMS form field.
   */
  public function getFormFieldCloneNumberStarlims() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['clone_number_starlims'],
      '#default_value' => $this->clone_number_starlims,
      '#attributes' => [
        'placeholder' => t('e.g. 15'),
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @return array
   *   Culture Medium form field.
   */
  public function getFormFieldCultureMedium() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['culture_medium'],
      '#default_value' => $this->culture_medium,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsMedium(),
    ];
  }

  /**
   * @return array
   *   Characterization form field.
   */
  public function getFormFieldCharacterization() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['characterization'],
      '#default_value' => $this->characterization,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsCharacterization(),
    ];
  }

  /**
   * @return array
   *   Start Date form field.
   */
  public function getFormFieldStartDate($i) {
    return [
      '#type' => 'date_popup',
      '#name' => 'StartDate',
      '#date_format' => RDP_CELLMODEL_DEFAULT_DATE_FORMAT,
      '#default_value' => $this->start_date,
      '#attributes' => ['autocomplete' => 'off',],
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Responsible form field.
   */
  public function getFormFieldResponsible($i) {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['responsible'],
      '#default_value' => $this->responsible,
      '#attributes' => [
        'placeholder' => t('e.g. Lukas Cyganek'),
      ],
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Morphology form field.
   */
  public function getFormFieldMorphology($i) {
    return [
      '#type' => 'checkbox',
      '#title' => CellmodelConstants::getFieldTitles()['morphology'],
      '#default_value' => $this->morphology,
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Pluripotency PCR form field.
   */
  public function getFormFieldPluripotencyPcr($i) {
    return [
      '#type' => 'checkbox',
      '#title' => CellmodelConstants::getFieldTitles()['pluriotency_pcr'],
      '#default_value' => $this->pluriotency_pcr,
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Pluripotency Immuno form field.
   */
  public function getFormFieldPluripotencyImmuno($i) {
    return [
      '#type' => 'checkbox',
      '#title' => CellmodelConstants::getFieldTitles()['pluriotency_immuno'],
      '#default_value' => $this->pluriotency_immuno,
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Pluripotency FACS form field.
   */
  public function getFormFieldPluripotencyFacs($i) {
    return [
      '#type' => 'checkbox',
      '#title' => CellmodelConstants::getFieldTitles()['pluriotency_facs'],
      '#default_value' => $this->pluriotency_facs,
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Differentiation Immuno form field.
   */
  public function getFormFieldDifferentiationImmuno($i) {
    return [
      '#type' => 'checkbox',
      '#title' => CellmodelConstants::getFieldTitles()['differentiation_immuno'],
      '#default_value' => $this->differentiation_immuno,
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *  HLA analyzed form field.
   */
  public function getFormFieldHlaAnalyzed($i) {
    return [
      '#type' => 'checkbox',
      '#title' => CellmodelConstants::getFieldTitles()['hla_analyzed'],
      '#default_value' => $this->hla_analyzed,
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Mutation analyzed form field.
   */
  public function getFormFieldMutationAnalyzed($i, $genetic_editing) {
    if (!$genetic_editing) {
      return [
        '#type' => 'checkbox',
        '#title' => CellmodelConstants::getFieldTitles()['mutation_analyzed'],
        '#default_value' => $this->mutation_analyzed,
        '#states' => [
          'visible' => [
            [
              ':input[name*="characterization_' . $i . '"]' => [
                [
                  'value' =>
                    'Completed',
                ],
              ],
            ],
            [
              ':input[name*="characterization_' . $i . '"]' => [
                [
                  'value' =>
                    'In Progress',
                ],
              ],
            ],
          ],
        ],
      ];
    }
    else {
      return [
        '#type' => 'hidden',
      ];
    }

  }

  /**
   * @return array
   *   Editing analyzed form field.
   */
  public function getFormFieldEditingAnalyzed($i, $genetic_editing) {
    if ($genetic_editing) {
      return [
        '#type' => 'checkbox',
        '#title' => CellmodelConstants::getFieldTitles()['editing_analyzed'],
        '#default_value' => $this->editing_analyzed,
        '#states' => [
          'visible' => [
            [
              ':input[name*="characterization_' . $i . '"]' => [
                [
                  'value' =>
                    'Completed',
                ],
              ],
            ],
            [
              ':input[name*="characterization_' . $i . '"]' => [
                [
                  'value' =>
                    'In Progress',
                ],
              ],
            ],
          ],
        ],
      ];
    }
    else {
      return [
        '#type' => 'hidden',
      ];
    }
  }

  /**
   * @return array
   *   STR analyzed form field.
   */
  public function getFormFieldStrAnalyzed($i) {
    return [
      '#type' => 'checkbox',
      '#title' => CellmodelConstants::getFieldTitles()['str_analyzed'],
      '#default_value' => $this->str_analyzed,
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Karyo analyzed form field.
   */
  public function getFormFieldKaryoAnalyzed($i) {
    return [
      '#type' => 'checkbox',
      '#title' => CellmodelConstants::getFieldTitles()['karyo_analyzed'],
      '#default_value' => $this->karyo_analyzed,
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *  Karyotype form field.
   */
  public function getFormFieldKaryotype($i) {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['karyotype'],
      '#default_value' => $this->karyotype,
      '#attributes' => [
        'placeholder' => t('e.g. isNoonSa1.4'),
        'autocomplete' => 'off',
      ],
      '#states' => [
        'visible' => [
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'Completed',
              ],
            ],
            ':input[name*="karyo_analyzed_' . $i . '"]' => [
              [
                'checked' =>
                  TRUE,
              ],
            ],
          ],
          [
            ':input[name*="characterization_' . $i . '"]' => [
              [
                'value' =>
                  'In Progress',
              ],
            ],
            ':input[name*="karyo_analyzed_' . $i . '"]' => [
              [
                'checked' =>
                  TRUE,
              ],
            ],
          ],
        ],
      ],
    ];
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellLineClone into the database.
   */
  public function save() {
    try {
      CellLineCloneRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}
