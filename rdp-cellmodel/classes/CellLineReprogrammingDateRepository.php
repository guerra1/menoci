<?php

/**
 * @file
 * Provides database layer for @see CellLineReprogrammingDate.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineReprogrammingDateRepository
 */
class CellLineReprogrammingDateRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLineReprogrammingDate.
   */
  static $tableName = 'cellmodel_cell_line_rep_date';

  /**
   * @var array
   *   All database fields for CellLineReprogrammingDate.
   */
  static $databaseFields = [
    'id',
    'cell_line',
    'date',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLineReprogrammingDate into the database.
   *
   * @param $rep_date \CellLineReprogrammingDate
   *   CellLineReprogrammingDate object to be saved.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($rep_date) {
    db_merge(self::$tableName)
      ->key(['id' => $rep_date->getId()])
      ->fields([
        'cell_line' => $rep_date->getCellLine(),
        'date' => $rep_date->getDate(),
      ])
      ->execute();
  }

  /**
   * Remove a CellLineReprogrammingDate entry from the database.
   *
   * @param int $rep_date_id
   *   ID of the respective CellLineReprogrammingDate object.
   */
  public static function delete($rep_date_id) {
    db_delete(self::$tableName)
      ->condition('id', $rep_date_id, '=')
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLineReprogrammingDate object.
   *
   * @param $result
   *   Database result of a finder function.
   *
   * @return \CellLineReprogrammingDate
   *   New CellLineReprogrammingDate object.
   */
  public static function databaseResultsToRepDate($result) {
    $rep_date = new CellLineReprogrammingDate();

    if (empty($result)) {
      return $rep_date;
    }

    // Set the variables.
    $rep_date->setId($result->id);
    $rep_date->setCellLine($result->cell_line);
    $rep_date->setDate($result->date);

    return $rep_date;
  }

  /**
   * Reads rep. date database results and creates an array with
   * CellLineReprogrammingDate objects.
   *
   * @param $results
   *   Database result of a finder function.
   *
   * @return \CellLineReprogrammingDate[]
   *   New CellLineReprogrammingDate objects.
   */
  public static function databaseResultsToRepDates($results) {
    $rep_dates = [];
    foreach ($results as $result) {
      $rep_dates[] = self::databaseResultsToRepDate($result);
    }

    return $rep_dates;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Return CellLineReprogrammingDate of given CellLine (Database ID).
   *
   * @param int $cell_line
   *   ID of the given CellLine.
   *
   * @return \CellLineReprogrammingDate[]
   *  Found CellLineReprogrammingDate object.
   */
  public static function findByCellLineId($cell_line) {
    $result = db_select(self::$tableName, 'a')
      ->condition('cell_line', $cell_line, '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToRepDates($result);
  }
}
