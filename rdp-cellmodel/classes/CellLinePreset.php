<?php

/**
 * @file
 * Provide class for CellLinePreset objects.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class CellLinePreset
{

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   Database ID of preset.
   */
  private $id;

  /**
   * @var string
   *   Name of preset.
   */
  private $name;

  /**
   * @var string
   *
   */
  private $ethics_preset_1;

  /**
   * @var string
   *
   */
  private $ethics_preset_2;

  /**
   * @var string
   *
   */
  private $ethics_preset_3;

  /**
   * @var string
   *
   */
  private $ethics_preset_4;

  /**
   * @var string
   *
   */
  private $ethics_preset_5;

  /**
   * @var string
   *
   */
  private $ethics_preset_6;

  /**
   * @var string
   *
   */
  private $ethics_preset_7;

  /**
   * @var string
   *
   */
  private $ethics_preset_8;

  /**
   * @var string
   *
   */
  private $ethics_preset_9;

  /**
   * @var string
   *
   */
  private $ethics_preset_10;

  /**
   * @var string
   *
   */
  private $ethics_preset_11;

  /**
   * @var string
   *
   */
  private $ethics_preset_12;

  /**
   * @var string
   *
   */
  private $ethics_preset_13;

  /**
   * @var string
   *
   */
  private $ethics_preset_14;

  /**
   * @var string
   *
   */
  private $ethics_preset_15;

  /**
   * @var string
   *
   */
  private $ethics_preset_16;

  /**
   * @var string
   *
   */
  private $ethics_preset_17;

  /**
   * @var string
   *
   */
  private $ethics_preset_18;

  /**
   * @var string
   *
   */
  private $ethics_preset_19;

  /**
   * @var string
   *
   */
  private $ethics_preset_20;

  /**
   * @var string
   *
   */
  private $ethics_preset_21;

  /**
   * @var string
   *
   */
  private $ethics_preset_22;

  /**
   * @var string
   *
   */
  private $ethics_preset_23;

  /**
   * @var string
   *
   */
  private $ethics_preset_24;

  /**
   * @var string
   *
   */
  private $ethics_preset_25;

  /**
   * @var string
   *
   */
  private $ethics_preset_26;

  /**
   * @var string
   *
   */
  private $ethics_preset_27;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getEthicsPreset1()
  {
    return $this->ethics_preset_1;
  }

  /**
   * @param string $ethics_preset_1
   */
  public function setEthicsPreset1($ethics_preset_1)
  {
    $this->ethics_preset_1 = $ethics_preset_1;
  }

  /**
   * @return string
   */
  public function getEthicsPreset2()
  {
    return $this->ethics_preset_2;
  }

  /**
   * @param string $ethics_preset_2
   */
  public function setEthicsPreset2($ethics_preset_2)
  {
    $this->ethics_preset_2 = $ethics_preset_2;
  }

  /**
   * @return string
   */
  public function getEthicsPreset3()
  {
    return $this->ethics_preset_3;
  }

  /**
   * @param string $ethics_preset_3
   */
  public function setEthicsPreset3($ethics_preset_3)
  {
    $this->ethics_preset_3 = $ethics_preset_3;
  }

  /**
   * @return string
   */
  public function getEthicsPreset4()
  {
    return $this->ethics_preset_4;
  }

  /**
   * @param string $ethics_preset_4
   */
  public function setEthicsPreset4($ethics_preset_4)
  {
    $this->ethics_preset_4 = $ethics_preset_4;
  }

  /**
   * @return string
   */
  public function getEthicsPreset5()
  {
    return $this->ethics_preset_5;
  }

  /**
   * @param string $ethics_preset_5
   */
  public function setEthicsPreset5($ethics_preset_5)
  {
    $this->ethics_preset_5 = $ethics_preset_5;
  }

  /**
   * @return string
   */
  public function getEthicsPreset6()
  {
    return $this->ethics_preset_6;
  }

  /**
   * @param string $ethics_preset_6
   */
  public function setEthicsPreset6($ethics_preset_6)
  {
    $this->ethics_preset_6 = $ethics_preset_6;
  }

  /**
   * @return string
   */
  public function getEthicsPreset7()
  {
    return $this->ethics_preset_7;
  }

  /**
   * @param string $ethics_preset_7
   */
  public function setEthicsPreset7($ethics_preset_7)
  {
    $this->ethics_preset_7 = $ethics_preset_7;
  }

  /**
   * @return string
   */
  public function getEthicsPreset8()
  {
    return $this->ethics_preset_8;
  }

  /**
   * @param string $ethics_preset_8
   */
  public function setEthicsPreset8($ethics_preset_8)
  {
    $this->ethics_preset_8 = $ethics_preset_8;
  }

  /**
   * @return string
   */
  public function getEthicsPreset9()
  {
    return $this->ethics_preset_9;
  }

  /**
   * @param string $ethics_preset_9
   */
  public function setEthicsPreset9($ethics_preset_9)
  {
    $this->ethics_preset_9 = $ethics_preset_9;
  }

  /**
   * @return string
   */
  public function getEthicsPreset10()
  {
    return $this->ethics_preset_10;
  }

  /**
   * @param string $ethics_preset_10
   */
  public function setEthicsPreset10($ethics_preset_10)
  {
    $this->ethics_preset_10 = $ethics_preset_10;
  }

  /**
   * @return string
   */
  public function getEthicsPreset11()
  {
    return $this->ethics_preset_11;
  }

  /**
   * @param string $ethics_preset_11
   */
  public function setEthicsPreset11($ethics_preset_11)
  {
    $this->ethics_preset_11 = $ethics_preset_11;
  }

  /**
   * @return string
   */
  public function getEthicsPreset12()
  {
    return $this->ethics_preset_12;
  }

  /**
   * @param string $ethics_preset_12
   */
  public function setEthicsPreset12($ethics_preset_12)
  {
    $this->ethics_preset_12 = $ethics_preset_12;
  }

  /**
   * @return string
   */
  public function getEthicsPreset13()
  {
    return $this->ethics_preset_13;
  }

  /**
   * @param string $ethics_preset_13
   */
  public function setEthicsPreset13($ethics_preset_13)
  {
    $this->ethics_preset_13 = $ethics_preset_13;
  }

  /**
   * @return string
   */
  public function getEthicsPreset14()
  {
    return $this->ethics_preset_14;
  }

  /**
   * @param string $ethics_preset_14
   */
  public function setEthicsPreset14($ethics_preset_14)
  {
    $this->ethics_preset_14 = $ethics_preset_14;
  }

  /**
   * @return string
   */
  public function getEthicsPreset15()
  {
    return $this->ethics_preset_15;
  }

  /**
   * @param string $ethics_preset_15
   */
  public function setEthicsPreset15($ethics_preset_15)
  {
    $this->ethics_preset_15 = $ethics_preset_15;
  }

  /**
   * @return string
   */
  public function getEthicsPreset16()
  {
    return $this->ethics_preset_16;
  }

  /**
   * @param string $ethics_preset_16
   */
  public function setEthicsPreset16($ethics_preset_16)
  {
    $this->ethics_preset_16 = $ethics_preset_16;
  }

  /**
   * @return string
   */
  public function getEthicsPreset17()
  {
    return $this->ethics_preset_17;
  }

  /**
   * @param string $ethics_preset_17
   */
  public function setEthicsPreset17($ethics_preset_17)
  {
    $this->ethics_preset_17 = $ethics_preset_17;
  }

  /**
   * @return string
   */
  public function getEthicsPreset18()
  {
    return $this->ethics_preset_18;
  }

  /**
   * @param string $ethics_preset_18
   */
  public function setEthicsPreset18($ethics_preset_18)
  {
    $this->ethics_preset_18 = $ethics_preset_18;
  }

  /**
   * @return string
   */
  public function getEthicsPreset19()
  {
    return $this->ethics_preset_19;
  }

  /**
   * @param string $ethics_preset_19
   */
  public function setEthicsPreset19($ethics_preset_19)
  {
    $this->ethics_preset_19 = $ethics_preset_19;
  }

  /**
   * @return string
   */
  public function getEthicsPreset20()
  {
    return $this->ethics_preset_20;
  }

  /**
   * @param string $ethics_preset_20
   */
  public function setEthicsPreset20($ethics_preset_20)
  {
    $this->ethics_preset_20 = $ethics_preset_20;
  }

  /**
   * @return string
   */
  public function getEthicsPreset21()
  {
    return $this->ethics_preset_21;
  }

  /**
   * @param string $ethics_preset_21
   */
  public function setEthicsPreset21($ethics_preset_21)
  {
    $this->ethics_preset_21 = $ethics_preset_21;
  }

  /**
   * @return string
   */
  public function getEthicsPreset22()
  {
    return $this->ethics_preset_22;
  }

  /**
   * @param string $ethics_preset_22
   */
  public function setEthicsPreset22($ethics_preset_22)
  {
    $this->ethics_preset_22 = $ethics_preset_22;
  }

  /**
   * @return string
   */
  public function getEthicsPreset23()
  {
    return $this->ethics_preset_23;
  }

  /**
   * @param string $ethics_preset_23
   */
  public function setEthicsPreset23($ethics_preset_23)
  {
    $this->ethics_preset_23 = $ethics_preset_23;
  }

  /**
   * @return string
   */
  public function getEthicsPreset24()
  {
    return $this->ethics_preset_24;
  }

  /**
   * @param string $ethics_preset_24
   */
  public function setEthicsPreset24($ethics_preset_24)
  {
    $this->ethics_preset_24 = $ethics_preset_24;
  }

  /**
   * @return string
   */
  public function getEthicsPreset25()
  {
    return $this->ethics_preset_25;
  }

  /**
   * @param string $ethics_preset_25
   */
  public function setEthicsPreset25($ethics_preset_25)
  {
    $this->ethics_preset_25 = $ethics_preset_25;
  }

  /**
   * @return string
   */
  public function getEthicsPreset26()
  {
    return $this->ethics_preset_26;
  }

  /**
   * @param string $ethics_preset_26
   */
  public function setEthicsPreset26($ethics_preset_26)
  {
    $this->ethics_preset_26 = $ethics_preset_26;
  }

  /**
   * @return string
   */
  public function getEthicsPreset27()
  {
    return $this->ethics_preset_27;
  }

  /**
   * @param string $ethics_preset_27
   */
  public function setEthicsPreset27($ethics_preset_27)
  {
    $this->ethics_preset_27 = $ethics_preset_27;
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Save the data of this CellLinePreset into the database.
   */
  public function save() {
    try {
      CellLinePresetRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }


}
