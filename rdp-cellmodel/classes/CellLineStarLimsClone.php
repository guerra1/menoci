<?php

/**
 * @file
 * Provide class for the imported CellLineStarLimsClones. Provide form fields
 * for each variable when building @see \rdp_cellmodel_cell_line_new_edit().
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineEditing
 */
class CellLineStarLimsClone
{

  // --------------------------- <<< VARIABLES >>> -----------------------------
  /**
   * @var int
   *   Database ID of the editing.
   */
  private $id;

  /**
   * @var int
   *   ID of the respective cell line.
   */
  private $cell_line;

  /**
   * @var string
   *   ID of the clone in StarLims.
   */
  private $clone_starlims_id;

  /**
   * @var string
   *   Lab ID of the clone in StarLims.
   */
  private $clone_starlims_lab_id;

  /**
   * @var int
   *   Number of tubes of clone in StarLims.
   */
  private $clone_starlims_tubes;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getCellLine()
  {
    return $this->cell_line;
  }

  /**
   * @param int $cell_line
   */
  public function setCellLine($cell_line)
  {
    $this->cell_line = $cell_line;
  }

  /**
   * @return string
   */
  public function getCloneStarlimsId()
  {
    return $this->clone_starlims_id;
  }

  /**
   * @param string $clone_starlims_id
   */
  public function setCloneStarlimsId($clone_starlims_id)
  {
    $this->clone_starlims_id = $clone_starlims_id;
  }

  /**
   * @return string
   */
  public function getCloneStarlimsLabId()
  {
    return $this->clone_starlims_lab_id;
  }

  /**
   * @param string $clone_starlims_lab_id
   */
  public function setCloneStarlimsLabId($clone_starlims_lab_id)
  {
    $this->clone_starlims_lab_id = $clone_starlims_lab_id;
  }

  /**
   * @return int
   */
  public function getCloneStarlimsTubes()
  {
    return $this->clone_starlims_tubes;
  }

  /**
   * @param int $clone_starlims_tubes
   */
  public function setCloneStarlimsTubes($clone_starlims_tubes)
  {
    $this->clone_starlims_tubes = $clone_starlims_tubes;
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellLineStarLimsClone into the database.
   */
  public function save() {
    try {
      CellLineStarLimsCloneRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}



