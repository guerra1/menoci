<?php

/**
 * @file
 * Provide database layer for @see \CellmodelLog.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellmodelLogRepository
 */
class CellmodelLogRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellmodelLog.
   */
  static $tableName = 'cellmodel_log';

  /**
   * @var array
   *   All database fields for CellmodelLog.
   */
  static $databaseFields = [
    'id',
    'cell_line',
    'date',
    'user',
    'action',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellmodelLog into the database.
   *
   * @param \CellmodelLog $log
   *   CellmodelLog object to be saved.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($log) {
    db_merge(self::$tableName)
      ->key(['id' => $log->getId()])
      ->fields([
        'cell_line' => $log->getCellLine(),
        'date' => $log->getDate(),
        'user' => $log->getUser(),
        'action' => $log->getAction(),
      ])
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellmodelLog object.
   *
   * @param $result
   *   Database result of a finder function.
   *
   * @return \CellmodelLog
   *   New CellmodelLog object.
   */
  public static function databaseResultsToLog($result) {
    $log = new CellmodelLog();

    if (empty($result)) {
      return $log;
    }

    // Set the variables.
    $log->setId($result->id);
    $log->setUser($result->user);
    $log->setCellLine($result->cell_line);
    $log->setDate($result->date);
    $log->setAction($result->action);

    return $log;
  }

  /**
   * Read database results and create an array with CellmodelLog objects.
   *
   * @param $results
   *   Database result of a finder function.
   *
   * @return \CellmodelLog[]
   *   New CellmodelLog objects.
   */
  public static function databaseResultsToLogs($results) {
    $logs = [];
    foreach ($results as $result) {
      $logs[] = self::databaseResultsToLog($result);
    }

    return $logs;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Return CellLineDonorRelative of given CellLine (Database ID).
   *
   * @param int $cell_line
   *   ID of the given CellLine.
   *
   * @return \CellmodelLog[]
   *  Found CellmodelLog object.
   */
  public static function findByCellLine($cell_line) {
    $result = db_select(self::$tableName, 'a')
      ->condition('cell_line', $cell_line, '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToLogs($result);
  }

  /**
   * Return all CellmodelLogs.
   *
   * @param int $limit
   *   Maximum number of logs per page.
   *
   * @return \CellmodelLog[]
   *   Found CellmodelLog objects.
   */
  public static function findAllUsePager($limit) {
    $result = db_select(self::$tableName, 'a')
      ->fields('a', self::$databaseFields)
      ->extend('PagerDefault')
      ->limit($limit)
      ->orderBy('date', 'DESC')
      ->execute();

    return self::databaseResultsToLogs($result);
  }
}
