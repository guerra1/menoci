<?php

/**
 * @file
 * Provide database layer for @see CellLineDonorRelative.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineDonorRelativeRepository
 */
class CellLineDonorRelativeRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLineDonorRelatives.
   */
  static $tableName = 'cellmodel_cell_line_relative';

  /**
   * @var array
   *   All database fields for CellLineDonorRelative.
   */
  static $databaseFields = [
    'id',
    'cell_line',
    'type',
    'starlims_id',
    'pseudonym',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLineDonorRelative into the database.
   *
   * @param $relative \CellLineDonorRelative
   *   CellLineDonorRelative object to be saved.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($relative) {
    db_merge(self::$tableName)
      ->key(['id' => $relative->getId()])
      ->fields([
        'cell_line' => $relative->getCellLine(),
        'type' => $relative->getType(),
        'starlims_id' => $relative->getStarlimsId(),
        'pseudonym' => $relative->getPseudonym(),
      ])
      ->execute();
  }

  /**
   * Remove a CellLineDonorRelative entry from the database.
   *
   * @param $relative_id
   *   ID of the respective CellLineDonorRelative object.
   */
  public static function delete($relative_id) {
    db_delete(self::$tableName)
      ->condition('id', $relative_id, '=')
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLineDonorRelative object.
   *
   * @param $result
   *   Database result of a finder function.
   *
   * @return \CellLineDonorRelative
   *   New CellLineDonorRelative object.
   */
  public static function databaseResultsToRelative($result) {
    $relative = new CellLineDonorRelative();

    if (empty($result)) {
      return $relative;
    }

    // Set the variables.
    $relative->setId($result->id);
    $relative->setCellLine($result->cell_line);
    $relative->setType($result->type);
    $relative->setStarlimsId($result->starlims_id);
    $relative->setPseudonym($result->pseudonym);

    return $relative;
  }

  /**
   * Read database results and create an array with CellLineDonorRelative
   * objects.
   *
   * @param $results
   *  Database results of a finder function.
   *
   * @return \CellLineDonorRelative[]
   *  New CellLineDonorRelative objects.
   */
  public static function databaseResultsToRelatives($results) {
    $relatives = [];
    foreach ($results as $result) {
      $relatives[] = self::databaseResultsToRelative($result);
    }
    return $relatives;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Return CellLineDonorRelative of given CellLine (Database ID).
   *
   * @param int $cell_line
   *   ID of the given CellLine.
   *
   * @return \CellLineDonorRelative[]
   *  Found CellLineDonorRelative object.
   */
  public static function findByCellLineId($cell_line) {
    $result = db_select(self::$tableName, 'a')
      ->condition('cell_line', $cell_line, '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToRelatives($result);
  }
}
