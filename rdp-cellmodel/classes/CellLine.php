<?php

/**
 * @file
 * Provide class for the CellLine objects. Provide form fields
 * for each variable when building @see \rdp_cellmodel_cell_line_new_edit().
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLine
 */
class CellLine {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   Database ID.
   */
  private $id;

  /**
   * @var string
   *   Date at which the CellLine was created.
   */
  private $created_date;

  /**
   * @var int
   *   ID of User who created the CellLine.
   */
  private $created_by;

  /**
   * @var string
   *   Date at which the CellLine was last edited.
   */
  private $last_modified_date;

  /**
   * @var int
   *   ID of the user who last edited the CellLine.
   */
  private $last_modified_by;

  /**
   * @var string
   *   Type regarding in which main table tab the cell line belongs
   *   (e.g. patient-derived cell lines, genetically modified cell lines).
   */
  private $tab_type;

  /**
   * @var string
   *   Laboratory ID.
   */
  private $lab_id;

  /**
   * @var string
   *   Alternative ID/Name.
   */
  private $alt_id;

  /**
   * @var string
   *   Provider institution.
   */
  private $provider;

  /**
   * @var string
   *   Physical location.
   */
  private $location;

  /**
   * @var string
   *   Associated project.
   */
  private $project;

  /**
   * @var string
   *   Type of the stem cells (e.g. iPSC).
   */
  private $cell_type;

  /**
   * @var string
   *   Grade label (e.g. GMP).
   */
  private $grade;

  /**
   * @var string
   *   IPSC ID.
   */
  private $ipsc_id;

  /**
   * @var string
   *   Does an other CellLine from the same donor exist?
   */
  private $same_donor;

  /**
   * @var int
   *   The ID of the CellLine of the same donor.
   */
  private $underlying_cell_line;

  /**
   * @var string
   *   Is the CellLine readily obtainable for third parties?
   */
  private $obtainable;

  /**
   * @var string
   *   Consent for specific areas/projects.
   */
  private $restric_areas;

  /**
   * @var string
   *   Consent for research use?
   */
  private $restric_research;

  /**
   * @var string
   *   Consent for clinical use?
   */
  private $restric_clinical;

  /**
   * @var string
   *   Consent for commercial use?
   */
  private $restric_commercial;

  /**
   * @var string
   *   Additional restrictions?
   */
  private $restric_additional;

  /**
   * @var string
   *   From hPSCreg generated standardised name.
   */
  private $hpscreg_name;

  /**
   * @var \CellLineDiagnosis[]
   *   The diagnosed diseases (primary or secondary) of the donor of the
   *   CellLine. These are getting saved into a separate database table.
   * @see \CellLineDiagnosisRepository
   */
  private $diagnoses = [];

  /**
   * @var string
   *   Is there a genetic predisposition of the donor known?
   */
  private $genetic_predisposition;

  /**
   * @var string
   *   Specification of the genetic predisposition.
   */
  private $genetic_predisposition_spec;

  /**
   * @var string
   *   Visibility of the genetic predisposition.
   */
  private $visibility_predisposition_spec;

  /**
   * @var string
   *   Gender of the donor.
   */
  private $donor_gender;

  /**
   * @var string
   *   Race/ethnicity of the donor (e.g. Caucasian).
   */
  private $donor_race;

  /**
   * @var int
   *   Age of the donor at date of sampling in years (max. 130).
   */
  private $donor_age;

  /**
   * @var string
   *   SecuTrial ID of the donor.
   */
  private $donor_secutrial_id;

  /**
   * @var string
   *   Pseudonym of the donor.
   */
  private $donor_pseudonym;

  /**
   * @var string
   *   STARLIMS ID of the donor.
   */
  private $donor_starlims_id;

  /**
   * @var \CellLineDonorRelative[]
   *   Relatives of the donor. These are getting saved into a separate database
   *   table.
   * @see \CellLineDonorRelativeRepository
   */
  private $donor_relatives = [];

  /**
   * @var string
   *   Type of the source material (e.g. Hair).
   */
  private $source_type;

  /**
   * @var string
   *   Sample clinic from which the sample originated.
   */
  private $sample_origin;

  /**
   * @var string
   *   Sampling date of the source material.
   */
  private $sampling_date;

  /**
   * @var string
   *   Is the primary culture available?
   */
  private $primary_culture;

  /**
   * @var string
   *   Method used for reprogramming.
   */
  private $reprogramming_method;

  /**
   * @var string
   *   Vector used for reprogramming.
   */
  private $reprogramming_vector;

  /**
   * @var \CellLineReprogrammingDate[]
   *   Dates of reprogramming. These are getting saved into a separate database
   *   table.
   * @see \CellLineReprogrammingDateRepository
   */
  private $reprogramming_dates = [];

  /**
   * @var string
   *   Result of the HIV testing.
   */
  private $hiv_result;

  /**
   * @var string
   *   Used test material for HIV  testing.
   */
  private $hiv_material;

  /**
   * @var string
   *   Date of the HIV testing.
   */
  private $hiv_date;

  /**
   * @var string
   *   Result of the Hepatitis B testing.
   */
  private $hep_b;

  /**
   * @var string
   *   Used test material for Hepatitis B  testing.
   */
  private $hep_b_material;

  /**
   * @var string
   *   Date of the Hepatitis B testing.
   */
  private $hep_b_date;

  /**
   * @var string
   *   Result of the Hepatitis C testing.
   */
  private $hep_c;

  /**
   * @var string
   *   Used test material for Hepatitis C testing.
   */
  private $hep_c_material;

  /**
   * @var string
   *   Date of the Hepatitis C testing.
   */
  private $hep_c_date;

  /**
   * @var string
   *   The result of HLA-A typing.
   */
  private $hla_a;

  /**
   * @var string
   *   The result of HLA-B typing.
   */
  private $hla_b;

  /**
   * @var string
   *   The result of HLA-DRB1 typing.
   */
  private $hla_drb1;

  /**
   * @var string
   *   Has consent been obtained from the donor of the tissue from which iPS
   *   cells have been derived?.
   */
  private $ethics1;

  /**
   * @var string
   *   Was the consent voluntarily given by the donor, custodian or parents?.
   */
  private $ethics2;

  /**
   * @var string
   *   Has the donor been informed that participation will not directly
   *   influence their personal treatment?
   */
  private $ethics3;

  /**
   * @var string
   *   Can you provide a copy of the Donor Information Sheet provided to the
   *   donor?
   */
  private $ethics4;

  /**
   * @var string
   *   Ethics 4 contact information.
   */
  private $ethics4_contact;

  /**
   * @var string
   *   Do you hold a copy of the signed Donor Consent Form?
   */
  private $ethics5;

  /**
   * @var string
   *   Ethics 5 contact information.
   */
  private $ethics5_contact;

  /**
   * @var string
   *   Please indicate whether the donated material has been pseudonymised or
   *   anonymised.
   */
  private $ethics6;

  /**
   * @var string
   *   Does consent expressly prevent derivation of iPS cells?
   */
  private $ethics7;

  /**
   * @var string
   *   Does consent prevent cells derived from the donated biosample from being
   *   made available to researchers anywhere in the world?
   */
  private $ethics8;

  /**
   * @var string
   *   How may genetic information associated with the cell line be accessed?
   */
  private $ethics9;

  /**
   * @var string
   *   Will the donor expect to receive financial benefit, beyond reasonable
   *   expenses, in return for donating the biosample?
   */
  private $ethics10;

  /**
   * @var string
   *   Has a favourable opinion been obtained from a research ethics committee,
   *   or other ethics review panel, in relation to the Research Protocol
   *   including the consent provisions?
   */
  private $ethics11;

  /**
   * @var string
   *   Name of the accrediting authority involved (Ethics 11).
   */
  private $ethics11_spec_name;

  /**
   * @var string
   *   Approval Number (Ethics 11).
   */
  private $ethics11_spec_number;

  /**
   * @var string
   *   Has a favourable opinion been obtained from a research ethics committee,
   *   or other ethics review panel, in relation to the proposed project,
   *   involving use of donated material or derived cells?
   */
  private $ethics12;

  /**
   * @var string
   *   Name of the accrediting authority involved (Ethics 12).
   */
  private $ethics12_spec_name;

  /**
   * @var string
   *   Approval Number (Ethics 12).
   */
  private $ethics12_spec_number;

  /**
   * @var string
   *   Has consent been obtained for the storage of the biopsied tissue and relevant patient data?
   */
  private $ethics13;

  /**
   * @var string
   *   Has consent been obtained for involved physicians to have access to the biopsied tissue and relevant patient data?
   */
  private $ethics14;

  /**
   * @var string
   *   Has consent been obtained for the storage of the biopsied tissue and derived iPS cells in the UMG Biobank for further use in scientific research?
   */
  private $ethics15;

  /**
   * @var string
   *   Has consent been obtained for the donated tissue to be passed into the ownership of the UMG?
   */
  private $ethics16;

  /**
   * @var string
   *   Has consent been obtained for the distribution of the biopsied tissue and products derived thereof to scientific cooperation partners?
   */
  private $ethics17;

  /**
   * @var string
   *   Has consent been obtained for the potential commercial use of the biopsied tissue and products derived thereof?
   */
  private $ethics18;

  /**
   * @var string
   *   Has consent been obtained for the pseudonymized storage and use of the biopsied tissue, products derived thereof, as well as the corresponding patient data for 15 years?
   */
  private $ethics19;

  /**
   * @var string
   *   Has consent been obtained for the collection of a blood sample to test for viral infections?
   */
  private $ethics20;

  /**
   * @var string
   *   Has consent been obtained for genetic and epigenetic testing?
   */
  private $ethics21;

  /**
   * @var string
   *   Does the tissue donor wish to be informed about potential medically relevant genetic findings?
   */
  private $ethics22;

  /**
   * @var string
   *   Has the donor agreed to the disposal of the primary tissue after 15 years?
   */
  private $ethics23;

  /**
   * @var string
   *   Has the donor agreed to the anonymization of products and data derived from the donated tissue?
   */
  private $ethics24;

  /**
   * @var string
   *   Has the donor been informed of their right to terminate their participation in this trial?
   */
  private $ethics25;

  /**
   * @var string
   *   Has the donor been informed that a destruction of the cell products and data obtained from their donation is not possible retrospectively?
   */
  private $ethics26;

  /**
   * @var string
   *   Has consent been obtained to contact relatives of the donor?
   */
  private $ethics27;

  /**
   * @var string
   *   Contact person E-Mail address.
   */
  private $contact_e_mail;

  /**
   * @var string
   *   Correspondent in the biobank for distribution.
   */
  private $correspondence;

  /**
   * @var string
   *   Correspondent for cost coverage.
   */
  private $cost_coverage;

  /**
   * @return string
   */
  public function getCostCoverage()
  {
    return $this->cost_coverage;
  }

  /**
   * @param string $cost_coverage
   */
  public function setCostCoverage($cost_coverage)
  {
    $this->cost_coverage = $cost_coverage;
  }

  /**
   * @var string
   *   Context of distribution.
   */
  private $distribution;

  /**
   * @var string
   *   Cell Lines in UMG Biobank.
   */
  private $lines_in_biobank;

  /**
   * @var string
   *   Biosafety Level.
   */
  private $biosafety;

  /**
   * @var string
   *   Comments of the creator/editor.
   */
  private $comments;

  /**
   * @var string
   *   Visibility of the cell line.
   */
  private $visibility;

  /**
   * @var \CellLineClone[]
   *   Clones of the cell line. These are getting saved into a separate database
   *   table.
   */
  private $clones;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------
  // ----------------------- <<< FOR OTHER OBJECTS >>> -------------------------

  /**
   * @return \CellLineDiagnosis[]
   */
  public function getDiagnoses() {
    return $this->diagnoses;
  }

  /**
   * @param \CellLineDiagnosis[] $diagnoses
   */
  public function setDiagnoses($diagnoses) {
    $this->diagnoses = $diagnoses;
  }

  /**
   * @return \CellLineDonorRelative[]
   */
  public function getDonorRelatives() {
    return $this->donor_relatives;
  }

  /**
   * @param \CellLineDonorRelative[] $donor_relatives
   */
  public function setDonorRelatives($donor_relatives) {
    $this->donor_relatives = $donor_relatives;
  }

  /**
   * @return \CellLineReprogrammingDate[]
   */
  public function getReprogrammingDates() {
    return $this->reprogramming_dates;
  }

  /**
   * @param \CellLineReprogrammingDate[] $reprogramming_dates
   */
  public function setReprogrammingDates($reprogramming_dates) {
    $this->reprogramming_dates = $reprogramming_dates;
  }

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------
  // -------------------- <<< FOR VARIABLES OF CELLLINE>>> ---------------------

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getCreatedDate() {
    return $this->created_date;
  }

  /**
   * @param string $created_date
   */
  public function setCreatedDate($created_date) {
    $this->created_date = $created_date;
  }

  /**
   * @return int
   */
  public function getCreatedBy() {
    return $this->created_by;
  }

  /**
   * @param int $created_by
   */
  public function setCreatedBy($created_by) {
    $this->created_by = $created_by;
  }

  /**
   * @return string
   */
  public function getLastModifiedDate() {
    return $this->last_modified_date;
  }

  /**
   * @param string $last_modified_date
   */
  public function setLastModifiedDate($last_modified_date) {
    $this->last_modified_date = $last_modified_date;
  }

  /**
   * @return int
   */
  public function getLastModifiedBy() {
    return $this->last_modified_by;
  }

  /**
   * @param int $last_modified_by
   */
  public function setLastModifiedBy($last_modified_by) {
    $this->last_modified_by = $last_modified_by;
  }

  /**
   * @return string
   */
  public function getTabType() {
    return $this->tab_type;
  }

  /**
   * @param string $tab_type
   */
  public function setTabType($tab_type) {
    $this->tab_type = $tab_type;
  }

  /**
   * @return string
   */
  public function getLabId() {
    return permissionCheck('lab_id') ? $this->lab_id : '';
  }

  /**
   * @param string $lab_id
   */
  public function setLabId($lab_id) {
    $this->lab_id = $lab_id;
  }

  /**
   * @return string
   */
  public function getAltId() {
    return $this->alt_id;
  }

  /**
   * @param string $alt_id
   */
  public function setAltId($alt_id) {
    $this->alt_id = $alt_id;
  }

  /**
   * @return string
   */
  public function getProvider() {
    return $this->provider;
  }

  /**
   * @param string $provider
   */
  public function setProvider($provider) {
    $this->provider = $provider;
  }

  /**
   * @return string
   */
  public function getLocation() {
    return $this->location;
  }

  /**
   * @param string $location
   */
  public function setLocation($location) {
    $this->location = $location;
  }

  /**
   * @return string
   */
  public function getProject() {
    return $this->project;
  }

  /**
   * @param string $project
   */
  public function setProject($project) {
    $this->project = $project;
  }

  /**
   * @return string
   */
  public function getCellType() {
    return permissionCheck('cell_type') ? $this->cell_type : '';
  }

  /**
   * @param string $cell_type
   */
  public function setCellType($cell_type) {
    $this->cell_type = $cell_type;
  }

  /**
   * @return string
   */
  public function getGrade() {
    return permissionCheck('grade') ? $this->grade : '';
  }

  /**
   * @param string $grade
   */
  public function setGrade($grade) {
    $this->grade = $grade;
  }

  /**
   * @return string
   */
  public function getIpscId() {
    return permissionCheck('ipsc_id') ? $this->ipsc_id : '';
  }

  /**
   * @param string $ipsc_id
   */
  public function setIpscId($ipsc_id) {
    $this->ipsc_id = $ipsc_id;
  }

  /**
   * @return string
   */
  public function getSameDonor() {
    return $this->same_donor;
  }

  /**
   * @param string $same_donor
   */
  public function setSameDonor($same_donor) {
    $this->same_donor = $same_donor;
  }

  /**
   * @return int
   */
  public function getUnderlyingCellLine() {
    return $this->underlying_cell_line;
  }

  /**
   * @param int $underlying_cell_line
   */
  public function setUnderlyingCellLine($underlying_cell_line) {
    $this->underlying_cell_line = $underlying_cell_line;
  }

  /**
   * @return string
   */
  public function getObtainable() {
    return $this->obtainable;
  }

  /**
   * @param string $obtainable
   */
  public function setObtainable($obtainable) {
    $this->obtainable = $obtainable;
  }

  /**
   * @return string
   */
  public function getRestricAreas() {
    return $this->restric_areas;
  }

  /**
   * @param string $restric_areas
   */
  public function setRestricAreas($restric_areas) {
    $this->restric_areas = $restric_areas;
  }

  /**
   * @return string
   */
  public function getRestricResearch() {
    return $this->restric_research;
  }

  /**
   * @param string $restric_research
   */
  public function setRestricResearch($restric_research) {
    $this->restric_research = $restric_research;
  }

  /**
   * @return string
   */
  public function getRestricClinical() {
    return $this->restric_clinical;
  }

  /**
   * @param string $restric_clinical
   */
  public function setRestricClinical($restric_clinical) {
    $this->restric_clinical = $restric_clinical;
  }

  /**
   * @return string
   */
  public function getRestricCommercial() {
    return $this->restric_commercial;
  }

  /**
   * @param string $restric_commercial
   */
  public function setRestricCommercial($restric_commercial) {
    $this->restric_commercial = $restric_commercial;
  }

  /**
   * @return string
   */
  public function getRestricAdditional() {
    return $this->restric_additional;
  }

  /**
   * @param string $restric_additional
   */
  public function setRestricAdditional($restric_additional) {
    $this->restric_additional = $restric_additional;
  }

  /**
   * @return string
   */
  public function getHpscregName() {
    return permissionCheck('hpscreg_name') ? $this->hpscreg_name : '';
  }

  /**
   * @param string $hpscreg_name
   */
  public function setHpscregName($hpscreg_name) {
    $this->hpscreg_name = $hpscreg_name;
  }

  /**
   * @return string
   */
  public function getGeneticPredisposition() {
    return $this->genetic_predisposition;
  }

  /**
   * @param string $genetic_predisposition
   */
  public function setGeneticPredisposition($genetic_predisposition) {
    $this->genetic_predisposition = $genetic_predisposition;
  }

  /**
   * @return string
   */
  public function getVisibilityPredispositionSpec() {
    return $this->visibility_predisposition_spec;
  }

  /**
   * @param string $visibility_predisposition_spec
   */
  public function setVisibilityPredispositionSpec($visibility_predisposition_spec) {
    $this->visibility_predisposition_spec = $visibility_predisposition_spec;
  }

  /**
   * @return string
   */
  public function getGeneticPredispositionSpec() {
    if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE) or
      user_access(RDP_CELLMODEL_PERMISSION_VIEW) or
      $this->getVisibilityPredispositionSpec() == 'Public') {
      return $this->genetic_predisposition_spec;
    }
    else {
      if (empty($this->genetic_predisposition_spec)) {
        return '';
      }
      else {
        return '<i>unreleased</i>';
      }
    }
  }

  /**
   * @param string $genetic_predisposition_spec
   */
  public function setGeneticPredispositionSpec($genetic_predisposition_spec) {
    $this->genetic_predisposition_spec = $genetic_predisposition_spec;
  }

  /**
   * @return string
   */
  public function getDonorGender() {
    return $this->donor_gender;
  }

  /**
   * @param string $donor_gender
   */
  public function setDonorGender($donor_gender) {
    $this->donor_gender = $donor_gender;
  }

  /**
   * @return string
   */
  public function getDonorRace() {
    return $this->donor_race;
  }

  /**
   * @param string $donor_race
   */
  public function setDonorRace($donor_race) {
    $this->donor_race = $donor_race;
  }

  /**
   * @return int
   */
  public function getDonorAge() {
    return $this->donor_age;
  }

  /**
   * @param int $donor_age
   */
  public function setDonorAge($donor_age) {
    $this->donor_age = $donor_age;
  }

  /**
   * @return string
   */
  public function getDonorSecutrialId() {
    return $this->donor_secutrial_id;
  }

  /**
   * @param string $donor_secutrial_id
   */
  public function setDonorSecutrialId($donor_secutrial_id) {
    $this->donor_secutrial_id = $donor_secutrial_id;
  }

  /**
   * @return string
   */
  public function getDonorPseudonym() {
    return $this->donor_pseudonym;
  }

  /**
   * @param string $donor_pseudonym
   */
  public function setDonorPseudonym($donor_pseudonym) {
    $this->donor_pseudonym = $donor_pseudonym;
  }

  /**
   * @return string
   */
  public function getDonorStarlimsId() {
    return $this->donor_starlims_id;
  }

  /**
   * @param string $donor_starlims_id
   */
  public function setDonorStarlimsId($donor_starlims_id) {
    $this->donor_starlims_id = $donor_starlims_id;
  }

  /**
   * @return string
   */
  public function getSourceType() {
    return permissionCheck('source_type') ? $this->source_type : '';
  }

  /**
   * @param string $source_type
   */
  public function setSourceType($source_type) {
    $this->source_type = $source_type;
  }

  /**
   * @return string
   */
  public function getSampleOrigin() {
    return $this->sample_origin;
  }

  /**
   * @param string $sample_origin
   */
  public function setSampleOrigin($sample_origin) {
    $this->sample_origin = $sample_origin;
  }

  /**
   * @return string
   */
  public function getSamplingDate() {
    return $this->sampling_date;
  }

  /**
   * @param string $sampling_date
   */
  public function setSamplingDate($sampling_date) {
    $this->sampling_date = $sampling_date;
  }

  /**
   * @return string
   */
  public function getPrimaryCulture() {
    return $this->primary_culture;
  }

  /**
   * @param string $primary_culture
   */
  public function setPrimaryCulture($primary_culture) {
    $this->primary_culture = $primary_culture;
  }

  /**
   * @return string
   */
  public function getReprogrammingMethod() {
    return permissionCheck('reprogramming_method') ? $this->reprogramming_method : '';
  }

  /**
   * @param string $reprogramming_method
   */
  public function setReprogrammingMethod($reprogramming_method) {
    $this->reprogramming_method = $reprogramming_method;
  }

  /**
   * @return string
   */
  public function getReprogrammingVector() {
    return $this->reprogramming_vector;
  }

  /**
   * @param string $reprogramming_vector
   */
  public function setReprogrammingVector($reprogramming_vector) {
    $this->reprogramming_vector = $reprogramming_vector;
  }

  /**
   * @return string
   */
  public function getHivResult() {
    return $this->hiv_result;
  }

  /**
   * @param string $hiv_result
   */
  public function setHivResult($hiv_result) {
    $this->hiv_result = $hiv_result;
  }

  /**
   * @return string
   */
  public function getHivMaterial() {
    return $this->hiv_material;
  }

  /**
   * @param string $hiv_material
   */
  public function setHivMaterial($hiv_material) {
    $this->hiv_material = $hiv_material;
  }

  /**
   * @return string
   */
  public function getHivDate() {
    return $this->hiv_date;
  }

  /**
   * @param string $hiv_date
   */
  public function setHivDate($hiv_date) {
    $this->hiv_date = $hiv_date;
  }

  /**
   * @return string
   */
  public function getHepB() {
    return $this->hep_b;
  }

  /**
   * @param string $hep_b
   */
  public function setHepB($hep_b) {
    $this->hep_b = $hep_b;
  }

  /**
   * @return string
   */
  public function getHepBMaterial() {
    return $this->hep_b_material;
  }

  /**
   * @param string $hep_b_material
   */
  public function setHepBMaterial($hep_b_material) {
    $this->hep_b_material = $hep_b_material;
  }

  /**
   * @return string
   */
  public function getHepBDate() {
    return $this->hep_b_date;
  }

  /**
   * @param string $hep_b_date
   */
  public function setHepBDate($hep_b_date) {
    $this->hep_b_date = $hep_b_date;
  }

  /**
   * @return string
   */
  public function getHepC() {
    return $this->hep_c;
  }

  /**
   * @param string $hep_c
   */
  public function setHepC($hep_c) {
    $this->hep_c = $hep_c;
  }

  /**
   * @return string
   */
  public function getHepCMaterial() {
    return $this->hep_c_material;
  }

  /**
   * @param string $hep_c_material
   */
  public function setHepCMaterial($hep_c_material) {
    $this->hep_c_material = $hep_c_material;
  }

  /**
   * @return string
   */
  public function getHepCDate() {
    return $this->hep_c_date;
  }

  /**
   * @param string $hep_c_date
   */
  public function setHepCDate($hep_c_date) {
    $this->hep_c_date = $hep_c_date;
  }

  /**
   * @return string
   */
  public function getHlaA() {
    return $this->hla_a;
  }

  /**
   * @param string $hla_a
   */
  public function setHlaA($hla_a) {
    $this->hla_a = $hla_a;
  }

  /**
   * @return string
   */
  public function getHlaB() {
    return $this->hla_b;
  }

  /**
   * @param string $hla_b
   */
  public function setHlaB($hla_b) {
    $this->hla_b = $hla_b;
  }

  /**
   * @return string
   */
  public function getHlaDrb1() {
    return $this->hla_drb1;
  }

  /**
   * @param string $hla_drb1
   */
  public function setHlaDrb1($hla_drb1) {
    $this->hla_drb1 = $hla_drb1;
  }

  /**
   * @return string
   */
  public function getEthics1() {
    return $this->ethics1;
  }

  /**
   * @param string $ethics1
   */
  public function setEthics1($ethics1) {
    $this->ethics1 = $ethics1;
  }

  /**
   * @return string
   */
  public function getEthics2() {
    return $this->ethics2;
  }

  /**
   * @param string $ethics2
   */
  public function setEthics2($ethics2) {
    $this->ethics2 = $ethics2;
  }

  /**
   * @return string
   */
  public function getEthics3() {
    return $this->ethics3;
  }

  /**
   * @param string $ethics3
   */
  public function setEthics3($ethics3) {
    $this->ethics3 = $ethics3;
  }

  /**
   * @return string
   */
  public function getEthics4() {
    return $this->ethics4;
  }

  /**
   * @param string $ethics4
   */
  public function setEthics4($ethics4) {
    $this->ethics4 = $ethics4;
  }

  /**
   * @return string
   */
  public function getEthics4Contact() {
    return $this->ethics4_contact;
  }

  /**
   * @param string $ethics4_contact
   */
  public function setEthics4Contact($ethics4_contact) {
    $this->ethics4_contact = $ethics4_contact;
  }

  /**
   * @return string
   */
  public function getEthics5() {
    return $this->ethics5;
  }

  /**
   * @param string $ethics5
   */
  public function setEthics5($ethics5) {
    $this->ethics5 = $ethics5;
  }

  /**
   * @return string
   */
  public function getEthics5Contact() {
    return $this->ethics5_contact;
  }

  /**
   * @param string $ethics5_contact
   */
  public function setEthics5Contact($ethics5_contact) {
    $this->ethics5_contact = $ethics5_contact;
  }

  /**
   * @return string
   */
  public function getEthics6() {
    return $this->ethics6;
  }

  /**
   * @param string $ethics6
   */
  public function setEthics6($ethics6) {
    $this->ethics6 = $ethics6;
  }

  /**
   * @return string
   */
  public function getEthics7() {
    return $this->ethics7;
  }

  /**
   * @param string $ethics7
   */
  public function setEthics7($ethics7) {
    $this->ethics7 = $ethics7;
  }

  /**
   * @return string
   */
  public function getEthics8() {
    return $this->ethics8;
  }

  /**
   * @param string $ethics8
   */
  public function setEthics8($ethics8) {
    $this->ethics8 = $ethics8;
  }

  /**
   * @return string
   */
  public function getEthics9() {
    return $this->ethics9;
  }

  /**
   * @param string $ethics9
   */
  public function setEthics9($ethics9) {
    $this->ethics9 = $ethics9;
  }

  /**
   * @return string
   */
  public function getEthics10() {
    return $this->ethics10;
  }

  /**
   * @param string $ethics10
   */
  public function setEthics10($ethics10) {
    $this->ethics10 = $ethics10;
  }

  /**
   * @return string
   */
  public function getEthics11() {
    return $this->ethics11;
  }

  /**
   * @param string $ethics11
   */
  public function setEthics11($ethics11) {
    $this->ethics11 = $ethics11;
  }

  /**
   * @return string
   */
  public function getEthics11SpecName() {
    return $this->ethics11_spec_name;
  }

  /**
   * @param string $ethics11_spec_name
   */
  public function setEthics11SpecName($ethics11_spec_name) {
    $this->ethics11_spec_name = $ethics11_spec_name;
  }

  /**
   * @return string
   */
  public function getEthics11SpecNumber() {
    return $this->ethics11_spec_number;
  }

  /**
   * @param string $ethics11_spec_number
   */
  public function setEthics11SpecNumber($ethics11_spec_number) {
    $this->ethics11_spec_number = $ethics11_spec_number;
  }

  /**
   * @return string
   */
  public function getEthics12() {
    return $this->ethics12;
  }

  /**
   * @param string $ethics12
   */
  public function setEthics12($ethics12) {
    $this->ethics12 = $ethics12;
  }

  /**
   * @return string
   */
  public function getEthics12SpecName() {
    return $this->ethics12_spec_name;
  }

  /**
   * @param string $ethics12_spec_name
   */
  public function setEthics12SpecName($ethics12_spec_name) {
    $this->ethics12_spec_name = $ethics12_spec_name;
  }

  /**
   * @return string
   */
  public function getEthics12SpecNumber() {
    return $this->ethics12_spec_number;
  }

  /**
   * @param string $ethics12_spec_number
   */
  public function setEthics12SpecNumber($ethics12_spec_number) {
    $this->ethics12_spec_number = $ethics12_spec_number;
  }

  /**
   * @return string
   */
  public function getEthics13() {
    return $this->ethics13;
  }

  /**
   * @param string $ethics13
   */
  public function setEthics13($ethics13) {
    $this->ethics13 = $ethics13;
  }

  /**
   * @return string
   */
  public function getEthics14()
  {
    return $this->ethics14;
  }

  /**
   * @param string $ethics14
   */
  public function setEthics14($ethics14)
  {
    $this->ethics14 = $ethics14;
  }

  /**
   * @return string
   */
  public function getEthics15()
  {
    return $this->ethics15;
  }

  /**
   * @param string $ethics15
   */
  public function setEthics15($ethics15)
  {
    $this->ethics15 = $ethics15;
  }

  /**
   * @return string
   */
  public function getEthics16()
  {
    return $this->ethics16;
  }

  /**
   * @param string $ethics16
   */
  public function setEthics16($ethics16)
  {
    $this->ethics16 = $ethics16;
  }

  /**
   * @return string
   */
  public function getEthics17()
  {
    return $this->ethics17;
  }

  /**
   * @param string $ethics17
   */
  public function setEthics17($ethics17)
  {
    $this->ethics17 = $ethics17;
  }

  /**
   * @return string
   */
  public function getEthics18()
  {
    return $this->ethics18;
  }

  /**
   * @param string $ethics18
   */
  public function setEthics18($ethics18)
  {
    $this->ethics18 = $ethics18;
  }

  /**
   * @return string
   */
  public function getEthics19()
  {
    return $this->ethics19;
  }

  /**
   * @param string $ethics19
   */
  public function setEthics19($ethics19)
  {
    $this->ethics19 = $ethics19;
  }

  /**
   * @return string
   */
  public function getEthics20()
  {
    return $this->ethics20;
  }

  /**
   * @param string $ethics20
   */
  public function setEthics20($ethics20)
  {
    $this->ethics20 = $ethics20;
  }

  /**
   * @return string
   */
  public function getEthics21()
  {
    return $this->ethics21;
  }

  /**
   * @param string $ethics21
   */
  public function setEthics21($ethics21)
  {
    $this->ethics21 = $ethics21;
  }

  /**
   * @return string
   */
  public function getEthics22()
  {
    return $this->ethics22;
  }

  /**
   * @param string $ethics22
   */
  public function setEthics22($ethics22)
  {
    $this->ethics22 = $ethics22;
  }

  /**
   * @return string
   */
  public function getEthics23()
  {
    return $this->ethics23;
  }

  /**
   * @param string $ethics23
   */
  public function setEthics23($ethics23)
  {
    $this->ethics23 = $ethics23;
  }

  /**
   * @return string
   */
  public function getEthics24()
  {
    return $this->ethics24;
  }

  /**
   * @param string $ethics24
   */
  public function setEthics24($ethics24)
  {
    $this->ethics24 = $ethics24;
  }

  /**
   * @return string
   */
  public function getEthics25()
  {
    return $this->ethics25;
  }

  /**
   * @param string $ethics25
   */
  public function setEthics25($ethics25)
  {
    $this->ethics25 = $ethics25;
  }

  /**
   * @return string
   */
  public function getEthics26()
  {
    return $this->ethics26;
  }

  /**
   * @param string $ethics26
   */
  public function setEthics26($ethics26)
  {
    $this->ethics26 = $ethics26;
  }

  /**
   * @return string
   */
  public function getEthics27()
  {
    return $this->ethics27;
  }

  /**
   * @param string $ethics27
   */
  public function setEthics27($ethics27)
  {
    $this->ethics27 = $ethics27;
  }

  /**
   * @return string
   */
  public function getContactEMail() {
    return $this->contact_e_mail;
  }

  /**
   * @param string $contact_e_mail
   */
  public function setContactEMail($contact_e_mail) {
    $this->contact_e_mail = $contact_e_mail;
  }

  /**
   * @return string
   */
  public function getCorrespondence() {
    return $this->correspondence;
  }

  /**
   * @param string $correspondence
   */
  public function setCorrespondence($correspondence) {
    $this->correspondence = $correspondence;
  }

  /**
   * @return string
   */
  public function getDistribution() {
    return $this->distribution;
  }

  /**
   * @param string $distribution
   */
  public function setDistribution($distribution) {
    $this->distribution = $distribution;
  }

  /**
   * @return string
   */
  public function getLinesInBiobank() {
    return $this->lines_in_biobank;
  }

  /**
   * @param string $lines_in_biobank
   */
  public function setLinesInBiobank($lines_in_biobank) {
    $this->lines_in_biobank = $lines_in_biobank;
  }

  /**
   * @return string
   */
  public function getComments() {
    return $this->comments;
  }

  /**
   * @param string $comments
   */
  public function setComments($comments) {
    $this->comments = $comments;
  }

  /**
   * @return string
   */
  public function getVisibility() {
    return $this->visibility;
  }

  /**
   * @param string $visibility
   */
  public function setVisibility($visibility) {
    $this->visibility = $visibility;
  }

  /**
   * @return string
   *   All DOIs of all linked publications concatenated.
   */
  public function getDOIs() {
    $rdp_linking = RDPLinking::getLinkingByName('sfb_literature',
      'pubreg_articles');
    $publication_ids = $rdp_linking->getLinkings('cellmodel_cell_line', $this->getId());

    $dois = '';
    foreach ($publication_ids as $publication_id) {
      $pub = PublicationRepository::findById($publication_id);
      $dois .= $pub->getDOI() . ', ';
    }
    $dois = substr($dois, 0, -2);
    if (empty ($dois)) {
      return '';
    }
    else {
      return substr($dois, 0, -2);
    }
  }

  /**
   * @return \CellLineClone[]
   */
  public function getClones() {
    return $this->clones;
  }

  /**
   * @param \CellLineClone[] $clones
   */
  public function setClones($clones) {
    $this->clones = $clones;
  }


  public function getProjectApiData() {
    return [
      'lab_id' => $this->lab_id,
      'ipsc_id' => $this->ipsc_id,
      'alt_name' => $this->alt_id,
      'project' => $this->project,
      'biosafety' => showBiosafety($this->biosafety),
    ];
  }

  /**
   * @return string
   */
  public function getBiosafety()
  {
    return $this->biosafety;
  }

  /**
   * @param string $biosafety
   */
  public function setBiosafety($biosafety)
  {
    $this->biosafety = $biosafety;
  }


  // ---------------------------<<< FORM FIELDS >>>-----------------------------
  // -----------------------------<<< GENERAL >>>-------------------------------

  /**
   * @param bool $editing
   *   Is this cell line a genetic modified cell line?
   *
   * @return array
   *   Lab ID form field.
   */
  public function getFormFieldLabId($editing) {
    // Rename the field to Editing ID when editing a genetically modified cell line.
    if ($editing) {
      $title = CellmodelConstants::getFieldTitles()['editing_id'] . ' *';
      $placeholder = t('e.g. isWT1-KRAS-G12V');
    }
    else {
      $title = CellmodelConstants::getFieldTitles()['lab_id'] . ' *';
      $placeholder = t('e.g. ALMSa1');
    }

    return [
      '#type' => 'textfield',
      '#title' => $title,
      '#default_value' => $this->lab_id,
      '#attributes' =>
        $attributes = [
          'placeholder' => $placeholder,
          'autocomplete' => 'off',
        ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Alternative ID form field.
   */
  public function getFormFieldAltId() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['alt_id'],
      '#default_value' => $this->alt_id,
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
      '#attributes' => [
        'placeholder' => t('e.g. isWT7'),
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @param bool $external
   *   Is the current cell line an external cell line?
   *
   * @return array
   *   Provider form field.
   */
  public function getFormFieldProvider($external) {
    // Only enable the field if creating/editing an external cell line.
    if ($external) {
      $attributes = ['placeholder' => t('e.g. Technical University of Munich')];
    }
    else {
      $attributes = ['disabled' => 'disabled'];
    }

    // Set the default value to 'UMG' if not an external cell line.
    if ($this->getProvider()) {
      $default_value = ($this->getProvider());
    }
    else {
      $default_value = $external ? '' : RDP_CELLMODEL_DEFAULT_PROVIDER[0];
    }

    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['provider'],
      '#default_value' => $default_value,
      '#attributes' => $attributes,
    ];
  }

  /**
   * @return array
   *   Location form field.
   */
  public function getFormFieldLocation() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['location'],
      '#default_value' => $this->location,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsLocation(),
    ];
  }

  /**
   * @return array
   *   Project form field.
   */
  public function getFormFieldProject() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['project'],
      '#default_value' => $this->project,
      '#attributes' => ['placeholder' => t('e.g. SCU')],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Cell Type form field.
   */
  public function getFormFieldCellType() {
    // Disable changing this value if the cell line has been registered at hPSCreg
    // and thus the hPSCreg name for this line is set. The generated name is dependent
    // of cell type.
    if (!empty($this->getHpscregName())) {
      $attributes = ['disabled' => 'disabled'];
    }
    else {
      $attributes = [];
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['cell_type'] . ' *',
      '#description' => t('<i>iPSC</i> = induced pluripotent stem cell'),
      '#default_value' => $this->cell_type,
      '#options' => CellmodelConstants::getOptionsCellType(),
      '#attributes' => $attributes,
    ];
  }

  /**
   * @return array
   *   Grade form field.
   */
  public function getFormFieldGrade() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['grade'],
      '#default_value' => $this->grade,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsGrade(),
      '#states' => [
        'visible' => [
          ':input[name*="cell_type"]' => [
            ['value' => 'iPSC'],
          ],
        ],
      ],
    ];
  }

  /**
   * @param bool $gen_mod
   *
   * @return array
   *   iPSC ID form field.
   */
  public function getFormFieldIpscId($gen_mod) {
    // Hide this field if editing a genetically modified cell line (not needed).
    if ($gen_mod) {
      $type = 'hidden';
    }
    else {
      $type = 'textfield';
    }

    return [
      '#type' => $type,
      '#title' => CellmodelConstants::getFieldTitles()['ipsc_id'],
      '#default_value' => $this->ipsc_id,
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
      '#attributes' => [
        'placeholder' => t('e.g. isWT8'),
        'autocomplete' => 'off',
      ],
      '#states' => [
        'visible' => [
          ':input[name*="cell_type"]' => [
            ['value' => 'iPSC'],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Same Donor form field.
   */
  public function getFormFieldSameDonor() {
    // Disable this field if this cell line has already been registered at hPSCreg.
    // The generated name is dependent of same donor.
    if (!empty($this->getHpscregName())) {
      $attributes = ['disabled' => 'disabled'];
    }
    else {
      $attributes = [];
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['same_donor'] . ' *',
      '#description' => t('Please select "Yes" if there is any other existing line with the same donor
      or if this cell line is a genetically modified subclone of another cell line.'),
      '#default_value' => $this->same_donor,
      '#options' => CellmodelConstants::getOptionsYesNo(),
      '#attributes' => $attributes,
      '#states' => [
        'visible' => [
          ':input[name*="cell_type"]' => [
            ['value' => 'iPSC'],
          ],
        ],
      ],
    ];
  }

  /**
   * @param bool $gen_mod
   *
   * @return array
   *   Same Donor Line form field.
   */
  public function getFormFieldUnderlyingCellLine($gen_mod) {
    // Assemble an array with every cell line lab id. Group them by tab type.
    $lines_options = [];
    foreach (CellmodelConstants::getOptionsTabType() as $key => $tab) {
      $lines_lab_ids = [];
      $lines = CellLineRepository::findByTabType($key);
      foreach ($lines as $line) {
        $line_string = $line->getLabId();
        $hpscreg = $line->getHpscregName();
        // If there is a hPSCreg name assigned, also show it.
        if (!empty($hpscreg)) {
          $line_string .= ' | ' . $hpscreg;

          // If the hPScreg name does not end with a digit (which would means
          // that it is already a subclone, and thus not really a suitable
          // underlying cell line) mark it as suitable with a *.
          if (!preg_match('/\d/i', substr($hpscreg, -1)) and
            $hpscreg !== $this->getHpscregName()) {
            $line_string = '* ' . $line_string;
          }
        }

        $lines_lab_ids [$line->getId()] = $line_string;
      }
      $lines_options = $lines_options + [$tab => $lines_lab_ids];
    }

    $attributes = [];

    // Disable this field if this cell line has already been registered at hPSCreg.
    // The generated name is dependent of underlying cell line.
    if (!empty($this->getHpscregName())) {
      $attributes = ['disabled' => 'disabled'];
    }
    // Rename the underlying cell line title according to the cell tab type.
    if ($gen_mod) {
      $title = CellmodelConstants::getFieldTitles()['underlying_cell_line'] . ' *';
    }
    else {
      $title = CellmodelConstants::getFieldTitles()['same_donor_line'] . ' *';
    }

    return [
      '#type' => 'select',
      '#title' => $title,
      '#description' => t('Please select the underlying cell line of the same donor. The list
      only shows the first respective cell lines registered at hPSCreg.'),
      '#empty_value' => '',
      '#default_value' => $this->underlying_cell_line,
      '#options' => $lines_options,
      '#attributes' => $attributes,
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'underlying-fieldset-wrapper',
        'event' => 'change',
        'effect' => 'fade',
      ],
      '#states' => [
        'visible' => [
          ':input[name*="same_donor"]' => [
            'value' =>
              'Yes',
          ],
          ':input[name*="cell_type"]' => [
            ['value' => 'iPSC'],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Availability form field.
   */
  public function getFormFieldObtainable() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['obtainable'],
      '#description' => t('If the cell line may only be used in-house, the answer should be "No".'),
      '#default_value' => isset($this->obtainable) ? $this->obtainable : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
      '#states' => [
        'visible' => [
          ':input[name*="cell_type"]' => [
            ['value' => 'iPSC'],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Availability Restriction form field.
   */
  public function getFormFieldRestricAreas() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['restric_areas'],
      '#default_value' => $this->restric_areas,
      '#states' => [
        'visible' => [
          ':input[name*="obtainable"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
      '#attributes' => ['placeholder' => t('e.g. only for SFB1002')],
    ];
  }

  /**
   * @return array
   *   Availability Restriction form field.
   */
  public function getFormFieldRestricResearch() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['restric_research'],
      '#default_value' => isset($this->restric_research) ? $this->restric_research : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
      '#states' => [
        'visible' => [
          ':input[name*="obtainable"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Availability Restriction form field.
   */
  public function getFormFieldRestricClinical() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['restric_clinical'],
      '#default_value' => isset($this->restric_clinical) ? $this->restric_clinical : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
      '#states' => [
        'visible' => [
          ':input[name*="obtainable"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Availability Restriction form field.
   */
  public function getFormFieldRestricCommercial() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['restric_commercial'],
      '#default_value' => isset($this->restric_commercial) ? $this->restric_commercial : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
      '#states' => [
        'visible' => [
          ':input[name*="obtainable"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   *   Availability Restriction form field.
   */
  public function getFormFieldRestricAdditional() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['restric_additional'],
      '#default_value' => $this->restric_additional,
      '#states' => [
        'visible' => [
          ':input[name*="obtainable"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
      '#attributes' => ['placeholder' => t('e.g. only with consideration of Human Tissue Act 2004')],
    ];
  }

  /**
   * @return array
   *   hPSCreg Helper form field.
   */
  public function getFormFieldHpscregHelper() {
    return [
      '#type' => 'hidden',
      '#default_value' => $this->hpscreg_name,
    ];
  }

  // ---------------------------- <<< DIAGNOSIS >>>----------------------------

  /**
   * Button for adding one more diagnosis.
   *
   * @return array
   *   Add Diagnosis button
   */
  public function getFormFieldDiagnosisAdd() {
    return [
      '#type' => 'submit',
      '#value' => t('Add'),
      '#name' => 'AddDiagnosisButton',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'diagnosis-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => ['class' => ['btn-primary btn-sm']],
    ];
  }

  /**
   * Button for removing a diagnosis.
   *
   * @param int $i
   *   Respective index number of fieldset.
   *
   * @return array
   *   Remove Diagnosis button.
   */
  public function getFormFieldDiagnosisRemove($i) {
    return [
      '#type' => 'submit',
      '#value' => 'Remove',
      '#name' => 'RemoveDiagnosisButton' . $i,
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'diagnosis-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn btn-danger btn-xs'],
        'style' => 'position: absolute; right: 70px; margin-top: -52px',
      ],
    ];
  }

  /**
   * @return array
   *   Genetic Predisposition form field.
   */
  public function getFormFieldPredis() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['genetic_predisposition'],
      '#default_value' => isset($this->genetic_predisposition) ? $this->genetic_predisposition : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsKnownUnknownNa(),
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#prefix' => '<div>&nbsp;</div>',
    ];
  }

  /**
   * @return array
   *   Genetic Predisposition Specification form field.
   */
  public function getFormFieldPredisSpec() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['genetic_predisposition_spec'],
      '#default_value' => $this->genetic_predisposition_spec,
      '#attributes' => [
        'placeholder' => t('e.g. SCN5A- p.L1327P/c.T3980C'),
        'autocomplete' => 'off',
      ],
      '#states' => [
        'visible' => [
          ':input[name*="predis"]' => [
            'value' =>
              'Known',
          ],
        ],
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Visibility of Genetic Predisposition form field.
   */
  public function getFormFieldVisibilityPredis() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['visibility_genetic_predisposition'] . ' *',
      '#default_value' => $this->visibility_predisposition_spec,
      '#options' => CellmodelConstants::getOptionsVisibility(),
      '#states' => [
        'visible' => [
          ':input[name*="dia_predis_spec"]' => [
            'filled' =>
              TRUE,
          ],
        ],
      ],
    ];
  }

  // ------------------------------ <<< DONOR >>>------------------------------

  /**
   * @return array
   *   Gender of Donor form field.
   */
  public function getFormFieldGender() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['donor_gender'],
      '#default_value' => isset($this->donor_gender) ? $this->donor_gender : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsGenderNa(),
    ];
  }

  /**
   * @return array
   *   Race of Donor form field.
   */
  public function getFormFieldRace() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['donor_race'],
      '#default_value' => $this->donor_race,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsRace(),
    ];
  }

  /**
   * @return array
   *   Age of Donor form field.
   */
  public function getFormFieldAge() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['donor_age'],
      '#default_value' => $this->donor_age,
      '#attributes' => [
        'placeholder' => t('e.g. 54'),
        'autocomplete' => 'off',
      ],
      '#maxlength' => 3,
      '#element_validate' => ['rdp_cellmodel_cell_line_new_edit_validate_integer'],
    ];
  }

  /**
   * @param $genetic_editing
   *   Is this a genetically modified cell line?
   *
   * @return array
   *   HLA-A form field.
   */
  public function getFormFieldHLAA($genetic_editing) {
    // Don't show this field if is this is a genetically modified cell line.
    if ($genetic_editing) {
      $type = 'hidden';
    }
    else {
      $type = 'textfield';
    }

    return [
      '#type' => $type,
      '#title' => CellmodelConstants::getFieldTitles()['hla_a'],
      '#default_value' => $this->hla_a,
      '#attributes' => [
        'placeholder' => t('e.g. 03:01:01G, 31:01:02G'),
        'autocomplete' => 'off',
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @param $genetic_editing
   *   Is this a genetically modified cell line?
   *
   * @return array
   *   HLA-B form field.
   */
  public function getFormFieldHLAB($genetic_editing) {
    // Don't show this field if is this is a genetically modified cell line.
    if ($genetic_editing) {
      $type = 'hidden';
    }
    else {
      $type = 'textfield';
    }

    return [
      '#type' => $type,
      '#title' => CellmodelConstants::getFieldTitles()['hla_b'],
      '#default_value' => $this->hla_b,
      '#attributes' => [
        'placeholder' => t('e.g. 35:03P, 49:01P'),
        'autocomplete' => 'off',
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @param $genetic_editing
   *   Is this a genetically modified cell line?
   *
   * @return array
   *   HLA-DRB1 form field.
   */
  public function getFormFieldHLADRB1($genetic_editing) {
    // Don't show this field if is this is a genetically modified cell line.
    if ($genetic_editing) {
      $type = 'hidden';
    }
    else {
      $type = 'textfield';
    }

    return [
      '#type' => $type,
      '#title' => CellmodelConstants::getFieldTitles()['hla_drb1'],
      '#default_value' => $this->hla_drb1,
      '#attributes' => [
        'placeholder' => t('e.g. 01:01P, 08:01P'),
        'autocomplete' => 'off',
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   SecuTrial ID  of Donor form field.
   */
  public function getFormFieldSecID() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['donor_secutrial_id'],
      '#default_value' => $this->donor_secutrial_id,
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
      '#attributes' => [
        'placeholder' => t('e.g. jjub871'),
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @return array
   *   Pseudonym of Donor form field.
   */
  public function getFormFieldPseudonym() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['donor_pseudonym'],
      '#default_value' => $this->donor_pseudonym,
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
      '#attributes' => [
        'placeholder' => t('e.g. 17B0061'),
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @param $show_field
   *   Is this a genetically modified cell line?
   *
   * @return array
   *   STARLIMD ID of Donor form field.
   */
  public function getFormFieldStarID($show_field) {
    if ($show_field) {
      $type = 'textfield';
    }
    else {
      $type = 'hidden';
    }
    return [
      '#type' => $type,
      '#title' => CellmodelConstants::getFieldTitles()['donor_starlims_id'],
      '#default_value' => $this->donor_starlims_id,
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
      '#attributes' => [
        'placeholder' => t('e.g. 8716299125'),
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * Button for adding one more relative.
   *
   * @return array
   *   Add Relative button.
   */
  public function getFormFieldRelativeAdd() {
    return [
      '#type' => 'submit',
      '#value' => t('Add'),
      '#name' => 'AddRelativeButton',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'donor-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => ['class' => ['btn-primary btn-sm']],
    ];
  }

  /**
   * Button for removing one relative.
   *
   * @param int $i
   *   Respective index number of fieldset.
   *
   * @return array
   *   Remove Relative button.
   */
  public function getFormFieldRelativeRemove($i) {
    return [
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#name' => 'RemoveRelativeButton' . $i,
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'donor-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn btn-danger btn-xs'],
        'style' => 'position: absolute; right: 70px; margin-top: -52px',
      ],
    ];
  }

  // --------------------------- <<< CELL CULTURE >>>---------------------------

  /**
   * @return array
   *   Source Type form field.
   */
  public function getFormFieldSourceType() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['source_type'],
      '#default_value' => $this->source_type,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsSource(),
    ];
  }

  /**
   * @return array
   *   Sample Origin form field.
   */
  public function getFormFieldSampleClinic() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['sample_clinic'],
      '#default_value' => $this->sample_origin,
      '#description' => t('In which clinic is the sample originated?'),
      '#attributes' => [
        'placeholder' => t('e.g. UMG, Cyganek'),
      ],
    ];
  }

  /**
   * @return array
   *   Sampling Date form field.
   */
  public function getFormFieldSampDate() {
    return [
      '#type' => 'date_popup',
      '#name' => 'SampDate',
      '#date_format' => RDP_CELLMODEL_DEFAULT_DATE_FORMAT,
      '#default_value' => $this->sampling_date,
      '#suffix' => '<div>&nbsp;</div>',
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @return array
   *   Primary Culture form field.
   */
  public function getFormFieldPrimary() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['primary_culture'],
      '#default_value' => isset($this->primary_culture) ? $this->primary_culture : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNona(),
    ];
  }

  /**
   * @return array
   *   Reprogramming Method form field.
   */
  public function getFormFieldRepMethod() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['reprogramming_method'],
      '#default_value' => $this->reprogramming_method,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsRepMethod(),
    ];
  }

  /**
   * @return array
   *   Reprogramming Vector form field.
   */
  public function getFormFieldRepVector() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['reprogramming_vector'],
      '#default_value' => $this->reprogramming_vector,
      '#attributes' => [
        'placeholder' => t('e.g. Cytotune 2.0'),
      ],
    ];
  }

  /**
   * Button for adding one more reprogramming date.
   *
   * @return array
   *   Add Rep. Date button.
   */
  public function getFormFieldRepDateAdd() {
    return [
      '#type' => 'submit',
      '#value' => t('Add'),
      '#name' => 'AddRepDateButton',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'culture-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => ['class' => ['btn-primary btn-sm']],
    ];
  }

  /**
   * Button for removing  the last reprogramming date.
   *
   * @param int $i
   *   Respective index number of fieldset.
   *
   * @return array
   *   Remove Rep. Date button.
   */
  public function getFormFieldRepDateRemove($i) {
    return [
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#name' => 'RemoveRepDateButton' . $i,
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'culture-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-danger btn-xs'],
        'style' => 'position: relative; margin: 3px',
      ],
    ];
  }

  // --------------------------- <<< CLONES >>>--------------------------

  /**
   * Button for adding one more editing date.
   *
   * @return array
   *   Add Clone button.
   */
  public function getFormFieldCloneAdd() {
    return [
      '#type' => 'submit',
      '#value' => t('Add'),
      '#name' => 'AddCloneButton',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'clones-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => ['class' => ['btn-primary btn-sm']],
    ];
  }

  /**
   * Button for removing one clone.
   *
   * @return array
   *   Remove Clone button.
   */
  public function getFormFieldCloneRemove($key) {
    return [
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#name' => 'RemoveCloneButton' . $key,
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'clones-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-primary btn-xs'],
        'style' => 'position: absolute; right: 8px; margin-top: -52px',
      ],
    ];
  }

  // --------------------------- <<< VIRUS TESTING >>>--------------------------

  /**
   * @return array
   *   HIV Results form field.
   */
  public function getFormFieldHiv() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['hiv_result'],
      '#default_value' => isset($this->hiv_result) ? $this->hiv_result : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsTestingResultsNa(),
    ];
  }

  /**
   * @return array
   *   HIV Material form field.
   */
  public function getFormFieldHivMaterial() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['hiv_material'],
      '#default_value' => isset($this->hiv_material) ? $this->hiv_material : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsTestingMaterialNa(),
    ];
  }

  /**
   * @return array
   *   HIV Date form field.
   */
  public function getFormFieldHivDate() {
    return [
      '#name' => 'HivDate',
      '#type' => 'date_popup',
      '#date_format' => RDP_CELLMODEL_DEFAULT_DATE_FORMAT,
      '#default_value' => $this->hiv_date,
      '#suffix' => '<div>&nbsp;</div>',
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @return array
   *   Hep B Result form field.
   */
  public function getFormFieldHepB() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['hep_b_result'],
      '#default_value' => isset($this->hep_b) ? $this->hep_b : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsTestingResultsNa(),
    ];
  }

  /**
   * @return array
   *   Hep B Material form field.
   */
  public function getFormFieldHepBMaterial() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['hep_b_material'],
      '#default_value' => isset($this->hep_b_material) ? $this->hep_b_material : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsTestingMaterialNa(),
    ];
  }

  /**
   * @return array
   *   Hep B Date form field.
   */
  public function getFormFieldHepBDate() {
    return [
      '#name' => 'HepBDate',
      '#type' => 'date_popup',
      '#date_format' => RDP_CELLMODEL_DEFAULT_DATE_FORMAT,
      '#default_value' => $this->hep_b_date,
      '#suffix' => '<div>&nbsp;</div>',
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
  }

  /**
   * @return array
   *   Hep C Result form field.
   */
  public function getFormFieldHepC() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['hep_c_result'],
      '#default_value' => isset($this->hep_c) ? $this->hep_c : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsTestingResultsNa(),
    ];
  }

  /**
   * @return array
   *   Hep C Material form field.
   */
  public function getFormFieldHepCMaterial() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['hep_c_material'],
      '#default_value' => isset($this->hep_c_material) ? $this->hep_c_material : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsTestingMaterialNa(),
    ];
  }

  /**
   * @return array
   *   Hep C Date form field.
   */
  public function getFormFieldHepCDate() {
    return [
      '#name' => 'HepCDate',
      '#type' => 'date_popup',
      '#date_format' => RDP_CELLMODEL_DEFAULT_DATE_FORMAT,
      '#default_value' => $this->hep_c_date,
      '#suffix' => '<div>&nbsp;</div>',
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
  }

  // ----------------------------- <<< ETHICS >>> ------------------------------

  /**
   * @return array
   *   Ethics Preset 1 button form field.
   */
  function getFormFieldPreset($name, $id) {
    return [
      '#type' => 'submit',
      '#value' => $name,
      '#name' => 'Preset-'.$id,
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'ethics-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-default btn-sm'],
      ],
    ];
  }

  /**
   * @return array
   *   Ethics Preset 2 button form field.
   */
  function getFormFieldPreset2() {
    return [
      '#type' => 'submit',
      '#value' => t('11.11.2015'),
      '#name' => 'Preset2',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'ethics-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-default btn-sm'],
      ],
    ];
  }

  /**
   * @return array
   *   Ethics Preset 3 button form field.
   */
  function getFormFieldPreset3() {
    return [
      '#type' => 'submit',
      '#value' => t('11.03.2016'),
      '#name' => 'Preset3',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'ethics-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-default btn-sm'],
      ],
    ];
  }

  /**
   * @return array
   *   Ethics Preset 3 button form field.
   */
  function getFormFieldPreset4() {
    return [
      '#type' => 'submit',
      '#value' => t('28.11.2017'),
      '#name' => 'Preset4',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'ethics-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-default btn-sm'],
      ],
    ];
  }

  /**
   * @return array
   *   Ethics Preset 3 button form field.
   */
  function getFormFieldPreset5() {
    return [
      '#type' => 'submit',
      '#value' => t('17.05./07.2018'),
      '#name' => 'Preset5',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'ethics-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-default btn-sm'],
      ],
    ];
  }

  /**
   * @return array
   *   Ethics Preset 3 button form field.
   */
  function getFormFieldPreset6() {
    return [
      '#type' => 'submit',
      '#value' => t('22.04.2020'),
      '#name' => 'Preset6',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'ethics-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-default btn-sm'],
      ],
    ];
  }

  /**
   * @return array
   *   Ethics Preset 3 button form field.
   */
  function getFormFieldPreset7() {
    return [
      '#type' => 'submit',
      '#value' => t('Mannheim 10.04.2012'),
      '#name' => 'Preset7',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'ethics-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-default btn-sm'],
      ],
      '#suffix' => '<div>&nbsp;</div>',
    ];
  }

  /**
   * @return array
   *   Ethics1 form field.
   */
  public function getFormFieldEthics1($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics1;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics1'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics2 form field.
   */
  public function getFormFieldEthics2($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics2;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics2'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics3 form field.
   */
  public function getFormFieldEthics3($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics3;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics3'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics4 form field.
   */
  public function getFormFieldEthics4($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics4;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics4'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics4Copy form field.
   */
  public function getFormFieldEthics4Contact() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['ethics4_contact'],
      '#default_value' => $this->ethics4_contact,
      '#states' => [
        'visible' => [
          ':input[name*="ethics4"]' => [
            'value' =>
              'No',
          ],
        ],
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Ethics4Upload form field.
   */
  public function getFormFieldEthics4Upload() {
    return [
      '#type' => 'textfield',
      '#title' => t('Upload'),
      '#states' => [
        'visible' => [
          ':input[name*="ethics4"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
      '#attributes' => [
        'placeholder' => t(' - upload not implemented yet - '),
        'readonly' => 'readonly',
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Ethics5 form field.
   */
  public function getFormFieldEthics5($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics5;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics5'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics4Copy form field.
   */
  public function getFormFieldEthics5Contact() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['ethics5_contact'],
      '#default_value' => $this->ethics5_contact,
      '#states' => [
        'visible' => [
          ':input[name*="ethics5"]' => [
            'value' =>
              'No',
          ],
        ],
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Ethics4Upload form field.
   */
  public function getFormFieldEthics5Upload() {
    return [
      '#type' => 'textfield',
      '#title' => t('Upload'),
      '#states' => [
        'visible' => [
          ':input[name*="ethics5"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
      '#attributes' => [
        'placeholder' => t(' - upload not implemented yet - '),
        'readonly' => 'readonly',
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Ethics6 form field.
   */
  public function getFormFieldEthics6($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics6;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics6'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsEthicsAnonym(),
    ];
  }

  /**
   * @return array
   *   Ethics7 form field.
   */
  public function getFormFieldEthics7($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics7;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics7'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics8 form field.
   */
  public function getFormFieldEthics8($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics8;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics8'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics9 form field.
   */
  public function getFormFieldEthics9($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics9;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics9'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsEthicsAccess(),
    ];
  }

  /**
   * @return array
   *   Ethics10 form field.
   */
  public function getFormFieldEthics10($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics10;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics10'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics11 form field.
   */
  public function getFormFieldEthics11($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics11;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics11'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics11 Institute Name form field.
   */
  public function getFormFieldEthics11SpecName() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['ethics11_spec_name'],
      '#default_value' => $this->ethics11_spec_name,
      '#states' => [
        'visible' => [
          ':input[name*="ethics11"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Ethics11 Approval Number form field.
   */
  public function getFormFieldEthics11SpecNumber() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['ethics11_spec_number'],
      '#default_value' => $this->ethics11_spec_number,
      '#states' => [
        'visible' => [
          ':input[name*="ethics11"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Ethics12 form field.
   */
  public function getFormFieldEthics12($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics12;
    }

    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics12'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNa(),
    ];
  }

  /**
   * @return array
   *   Ethics12 Institute Name form field.
   */
  public function getFormFieldEthics12SpecName() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['ethics12_spec_name'],
      '#default_value' => $this->ethics12_spec_name,
      '#states' => [
        'visible' => [
          ':input[name*="ethics12"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Ethics12 Approval Number form field.
   */
  public function getFormFieldEthics12SpecNumber() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['ethics12_spec_number'],
      '#default_value' => $this->ethics12_spec_number,
      '#states' => [
        'visible' => [
          ':input[name*="ethics12"]' => [
            'value' =>
              'Yes',
          ],
        ],
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Ethics13 form field.
   */
  public function getFormFieldEthics13($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics13;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics13'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics14 form field.
   */
  public function getFormFieldEthics14($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics14;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics14'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics15 form field.
   */
  public function getFormFieldEthics15($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics15;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics15'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics16 form field.
   */
  public function getFormFieldEthics16($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics16;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics16'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics17 form field.
   */
  public function getFormFieldEthics17($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics17;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics17'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics18 form field.
   */
  public function getFormFieldEthics18($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics18;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics18'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics19 form field.
   */
  public function getFormFieldEthics19($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics19;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics19'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics20 form field.
   */
  public function getFormFieldEthics20($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics20;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics20'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics21 form field.
   */
  public function getFormFieldEthics21($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics21;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics21'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics22 form field.
   */
  public function getFormFieldEthics22($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics22;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics22'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics23 form field.
   */
  public function getFormFieldEthics23($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics23;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics23'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics24 form field.
   */
  public function getFormFieldEthics24($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics24;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics24'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsEthicsAnonymization(),
    ];
  }

  /**
   * @return array
   *   Ethics25 form field.
   */
  public function getFormFieldEthics25($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics25;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics25'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsEthicsTerminate(),
    ];
  }

  /**
   * @return array
   *   Ethics26 form field.
   */
  public function getFormFieldEthics26($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics26;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics26'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  /**
   * @return array
   *   Ethics27 form field.
   */
  public function getFormFieldEthics27($default = NULL) {
    if (!isset($default)) {
      $default = $this->ethics27;
    }
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['ethics27'],
      '#default_value' => isset($default) ? $default : CellmodelConstants::getOptionNa(),
      '#options' => CellmodelConstants::getOptionsYesNoNotAskedNa(),
    ];
  }

  // -------------------------- <<< MISCELLANEOUS >>> --------------------------

  /**
   * @return array
   *   Contact form field.
   */
  public function getFormFieldContact() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['contact'],
      '#default_value' => $this->contact_e_mail,
      '#attributes' => ['placeholder' => t('e.g. Lukas Cyganek')],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Correspondence form field.
   */
  public function getFormFieldCorrespondence() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['correspondence'],
      '#default_value' => $this->correspondence,
      '#attributes' => ['placeholder' => t('e.g. SCU')],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Correspondence form field.
   */
  public function getFormFieldCostCoverage() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['cost_coverage'],
      '#default_value' => $this->cost_coverage,
      '#attributes' => ['placeholder' => t('e.g. SCU')],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Context of distribution.
   */
  public function getFormFieldDistribution() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['distribution'],
      '#default_value' => $this->distribution,
      '#options' => CellmodelConstants::getOptionsDistribution(),
      '#empty_value' => '',
    ];
  }

  /**
   * @return array
   *   Cell Lines in Biobank fom field.
   */
  public function getFormFieldLinesInBiobank() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['lines_in_biobank'],
      '#default_value' => $this->lines_in_biobank,
      '#attributes' => [
        'placeholder' => t('e.g. isWT7'),
        'autocomplete' => 'off',
      ],
      '#maxlength' => RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH,
    ];
  }

  /**
   * @return array
   *   Biosafety Level form field.
   */
  public function getFormFieldBiosafety($gen_mod) {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['biosafety'],
      '#options' => CellmodelConstants::getOptionsBiosafety($gen_mod),
      '#default_value' => isset($this->biosafety) ? $this->biosafety : CellmodelConstants::getOptionNa(),
    ];
  }

  /**
   * @return array
   *   Comments form field.
   */
  public function getFormFieldComments() {
    return [
      '#type' => 'textarea',
      '#title' => CellmodelConstants::getFieldTitles()['comments'],
      '#default_value' => $this->comments,
      '#attributes' => [
        'placeholder' => t('e.g. Cl. 3/7 > SL 1/2'),
        'autocomplete' => 'off',
      ],
      '#maxlength' => 1024,
    ];
  }

  /**
   * @return array
   *   Comments form field.
   */
  public function getFormFieldVisibility() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['visibility'] . ' *',
      '#default_value' => $this->visibility,
      '#description' => t('<i>Public</i> = everybody can see the basic cell line information, &nbsp;
                                 <i>Private</i> = only permitted users can see all cell line information'),
      '#options' => CellmodelConstants::getOptionsVisibility(),
    ];
  }

  /**
   * @return array
   *   Tab Type form field.
   */
  public function getFormFieldTabType() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['tab_type'],
      '#default_value' => $this->tab_type,
      '#options' => CellmodelConstants::getOptionsTabType(),
      '#attributes' => ['disabled' => 'disabled'],
    ];
  }
  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellLine into the database.
   */
  public function save() {
    try {
      CellLineRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}
