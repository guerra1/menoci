<?php

/**
 * @file
 * Provide class for the CellLineEditingDate objects. Provide form fields
 * for each variable when building @see \rdp_cellmodel_cell_line_new_edit().
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineEditingDate
 */
class CellLineEditingDate {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   The database id of the editing date.
   */
  private $id;

  /**
   * @var int
   *   The ID of the respective cell line.
   */
  private $cell_line;

  /**
   * @var string
   *   The editing date.
   */
  private $date;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getCellLine() {
    return $this->cell_line;
  }

  /**
   * @param int $cell_line
   */
  public function setCellLine($cell_line) {
    $this->cell_line = $cell_line;
  }

  /**
   * @return string
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * @param string $date
   */
  public function setDate($date) {
    $this->date = $date;
  }

  // ---------------------------<<< FORM FIELDS >>>-----------------------------

  /**
   * @param $count int
   *   The current count of editing dates.
   *
   * @return array
   *   Editing Date form field.
   */
  public function getFormFieldEditingDate($count) {
    $count++;
    $title_prefix = generate_counting_string($count);

    return [
      '#date_label_position' => 'none',
      '#type' => 'date_popup',
      '#name' => 'EditingDate',
      '#default_value' => $this->getDate(),
      '#date_format' => RDP_CELLMODEL_DEFAULT_DATE_FORMAT,
      '#description' => t('The date at which the editing took place.'),
      '#prefix' => '<p style="font-size:9pt;"> ' . $title_prefix . ' Editing Date </p>',
      '#suffix' => '<div>&nbsp;</div>',
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellLineEditingDate into the database.
   */
  public function save() {
    try {
      CellLineEditingDateRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}
