<?php

/**
 * @file
 * Provide class for the CellmodelLog objects.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellmodelLog
 */
class CellmodelLog {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   Database id of the log.
   */
  private $id;

  /**
   * @var int
   *   Respective cell line.
   */
  private $cell_line;

  /**
   * @var string
   *   Date at which the log was created.
   */
  private $date;

  /**
   * @var int
   *   The user who created the log.
   */
  private $user;

  /**
   * @var string
   *   The action associated with the log.
   */
  private $action;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * CellmodelLog constructor.
   */
  public function __construct() {
    global $user;

    $this->setDate(date(RDP_CELLMODEL_DEFAULT_LOG_DATE_FORMAT));
    $this->setUser($user->uid);
  }

  /**
   * @return mixed $id
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return mixed $cell_line
   */
  public function getCellLine() {
    return $this->cell_line;
  }

  /**
   * @param mixed $cell_line
   */
  public function setCellLine($cell_line) {
    $this->cell_line = $cell_line;
  }

  /**
   * @return mixed $date
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * @param mixed $date
   */
  public function setDate($date) {
    $this->date = $date;
  }

  /**
   * @return mixed $user
   */
  public function getUser() {
    return $this->user;
  }

  /**
   * @param mixed $user
   */
  public function setUser($user) {
    $this->user = $user;
  }

  /**
   * @return mixed $action
   */
  public function getAction() {
    return $this->action;
  }

  /**
   * @param mixed $action
   */
  public function setAction($action) {
    $this->action = $action;
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellmodelLog into the database.
   */
  public function save() {
    try {
      CellmodelLogRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}
