<?php

/**
 * @file
 * Provides the database layer for @see \CellLineSample.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineSampleRepository
 */
class CellLineSampleRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLineSample.
   */
  static $tableName = 'cellmodel_cell_line_sample';

  /**
   * @var array
   *   All database fields for CellLineSample.
   */
  static $databaseFields = [
    'id',
    'cell_line',
    'type_sample',
    'type_primary_container',
    'precentrifugation',
    'centrifugation1',
    'centrifugation2',
    'postcentrifugation',
    'long_term_storage',
    'amount_left',
    'container_external_id',
    'container_inventory_id',
    'container_pos_x',
    'container_pos_y',
    'internal_inventory_id',
    'kit_id',
    'location',
    'material_code',
    'project_name',
    'sample_container',
    'sample_id',
    'tube_barcode',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLineSample into the database.
   *
   * @param \CellLineSample $sample
   *   CellLineSample object to be saved.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($sample) {
    db_merge(self::$tableName)
      ->key(['id' => $sample->getId()])
      ->fields([
        'cell_line' => $sample->getCellLine(),
        'type_sample' => $sample->getTypeSample(),
        'type_primary_container' => $sample->getTypePrimaryContainer(),
        'precentrifugation' => $sample->getPrecentrifugation(),
        'centrifugation1' => $sample->getCentrifugation1(),
        'centrifugation2' => $sample->getCentrifugation2(),
        'postcentrifugation' => $sample->getPostcentrifugation(),
        'long_term_storage' => $sample->getLongTermStorage(),
        'amount_left' => $sample->getAmountLeft(),
        'container_external_id' => $sample->getContainerExternalId(),
        'container_inventory_id' => $sample->getContainerInventoryId(),
        'container_pos_x' => $sample->getContainerPosX(),
        'container_pos_y' => $sample->getContainerPosY(),
        'internal_inventory_id' => $sample->getInternalInventoryId(),
        'kit_id' => $sample->getKitId(),
        'location' => $sample->getLocation(),
        'material_code' => $sample->getMaterialCode(),
        'project_name' => $sample->getProjectName(),
        'sample_container' => $sample->getSampleContainer(),
        'sample_id' => $sample->getSampleId(),
        'tube_barcode' => $sample->getTubeBarcode(),
      ])
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLineSample object.
   *
   * @param $result
   *   Database result of a finder function.
   *
   * @return \CellLineSample
   *   New CellLineSample object.
   */
  public static function databaseResultsToCellLineSample($result) {
    $sample = new CellLineSample();

    if (empty($result)) {
      return NULL;
    }

    // Set the variables.
    $sample->setId($result->id);
    $sample->setCellLine($result->cell_line);
    $sample->setTypeSample($result->type_sample);
    $sample->setTypePrimaryContainer($result->type_primary_container);
    $sample->setPrecentrifugation($result->precentrifugation);
    $sample->setCentrifugation1($result->centrifugation1);
    $sample->setCentrifugation2($result->centrifugation2);
    $sample->setPostcentrifugation($result->postcentrifugation);
    $sample->setLongTermStorage($result->long_term_storage);
    $sample->setAmountLeft($result->amount_left);
    $sample->setContainerExternalId($result->container_external_id);
    $sample->setContainerInventoryId($result->container_inventory_id);
    $sample->setContainerPosX($result->container_pos_x);
    $sample->setContainerPosY($result->container_pos_y);
    $sample->setInternalInventoryId($result->internal_inventory_id);
    $sample->setKitId($result->kit_id);
    $sample->setLocation($result->location);
    $sample->setMaterialCode($result->material_code);
    $sample->setProjectName($result->project_name);
    $sample->setSampleContainer($result->sample_container);
    $sample->setSampleId($result->sample_id);
    $sample->setTubeBarcode($result->tube_barcode);
    return $sample;
  }

  /**
   * Read database results and create an array with CellLineSample objects.
   *
   * @param $results
   *   Database result of a finder function.
   *
   * @return \CellLineSample[]
   *   New CellLineSample objects.
   */
  public static function databaseResultsToCellLineSamples($results) {
    $samples = [];
    foreach ($results as $result) {
      $samples[] = self::databaseResultsToCellLineSample($result);
    }

    return $samples;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Return CellLineSample of given cell line.
   *
   * @param string $cell_line
   *   Cell line of the given CellLineSample.
   *
   * @return \CellLineSample[]
   *   Found CellLineSample objects.
   */
  public static function findByCellLine($cell_line) {
    $result = db_select(self::$tableName, 'a')
      ->condition('cell_line', $cell_line, '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToCellLineSamples($result);
  }

}
