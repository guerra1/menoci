<?php

/**
 * @file
 * Provide database layer for @see CellLineDisease.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class CellLineDiseaseRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLineDisease.
   */
  static $tableName = 'cellmodel_cell_line_disease';

  /**
   * @var array
   *   All database fields for CellLineDisease.
   */
  static $databaseFields = [
    'id',
    'disease_name',
    'disease_icd',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLineDisease into the database.
   *
   * @param \CellLineDisease $disease
   *   CellLineDisease object to be stored.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($disease) {
    db_merge(self::$tableName)
      ->key(['id' => $disease->getId()])
      ->fields([
        'disease_name' => $disease->getDiseaseName(),
        'disease_icd' => $disease->getIcd(),
      ])
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLineDisease object.
   *
   * @param $result
   *  Database results of a finder function.
   *
   * @return \CellLineDisease
   *   New CellLineDisease object.
   */
  public static function databaseResultsToDisease($result) {
    $disease = new CellLineDisease();

    if (empty($result)) {
      return $disease;
    }

    // Set the variables.
    $disease->setId($result->id);
    $disease->setDiseaseName($result->disease_name);
    $disease->setIcd($result->disease_icd);

    return $disease;
  }

  /**
   * Read database results and create an array with CellLineDisease objects.
   *
   * @param $results
   *  Database results of a finder function.
   *
   * @return \CellLineDisease[]
   *   New CellLineDisease objects.
   */
  public static function databaseResultsToDiseases($results) {
    $diseases = [];
    foreach ($results as $result) {
      $diseases[] = self::databaseResultsToDisease($result);
    }
    return $diseases;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Find all CellLineDiseases.
   *
   * @return \CellLineDisease[]
   *   Found CellLineDisease objects.
   */
  public static function findAll() {
    $result = db_select(self::$tableName, 'a')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToDiseases($result);
  }

  /**
   * Find the CellLineDisease with a specific database ID.
   *
   * @param $id
   *   The ID of the given CellLineDisease.
   *
   * @return \CellLineDisease
   *   Found CellLineDisease object.
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 'a')
      ->condition('id', $id, '=')
      ->fields('a', self::$databaseFields)
      ->execute()
      ->fetch();

    return self::databaseResultsToDisease($result);
  }

  /**
   * Find the CellLineDisease with a specific disease name.
   *
   * @param $disease_name
   *   The disease name of the given CellLineDisease.
   *
   * @return \CellLineDisease
   *   Found CellLineDisease object.
   */
  public static function findByDiseaseName($disease_name) {
    $result = db_select(self::$tableName, 'a')
      ->condition('disease_name', $disease_name, '=')
      ->fields('a', self::$databaseFields)
      ->execute()
      ->fetch();

    return self::databaseResultsToDisease($result);
  }
}
