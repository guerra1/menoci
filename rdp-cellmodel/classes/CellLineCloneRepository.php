<?php

/**
 * @file
 * Provide database layer for @see \CellLineClone.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineCloneRepository
 */
class CellLineCloneRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLineClone.
   */
  static $tableName = 'cellmodel_cell_line_clone';

  /**
   * @var array
   *   All database fields for CellLineClone.
   */
  static $databaseFields = [
    'id',
    'cell_line',
    'clone_number',
    'genomic_modification',
    'hdr_nhej',
    'zygosity',
    'specification',
    'visibility',
    'visibility_clone',
    'clone_number_starlims',
    'culture_medium',
    'characterization',
    'start_date',
    'responsible',
    'morphology',
    'pluriotency_pcr',
    'pluriotency_immuno',
    'pluriotency_facs',
    'differentiation_immuno',
    'hla_analyzed',
    'mutation_analyzed',
    'editing_analyzed',
    'str_analyzed',
    'karyo_analyzed',
    'karyotype',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLineClone into the database.
   *
   * @param \CellLineClone $clone
   *   CellLineClone object to be saved.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($clone) {
    db_merge(self::$tableName)
      ->key(['id' => $clone->getId()])
      ->fields([
        'cell_line' => $clone->getCellLine(),
        'clone_number' => $clone->getCloneNumber(),
        'genomic_modification' => $clone->getGenomicModification(),
        'hdr_nhej' => $clone->getHdrNhej(),
        'zygosity' => $clone->getZygosity(),
        'specification' => $clone->getSpecification(),
        'visibility' => $clone->getVisibility(),
        'visibility_clone' => $clone->getVisibilityClone(),
        'clone_number_starlims' => $clone->getCloneNumberStarlims(),
        'culture_medium' => $clone->getCultureMedium(),
        'characterization' => $clone->getCharacterization(),
        'start_date' => $clone->getStartDate(),
        'responsible' => $clone->getResponsible(),
        'morphology' => $clone->getMorphology(),
        'pluriotency_pcr' => $clone->getPluriotencyPcr(),
        'pluriotency_immuno' => $clone->getPluriotencyImmuno(),
        'pluriotency_facs' => $clone->getPluriotencyFacs(),
        'differentiation_immuno' => $clone->getDifferentiationImmuno(),
        'hla_analyzed' => $clone->getHlaAnalyzed(),
        'mutation_analyzed' => $clone->getMutationAnalyzed(),
        'editing_analyzed' => $clone->getEditingAnalyzed(),
        'str_analyzed' => $clone->getStrAnalyzed(),
        'karyo_analyzed' => $clone->getKaryoAnalyzed(),
        'karyotype' => $clone->getKaryotype(),
      ])
      ->execute();
  }

  /**
   * Remove a CellLineClone entry from the database.
   *
   * @param int $clone_id
   *   ID of the respective CellLineClone object.
   */
  public static function delete($clone_id) {
    db_delete(self::$tableName)
      ->condition('id', $clone_id, '=')
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLineClone object.
   *
   * @param \stdClass $result
   *   Database result of a finder function.
   *
   * @return \CellLineClone
   *   New CellLineClone object.
   */
  public static function databaseResultsToClone($result) {
    $clone = new CellLineClone();

    if (empty($result)) {
      return $clone;
    }

    // Set the variables.
    $clone->setId($result->id);
    $clone->setCellLine($result->cell_line);
    $clone->setCloneNumber($result->clone_number);
    $clone->setGenomicModification($result->genomic_modification);
    $clone->setHdrNhej($result->hdr_nhej);
    $clone->setZygosity($result->zygosity);
    $clone->setSpecification($result->specification);
    $clone->setVisibility($result->visibility);
    $clone->setVisibilityClone($result->visibility_clone);
    $clone->setCloneNumberStarlims($result->clone_number_starlims);
    $clone->setCultureMedium($result->culture_medium);
    $clone->setCharacterization($result->characterization);
    $clone->setStartDate($result->start_date);
    $clone->setResponsible($result->responsible);
    $clone->setMorphology($result->morphology);
    $clone->setPluriotencyPcr($result->pluriotency_pcr);
    $clone->setPluriotencyImmuno($result->pluriotency_immuno);
    $clone->setPluriotencyFacs($result->pluriotency_facs);
    $clone->setDifferentiationImmuno($result->differentiation_immuno);
    $clone->setHlaAnalyzed($result->hla_analyzed);
    $clone->setMutationAnalyzed($result->mutation_analyzed);
    $clone->setEditingAnalyzed($result->editing_analyzed);
    $clone->setStrAnalyzed($result->str_analyzed);
    $clone->setKaryoAnalyzed($result->karyo_analyzed);
    $clone->setKaryotype($result->karyotype);

    return $clone;
  }

  /**
   * Read database results and create an array with CellLineClone
   * objects.
   *
   * @param \DatabaseStatementInterface $results
   *   Database result of a finder function.
   *
   * @return \CellLineClone[]
   *   New CellLineClone objects.
   */
  public static function databaseResultsToClones($results) {
    $clones = [];
    foreach ($results as $result) {
      $clones[] = self::databaseResultsToClone($result);
    }

    return $clones;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Return CellLineClone of given CellLine (Database ID).
   *
   * @param int $cell_line_id
   *   The ID of the given CellLine.
   *
   * @return \CellLineClone[]
   *  Found CellLineClone objects.
   */
  public static function findByCellLineId($cell_line_id, $header = []) {
    $result = db_select(self::$tableName, 't')
      ->condition('cell_line', $cell_line_id, '=')
      ->fields('t', self::$databaseFields)
      ->extend('TableSort')
      ->orderByHeader($header)
      ->execute();

    return self::databaseResultsToClones($result);
  }

  /**
   * Return all CellLineClones.
   *
   * @return \CellLineClone[]
   *  Found CellLineClone objects.
   */
  public static function findAll($header = []) {
    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->extend('TableSort')
      ->orderByHeader($header)
      ->execute();

    return self::databaseResultsToClones($result);
  }
}
