# Authors and contributors of the menoci project

## Current and past maintainers

Ordered chronologically (descending) beginning 
with the current maintainer on top.

* [Christian Henke, 0000-0002-4541-4018](https://orcid.org/0000-0002-4541-4018)
* [Linus Weber, 0000-0001-7973-7491](https://orcid.org/0000-0001-7973-7491)
* [Luca Freckmann, 0000-0002-8285-2586](https://orcid.org/0000-0002-8285-2586)
* [Markus Suhr, 0000-0002-6307-3253](https://orcid.org/0000-0002-6307-3253)
* [Christoph Lehmann, 0000-0002-6801-7170](https://orcid.org/0000-0002-6801-7170)
* [Bartlomiej Marzec, 0000-0002-9640-8881](https://orcid.org/0000-0002-9640-8881)
* Alexander Wildschütz

## Other contributors

Ordered alphabetically (by surname):



* Leonhard Braun
* [Georg Aschenbrandt, 0000-0003-0333-3364](https://orcid.org/0000-0003-0333-3364)
* Marcel Hellkamp
* Vanessa Klauenberg
* Lea Katharina Kühlborn
* [Harald Kusch, 0000-0002-9895-2469](https://orcid.org/0000-0002-9895-2469)
* Roman Paul
* Georg Rakovszky
* [Sophia Rheinländer, 0000-0002-5406-4427](https://orcid.org/0000-0002-5406-4427)
* Jonas Adrian Rieling
* Nils Rosenboom
* Marcel Schneider
* Maik Taylor
* Savvas Tellidis
* [Oliver Wannenwetsch, 0000-0001-6629-7781](https://orcid.org/0000-0001-6629-7781)
* [Philipp Wieder, 0000-0002-6992-1866](https://orcid.org/0000-0002-6992-1866)
