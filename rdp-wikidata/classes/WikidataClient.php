<?php
/**
 * @file Encapsulates the "freearhey/wikidata" library for usage within the
 *   RDP-Wikidata module.
 *
 * https://github.com/freearhey/wikidata
 */

/**
 * Class WikidataClient
 */
class WikidataClient {

  const WIKIDATA_BASE_URL = 'https://www.wikidata.org/wiki/';

  const PROPERTY_DOI = 'P356';

  const PROPERTY_PMID = 'P698';

  /**
   * @var \Wikidata\Wikidata $wikidata
   */
  private $wikidata;

  public function __construct() {
    RDPWikidataCommons::autoload();
    $this->wikidata = new \Wikidata\Wikidata();
  }

  /**
   * @param string $id A wikidata ID.
   *
   * @return string URL for the external Wikidata entry page.
   */
  public static function idToUrl($id) {
    return self::WIKIDATA_BASE_URL . $id;
  }

  /**
   * @param string $doi A digital object identifier.
   *
   * @return string|bool A Wikidata identifier on success, FALSE otherwise.
   */
  public function findByDoi($doi) {
    return $this->findIdByProperty(self::PROPERTY_DOI, strtoupper($doi));
  }

  /**
   * @param string $pmid A PubMed ID.
   *
   * @return string|bool A Wikidata identifier on success, FALSE otherwise.
   */
  public function findByPMID($pmid) {
    return $this->findIdByProperty(self::PROPERTY_PMID, $pmid);
  }

  /**
   * @param string $property Wikidata property to search by, i.e. DOI is 'P356'
   * @param string $value Value of the property to find
   *
   * @return string|bool A Wikidata identifier on success, FALSE otherwise.
   */
  private function findIdByProperty($property, $value) {
    try {
      $result = $this->wikidata->searchBy($property, $value);

      if (!$result->isEmpty()) {
        return $result->first()->id;
      }
    } catch (Exception $e) {
      watchdog_exception(__CLASS__, $e);
    }
    return FALSE;
  }
}