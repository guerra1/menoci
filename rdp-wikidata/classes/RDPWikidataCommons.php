<?php
/** @file Common resources for RDP-Wikidata module.
 */

/**
 * Class RDPWikidataCommons
 */
abstract class RDPWikidataCommons {

  const MODULE_NAME = 'rdp_wikidata';

  private static $module_path = '';

  /**
   * Return file system path to rdp_wikidata module
   *
   * @return string file system path
   */
  public static function module_path() {
    if (empty(self::$module_path)) {
      self::$module_path = drupal_get_path('module', self::MODULE_NAME);
    }
    return self::$module_path;
  }

  /**
   * Trigger autoloading for composer-installed dependencies.
   */
  public static function autoload() {
    include_once(self::module_path()
      . '/vendor/autoload.php');
  }

  /**
   * @param string $type Which type of resource path is requested, resource
   *   directory path is returned by default.
   *
   * @return string
   */
  public static function path_resources($type = '') {
    $path = base_path() . self::module_path()
      . '/resources/';

    if ($type == 'icon') {
      $path .= 'wikidata_logo.svg';
    }
    elseif ($type == 'icon_scholia') {
      $path .= 'scholia_logo.png';
    }

    return $path;
  }

  /**
   * @return string HTML image tag for an inline Wikidata logo icon that floats
   *   to the right.
   */
  public static function icon_wikidata_inline_right() {
    $style = "vertical-align: top; float: right;";
    $icon = self::icon_basic_element();
    $icon['attributes']['style'] = $style;
    return theme_image($icon);
  }

  /**
   * @return string HTML image tag for an inline Wikidata logo icon.
   */
  public static function icon_wikidata_inline() {
    return theme_image(self::icon_basic_element());
  }

  /**
   * @return string HTML image tag for inline Scholia icon.
   */
  public static function icon_scholia_inline() {
    $icon = self::icon_basic_element();
    $icon['path'] = self::path_resources('icon_scholia');
    $icon['title'] = 'Scholia';
    $icon['alt'] = 'Scholia';
    return theme_image($icon);
  }

  /**
   * @return array Drupal theme_image render element for Wikidata logo icon
   */
  private static function icon_basic_element() {
    return [
      'path' => self::path_resources('icon'),
      'height' => '16px',
      'title' => 'Wikidata',
      'alt' => 'Wikidata',
      'attributes' => [
        'style' => "vertical-align: top;",
      ],
    ];
  }

  /**
   * Returns internal ID of the Subject defined by the External-ID module. @return int
   *
   * @see \ExternalIDSubject
   *
   */
  public static function rdp_external_id_wikidata_id_type() {
    return "Wikidata ID";
  }

  /**
   * Check if a Wikidata ID exists for a \Publication based on PubMed-ID and/or DOI
   *
   * @param int $publication_id
   *
   * @return bool|string
   */
  public static function publication_fetch_wikidata_id($publication_id) {

    $type = RDPWikidataCommons::rdp_external_id_wikidata_id_type();
    $subject = PublicationExternalIdentifier::EX_ID_SUBJECT;
    $exid = ExternalIDRepository::findByTypeSubjectId($type, $subject, $publication_id);
    /**
     * @var \ExternalID $exid
     */
    if ($exid) {
      $wikidata_id = $exid->getValue();
    }
    else {
      /**
       * Try to find Wikidata ID for the publication by DOI, then PMID.
       */
      $publication = PublicationRepository::findById($publication_id);
      $client = new WikidataClient();
      $wikidata_id = $client->findByDoi($publication->getDOI());
      if (!$wikidata_id) {
        $wikidata_id = $client->findByPMID($publication->getPMID());
      }
      if ($wikidata_id) {
        /**
         * Store \ExternalIdentifier for future use.
         */
        $exid = new ExternalID();
        $exid->setSubject(ExternalIDSubjectRepository::findBySubject(PublicationExternalIdentifier::EX_ID_SUBJECT)
          ->getId());
        $exid->setSubjectId($publication_id);
        $exid->setType(ExternalIDTypeRepository::findByLabel(RDPWikidataCommons::rdp_external_id_wikidata_id_type())
          ->getId());
        $exid->setValue($wikidata_id);
        $exid->save();
        $pub_exid = new PublicationExternalIdentifier();
        $pub_exid->setExternalId($exid->getId());
        $pub_exid->setDescription("Wikidata ID");
        $pub_exid->save();
      }
    }
    return $wikidata_id;
  }
}