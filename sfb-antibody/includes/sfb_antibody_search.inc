<?php

function sfb_antibody_search() {

  $search_query = $_GET['search_query'];
  $search_params = explode(' ', $search_query);

  $structured_params = array(
    'string' => array(),
    'fields' => array(),
  );

  // use db_and as default condition
  $db_main_condition = db_and();
  // free text search conditions are queried by using 'OR' db condition
  $db_freetext_condition = db_or();

  for($i = 0 ; $i < count($search_params); $i++) {
    $param = $search_params[$i];

    //
    // look for parameters
    //

    // brackets regex: (\(([^()]|(?R))*\))

    //TODO: another antibody fields
    //TODO: application (right join needed!)

    if(preg_match('/^(?:\[type\])=([^\\-_]+)$/i', $param, $match)) {
      $db_main_condition->condition('type',$match[1],'=');
      $structured_params['fields'][] = array ('type' => $match[1]);

    } else if(preg_match('/^(?:\[sharing_level\])=([^\\-_]+)$/i', $param, $match)) {

      if($match[1] == 'private') {
        $db_main_condition->condition('sharing_level', AntibodySharingLevel::PRIVATE_LEVEL,'=');
      } else if($match[1] == 'group') {
        $db_main_condition->condition('sharing_level', AntibodySharingLevel::GROUP_LEVEL,'=');
      } else if($match[1] == 'public') {
        $db_main_condition->condition('sharing_level', AntibodySharingLevel::PUBLIC_LEVEL,'=');
      } else if($match[1] == 'site') {
        $db_main_condition->condition('sharing_level', AntibodySharingLevel::SITE_LEVEL,'=');
      } else {
        $db_main_condition->condition('sharing_level', $match[1], '=');
      }
      $structured_params['fields'][] = array ('sharing_level' => $match[1]);

    } else if(preg_match('/^(?:\[antigen_symbol\])=([^\\-_]+)$/i', $param, $match)) {
      $db_main_condition->condition('antigen_symbol',$match[1],'=');
      $structured_params['fields'][] = array ('antigen_symbol' => $match[1]);

    } else if(preg_match('/^(?:\[antibody_registry_id\])=(.+)$/i', $param, $match)) {
      $db_main_condition->condition('registry_id',$match[1],'=');
      $structured_params['fields'][] = array ('antibody_registry_id' => $match[1]);

    } else if(preg_match('/^(?:\[name\])=([^\\-_]+)$/i', $param, $match)) {
      $db_main_condition->condition('name',$match[1],'=');
      $structured_params['fields'][] = array ('name' => $match[1]);

    } else if(preg_match('/^(?:\[alt_name\])=([^\\-_]+)$/i', $param, $match)) {
      $db_main_condition->condition('alt_name',$match[1],'=');
      $structured_params['fields'][] = array ('alt_name' => $match[1]);

    } else if(preg_match('/^(?:\[clonality\])=([^\\-_]+)$/i', $param, $match)) {
      $db_main_condition->condition('clonality',$match[1],'=');
      $structured_params['fields'][] = array ('clonality' => $match[1]);

    } else if(preg_match('/^(?:\[antigen\])=([^\\-_]+)$/i', $param, $match)) {
      $db_main_condition->condition('antigen',$match[1],'=');
      $structured_params['fields'][] = array ('antigen' => $match[1]);
    } else if(preg_match('/^(?:\[company\])=([^\\-_]+)$/i', $param, $match)) {
      $db_main_condition->condition('company',$match[1],'=');
      $structured_params['fields'][] = array ('company' => $match[1]);
    }  else if(preg_match('/^(?:\[research_group\])=([^\\-_]+)$/i', $param, $match)) {
      $structured_params['fields'][] = array ('research_group' => $match[1]);
      //TODO: not yet implemented
      $db_main_condition->condition('working_group_id',$match[1],'=');
      $structured_params['fields'][] = array ('research_group' => $match[1]);
    }  else {

      // free text search over following fields:
      // - antigen_symbol
      // - name
      // - alternative name
      // - antigen
      // - antibody registry id
      $db_freetext_condition->condition('antigen_symbol', '%'.db_like($param).'%', 'LIKE');
      $db_freetext_condition->condition('name', '%'.db_like($param).'%', 'LIKE');
      $db_freetext_condition->condition('alt_name', '%'.db_like($param).'%', 'LIKE');
      $db_freetext_condition->condition('antigen', '%'.db_like($param).'%', 'LIKE');
      $db_freetext_condition->condition('clonality', '%'.db_like($param).'%', 'LIKE');
      $db_freetext_condition->condition('registry_id', '%'.db_like($param).'%', 'LIKE');
      $db_freetext_condition->condition('company', '%'.db_like($param).'%', 'LIKE');
      $db_freetext_condition->condition('catalog_no', '%'.db_like($param).'%', 'LIKE');
      $db_freetext_condition->condition('description', '%'.db_like($param).'%', 'LIKE');
      $structured_params['string'][] = $param;
    }
  }
  // Export button
  $btn_export = l('<span class="glyphicon glyphicon-share"></span> Export',
    sfb_antibody_url(SFB_ANTIBODY_URL_DOWNLOAD_EXCEL), [
      'html' => TRUE,
      'attributes' => [
        'data-toggle' => 'tooltip',
        'data-placement' => 'right',
        'title' => "Download current search result as Excel sheet.",
        'class' => ['btn btn-default btn-xs'],
        'style' => 'position: absolute; right: 15px;margin: 3px',
      ],
    ]);
  $output = '';

  // print search box
  $output .= $btn_export;
  $search_form = drupal_get_form('sfb_anitbody_form_search', $search_query);
  $output .= drupal_render($search_form);


  // print explanation
  $output .= 'Searched for:';

  //print free text
  foreach($structured_params['string'] as $search_str)
    $output .= '<code>'.check_plain($search_str).'</code>, ';


  //print field parameters
  foreach($structured_params['fields'] as $field) {
    foreach($field as $key => $value)
      $output .= ' <code>'.print_r($key, true).'='.print_r($value, true).'</code> ';
  }

  if(count($structured_params['string']) > 0)
    $db_main_condition->condition($db_freetext_condition);
  $antibodies = AntibodiesRepository::findBy($db_main_condition);

  $output .= 'and found <strong>'.count($antibodies).'</strong> matches.';

  module_load_include('inc', 'sfb_antibody', 'includes/session');
  lists_session("antibodies", $antibodies);

  //
  // prepare and print the primary antibodies table
  //

  // table header
  // columns with 'field' definition can be sorted
  $header = array(
    array('data' => t('AG')),
    array('data' => t('PID')),
    array('data' => t('Antigen Symbol')),
    array('data' => t('Antibody Registry')),
    array('data' => t('Name')),
    array('data' => t('Clonality')),
    array('data' => t('Antigen')),
    array('data' => t('Quality')),
    array('data' => t('Company')),
    array('data' => t('Catalog no.')),
  );

  // create table rows
  $rows = array();

  // each table row is a table row
  foreach($antibodies as $antibody) {

    // prepare an array with applications and their quality
    // variable $apps_arr will be later on used for quality collapse theme
    // @see theme_sfb_antibody_quality_collapsible()
    $apps_arr = array();
    $apps = AntibodyAntibodyApplicationsRepository::findByAntibodyIdGroupByApplication($antibody->getId());
    foreach($apps as $app) {
      $apps_arr[] = array(
        'quality' => $antibody->getApplicationQuality($app->getApplicationAbbr()),
        'application_abbr' => $app->getApplicationAbbr(),
      );
    }

    $header = array(
      array('data' => t('AG')),
      array('data' => t('PID')),
      array('data' => t('Type')),
      array('data' => t('Antigen Symbol')),
      array('data' => t('Antibody Registry')),
      array('data' => t('Name')),
      array('data' => t('Clonality')),
      array('data' => t('Antigen')),
      array('data' => t('Quality')),
      array('data' => t('Company')),
      array('data' => t('Catalog no.')),
    );

    // fill table rows with antibody data
    $rows[] = array(
      // research group icon
      $antibody->getElementWorkingGroupIcon(),
      // field: antibody pid and link to antibody view
      l($antibody->getElementPID(), sfb_antibody_url_antibody_view($antibody)),
      //field: type
      $antibody->getElementType(),
      // field: antigen symbol
      _sfb_antibody_search_mark_found_string($structured_params,$antibody->getElementAntigenSymbol()),
      // field: antibody registry
      _sfb_antibody_search_mark_found_string($structured_params,$antibody->getElementRegistryId(true)),
      // field: name
      _sfb_antibody_search_mark_found_string($structured_params, $antibody->getElementName()),
      //field: clonality
      _sfb_antibody_search_mark_found_string($structured_params, $antibody->getElementClonality()),
      // field: antigen
      _sfb_antibody_search_mark_found_string($structured_params, $antibody->getElementAntigen()),
      // collapsible field with applications rating
      array('data' => theme('sfb_antibody_quality_collapsible', array('antibody_id'=> $antibody->getId(), 'applications_quality' => $apps_arr)), 'style' => 'min-width: '.variable_get(SFB_ANTIBODY_CONFIG_STYLE_QUALITY_WIDTH, 100).'px;' ),
      //field: company
      _sfb_antibody_search_mark_found_string($structured_params, $antibody->getElementCompany()),
      // field: catalog no.
      _sfb_antibody_search_mark_found_string($structured_params, $antibody->getCatalogNo()),
    );
  }

  // print main table and pager
  $output .= theme('table', array('header' => $header, 'rows' => $rows));


  return $output;
}

function sfb_antibody_search_help() {
  $output = '';

  $output .= 'In following you can find information to help you easily find antibodies in Antibody Catalogue';
  $output .= '<h2>Search parameters</h2>';

  $output .= '<h3>Free text search</h3>';


  return $output;
}

function _sfb_antibody_search_mark_found_string($structured_params, $string) {

  //check free text
  foreach($structured_params['string'] as $query) {
    $string = str_replace($query, '<span style="color: #c7254e; background-color: #f9f2f4">'.$query.'</span>', $string);
  }

  //TODO: farben für fields search

  return $string;
}
