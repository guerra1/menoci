<?php

/**
 * Drupal form for creating new primary antibody.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function sfb_antibody_primary_new($form, &$form_state) {

  // if user has no registrar permission than he cannot create any antibodies
  if(!user_access(SFB_ANTIBODY_PERMISSION_REGISTRAR)) {
    drupal_access_denied();
    exit();
  }

  // create new temporary antibody
  // this object is not used for storing data, but for rendering of fields.
  $antibody = new Antibody();
  $images = [];

  //
  // prepare new antibody form
  //

  $form['fieldset-general'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('General'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['fieldset-general']['antigen_symbol'] = $antibody->getFormFieldAntigenSymbol();
  $form['fieldset-general']['registry_id'] = $antibody->getFormFieldRegistryId();
  $form['fieldset-general']['antibodypedia_url'] = $antibody->getFormFieldAntibodypediaUrl();
  $form['fieldset-general']['name'] = $antibody->getFormFieldName();
  $form['fieldset-general']['alt_name'] = $antibody->getFormFieldAltName();
  $form['fieldset-general']['lab_id'] = $antibody->getFormFieldLabId();

  $form['fieldset-details'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Details'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['fieldset-details']['tag'] = $antibody->getFormFieldTag();    //
  $form['fieldset-details']['raised_in_species'] = $antibody->getFormFieldRaisedInSpecies();
  $form['fieldset-details']['reacts_with'] = $antibody->getFormFieldReactsWith();
  $form['fieldset-details']['clone'] = $antibody->getFormFieldClone();
  $form['fieldset-details']['isotype'] = $antibody->getFormFieldIsotype();
  $form['fieldset-details']['clonality'] = $antibody->getFormFieldClonality();
  $form['fieldset-details']['demasking'] = $antibody->getFormFieldDemasking();
  $form['fieldset-details']['antigen'] = $antibody->getFormFieldAntigen();

  $form['fieldset-origin'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Origin'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-origin']['crafted_by'] = $antibody->getFormFieldCraftedBy();
  $form['fieldset-origin']['company'] = $antibody->getFormFieldCompany();
  $form['fieldset-origin']['catalog_no'] = $antibody->getFormFieldCatalogNo();
  $form['fieldset-origin']['lot_no'] = $antibody->getFormFieldLotNo();

  $form['fieldset-description'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Description'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-description']['description'] = $antibody->getFormFieldDescription();
  $form['fieldset-description']['localization'] = $antibody->getFormFieldLocalization();
  $form['fieldset-description']['storage_instruction'] = $antibody->getFormFieldStorageInstruction();
  $form['fieldset-description']['receipt_date'] = $antibody->getFormFieldReceiptDate();
  $form['fieldset-description']['preparation_date'] = $antibody->getFormFieldPreparationDateNew();

  $form['fieldset-settings'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Settings'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-settings']['working_group_id'] = $antibody->getFormFieldWorkingGroup();
  $form['fieldset-settings']['sharing_level'] =  $antibody->getFormFieldSharingLevel();

  $form['fieldset-image'] = array(
    '#type' => 'fieldset',
    '#title' => t('Attach Images'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="image-fieldset-wrapper">',
    '#suffix' => '&nbsp;</div>',
  );

  // set the antibodies and the count once when loading the page first
  if (!isset($form_state['images'])) {
    $form_state['images'] = AntibodyImageRepository::findByAntibodyId($antibody->getId());
  }

  if (empty($form_state['count_images'])) {
    $form_state['count_images'] = count($form_state['images']);
  }

  // $form_state['add'] defines if the add button is shown
  if (!isset($form_state['add'])) {
    $form_state['add'] = TRUE;
  }

  // load all image upload forms
  for ($i = 0; $i < $form_state['count_images']; $i++) {
    // if editing an antibody, take the saved images from the db
    if (!isset($form_state['images'][$i])) {
      $form_state['images'][$i] = new AntibodyImage();
    }

    $form['fieldset-image']['fieldset-new-image'][$i] =
      $form_state['images'][$i]->getFormFieldImageFieldset($i);

    $form['fieldset-image']['fieldset-new-image'][$i]['file_'.$i] =
      $form_state['images'][$i]->getFormFieldImageUpload($i);

    $form['fieldset-image']['fieldset-new-image'][$i]['description_'.$i] =
      $form_state['images'][$i]->getFormFieldImageDescription();

    $form['fieldset-image']['fieldset-new-image'][$i]['comment_'.$i] =
      $form_state['images'][$i]->getFormFieldImageComment();

    // this is only true after the rebuild initiated by uploading an image, thus
    // the FID of the uploaded image is saved here
    if (isset($form_state['values']['file_'.$i])) {
      $form_state['images'][$i]->setFid($form_state['values']['file_'.$i]);
      $form_state['add'] = TRUE;
    }

    $form['fieldset-image']['fieldset-new-image'][$i]['preview_'.$i] =
      $form_state['images'][$i]->getFormFieldImagePreview();

  }

  // only render add button if $form_state['add'] is TRUE
  if ($form_state['add']) {
    $form['fieldset-image']['image_add'] = $antibody->getFormFieldImageAdd();
  }

  //
  // buttons
  //

  $form['cancel'] = array(
    '#type' => 'button',
    '#submit' => array('sfb_antibody_primary_new_cancel'),
    '#value' => t('Cancel'),
    '#executes_submit_callback' => true,
    '#limit_validation_errors' => array(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save'
  );

  $form['submit_and_new'] = array(
      '#type' => 'submit',
      '#value' => 'Save and create new',
      '#submit' => array('sfb_antibody_primary_new_submit_and_new'),
      '#weight' => 1,
  );

  return $form;
}

/**
 * Return the desired part of the page (wrapper).
 */
function ajax_callback ($form, $form_state) {
  return $form['fieldset-image'];
}

/**
 * Handle what happens on an AJAX submit e.g. button press.
 */
function ajax_submit ($form, &$form_state) {
  // If the add button is pressed, increment the image field counter which gets added after the rebuild.
  if ($form_state['triggering_element']['#name'] == 'AddImageButton') {
    if (!isset($form_state['count_images'])) {
      $form_state['count_images'] = 0;
      $form_state['count_images']++;
    }
    $form_state['count_images']++;
    // Set this to false so only another image field can be added if the current new empty one was used to upload an image.
    $form_state['add'] = FALSE;
  }
  // This gets triggered if the Remove button is pressed (the only other AJAX button)
  else {
    // in the button name is the $i of the respective image field saved
    // (name example: file_0_remove_button
    // -> get the 0 (or n) out of it
    $button_name = $form_state['triggering_element']['#name'];
    $i = explode('_',$button_name)[1];
    $img = $form_state['images'][$i];
    // load the file so it can be deleted
    $file = file_load($img->getFid());
    file_delete($file);

    // also delete the database entry of the image
    if($img->getId()) {
      AntibodyImageRepository::delete($img->getId());
    }

    // reduce the number of image fields by one
    $form_state['count_images']--;


    $new_arr = [];
    foreach ($form_state['images'] as $key=>$img) {
      // save the comment and description so it can be loaded later again (since the form_states have to be unset)
      if (isset($form_state['input']['description_'.$key])) {
        $form_state['images'][$key]->setDescription($form_state['input']['description_'.$key]);
      }
      if (isset($form_state['input']['comment_'.$key])) {
        $form_state['images'][$key]->setComment($form_state['input']['comment_'.$key]);
      }

      // unset all input and value form fields
      unset($form_state['input']['file_'.$key]);
      unset($form_state['values']['file_'.$key]);
      unset($form_state['input']['description_'.$key]);
      unset($form_state['values']['description_'.$key]);
      unset($form_state['input']['comment_'.$key]);
      unset($form_state['values']['comment_'.$key]);
      unset($form_state['input']['preview_'.$key]);
      unset($form_state['values']['preview_'.$key]);

      // build the new array, save all images except the one just deleted
      if ($key == $i ) {
        continue;
      }
      else {
        $new_arr[] = $img;
      }
    }

    $form_state['images'] = $new_arr;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Validation function checks whether the there are any other symbols than integers included in the element
 *
 * @param $element
 * @param $form_state
 */
function sfb_antibody_primary_new_validate_integer($element, &$form_state){
  $value = $element['#value'];
  if($value != '' && (!is_numeric($value) || intval($value) != $value  || $value <= 0)){
    form_error($element, t('%name must be a positive integer.', array('%name' => $element['#title'])));
  }
}

/** implements routine to save primary antibody
 * @param $form
 * @param $form_state
 * @return integer
 */
function sfb_antibody_primary_new_save_routine($form, &$form_state){
    global $user;

    // create new empty antibody instance
    $antibody = new Antibody();
    $antibody->setType(AntibodyType::PRIMARY);

    // fill new antibody object with form data
    $antibody->setAntigenSymbol($form_state['values']['antigen_symbol']);
    $antibody->setLabId($form_state['values']['lab_id']);
    $antibody->setRegistryId($form_state['values']['registry_id']);
    $antibody->setSharingLevel($form_state['values']['sharing_level']);
    $antibody->setWorkingGroupId($form_state['values']['working_group_id']);
    $antibody->setName($form_state['values']['name']);
    $antibody->setAlternativeName($form_state['values']['alt_name']);
    $antibody->setDemasking($form_state['values']['demasking']);
    $antibody->setClonality($form_state['values']['clonality']);
    $antibody->setCatalogNo($form_state['values']['catalog_no']);
    $antibody->setLotNo($form_state['values']['lot_no']);
    $antibody->setAntigen($form_state['values']['antigen']);
    $antibody->setDescription($form_state['values']['description']);
    $antibody->setLocalization($form_state['values']['localization']);
    $antibody->setStorageInstruction($form_state['values']['storage_instruction']);
    $antibody->setCompany($form_state['values']['company']);
    $antibody->setCraftedBy($form_state['values']['crafted_by']);
    $antibody->setRaisedInSpecies($form_state['values']['raised_in_species']);
    $antibody->setReactsWith($form_state['values']['reacts_with']);
    $antibody->setIsotype($form_state['values']['isotype']);
    $antibody->setTag($form_state['values']['tag']);
    $antibody->setClone($form_state['values']['clone']);
    $antibody->setCreatedBy($user->uid);
    $antibody->setLastModified($user->uid);
    $antibody->setAntibodypediaUrl($form_state['values']['antibodypedia_url']);
    $antibody->setCreatedDate(date('Y-m-d'));

  // save all values as image objects from the image fields into an array to be saved into the db later
  $images = [];
  for ($i = 0; $i < $form_state['count_images']; $i++) {
    if (isset ($form_state['values']['file_'.$i]) and
      $form_state['values']['file_'.$i] !== 0) {
      $image = new AntibodyImage();
      $image->setId($form_state['images'][$i]->getId());
      $image->setFid($form_state['values']['file_'.$i]);
      $image->setDescription($form_state['values']['description_'.$i]);
      $image->setComment($form_state['values']['comment_'.$i]);
      $image->setImageType('ab');
      if ($form_state['images'][$i]->getUploader()) {
        $image->setUploader($form_state['images'][$i]->getUploader());
      }
      else {
        $image->setUploader($user->uid);
      }
      if ($form_state['images'][$i]->getUploadDate()) {
        $image->setUploadDate($form_state['images'][$i]->getUploadDate());
      }
      else {
        $image->setUploadDate(date(DEFAULT_DATETIME_FORMAT));
      }
      $images[] = $image;
    }
  }
  $antibody->setImages($images);

    //
    // prepare an array with species reacting with this antibody
    //
    $fvalue_species_reacting_with = $form_state['values']['reacts_with'];
    // since select2 library (which is used for entering 'reacts_with' values)
    // uses ids of known options, we need to distinguish between ids and entered
    // new species, that's why findById function is used
    $species_reacting_with = array();
    foreach($fvalue_species_reacting_with as $item) {
        $species = AntibodySpeciesRepository::findById($item);
        if($species->isEmpty()) {
            $species = new AntibodySpecies();
            $species->setName($item);
        }

        $species_reacting_with[] = $species;
    }
    $antibody->setReactsWith($species_reacting_with);

    // read data from receipt date field and set receiptdate class variable
    $receipt_date = $form_state['values']['receipt_date'];
    $antibody->setReceiptDate(
        $receipt_date['year'] . '-' .
        $receipt_date['month'] . '-' .
        $receipt_date['day']
    );

    //read data from preparation date field and set preparation date class variable
    $preparation_date = $form_state['values']['preparation_date'];
    $antibody->setPreparationDate(
        $preparation_date['year'] . '-' .
        $preparation_date['month'] . '-' .
        $preparation_date['day']
    );

    // save antibody object into the database
    $antibody->save();

    // create new log message
    $log = new AntibodyLog();
    $log->setMessage('Antibody created');
    $log->setAntibodyId($antibody->getId());
    $log->setActionType(AntibodyLogActionType::CREATE);
    $log->save();

    // if antibody has been published, then store this to the log action
    if($antibody->getSharingLevel() == AntibodySharingLevel::PUBLIC_LEVEL) {
        $log = new AntibodyLog();
        $log->setMessage('Antibody has been published');
        $log->setAntibodyId($antibody->getId());
        $log->setActionType(AntibodyLogActionType::PUBLISH);
        $log->save();
    }

    // print user notification
    drupal_set_message('New antibody with ID '.l($antibody->getElementPID(), sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY_VIEW, $antibody->getElementPID())).' has been successfully stored.');

    //return antibody PID to redirect to antibody
    return $antibody->getElementPID();
}

/**
 * Implements hook_form_submit()
 *
 * @param $form
 * @param $form_state
 */
function sfb_antibody_primary_new_submit($form, &$form_state) {
    // call routine to save antibody into database and notify user
    $pid = sfb_antibody_primary_new_save_routine($form, $form_state);

  $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY_VIEW, $pid);
}
/**
 * after saving the entered antibody data
 * redirect to create new primary AB page
 *
 * @param $form
 * @param $form_state
 */
function sfb_antibody_primary_new_submit_and_new($form, &$form_state){
    // call routine to save antibody into database and notify user
    sfb_antibody_primary_new_save_routine($form, $form_state);

    $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY_NEW);

}

/**
 * Form 'Cancel' button handler
 *
 * @param $form
 * @param $form_state
 */
function sfb_antibody_primary_new_cancel($form, &$form_state) {

  // delete not saved antibody images
  $imgs = $form_state['images'];
  foreach ($imgs as $img) {
    if (!$img->getId()) {
      if ($img->getFid()) {
        $file = file_load($img->getFid());
        file_delete($file);
      }
    }
  }

  //redirect to antibody view page
  $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY);
}
