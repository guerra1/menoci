<?php

/**
 * @param $antibody_pid
 * @return string
 */
function sfb_antibody_secondary_view($antibody_pid) {

  // extract the antibody id from PID string
  $id = Antibody::extractIDFromPID($antibody_pid);

  if($id == NULL) {
    drupal_not_found();
    exit();
  }

  // read antibody data from database
  $antibody = AntibodiesRepository::findByIdAndType($id, AntibodyType::SECONDARY);

  if($antibody->isEmpty()) {
    drupal_not_found();
    exit();
  }

  // check if user has permission to view this antibody
  if(!$antibody->userHasPermissionTo(AntibodyAction::READ)) {
    drupal_access_denied();
    exit();
  }

  // increment view counter
  $antibody->incrementViews();

  // output variable with html code
  $output = '';

  $output .= '<div class="row">';
  $output .= '<div class="col-md-6">';

  //
  // table with basic data of this antibody
  //
  $datasheet_link = '';
  if(user_is_logged_in() || variable_get(SFB_ANTIBODY_CONFIG_DATASHEET_PUBLIC, 0)) {
    $datasheet_link .= '
      <a href="'.sfb_antibody_url(SFB_ANTIBODY_URL_DOWNLOAD_DATASHEET, $antibody->getId()).'">
      <img src="'.base_path().drupal_get_path('module', 'sfb_antibody').'/resources/pdf_25px.png" alt="" data-toggle="tooltip" title="Download PDF datasheet" style="float:right; margin: 5px;" />
      </a>
      ';
  }

  if(user_is_logged_in() || variable_get(SFB_ANTIBODY_CONFIG_JSON_PUBLIC, 0)) {
    $datasheet_link .= '
      <a href="'.sfb_antibody_url(SFB_ANTIBODY_API_ANTIBODY, $antibody->getElementPID()).'">
      <img src="'.base_path().drupal_get_path('module', 'sfb_antibody').'/resources/json_25px.png" alt="" data-toggle="tooltip" title="View JSON data" style="float:right; margin: 5px;" />
      </a>
      ';
  }

    $output .= '<h3>Basic data '.$datasheet_link.'</h3>';

  $tbl_general_rows = array();
  $tbl_general_rows[] = array('<strong>PID</strong>', $antibody->getElementPID());
  $tbl_general_rows[] = array('<strong>EPIC PID</strong>', l($antibody->getElementEPICPID(),$antibody->getElementEPICPID()));
  $tbl_general_rows[] = array('<strong>Research group</strong>', $antibody->getElementWorkingGroup());
  $tbl_general_rows[] = array('<strong>Quality (mean)</strong>', $antibody->getElementOverallQuality());
  $tbl_general_rows[] = array('<strong>Sharing level</strong>', $antibody->getElementSharingLevel());

  $output .= theme('table', array('header' => array(), 'rows' => $tbl_general_rows));

  //
  // antibody data
  //

  $output .= '<h3>Antibody data</h3>';

  $tbl_antibody_rows = array();
  $tbl_antibody_rows[] = array('<strong>Antibody Registry ID(s)</strong>',$antibody->getElementRegistryId());
  $tbl_antibody_rows[] = array('<strong>Name</strong>', $antibody->getElementName());
  $tbl_antibody_rows[] = array('<strong>Alternative name</strong>', $antibody->getAlternativeName());
  $tbl_antibody_rows[] = array('<strong>Lab ID</strong>', $antibody->getLabId());
  $tbl_antibody_rows[] = array('<strong>Tag / Fluorophore</strong>', $antibody->getElementTag());
  $tbl_antibody_rows[] = array('<strong>Raised in</strong>', $antibody->getElementRaisedIn());
  $tbl_antibody_rows[] = array('<strong>Clone</strong>', $antibody->getElementClone());
  $tbl_antibody_rows[] = array('<strong>Isotype</strong>', $antibody->getElementIsotype());
  $tbl_antibody_rows[] = array('<strong>Clonality</strong>', $antibody->getClonality());
  $tbl_antibody_rows[] = array('<strong>Antigen</strong>', $antibody->getElementAntigen());
  $tbl_antibody_rows[] = array('<strong>Excitation Max.</strong>', $antibody->getElementExcitationMax());
  $tbl_antibody_rows[] = array('<strong>Emission Max.</strong>', $antibody->getElementEmissionMax());

  $tbl_antibody_rows[] = array('<strong>Crafted By</strong>', $antibody->getElementCraftedBy());
  $tbl_antibody_rows[] = array('<strong>Company / Manufacturer</strong>', $antibody->getElementCompany());
  $tbl_antibody_rows[] = array('<strong>Catalog no.</strong>', $antibody->getElementCatalogNo());
  $tbl_antibody_rows[] = array('<strong>Lot no.</strong>', $antibody->getElementLotNo());

  $tbl_antibody_rows[] = array('<strong>Description</strong>', $antibody->getElementDescription());
  $tbl_antibody_rows[] = array('<strong>Localization</strong>', $antibody->getElementLocalization());
  $tbl_antibody_rows[] = array('<strong>Storage instruction</strong>', $antibody->getElementStorageInstruction());
  $tbl_antibody_rows[] = array('<strong>Receipt date</strong>', $antibody->getElementReceiptDate());
  $tbl_antibody_rows[] = array('<strong>Preparation date</strong>', $antibody->getElementPreparationDate());

  $tbl_antibody_rows[] = array('<strong>Created by</strong>', $antibody->getElementCreatedBy());
  $tbl_antibody_rows[] = array('<strong>Last modified</strong>', $antibody->getElementLastModified());

  $output .= theme('table', array('header' => array(), 'rows' => $tbl_antibody_rows));

  //
  // External links
  //

  $output .= '<h3>External links</h3>';

    $tbl_links_rows[] = [
        '<strong>Antibodypedia</strong>',
        $antibody->getElementAntibodypediaUrl(),
    ];
    $tbl_links_rows[] = ['<strong>HGNC</strong>', $antibody->getElementHGNCUrl()];
    $tbl_links_rows[] = [
        '<strong>Wikipedia</strong>',
        $antibody->getElementWikipediaUrl(),
    ];
    $tbl_links_rows[] = [
        '<strong>Uniprot</strong>',
        $antibody->getElementUniprotUrl(),
    ];
    $tbl_links_rows[] = [
        '<strong>GeneCards</strong>',
        $antibody->getElementGeneCardsUrl(),
    ];
    $tbl_links_rows[] = [
        '<strong>Antibody Registry</strong>',
        $antibody->getElementAntibodyRegistryUrls(),
    ];
  $output .= theme('table', array('header' => array(), 'rows' => $tbl_links_rows));

  if($antibody->userHasPermissionTo(AntibodyAction::WRITE))
    $output .= '<a href="'.sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY_EDIT, $antibody->getElementPID()).'" class="btn btn-primary">Edit</a>'; //TODO in antibody auslagern

  //
  // END OF LEFT COLUMN
  //
  $output .= '</div>';
  $output .= '<div class="col-md-6">';

  //
  // application ratings
  //
  $application_types = $antibody->getApplicationsDistinctList();

  $output .= '<h3>Applications</h3><hr />';

  if(count($application_types) == 0)
    $output .= '<p>No application comments yet.</p>';

  foreach($application_types as $application_type) {
    $applications = $antibody->getApplication($application_type->getApplicationAbbr());
    $application_type_quality = theme('sfb_antibody_quality', array('quality' => $antibody->getApplicationQuality($application_type->getApplicationAbbr()), 'quality_string' => true));

    $app = $application_type->getApplication();
    $output .= '<h4 id="'.$application_type->getApplicationAbbr().'">'.$app->getName().' ('.$application_type->getApplicationAbbr().') <small>'.$application_type_quality.' </small></h4>';

    $tbl_application_rows = array();
    $tbl_application_header = array(
      array('data' => 'AG', 'class' => array('col-md-1')),
      array('data' => 'Dilution', 'class' => array('col-md-1')),
      array('data' => 'Concentration', 'class' => array('col-md-1')),
      array('data' => 'Quality', 'class' => array('col-md-2')),
      array('data' => 'Comments', 'class' => array('col-md-6')),
      array('data' => '', 'class' => array('col-md-1')),
    );
    foreach($applications as $application) {
      $tbl_application_rows[] = array(
        $application->getElementWorkingGroupIcon(),
        $application->getDilution(),
        $application->getElementConcentration(),
        theme('sfb_antibody_quality', array('quality' => $application->getQuality())),
        $application->getComment(),
        $application->getElementEditButton(),
      );
    }

    $output .= theme('table', array('header' => $tbl_application_header, 'rows' => $tbl_application_rows));

    // Images table
    $images = AntibodyImageRepository::findByApplicationId($application->getId());
    $image_rows = [];
    foreach ($images as $image) {
      drupal_set_message('boop');
      $url = file_create_url(file_load($image->getFid())->uri);
      $image_button = '<span style="title = "' . t('DESCR') . '"><a href="'.$url.'" target="_blank"><span class="glyphicon glyphicon-picture"></span> View</a></span>';
      $image_rows [] = [
        $image_button,
        $image->getDescription(),
        $image->getComment(),
        UsersRepository::findByUid($image->getUploader())->getFullname(),
        $image->getUploadDate(),
      ];
    }

    if ($image_rows) {
      $output .= theme('table', [
        'header' => ['', 'Description', 'Comment', 'Uploader', 'Upload Date'],
        'rows' => $image_rows,
      ]);
    }
    $output .= ' <hr/>';
  }

  if(user_is_logged_in())
    $output .= '<a href="'.sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY_APPLICATION_NEW, $antibody->getElementPID()).'" class="btn btn-default">Add new application comment</a>';

    //
    // Linked publications
    //
    $output .= '<h3>Linked publications</h3><hr />';
    $publications = $antibody->getLinkedPublications();

    //TODO: das ist eine temporäre lösung:
    if (count($publications['table']['rows']) == 0) {
        $output .= 'There are no linked publications.';
    }
    else {
        foreach ($publications['table']['rows'] as $publication) {
            $output .= '<p>';
            /**
             * Make publication title a link, if URL is available (requires literature module version >=1.1)
             */
            if (isset($publication[8])) {
                $output .= l('<span style="font-size: 0.85em; font-weight: bold;">' . $publication[0] . '</span><br />', $publication[8], ['html' => TRUE]); //Title
            }
            else {
                $output .= '<span style="font-size: 0.85em; font-weight: bold;">' . $publication[0] . '</span><br />';
            }
            $output .= '<span style="font-size: 0.8em">' . $publication[1] . '</span><br />'; // Authors
            $output .= '<span style="font-size: 0.8em">' . $publication[6] . '</span> '; // Journal
            $output .= '<span style="font-size: 0.8em">' . $publication[4] . '</span> : '; // publication year
            $output .= '<span style="font-size: 0.8em"><a href="https://dx.doi.org/' . $publication[3] . '">' . $publication[3] . '</a></span>'; // doi
            $output .= '</p>';
        }
    }
  //
  // Attached Images
  //
  $images = AntibodyImageRepository::findByAntibodyId($antibody->getId());
  if (!empty($images)) {
    $output .= '<h3>Attached images</h3><hr />';
  }
  $img_output = '';
  $img_uri = '';
  foreach($images as $img) {
    if ($img->getFid()) {
      $file = file_load($img->getFid());
      $img_uri = $file->uri;
      $info = image_get_info($img_uri);
      $width = $info['width'];
      $height = $info['height'];
      $max_width = 550;
      if ($width > $max_width) {
        $ratio = $width/$height;
        $width = $max_width;
        $height = $width / $ratio;
      }

      $img_render = theme_image(['path' => $img_uri, 'width' => $width, 'height' => $height, 'alt' => 'antibody_image','attributes' => []]);
    }
    else {
      $img_render = '';
    }
    $userc = UsersRepository::findByUid($img->getUploader());

    $url = file_create_url($img_uri);
    $img_output .= '<a href="'.$url.'" target="_blank">'.$img_render.'</a>';
    $img_output.= '<div></div>';
    if ($img->getDescription()) {
      $img_output.= '<span style="font-size: 0.85em; font-weight: bold;">Description: </span><span style="font-size: 0.85em;">' .$img->getDescription().'</span><br>';
    }
    if ($img->getComment()) {
      $img_output.= '<span style="font-size: 0.85em; font-weight: bold;">Comment: </span><span style="font-size: 0.85em;">' .$img->getComment().'</span><br>';
    }
    $img_output.= '<span style="font-size: 0.85em; font-weight: bold;">Uploader: </span><span style="font-size: 0.85em;">' .$userc->getFullname().'</span><br>';
    $img_output.= '<span style="font-size: 0.85em; font-weight: bold;">Upload Date: </span><span style="font-size: 0.85em;">' .$img->getUploadDate().'</span><br>';
    $img_output.= '<div>&nbsp;</div>';
  }
  $output .= $img_output;
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}
