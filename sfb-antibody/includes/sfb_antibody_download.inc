<?php

/**
 * Download page
 *
 * @param $option string
 *    Download handler options
 */
function sfb_antibody_download($option) {
  if ($option == 'datasheet' && !empty(arg(3))) {
    return sfb_antibody_download_sheet(arg(3));
  }
  elseif ($option == 'excel') {
    return sfb_antibody_download_excel();
  }
  elseif (arg(2) == 'template') {
    return sfb_antibody_download_template();
  }
  else {
    drupal_not_found();
    exit();
  }
}

/**
 * @param $antibody_id int
 */
function sfb_antibody_download_sheet($antibody_id) {

  // get an antibody
  $antibody = AntibodiesRepository::findById($antibody_id);

  if ($antibody->isEmpty()) {
    drupal_not_found();
    exit();
  }

  // check user permission
  if (!$antibody->userHasPermissionTo(AntibodyAction::READ)) {
    drupal_not_found();
    exit();
  }

  // load pdf library
  module_load_include('php', 'sfb_commons', 'lib/tcpdf_min_6.2.12/tcpdf');
  module_load_include('php', 'sfb_commons', 'lib/fpdi_1.6.1/fpdi');

  class PDF extends FPDI {

    /**
     * "Remembers" the template id of the imported page
     */

    var $_tplIdx;

    var $templateFileName;

    /**
     * choose primary/secondary template based on antibody
     */
    function setTemplateFile($antibody_id) {
      $antibody = AntibodiesRepository::findById($antibody_id);

      if ($antibody->getType() == AntibodyType::PRIMARY) {
        $this->templateFileName = 'template_antibody_primary.pdf';
      }
      else {
        $this->templateFileName = 'template_antibody_secondary.pdf';
      }
    }

    /**
     * Draw an imported PDF logo on every page
     */
    function Header() {

      if (is_null($this->_tplIdx)) {
        $template_file = $_SERVER['DOCUMENT_ROOT'] . base_path() . drupal_get_path('module', 'sfb_antibody') . '/resources/' . $this->templateFileName;
        $this->setSourceFile($template_file);
        $this->_tplIdx = $this->importPage(1);
      }
      $tplIdx = $this->importPage(1, '/MediaBox');
      $size = $this->useTemplate($tplIdx, 0, 0, 0, 0, TRUE);
    }

    function Footer() {
      // position at 15 mm from bottom
      $this->SetY(-15);
      // set font
      $this->SetFont('helvetica', 'I', 8);
      // page number
      $this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, FALSE, 'C', 0, '', 0, FALSE, 'T', 'M');
    }

    function setTemplateFileName($filename) {
      $this->templateFileName = $filename;
    }
  }

  // initiate PDF object
  $pdf = new PDF();

  $pdf->setTemplateFile($antibody_id);
  $pdf->SetFont('sourcesanspro', '', 10, '', FALSE);
  $pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
  $pdf->SetAutoPageBreak(TRUE, 40);
  $pdf->setFontSubsetting(FALSE);

  if ($antibody->getType() == AntibodyType::PRIMARY) {
    _sfb_antibody_download_sheet_add_primary_antibody_data($pdf, $antibody);

  }
  else {
    _sfb_antibody_download_sheet_add_secondary_antibody_data($pdf, $antibody);
  }

  // prepare drupal for pdf output
  drupal_add_http_header('Access-Control-Allow-Origin', "*");
  drupal_add_http_header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  header('Content-Type: application/pdf');
  header('Content-Disposition: attachment;filename="antibody-datasheet-' . $antibody_id . '.pdf"');

  // print pdf file
  $pdf->Output();
  exit();

}

/**
 *
 * @param $pdf
 * @param $antibody \Antibody
 */
function _sfb_antibody_download_sheet_add_primary_antibody_data(&$pdf, $antibody) {
  $pdf->AddPage();

  // add page header: antibody pid and epic pid
  _sfb_antibody_download_sheet_add_primary_antibody_header($pdf, $antibody);

  // add basic antibody data
  _sfb_antibody_download_sheet_add_primary_antibody_basic_data($pdf, $antibody);

  $pdf->AddPage();

  // add page header: antibody pid and epic pid
  _sfb_antibody_download_sheet_add_primary_antibody_header($pdf, $antibody);

  //add applications data
  _sfb_antibody_download_sheet_add_primary_antibody_applications($pdf, $antibody);
}

/**
 *
 * @param $pdf
 * @param $antibody \Antibody
 */
function _sfb_antibody_download_sheet_add_secondary_antibody_data(&$pdf, $antibody) {
  $pdf->AddPage();

  // add page header: antibody pid and epic pid
  _sfb_antibody_download_sheet_add_secondary_antibody_header($pdf, $antibody);

  // add basic antibody data
  _sfb_antibody_download_sheet_add_secondary_antibody_basic_data($pdf, $antibody);

  $pdf->AddPage();

  // add page header: antibody pid and epic pid
  _sfb_antibody_download_sheet_add_secondary_antibody_header($pdf, $antibody);

  //add applications data
  _sfb_antibody_download_sheet_add_secondary_antibody_applications($pdf, $antibody);
}

/**
 * @param $pdf
 * @param $antibody
 */
function _sfb_antibody_download_sheet_add_primary_antibody_header(&$pdf, $antibody) {
  $html = '<span style="font-size: 1.8em;font-family: sourcesansprolight">' . $antibody->getElementPID() . '</span><br />';
  $html .= '<span style="font-size: 0.7em">' . $antibody->getElementEPICPID() . '</span><br />';
  $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
}

/**
 * @param $pdf
 * @param $antibody
 */
function _sfb_antibody_download_sheet_add_secondary_antibody_header(&$pdf, $antibody) {
  $html = '<span style="font-size: 1.8em;font-family: sourcesansprolight">' . $antibody->getElementPID() . '</span><br />';
  $html .= '<span style="font-size: 0.7em">' . $antibody->getElementEPICPID() . '</span><br />';
  $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');
}

/**
 * @param $pdf \TCPDF
 * @param $antibody \Antibody
 */
function _sfb_antibody_download_sheet_add_primary_antibody_basic_data(&$pdf, $antibody) {

  $html = '
    <table cellpadding="3">
      <tr><td style="border: 0.5px solid #e4dfcd; width: 25%; font-family: sourcesansprolight;">Antigen symbol</td><td style="border: 0.5px solid #e4dfcd; width:70%">' . $antibody->getElementAntigenSymbol() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Name</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementName() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Alternative name</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementAlternativeName() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Lab ID</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getLabId() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Clonality</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementClonality() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Antigen</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementAntigen() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Description</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementDescription() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Catalog no.</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementCatalogNo() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Lot no.</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementLotNo() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Tag</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementTag() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Reacts with</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementReactsWith() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Raised in</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementRaisedIn() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Demasking</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementDemasking() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Storage instruction</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementStorageInstruction() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Localization</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementLocalization() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Isotype</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementIsotype() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Receipt date</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementReceiptDate() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Preparation date</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementPreparationDate() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Crafted by</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementCraftedBy() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Company / Manufacturer</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementCompany() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Registry ID</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementRegistryId() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Antibodypedia URL</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementAntibodypediaUrl() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Created by</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementCreatedBy() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Last modified</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementLastModified() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Research Group</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementWorkingGroup() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Sharing level</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementSharingLevel() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
    </table>';

  $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');

}

/**
 * @param $pdf \TCPDF
 * @param $antibody \Antibody
 */
function _sfb_antibody_download_sheet_add_secondary_antibody_basic_data(&$pdf, $antibody) {

  $html = '
    <table cellpadding="3">
      <tr><td style="border: 0.5px solid #e4dfcd; width: 25%; font-family: sourcesansprolight;">Antigen symbol</td><td style="border: 0.5px solid #e4dfcd; width:70%">' . $antibody->getElementAntigenSymbol() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Name</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementName() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Alternative name</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementAlternativeName() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Clone</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementClone() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Antigen</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementAntigen() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Description</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementDescription() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Catalog no.</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementCatalogNo() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Lot no.</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementLotNo() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Tag</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementTag() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Excitation Max.</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementExcitationMax() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Emission Max.</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementEmissionMax() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Raised in</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementRaisedIn() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Storage instruction</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementStorageInstruction() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Localization</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementLocalization() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Receipt date</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementReceiptDate() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Preparation date</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementPreparationDate() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Crafted by</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementCraftedBy() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Company / Manufacturer</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementCompany() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Registry ID</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementRegistryId() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Antibodypedia URL</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementAntibodypediaUrl() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Created by</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementCreatedBy() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Last modified</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementLastModified() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Research Group</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementWorkingGroup() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Sharing level</td><td style="border: 0.5px solid #e4dfcd;">' . $antibody->getElementSharingLevel() . '</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
    </table>';

  $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');

}

/**
 * @param $pdf TCPDF
 * @param $antibody \Antibody
 */
function _sfb_antibody_download_sheet_add_primary_antibody_applications(&$pdf, $antibody) {

  $html = '';

  $application_types = $antibody->getApplicationsDistinctList();

  foreach ($application_types as $app_type) {
    $apps = $antibody->getApplication($app_type->getApplicationAbbr());

    $app_details = '<table cellpadding="1">';
    $app_details .= '<tr>
        <td style="width: 18%;"><small>Research Group</small></td>
        <td style="width: 9%;"><small>Dilution</small></td>
        <td style="width: 13%;"><small>Concentration</small></td>
        <td style="width: 8%; text-align: center;"><small>Quality</small></td>
        <td style="width: 52%;"><small>Comment</small></td>
        </tr>';
    foreach ($apps as $app) {
      $app_details .= '<tr>';
      $app_details .= '<td>';
      $app_details .= $app->getElementWorkingGroup();
      $app_details .= '</td>';
      $app_details .= '<td>';
      $app_details .= $app->getDilution();
      $app_details .= '</td>';
      $app_details .= '<td>';
      $app_details .= $app->getConcentrationRaw();
      $app_details .= '</td>';
      $app_details .= '<td style="text-align: center;">';
      $app_details .= $app->getQuality() == 0 ? '-' : $app->getQuality();
      $app_details .= '</td>';
      $app_details .= '<td><i>';
      $app_details .= $app->getComment();
      $app_details .= '</i></td>';
      $app_details .= '</tr>';
    }
    $app_details .= '</table>';

    $html .= '
      <table cellpadding="10">
        <tr>
          <td style="border-bottom: 0.5px dashed #e4dfcd; border-left: 0.5px dashed #e4dfcd; border-right: 0.5px dashed #e4dfcd; text-align: justify"><span style="font-size: larger;">' . $app_type->getApplicationAbbr() . '</span><br />&nbsp;<br />
            <span style="font-family: sourcesansprolight">
              ' . $app_details . '
            </span>
          </td>
        </tr>
      </table><br /><br />
    ';
  }

  $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');

}

/**
 * @param $pdf TCPDF
 * @param $antibody \Antibody
 */
function _sfb_antibody_download_sheet_add_secondary_antibody_applications(&$pdf, $antibody) {

  $html = '';

  $application_types = $antibody->getApplicationsDistinctList();

  foreach ($application_types as $app_type) {
    $apps = $antibody->getApplication($app_type->getApplicationAbbr());

    $app_details = '<table cellpadding="1">';
    $app_details .= '<tr>
        <td style="width: 18%;"><small>Research Group</small></td>
        <td style="width: 9%;"><small>Dilution</small></td>
        <td style="width: 13%;"><small>Concentration</small></td>
        <td style="width: 8%; text-align: center;"><small>Quality</small></td>
        <td style="width: 52%;"><small>Comment</small></td>
        </tr>';
    foreach ($apps as $app) {
      $app_details .= '<tr>';
      $app_details .= '<td>';
      $app_details .= $app->getElementWorkingGroup();
      $app_details .= '</td>';
      $app_details .= '<td>';
      $app_details .= $app->getDilution();
      $app_details .= '</td>';
      $app_details .= '<td>';
      $app_details .= $app->getConcentrationRaw();
      $app_details .= '</td>';
      $app_details .= '<td style="text-align: center;">';
      $app_details .= $app->getQuality() == 0 ? '-' : $app->getQuality();
      $app_details .= '</td>';
      $app_details .= '<td><i>';
      $app_details .= $app->getComment();
      $app_details .= '</i></td>';
      $app_details .= '</tr>';
    }
    $app_details .= '</table>';

    $html .= '
      <table cellpadding="10">
        <tr>
          <td style="border-bottom: 0.5px dashed #e4dfcd; border-left: 0.5px dashed #e4dfcd; border-right: 0.5px dashed #e4dfcd; text-align: justify"><span style="font-size: larger;">' . $app_type->getApplicationAbbr() . '</span><br />&nbsp;<br />
            <span style="font-family: sourcesansprolight">
              ' . $app_details . '
            </span>
          </td>
        </tr>
      </table><br /><br />
    ';
  }

  $pdf->writeHTML($html, TRUE, FALSE, TRUE, FALSE, '');

}

function sfb_antibody_download_excel() {
  module_load_include('inc', 'sfb_antibody', 'includes/session');
  $antibodies = lists_session("antibodies");

  // Load the library and initialize.
  module_load_include('php', 'sfb_commons', 'lib/phpexcel_1.8.0/PHPExcel');
  $objPHPExcel = fillExcelSheet($antibodies);

  // Redirect the output to a client's web browser (Excel2007).
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="antibodies_' . date(SFB_ANTIBODY_DEFAULT_DATE_FORMAT) . '.xlsx"');
  header('Cache-Control: max-age=0');

  // Set the basic file properties.
  $objPHPExcel->getProperties()->setCreator("Antibody Catalogue")
    ->setLastModifiedBy('Antibody Catalogue')
    ->setTitle("Antibody Catalogue - Export");

  // The output.
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->save('php://output');

  return NULL;
}

/**
 * @param \Antibody[] $antibodies
 *
 * @return \PHPExcel
 */
function fillExcelSheet($antibodies) {
  $objPHPExcel = new PHPExcel();

  $first_content_row = 2;
  if ($antibodies) {
    $num_antibodies = count($antibodies);
  }
  else {
    $num_antibodies = 0;
  }

  for ($i = 0; $i < $num_antibodies; $i++) {
    $antibody = $antibodies[$i];
    $data = [
      ['PID', $antibody->getElementPID(),],
      ['EPIC PID', $antibody->getElementEPICPID(),],
      ['Research group', $antibody->getWorkingGroup()->getName(),],
      ['Quality (mean)', $antibody->getOverallQuality() == 0 ? '' : $antibody->getOverallQuality() . '/5 stars',],
      ['Sharing level', $antibody->getSharingLevelName(),],
      ['Antigen symbol', $antibody->getAntigenSymbol(),],
      ['Antibody Registry ID(s)', $antibody->getRegistryId(),],
      ['Name', $antibody->getName(),],
      ['Lab ID', $antibody->getLabId()],
      ['Alternative name', $antibody->getAlternativeName(),],
      ['Tag / Fluorophore', $antibody->getTag(),],
      ['Raised in', $antibody->getElementRaisedIn(),],
      ['Reacts with', $antibody->getElementReactsWith(),],
      ['Clone', $antibody->getClone(),],
      ['Isotype', $antibody->getIsotype(),],
      ['Clonality', $antibody->getClonality(),],
      ['Demasking', $antibody->getDemasking(),],
      ['Antigen', $antibody->getAntigen(),],
      ['Crafted By', $antibody->getCraftedBy(),],
      ['Company / Manufacturer', $antibody->getCompany(),],
      ['Catalog no.', $antibody->getCatalogNo(),],
      ['Lot no.', $antibody->getLotNo(),],
      ['Description', $antibody->getDescription(),],
      ['Localization', $antibody->getLocalization(),],
      ['Storage instruction', $antibody->getStorageInstruction(),],
      ['Receipt date', $antibody->getReceiptDate(),],
      ['Preparation date', $antibody->getPreparationDate(),],
      ['Created by', $antibody->getCraftedBy(),],
      ['Last modified', $antibody->getElementLastModified(),],
      ['Antibodypedia', $antibody->getAntibodypediaUrl(),],
      ['HGNC', $antibody->getHGNCUrl(),],
      ['Wikipedia', $antibody->getWikipediaUrl(),],
      ['Uniprot', $antibody->getUniprotUrl(),],
      ['GeneCards', $antibody->getGeneCardsUrl(),],
      ['Antibody Registry',  $antibody->getAntibodyRegistryUrls(),],
      ['Applications', ''],
    ];

    if ($i == 0) {
      $count_char = 'A';
      foreach ($data as $header_element) {
        $objPHPExcel->getActiveSheet()
          ->setCellValue($count_char . '1', $header_element[0]);
        $count_char++;
      }
    }

    $count_char = 'A';
    foreach ($data as $rows) {
      if ($rows[0] == 'Applications') {
        continue;
      }
      $objPHPExcel->getActiveSheet()
        ->setCellValue($count_char . ($first_content_row + $i), $rows[1]);
      $count_char++;
    }

    $application_types = $antibody->getApplicationsDistinctList();
    foreach ($application_types as $application_type) {
      $application_string =
        $application_type->getWorkingGroup()->getName() . ' | ' .
        $application_type->getApplicationAbbr() . ' | ' .
        $application_type->getDilution() . ' | ' .
        $application_type->getElementConcentration() . ' | '  .
      $application_type->getQuality() . '/5 stars | ' .
        $application_type->getComment();

      $objPHPExcel->getActiveSheet()
        ->setCellValue($count_char . ($first_content_row + $i), $application_string);
      $count_char++;
    }
  }

  // automatically set the size of the columns for better readability, but
  // limit column width to 100
  $columnID = 'A';
  while ($columnID !== 'AZ') {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
      ->setAutoSize(TRUE);
    $objPHPExcel->getActiveSheet()->calculateColumnWidths();
    $colwidth = $objPHPExcel->getActiveSheet()
      ->getColumnDimension($columnID)
      ->getWidth();
    if ($colwidth > 100) {
      $objPHPExcel->getActiveSheet()
        ->getColumnDimension($columnID)
        ->setAutoSize(FALSE);
      $objPHPExcel->getActiveSheet()
        ->getColumnDimension($columnID)
        ->setWidth(100);
    }
    $columnID++;
  }

  // set the first row bold
  $from = "A1";
  $to = "AJ1";
  $objPHPExcel->getActiveSheet()
    ->getStyle("$from:$to")
    ->getFont()
    ->setBold(TRUE);

  return $objPHPExcel;
}

function sfb_antibody_download_template() {
  // Check if the template file exist.
  $template_file = $_SERVER['DOCUMENT_ROOT'] . base_path() . drupal_get_path('module', 'sfb_antibody') . SFB_ANTIBODY_PATH_EXCEL_TEMPLATE;
  // If the template does not exist, throw an error.
  if (!file_exists($template_file)) {
    drupal_set_message('Error: Could not create the Excel file! The template file could not be found. ' . $template_file);
    watchdog('rdp_cellmodel', 'PHPExcel could not create Excel file! Template file missing.');
    return '';
  }

  // Load the library and initialize.
  module_load_include('php', 'sfb_commons', 'lib/phpexcel_1.8.0/PHPExcel');
  $objReader = PHPExcel_IOFactory::createReader('Excel2007');

  // Load the template file.
  $objPHPExcel = $objReader->load($template_file);

  // Redirect the output to a client's web browser (Excel2007).
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="template_import_antibodies' . '.xlsx"');
  header('Cache-Control: max-age=0');

  // Write strings for wg and species in a separate sheet because of the char limitation of a raw validation list string of 256
  $objPHPExcel->setActiveSheetIndex(2);
  $wgs = WorkingGroupRepository::findAll();
  $row = 1;
  foreach ($wgs as $wg) {
    $objPHPExcel->getActiveSheet()
      ->setCellValue('A' . $row, $wg->getShortName());
    $row++;
  }

  $species = AntibodySpeciesRepository::findAll();
  $row = 1;
  foreach ($species as $specie) {
    $objPHPExcel->getActiveSheet()
      ->setCellValue('B' . $row, $specie->getName());
    $row++;
  }

  // Prepare all remaining needed strings
  $clonality_string = 'unknown,polyclonal,monoclonal';
  $demasking_string = 'unknown,yes,no';
  $crafted_by_string = 'unknown,self-made,company';
  $sharing_level_string = 'Site, Group, Public';
  $quality_string = '1,2,3,4,5';
  $concentration_string = 'unknown,µg/ml,mg/ml';
  // start and end of data validation per column
  $min = 3;
  $max = 200;

  //Primary
  $columns_primary = [
    ['I', 'specie'],
    ['L', $clonality_string],
    ['M', $demasking_string],
    ['O', $crafted_by_string],
    ['V', 'date'],
    ['W', 'date'],
    ['X', 'wg'],
    ['Y', $sharing_level_string],
    ['AD', $quality_string],
    ['AC', $concentration_string],
  ];
  $columns_secondary = [
    ['I', $clonality_string],
    ['M', $crafted_by_string],
    ['T', 'date'],
    ['U', 'date'],
    ['V', 'wg'],
    ['W', $sharing_level_string],
    ['AB', $quality_string],
    ['AA', $concentration_string],
  ];
  $validation_sheets = [
    [0, $columns_primary],
    [1, $columns_secondary],
  ];
  foreach ($validation_sheets as $validation_sheet) {
    $objPHPExcel->setActiveSheetIndex($validation_sheet[0]);
    foreach ($validation_sheet[1] as $column) {
      for ($i = $min; $i <= $max; $i++) {
        if ($column[1] == 'date') {
          $objValidation = $objPHPExcel->getActiveSheet()
            ->getCell($column[0] . $i)
            ->getDataValidation();
          $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_DATE);
          $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidation->setAllowBlank(FALSE);
          $objValidation->setShowErrorMessage(TRUE);
          $objValidation->setErrorTitle('Input error');
          $objValidation->setError('Value is not a date.');
        }
        else {
          $objValidation = $objPHPExcel->getActiveSheet()
            ->getCell($column[0] . $i)
            ->getDataValidation();
          $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidation->setAllowBlank(FALSE);
          $objValidation->setShowInputMessage(TRUE);
          $objValidation->setShowErrorMessage(TRUE);
          $objValidation->setShowDropDown(TRUE);
          $objValidation->setErrorTitle('Input error');
          $objValidation->setError('Value is not in list.');
          $objValidation->setPromptTitle('Pick from list');
          $objValidation->setPrompt('Please pick a value from the drop-down list.');
          if ($column[1] == 'wg') {
            $objValidation->setFormula1('Data!A1:A' . sizeof($wgs));
          }
          elseif ($column[1] == 'specie') {
            $objValidation->setFormula1('Data!B1:B' . sizeof($species));
          }
          else {
            $objValidation->setFormula1('"' . $column[1] . '"');
          }
        }
      }
    }
  }

  function excelColumnRange($lower, $upper) {
    ++$upper;
    for ($i = $lower; $i !== $upper; ++$i) {
      yield $i;
    }
  }

  // automatically set the size of the columns for better readability, but
  // limit column width to 100

  $sheets = [
    [0, 'AE'],
    [1, 'AC'],
  ];

  foreach ($sheets as $sheet) {
    $objPHPExcel->setActiveSheetIndex($sheet[0]);
    foreach(excelColumnRange('A',$sheet[1]) as $columnID) {
      $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
      $objPHPExcel->getActiveSheet()->calculateColumnWidths();
      $colwidth = $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->getWidth();
      if ($colwidth>100) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setWidth(100);
      }
    }
  }

  $objPHPExcel->setActiveSheetIndex(0);
  // Set the basic file properties.
  $objPHPExcel->getProperties()->setCreator("Antibodies")
    ->setLastModifiedBy('Antibodies')
    ->setTitle("Antibodies - Import Table")
    ->setCustomProperty("Version", SFB_ANTIBODY_IMPORT_FILE_VERSION_NUMBER);

  // The output.
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->save('php://output');
  return NULL;
}
