<?php

function sfb_antibody_export($form, &$form_state) {
  $form = [];

  $form ['select_ab'] = [
    '#type' => 'radios',
    '#title' => 'Select Antibodies',
    '#options' => ['All' => 'All', 'Custom' => 'Custom', 'Search Query' => 'Search Query'],
    '#required' => true,
  ];


  sfb_commons_add_select2(SFB_ANTIBODY_API_SEARCH_ANTIBODY, 'rdp_antibodies_export',
    'formatAntibodies', 'formatAntibodiesSelection', 2,
    'e.g. PP1');

  $form ['select_custom'] = [
    '#type' => 'select',
    '#attributes' => ['class' => ['rdp_antibodies_export']],
    '#options' => [],
    '#validated' => 'true',
    '#multiple' => 'true',
    '#states' => [
      'visible' => [
        ':input[name*="select_ab"]' => [
          'value' =>
            'Custom',
        ],
      ],
    ],
  ];

  $form['select_search'] = [
    '#type' => 'textfield',
    '#default_value' => [],
    '#attributes' => ['placeholder' => 'Search ...'],
    '#states' => [
      'visible' => [
        ':input[name*="select_ab"]' => [
          'value' =>
            'Search Query',
        ],
      ],
    ],
  ];

  $form ['select_format'] = [
    '#type' => 'select',
    '#title' => 'Select Export Format',
    '#options' => ['Excel' => 'Excel'],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Export'),
    '#attributes' => ['class' => ['btn-success']],
  ];

  $form['cancel'] = [
    '#type' => 'button',
    '#submit' => ['sfb_antibody_export_cancel'],
    '#value' => t(' Back'),
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => [],
    '#attributes' => ['class' => ['btn-default']],
  ];



  return $form;
}

function sfb_antibody_export_submit ($form, &$form_state) {
  $antibodies = [];
  if ($form_state['values']['select_ab'] == 'All') {
    $antibodies = array_merge(AntibodiesRepository::findByType('primary'), AntibodiesRepository::findByType('secondary'));
  }
  elseif ($form_state['values']['select_ab'] == 'Custom') {
    $antibody_ids = $form_state['values']['select_custom'];
    foreach ($antibody_ids as $antibody_id) {
      $antibodies [] = AntibodiesRepository::findById($antibody_id);
    }
  }
  elseif ($form_state['values']['select_ab'] == 'Search Query') {
    $query = $form_state['values']['select_search'];
    $params = explode(' ', $query);
    $db_condition = db_or();
    foreach($params as $param) {
      $db_condition->condition('antigen_symbol', '%'.db_like($param).'%', 'LIKE');
      $db_condition->condition('name', '%'.db_like($param).'%', 'LIKE');
      $db_condition->condition('alt_name', '%'.db_like($param).'%', 'LIKE');
      $db_condition->condition('antigen', '%'.db_like($param).'%', 'LIKE');
      $db_condition->condition('clonality', '%'.db_like($param).'%', 'LIKE');
      $db_condition->condition('registry_id', '%'.db_like($param).'%', 'LIKE');
      $db_condition->condition('company', '%'.db_like($param).'%', 'LIKE');
      $db_condition->condition('catalog_no', '%'.db_like($param).'%', 'LIKE');
      $db_condition->condition('description', '%'.db_like($param).'%', 'LIKE');
    }
    $antibodies = AntibodiesRepository::findBy($db_condition);
  }

  module_load_include('inc', 'sfb_antibody', 'includes/session');
  lists_session("antibodies", $antibodies);

  if ($form_state['values']['select_format'] == 'Excel') {
    $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_DOWNLOAD_EXCEL);
  }
}

function sfb_antibody_export_cancel ($form, &$form_state) {
  $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_PAGE_DEFAULT);
}
