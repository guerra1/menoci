<?php

/**
 * Primary and secondary(!) antibody application rating - Create new (form)
 *
 * Returns the form for creating new application rating for a given ($antibody_pid) antibody
 *
 * Access permissions:
 *   New application rating can be created by any user, which has a permission
 *   to view this antibody
 *
 * @see drupal_get_form()
 * @see drupal_render()
 *
 * @param $form
 * @param $form_state
 * @param $antibody_pid
 *    PID of an antibody
 * @param $antibody_type
 *    Type of the antibody
 * @return mixed
 */
function sfb_antibody_primary_application_new($form, &$form_state, $antibody_pid, $antibody_type = AntibodyType::PRIMARY) {

  watchdog(
    'debug',
    'Here is the printed variable: <pre>@placeholder_name</pre>',
    array('@placeholder_name' => print_r( variable_get(SFB_ANTIBODY_CONFIG_WB_BAND_APPLICATIONS), TRUE)),
    WATCHDOG_INFO
  );
  // extract id from pid (url)
  $antibody_id = Antibody::extractIDFromPID($antibody_pid);
  if(is_null($antibody_id)) {
    drupal_not_found();
    exit();
  }

  // check if given antibody exists
  $antibody = AntibodiesRepository::findByIdAndType($antibody_id, $antibody_type);
  if($antibody->isEmpty()) {
    drupal_not_found();
    exit();
  }

  // check user permission to create new application rating for this antibody
  // and user is logged in
  if(!$antibody->userHasPermissionTo(AntibodyAction::READ) || user_is_anonymous()) {
    drupal_access_denied();
    exit();
  }

  // set form state variable with antibody id
  $form_state['antibody_id'] = $antibody_id;

  // create new antibody application instance
  $application = new AntibodyAntibodyApplication();

  // add form fields
  $form['working_group_id'] = $application->getFormFieldWorkingGroup();
  $form['application_abbr'] = $application->getFormFieldApplication();
  $form['dilution'] = $application->getFormFieldDilution();
  $form['concentration_raw'] = $application->getFormFieldConcentrationRaw();
  $form['concentration_dimension'] = $application->getFormFieldConcentrationDimension();
  $form['wb_band'] = $application->getFormFieldWbBand();
  $form['quality'] = $application->getFormFieldQuality();
  $form['comments'] = $application->getFormFieldComments();

  $form['fieldset-image'] = array(
    '#type' => 'fieldset',
    '#title' => t('Attach Images'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="image-fieldset-wrapper">',
    '#suffix' => '&nbsp;</div>',
  );

  // set the antibodies and the count once when loading the page first
  if (!isset($form_state['images'])) {
    $form_state['images'] = [];
  }

  if (empty($form_state['count_images'])) {
    $form_state['count_images'] = count($form_state['images']);
  }

  // $form_state['add'] defines if the add button is shown
  if (!isset($form_state['add'])) {
    $form_state['add'] = TRUE;
  }

  // load all image upload forms
  for ($i = 0; $i < $form_state['count_images']; $i++) {
    // if editing an antibody, take the saved images from the db
    if (!isset($form_state['images'][$i])) {
      $form_state['images'][$i] = new AntibodyImage();
    }

    $form['fieldset-image']['fieldset-new-image'][$i] =
      $form_state['images'][$i]->getFormFieldImageFieldset($i);

    $form['fieldset-image']['fieldset-new-image'][$i]['file_'.$i] =
      $form_state['images'][$i]->getFormFieldImageUpload($i);

    $form['fieldset-image']['fieldset-new-image'][$i]['description_'.$i] =
      $form_state['images'][$i]->getFormFieldImageDescription();

    $form['fieldset-image']['fieldset-new-image'][$i]['comment_'.$i] =
      $form_state['images'][$i]->getFormFieldImageComment();

    // this is only true after the rebuild initiated by uploading an image, thus
    // the FID of the uploaded image is saved here
    if (isset($form_state['values']['file_'.$i])) {
      $form_state['images'][$i]->setFid($form_state['values']['file_'.$i]);
      $form_state['add'] = TRUE;
    }

    $form['fieldset-image']['fieldset-new-image'][$i]['preview_'.$i] =
      $form_state['images'][$i]->getFormFieldImagePreview();

  }

  // only render add button if $form_state['add'] is TRUE
  if ($form_state['add']) {
    $form['fieldset-image']['image_add'] = $antibody->getFormFieldImageAdd();
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['cancel'] = array(
    '#type' => 'button',
    '#submit' => array('sfb_antibody_primary_application_new_cancel'),
    '#value' => t('Cancel'),
    '#executes_submit_callback' => true,
    '#limit_validation_errors' => array(),
  );

  return $form;
}

/**
 * Return the desired part of the page (wrapper).
 */
function ajax_callback ($form, $form_state) {
  return $form['fieldset-image'];
}

/**
 * Handle what happens on an AJAX submit e.g. button press.
 */
function ajax_submit ($form, &$form_state) {
  // If the add button is pressed, increment the image field counter which gets added after the rebuild.
  if ($form_state['triggering_element']['#name'] == 'AddImageButton') {
    if (!isset($form_state['count_images'])) {
      $form_state['count_images'] = 0;
      $form_state['count_images']++;
    }
    $form_state['count_images']++;
    // Set this to false so only another image field can be added if the current new empty one was used to upload an image.
    $form_state['add'] = FALSE;
  }
  // This gets triggered if the Remove button is pressed (the only other AJAX button)
  else {
    // in the button name is the $i of the respective image field saved
    // (name example: file_0_remove_button
    // -> get the 0 (or n) out of it
    $button_name = $form_state['triggering_element']['#name'];
    $i = explode('_',$button_name)[1];
    $img = $form_state['images'][$i];
    // load the file so it can be deleted
    $file = file_load($img->getFid());
    file_delete($file);

    // also delete the database entry of the image
    if($img->getId()) {
      AntibodyImageRepository::delete($img->getId());
    }

    // reduce the number of image fields by one
    $form_state['count_images']--;


    $new_arr = [];
    foreach ($form_state['images'] as $key=>$img) {
      // save the comment and description so it can be loaded later again (since the form_states have to be unset)
      if (isset($form_state['input']['description_'.$key])) {
        $form_state['images'][$key]->setDescription($form_state['input']['description_'.$key]);
      }
      if (isset($form_state['input']['comment_'.$key])) {
        $form_state['images'][$key]->setComment($form_state['input']['comment_'.$key]);
      }

      // unset all input and value form fields
      unset($form_state['input']['file_'.$key]);
      unset($form_state['values']['file_'.$key]);
      unset($form_state['input']['description_'.$key]);
      unset($form_state['values']['description_'.$key]);
      unset($form_state['input']['comment_'.$key]);
      unset($form_state['values']['comment_'.$key]);
      unset($form_state['input']['preview_'.$key]);
      unset($form_state['values']['preview_'.$key]);

      // build the new array, save all images except the one just deleted
      if ($key == $i ) {
        continue;
      }
      else {
        $new_arr[] = $img;
      }
    }

    $form_state['images'] = $new_arr;
  }
  $form_state['rebuild'] = TRUE;
}


/**
 * Form 'Cancel' button handler
 *
 * @param $form
 * @param $form_state
 */
function sfb_antibody_primary_application_new_cancel($form, &$form_state) {
  $antibody_id = $form_state['antibody_id'];

  //read antibody from database
  $antibody = AntibodiesRepository::findById($antibody_id);
  $antibody_redirect = SFB_ANTIBODY_URL_PRIMARY_VIEW;
  if($antibody->getType() == AntibodyType::SECONDARY)
    $antibody_redirect = SFB_ANTIBODY_URL_SECONDARY_VIEW;

  //redirect to antibody view page
  $form_state['redirect'] = sfb_antibody_url($antibody_redirect, $antibody->getElementPID());
}

/**
 * Form validation function
 *
 * @param $form
 * @param $form_state
 */
function sfb_antibody_primary_application_new_validate($form, &$form_state)
{

  // read values from the form and form state
  $antibody_id = $form_state['antibody_id'];
  $application_abbr = $form_state['values']['application_abbr'];
  $working_group_id = $form_state['values']['working_group_id'];
  $concentration_raw = $form_state['values']['concentration_raw'];
  $dilution = $form_state['values']['dilution'];

  //TODO: die Funktionen müssen nochmal überarbeitet werden
  /*
  //check if concentration is empty
  if (!empty($concentration_raw)) {
    //check if concentration is numeric
    if(!is_numeric($concentration_raw)){
      form_set_error('concentration_raw', 'The concentration value ' . $concentration_raw . ' is not allowed. Please enter integers or decimals.');
    }
    else {
      //check if concentration is integer, if so then skip next validation part of concentration
      if (is_int($concentration_raw)) {
        //check if concentration has a decimal separator
        //either one ',' or one '.' is allowed
        if ((substr_count($concentration_raw, ',') != 1) && (substr_count($concentration_raw, '.') != 1)) {
          form_set_error('concentration_raw', 'The concentration value ' . $concentration_raw . ' is not allowed. ');
        }
        //check if there are integers before and after '.'
        if (!is_double($concentration_raw)) {
          form_set_error('concentration', 'The concentration value ' . $concentration_raw . ' is not allowed. Concentration must have the form: \'x.y with x,y from the set of natural numbers\'.');
        }
      }
    }
  }

  //check if dilution has a ':' and it's surrounded by to integers
  if(!empty($dilution)){
    //check if there is a ':'
    if(substr_count($dilution, ':') != 1){
      form_set_error('dilution', 'The dilution value ' . $dilution . ' is not allowed. Dilution must have one colon.');
    }
    else {
      $dilution_array = explode(':', $dilution);

      //check if there are integers before and after ':'
      if (!is_numeric($dilution_array[0]) || !is_numeric($dilution_array[1])) {
        form_set_error('dilution', 'The dilution value ' . $dilution . ' is not allowed. Dilution must have the form: \'x:y with x,y from the set of natural numbers\'.');
      }
    }
  }
  */

  // check if commented application already exists
  // if commented application already exists, than it cannot be overridden here.
  // in this case user has to use edit form
  if(isset($application_abbr)) {
    $app_comment = AntibodyAntibodyApplicationsRepository::findByAntibodyIdAndByApplicationAbbreviationAndByWorkingGroupId($antibody_id,$application_abbr,$working_group_id);
    if(!$app_comment->isEmpty())
      form_set_error('application_abbr','Comment for chose application ('.$application_abbr.') already exists. '); //TODO: 'If you want to edit existing comment click here or change application_abbr'
  }
}

/**
 * Main submit function of application (rating) new form.
 *
 * @param $form
 * @param $form_state
 */
function sfb_antibody_primary_application_new_submit($form, &$form_state) {

  // read values from the form and form state
  $antibody_id = $form_state['antibody_id'];
  $application_abbr = $form_state['values']['application_abbr'];
  $working_group_id = $form_state['values']['working_group_id'];
  $comments = $form_state['values']['comments'];
  $quality = $form_state['values']['quality'];
  $dilution = $form_state['values']['dilution'];
  $concentration_raw = $form_state['values']['concentration_raw'];
  $concentration_dimension = $form_state['values']['concentration_dimension'];
  $wb_band = $form_state['values']['wb_band'];
  //
  // store application into database
  //
  $application = AntibodyApplicationsRepository::findByAbbreviation($application_abbr);
  if($application->isEmpty())
    $application->setAbbreviation($application_abbr);
  $application->save();

  global $user;

  //
  // store application comments into database
  //
  $app_comments = new AntibodyAntibodyApplication();
  $app_comments->setAntibodyId($antibody_id);
  $app_comments->setWorkingGroupId($working_group_id);
  $app_comments->setApplicationAbbr($application_abbr);
  $app_comments->setComment($comments);
  $app_comments->setQuality($quality);
  $app_comments->setDilution($dilution);
  $app_comments->setWbBand($wb_band);
  $app_comments->setCreatedBy($user->uid);
  $app_comments->setLastModified($user->uid);

  //change all "," to "." before storing into db
  $concentration_raw = str_replace(',','.',$concentration_raw);

  //delete all unnecessary dots
  $concentration_raw = preg_replace('/\.+/', '.', $concentration_raw);

  //if there is no dimension given or it is unknown, save into concentration_raw
  if(strcmp($concentration_dimension,'unknown') == 0){
    $app_comments->setConcentrationRaw($concentration_raw);
  }
  //else save in concentration_value and save the related unit in concentration_dimension, also save in concentration_raw
  else{
    $app_comments->setConcentrationRaw($concentration_raw);
    $app_comments->setConcentrationValue($concentration_raw);
    $app_comments->setConcentrationDimension($concentration_dimension);
  }

  // save all values as image objects from the image fields into an array to be saved into the db later
  $images = [];
  for ($i = 0; $i < $form_state['count_images']; $i++) {
    if (isset ($form_state['values']['file_'.$i]) and
      $form_state['values']['file_'.$i] !== 0) {
      $image = new AntibodyImage();
      $image->setId($form_state['images'][$i]->getId());
      $image->setFid($form_state['values']['file_'.$i]);
      $image->setDescription($form_state['values']['description_'.$i]);
      $image->setComment($form_state['values']['comment_'.$i]);
      $image->setImageType('app');
      if ($form_state['images'][$i]->getUploader()) {
        $image->setUploader($form_state['images'][$i]->getUploader());
      }
      else {
        $image->setUploader($user->uid);
      }
      if ($form_state['images'][$i]->getUploadDate()) {
        $image->setUploadDate($form_state['images'][$i]->getUploadDate());
      }
      else {
        $image->setUploadDate(date(DEFAULT_DATETIME_FORMAT));
      }
      $images[] = $image;
    }
  }
  $app_comments->setImages($images);


  $app_comments->save();

  //read antibody from database
  $antibody = AntibodiesRepository::findById($antibody_id);
  $antibody_redirect = SFB_ANTIBODY_URL_PRIMARY_VIEW;
  if($antibody->getType() == AntibodyType::SECONDARY)
    $antibody_redirect = SFB_ANTIBODY_URL_SECONDARY_VIEW;

  $form_state['redirect'] = sfb_antibody_url($antibody_redirect, $antibody->getElementPID());
  drupal_set_message('Application '.$application_abbr.' has been successfully added.');
}

function sfb_antibody_ajax_textfield_callback($form, &$form_state){
  $application_abbr = $form_state['values']['application_abbr'];

  foreach(variable_get(SFB_ANTIBODY_CONFIG_WB_BAND_APPLICATIONS, array()) as $app) {

    // Config variable contains not only application abbeviations
    // but also '0';
    // If form value equals the application abbreviation than fade in new form
    // field for wb_band value
    if(strcmp($application_abbr, $app) == 0 && $app != '0'){
      return $form["wb_band"];
    }
  }

  return NULL;
}
