<?php

/**
 * Created by PhpStorm.
 * User: lehmann76
 * Date: 20.02.2019
 * Time: 10:17
 */

function sfb_antibody_upload_import($form, $form_state) {

  if(!user_access(SFB_ANTIBODY_PERMISSION_REGISTRAR)) {
    drupal_access_denied();
  }

  // Initialize form
  $form = [];

  $form['template'] = [
    '#type' => 'submit',
    '#value' => t('Download Template'),
    '#attributes' => ['class' => ['btn-primary btn-sm']],
    '#submit' => ['sfb_antibody_upload_import_submit_template'],
    '#suffix' => '<div> </div>'
  ];

  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('Choose an xlsx-file based on the most recent template version.'),
  );
  $form['info_archive_name'] = [
    '#type' => 'markup',
    '#markup' => '<i class="glyphicon glyphicon-exclamation-sign"></i> 
    The import can take some time. Please do not close this tab. <br>',
  ];

  $form['cancel'] = [
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => ['sfb_antibody_upload_import_cancel_submit'],
    '#limit_validation_errors' => [],
    '#attributes' => ['class' => ['btn-danger btn-sm']],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Import'),
    '#attributes' => ['class' => ['btn-primary btn-sm']],
  ];

  return $form;
}

function sfb_antibody_upload_import_cancel_submit($form, &$form_state) {
  $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_PAGE_DEFAULT);
}

function sfb_antibody_upload_import_submit($form, &$form_state) {
  // Process the file provided by the user.
  $file_destination = 'public://';

  $validators = ['file_validate_extensions' => ['xlsx']];

  $file = file_save_upload('file', $validators, $file_destination, FILE_EXISTS_REPLACE);

  if ($wrapper = file_stream_wrapper_get_instance_by_uri('public://')) {
    $file_path = $wrapper->realpath().'/';
  }
  else {
    $file_path = $_SERVER['DOCUMENT_ROOT'] . base_path().'sites/default/files/';
  }

  $file_name = $file->filename;
  $import_file = $file_path.$file_name;

  // Load the library and initialize.
  module_load_include('php', 'sfb_commons', 'lib/phpexcel_1.8.0/PHPExcel');

  // Load the import file.
  try {
    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
    $objPHPExcel = $objReader->load($import_file);
  }
  catch (Exception $exception) {
    drupal_set_message($exception);
    watchdog('sfb_antibody', $exception);
  }
  $version_number = $objPHPExcel->getProperties()->getCustomPropertyValue("Version");
  if (!$version_number) {
    $version_number = '0';
  }

  // Check if the most recent version of the template was used.
  if ($version_number !== SFB_ANTIBODY_IMPORT_FILE_VERSION_NUMBER){
    file_delete($file);
    $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_PAGE_DEFAULT);
    drupal_set_message(t('The antibody import was canceled since you are not using the the most recent version of the import template. <br>
    You are using version '. $version_number. ' but the most recent version is '. SFB_ANTIBODY_IMPORT_FILE_VERSION_NUMBER.'.'),'error');
    return NULL;
  }


  // Get the Antibody objects.
  $first_content_row = 3;
  $sheetCount = 2;
  global $user;


  // Loop through each data row of the worksheet in turn.
  for ($i = 0; $i < $sheetCount; $i++) {
    $sheet = $objPHPExcel->setActiveSheetIndex($i);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();
    for ($row = $first_content_row; $row <= $highestRow; $row++) {
      // Read a row of data into an array.
      $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE, TRUE);

      $antibody = new Antibody();

      $antibody->setCreatedBy($user->uid);
      $antibody->setLastModified($user->uid);
      $antibody->setCreatedDate(date('Y-m-d'));

      // Primary AB sheet
      if ($i == 0) {
        $antibody->setType(AntibodyType::PRIMARY);
        $antibody->setAntigenSymbol($rowData[$row]['A']);
        $antibody->setRegistryId($rowData[$row]['B']);
        $antibody->setAntibodypediaUrl($rowData[$row]['C']);
        $antibody->setName($rowData[$row]['D']);
        $antibody->setLabId($rowData[$row]['E']);
        $antibody->setAlternativeName($rowData[$row]['F']);
        $antibody->setTag($rowData[$row]['G']);
        $antibody->setRaisedInSpecies($rowData[$row]['H']);
        // @todo  enable the import of multiple reacting species
        if ($rowData[$row]['I']) {
          $species = AntibodySpeciesRepository::findByName($rowData[$row]['J']);
          $antibody->setReactsWith([$species]);
        }
        $antibody->setClone($rowData[$row]['J']);
        $antibody->setIsotype($rowData[$row]['K']);
        $antibody->setClonality($rowData[$row]['L']);
        $antibody->setDemasking($rowData[$row]['M']);
        $antibody->setAntigen($rowData[$row]['N']);
        $antibody->setCraftedBy($rowData[$row]['O']);
        $antibody->setCompany($rowData[$row]['P']);
        $antibody->setCatalogNo($rowData[$row]['Q']);
        $antibody->setLotNo($rowData[$row]['R']);
        $antibody->setDescription($rowData[$row]['S']);
        $antibody->setLocalization($rowData[$row]['T']);
        $antibody->setStorageInstruction($rowData[$row]['U']);
        $date = NULL;
        if ($rowData[$row]['V']) {
          $date = date(SFB_ANTIBODY_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row]['V']));
        }
        $antibody->setReceiptDate($date);

        $date = NULL;
        if ($rowData[$row]['W']) {
          $date = date(SFB_ANTIBODY_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row]['W']));
        }
        $antibody->setPreparationDate($date);
        if ($rowData[$row]['X']) {
          $wg = WorkingGroupRepository::findByShortName($rowData[$row]['X']);
          $antibody->setWorkingGroupId($wg->getId());
        }
        else {
          $antibody->setWorkingGroupId(1);
        }

        $sharing_level = $rowData[$row]['Y'];
        switch ($sharing_level){
          case 'Public':
            $sharing_level_id = AntibodySharingLevel::PUBLIC_LEVEL;
            break;
          case 'Group':
            $sharing_level_id = AntibodySharingLevel::GROUP_LEVEL;
            break;
          case 'Site':
            $sharing_level_id = AntibodySharingLevel::SITE_LEVEL;
            break;
          default:
            $sharing_level_id = AntibodySharingLevel::GROUP_LEVEL;
        }
        $antibody->setSharingLevel($sharing_level_id);

        $antibody->save();

        if ($rowData[$row]['Z'] or
          $rowData[$row]['AA'] or
          $rowData[$row]['AB'] or
          $rowData[$row]['AC'] or
          $rowData[$row]['AD'] or
          $rowData[$row]['AE']) {
          $app = new AntibodyAntibodyApplication();
          $app->setAntibodyId($antibody->getId());
          $app->setWorkingGroupId($antibody->getWorkingGroupId());
          if ($rowData[$row]['Z']) {
            $app->setApplicationAbbr($rowData[$row]['Z']);
          }
          else {
            $app->setApplicationAbbr('unnamed');
          }
          $app->setDilution($rowData[$row]['AA']);
          $app->setConcentrationRaw($rowData[$row]['AB']);
          $app->setConcentrationDimension($rowData[$row]['AC']);
          $app->setQuality($rowData[$row]['AD']);
          $app->setComment($rowData[$row]['AE']);

          $app->save();
        }


      }
      // Secondary AB sheet
      else {
        $antibody->setType(AntibodyType::SECONDARY);
        $antibody->setRegistryId($rowData[$row]['A']);
        $antibody->setName($rowData[$row]['B']);
        $antibody->setLabId($rowData[$row]['C']);
        $antibody->setAlternativeName($rowData[$row]['D']);
        $antibody->setTag($rowData[$row]['E']);
        $antibody->setRaisedInSpecies($rowData[$row]['F']);
        $antibody->setClone($rowData[$row]['G']);
        $antibody->setIsotype($rowData[$row]['H']);
        $antibody->setClonality($rowData[$row]['I']);
        $antibody->setAntigen($rowData[$row]['J']);
        $antibody->setExcitationMax($rowData[$row]['K']);
        $antibody->setEmissionMax($rowData[$row]['L']);
        $antibody->setCraftedBy($rowData[$row]['M']);
        $antibody->setCompany($rowData[$row]['N']);
        $antibody->setCatalogNo($rowData[$row]['O']);
        $antibody->setLotNo($rowData[$row]['P']);
        $antibody->setDescription($rowData[$row]['Q']);
        $antibody->setLocalization($rowData[$row]['R']);
        $antibody->setStorageInstruction($rowData[$row]['S']);

        $date = NULL;
        if ($rowData[$row]['T']) {
          $date = date(SFB_ANTIBODY_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row]['T']));
        }
        $antibody->setReceiptDate($date);

        $date = NULL;
        if ($rowData[$row]['U']) {
          $date = date(SFB_ANTIBODY_DEFAULT_DATE_FORMAT, PHPExcel_Shared_Date::ExcelToPHP($rowData[$row]['U']));
        }
        $antibody->setPreparationDate($date);

        if ($rowData[$row]['V']) {
          $wg = WorkingGroupRepository::findByShortName($rowData[$row]['V']);
          $antibody->setWorkingGroupId($wg->getId());
        }
        else {
          $antibody->setWorkingGroupId(1);
        }
        $sharing_level = $rowData[$row]['W'];
        switch ($sharing_level){
          case 'Public':
            $sharing_level_id = AntibodySharingLevel::PUBLIC_LEVEL;
            break;
          case 'Group':
            $sharing_level_id = AntibodySharingLevel::GROUP_LEVEL;
            break;
          case 'Site':
            $sharing_level_id = AntibodySharingLevel::SITE_LEVEL;
            break;
          default:
            $sharing_level_id = AntibodySharingLevel::GROUP_LEVEL;
        }
        $antibody->setSharingLevel($sharing_level_id);

        $antibody->save();

        if ($rowData[$row]['X'] or
          $rowData[$row]['Y'] or
          $rowData[$row]['Z'] or
          $rowData[$row]['AA'] or
          $rowData[$row]['AB'] or
          $rowData[$row]['AC']) {
          $app = new AntibodyAntibodyApplication();
          $app->setAntibodyId($antibody->getId());
          $app->setWorkingGroupId($antibody->getWorkingGroupId());
          if ($rowData[$row]['X']) {
            $app->setApplicationAbbr($rowData[$row]['X']);
          }
          else {
            $app->setApplicationAbbr('unnamed');
          }
          $app->setDilution($rowData[$row]['Y']);
          $app->setConcentrationRaw($rowData[$row]['Z']);
          $app->setConcentrationDimension($rowData[$row]['AA']);
          $app->setQuality($rowData[$row]['AB']);
          $app->setComment($rowData[$row]['AC']);

          $app->save();
        }

      }

      // Create new log message.
      $log = new AntibodyLog();
      $log->setMessage('Antibody imported');
      $log->setAntibodyId($antibody->getId());
      $log->setActionType(AntibodyLogActionType::CREATE);
      $log->save();

      // If antibody has been published, then store this to the log action.
      if($antibody->getSharingLevel() == AntibodySharingLevel::PUBLIC_LEVEL) {
        $log = new AntibodyLog();
        $log->setMessage('Antibody has been published');
        $log->setAntibodyId($antibody->getId());
        $log->setActionType(AntibodyLogActionType::PUBLISH);
        $log->save();
      }
    }
  }


  // Delete the file and redirect the user to the main page.
  file_delete($file);
  $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_PAGE_DEFAULT);
  drupal_set_message("The antibodies were successfully imported.");
}

function sfb_antibody_upload_import_submit_template ($form, &$form_state) {
  $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_DOWNLOAD_EXCEL_TEMPLATE);
}
