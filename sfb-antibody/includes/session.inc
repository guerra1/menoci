<?php

function lists_session($key, $value = NULL) {
  static $storage;
  if ($value) {
    $storage[$key] = $value ;
    $_SESSION['lists'][$key] = $value ;
  }
  else if (empty($storage[$key]) && isset($_SESSION['lists'][$key])) {
    $storage[$key] = $_SESSION['lists'][$key];
  }
  return $storage[$key];
}