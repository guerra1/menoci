SFB Antibody Catalogue 2.xx, xxxx-xx-xx (development version)
-----------------------
- Added PDF download for secondary antibodies
- Added automatic database installation
- Added auto-deploy of roles and permissions

SFB Antibody Catalogue 2.0, 2017-06-19
-----------------------
- Renewed data model
- Comments can be added to each application type
- PDF output
- JSON output
Old version of Antibody Catalogue can be found on projects.gwdg.de: https://projects.gwdg.de/projects/sfb-antibody/