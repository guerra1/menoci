<?php

/**
 * Class Antibody
 */
class Antibody {

  /**
   * Default id for empty antibodies. 'Empty' antibody means, that this antibody
   * does not exists in the database or has not been saved yet.
   *
   * @var int
   */
  const EMPTY_ANTIBODY_ID = -1;

  /**
   * @var int
   */
  private $id = Antibody::EMPTY_ANTIBODY_ID;

  /**
   * @var string
   *
   * @see AntibodyType
   */
  private $type = AntibodyType::PRIMARY;

  /**
   * @var string
   */
  private $antigenSymbol;

  /**
   * @var int
   */
  private $workingGroupId;

  /**
   * Variable used for anti-concurrent editing.
   *
   * @var int
   */
  private $locked;

  /**
   * @var int
   */
  private $sharingLevel;

  /**
   * @var string
   */
  private $labId;

  /**
   * @var string
   */
  private $name;

  /**
   * @var string
   */
  private $alternativeName;

  /**
   * Possible values: unknown, polyclonal, monoclonal
   *
   * @var string
   */
  private $clonality;

  /**
   * @var string
   */
  private $antigen;

  /**
   * @var string
   */
  private $description;

  /**
   * @var string
   */
  private $catalogNo;

  /**
   * @var string
   */
  private $lotNo;

  /**
   * @var string
   */
  private $storageInstruction;

  /**
   * @var string
   */
  private $localization;

  /**
   * @var string
   */
  private $company;

  /**
   * @var string
   */
  private $craftedBy;

  /**
   * @var string
   */
  private $clone;

  /**
   * @var int
   */
  private $excitationMax;

  /**
   * @var int
   */
  private $emissionMax;

  /**
   * @var string containing date, format defined by
   *   SFB_ANTIBODY_DEFAULT_DATE_FORMAT
   * @see SFB_ANTIBODY_DEFAULT_DATE_FORMAT
   */
  private $receiptDate;

  /**
   * @var string containing date, format defined by
   *   SFB_ANTIBODY_DEFAULT_DATE_FORMAT
   */
  private $preparationDate;

  /**
   * @var
   */
  private $antibodypediaUrl;

  /**
   * Possible values: unknown, yes, no
   *
   * @var string
   */
  private $demasking;

  /**
   * @var array
   */
  private $raisedInSpecies;

  /**
   * An array with Antibody species reacting with this antibody.
   * If reacts with is null that means, that species has not been read yet from
   * database.
   *
   * It is recommended to use '===' to check whether this array has been set.
   *
   * @var AntibodySpecies[]
   */
  private $reactsWith = NULL;

  /**
   * @var string
   */
  private $registryId;

  /**
   * @var string (DateTime)
   */
  private $createdBy;

  /**
   * @var string (DateTime)
   */
  private $lastModified;

  /**
   * @var string
   */
  private $isotype;

  /**
   * @var string
   */
  private $tag;

  /**
   * Created date.
   * In MySQL format.
   *
   * @var string
   */
  private $createdDate;

  /**
   * @var array
   */
  private $images;

  /**
   * @return string
   */
  public function getCreatedDate() {
    return $this->createdDate;
  }

  /**
   * @param string $createdDate
   */
  public function setCreatedDate($createdDate) {
    $this->createdDate = $createdDate;
  }

  /**
   * @return string
   */
  public function getClone() {
    return $this->clone;
  }

  /**
   * @param $clone
   */
  public function setClone($clone) {
    $this->clone = $clone;
  }

  /**
   * @return string
   */
  public function getLabId()
  {
    return $this->labId;
  }

  /**
   * @param string $labId
   */
  public function setLabId($labId)
  {
    $this->labId = $labId;
  }

  /**
   * @return string
   */
  public function getCraftedBy() {
    return $this->craftedBy;
  }

  /**
   * @param $craftedBy string
   */
  public function setCraftedBy($craftedBy) {
    $this->craftedBy = $craftedBy;
  }

  /**
   * @return int
   */
  public function getExcitationMax() {
    return $this->excitationMax;
  }

  /**
   * @param $excitationMax
   */
  public function setExcitationMax($excitationMax) {
    $this->excitationMax = $excitationMax;
  }

  /**
   * @return int
   */
  public function getEmissionMax() {
    return $this->emissionMax;
  }

  /**
   * @param $emissionMax
   */
  public function setEmissionMax($emissionMax) {
    $this->emissionMax = $emissionMax;
  }

  /**
   * @return string
   */
  public function getAntibodypediaUrl() {
    return $this->antibodypediaUrl;
  }

  /**
   * @param $antibodypediaUrl
   */
  public function setAntibodypediaUrl($antibodypediaUrl) {
    $this->antibodypediaUrl = $antibodypediaUrl;
  }

  /**
   * @return mixed
   */
  public function getDemasking() {
    return $this->demasking;
  }

  /**
   * @param $demasking
   */
  public function setDemasking($demasking) {
    $this->demasking = $demasking;
  }

  /**
   * @return string
   */
  public function getRaisedInSpecies() {
    return $this->raisedInSpecies;
  }

  /**
   * @return array
   */
  public function getImages() {
    return $this->images;
  }

  /**
   * @param array $images
   */
  public function setImages($images) {
    $this->images = $images;
  }

  /**
   * @param string $raisedInSpecies
   */
  public function setRaisedInSpecies($raisedInSpecies) {
    $this->raisedInSpecies = $raisedInSpecies;
  }

  /**
   * Returns an array with AntibodySpecies reacting with this antibody.
   *
   * @see AntibodySpecies
   * @return AntibodySpecies[]
   */
  public function getReactsWith() {

    // If reactsWith variable is still null, it means that the reactsWith
    // has not been initiatied (read from database) yet.
    // In this case this function will be read AntibodySpecies from DB.
    if ($this->reactsWith === NULL) {

      if ($this->isEmpty()) {
        $this->reactsWith = [];
      }
      else {
        $this->reactsWith = AntibodySpeciesRepository::findSpeciesReactingWithAntibody($this->id);
      }
    }

    return $this->reactsWith;
  }

  /**
   * Sets an array containing AntibodySpecies reacting with this antibody-
   *
   * @param $species AntibodySpecies[]
   */
  public function setReactsWith($species) {
    $this->reactsWith = $species;
  }

  /**
   * Adds single species to the array containing species reacting with this
   * antibody.
   *
   * IMPORTANT:
   * This function adds given species to the existing (database) array.
   * If you want to use an empty array, that please use before
   * setReactsWith(array()).
   *
   * @param $species AntibodySpecies
   */
  public function addReactsWith($species) {
    // initialize reacts with array, because it might be empty (null) at this point
    $this->getReactsWith();

    array_push($this->reactsWith, $species);
  }

  /**
   * @return string
   */
  public function getRegistryId() {
    return $this->registryId;
  }

  /**
   * @param $registryId string
   */
  public function setRegistryId($registryId) {
    $this->registryId = $registryId;
  }

  /**
   * @return string
   */
  public function getCreatedBy() {
    return $this->createdBy;
  }

  /**
   * @return \User
   */
  public function getCreator() {
    if (!empty($this->createdBy)) {
      return UsersRepository::findByUid($this->createdBy);
    }

    return new User();
  }

  /**
   * @param $createdBy
   */
  public function setCreatedBy($createdBy) {
    $this->createdBy = $createdBy;
  }

  /**
   * @return string (DateTime)
   */
  public function getLastModified() {
    return $this->lastModified;
  }

  /**
   * @param $lastModified
   */
  public function setLastModified($lastModified) {
    $this->lastModified = $lastModified;
  }

  /**
   * @return string
   */
  public function getIsotype() {
    return $this->isotype;
  }

  /**
   * @param string $isotype
   */
  public function setIsotype($isotype) {
    $this->isotype = $isotype;
  }

  /**
   * @return string
   */
  public function getTag() {
    return $this->tag;
  }

  /**
   * @param string $tag
   */
  public function setTag($tag) {
    $this->tag = $tag;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return string
   */
  public function getAntigenSymbol() {
    return $this->antigenSymbol;
  }

  /**
   * @param string $antigenSymbol
   */
  public function setAntigenSymbol($antigenSymbol) {
    $this->antigenSymbol = $antigenSymbol;
  }

  /**
   * @return string
   */
  public function getLocked() {
    return $this->locked;
  }

  /**
   * @param string $locked
   */
  public function setLocked($locked) {
    $this->locked = $locked;
  }

  /**
   * @return int
   */
  public function getWorkingGroupId() {
    return $this->workingGroupId;
  }

  /**
   * @param int $workingGroupId
   */
  public function setWorkingGroupId($workingGroupId) {
    $this->workingGroupId = $workingGroupId;
  }

  /**
   * @return \WorkingGroup
   */
  public function getWorkingGroup() {
    return WorkingGroupRepository::findById($this->workingGroupId);
  }

  /**
   * @return int
   */
  public function getSharingLevel() {
    return $this->sharingLevel;
  }

  public function getSharingLevelName() {
    return AntibodySharingLevel::getName($this->sharingLevel);
  }

  /**
   * @param int $sharingLevel
   */
  public function setSharingLevel($sharingLevel) {
    $this->sharingLevel = $sharingLevel;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getAlternativeName() {
    return $this->alternativeName;
  }

  /**
   * @param string $alternativeName
   */
  public function setAlternativeName($alternativeName) {
    $this->alternativeName = $alternativeName;
  }

  /**
   * @return string
   */
  public function getClonality() {
    return $this->clonality;
  }

  /**
   * @param string $clonality
   */
  public function setClonality($clonality) {
    $this->clonality = $clonality;
  }

  /**
   * @return string
   */
  public function getAntigen() {
    return $this->antigen;
  }

  /**
   * @param string $antigen
   */
  public function setAntigen($antigen) {
    $this->antigen = $antigen;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return string
   */
  public function getCatalogNo() {
    return $this->catalogNo;
  }

  /**
   * @param string $catalog_no
   */
  public function setCatalogNo($catalogNo) {
    $this->catalogNo = $catalogNo;
  }

  /**
   * @return string
   */
  public function getLotNo() {
    return $this->lotNo;
  }

  /**
   * @param $lotNo string
   */
  public function setLotNo($lotNo) {
    $this->lotNo = $lotNo;
  }

  /**
   * @return string
   */
  public function getStorageInstruction() {
    return $this->storageInstruction;
  }

  /**
   * @param $storageInstruction
   */
  public function setStorageInstruction($storageInstruction) {
    $this->storageInstruction = $storageInstruction;
  }

  /**
   * @return string
   */
  public function getLocalization() {
    return $this->localization;
  }

  /**
   * @param string $localization
   */
  public function setLocalization($localization) {
    $this->localization = $localization;
  }

  /**
   * @return string
   */
  public function getReceiptDate() {
    return $this->receiptDate;
  }

  /**
   * @param $receiptDate
   */
  public function setReceiptDate($receiptDate) {
    $this->receiptDate = $receiptDate;
  }

  /**
   * @param $preparationDate
   */
  public function setPreparationDate($preparationDate) {
    $this->preparationDate = $preparationDate;
  }

  /**
   * @return string
   */
  public function getPreparationDate() {
    return $this->preparationDate;
  }

  /**
   * @return string
   */
  public function getCompany() {
    return $this->company;
  }

  /**
   * @param string $company
   */
  public function setCompany($company) {
    $this->company = $company;
  }

  /**
   * Returns an array containing all application descriptions correlated to this
   * antibody.
   *
   * @return \AntibodyAntibodyApplication[]
   */
  public function getApplications() {

    if ($this->isEmpty()) {
      return [];
    }

    return AntibodyAntibodyApplicationsRepository::findByAntibodyId($this->id);
  }

  /**
   * Returns an array with application details for a given AntibodyApplication
   * type (defined by application abbreviation).
   *
   * @param $applicationAbbr string
   *   Application abbreviation
   *
   * @return \AntibodyAntibodyApplication[]
   */
  public function getApplication($applicationAbbr) {
    $app = AntibodyAntibodyApplicationsRepository::findByAntibodyIdAndByApplication($this->id,
      $applicationAbbr);
    return $app;
  }

  /**
   * Returns distinct list of applications (here: AntibodyAntibodyApplications)
   *
   * Important
   * Each application occurs, in contract to getApplication, only once.
   *
   * @return \AntibodyAntibodyApplication[]
   */
  public function getApplicationsDistinctList() {
    return AntibodyAntibodyApplicationsRepository::findByAntibodyIdGroupByApplication($this->getId());
  }

  /**
   * Returns a mean quality for a given application
   *
   * @param $applicationAbbr
   *   Abbreviation of application
   *
   * @return float
   */
  public function getApplicationQuality($applicationAbbr) {

    // get all comments for given application
    $appEntries = AntibodyAntibodyApplicationsRepository::findByAntibodyIdAndByApplication($this->id,
      $applicationAbbr);

    $quality = 0;
    $qualityNull = 0;

    foreach ($appEntries as $entry) {
      $quality += $entry->getQuality();
      if ($entry->getQuality() == 0) {
        $qualityNull++;
      }
    }

    if (count($appEntries) == $qualityNull) {
      return 0;
    }

    return ($quality / (count($appEntries) - $qualityNull));
  }

  /**
   * Returns a mean quality for all applications
   *
   * @see getApplicationQuality()
   *
   * @return float
   */
  public function getOverallQuality() {

    // get each type of publication described for this antibody
    $applications = $this->getApplicationsDistinctList();

    if (count($applications) == 0) {
      return 0;
    }

    $quality_sum = 0;
    $quality_null = 0;

    foreach ($applications as $application) {
      $app_quality = $this->getApplicationQuality($application->getApplicationAbbr());
      $quality_sum += $app_quality;
      if ($app_quality == 0) {
        $quality_null++;
      }
    }

    if (count($applications) == $quality_null) {
      return 0;
    }

    $quality = $quality_sum / (count($applications) - $quality_null);

    return $quality;
  }

  /**
   * Increment the views number
   *
   * @return int
   */
  public function incrementViews() {
    return AntibodiesRepository::incrementCounter($this->id);
  }

  /**
   * Associative array containing the number of views and anonymous views:
   * 'views' - total number of views
   * 'anonymous_views' - number of views did by anonymous users
   *
   * @return array
   */
  public function getCounter() {
    return AntibodiesRepository::getCounter($this->id);
  }

  public function getLinkedPublications() {
    $rdp_linking = RDPLinking::getLinkingByName('sfb_literature',
      'pubreg_articles');
    if ($rdp_linking != NULL) {
      $data = $rdp_linking->getViewData('antibody_antibody2', $this->id);
      return $data;
    }

    return ['table' => ['rows' => [], 'header' => []]];
  }

  /**
   * Returns a list with log entries related to this antibody.
   *
   * @return \AntibodyLog[]
   */
  public function getLogs() {
    return AntibodyLogsRepository::findByAntibodyId($this->id);
  }

  public function getEditUrl() {
    if ($this->type == AntibodyType::PRIMARY) {
      return sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY_EDIT,
        $this->getElementPID());
    }
    else {
      return sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY_EDIT,
        $this->getElementPID());
    }
  }

  public function getViewUrl() {
    if ($this->type == AntibodyType::PRIMARY) {
      return sfb_antibody_url(SFB_ANTIBODY_URL_PRIMARY_VIEW,
        $this->getElementPID());
    }
    else {
      return sfb_antibody_url(SFB_ANTIBODY_URL_SECONDARY_VIEW,
        $this->getElementPID());
    }
  }

  /* ------------------------------ Form fields ----------------------------- */
  // @see https://www.drupal.org/docs/7/api/form-api

  /**
   * Return an associative array with form control data for 'Antigen symbol'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldAntigenSymbol() {
    return [
      '#type' => 'textfield',
      '#title' => t('Antigen Symbol'),
      '#default_value' => $this->antigenSymbol,
      '#description' => 'HUGO Gene Nomenclature Committee (HGNC) www.genenames.org',
      '#attributes' => ['placeholder' => t('e.g. PPP1CA')],
      '#maxlength' => 100,
    ];
  }

  /**
   * Return an associative array with form control data for 'Antigen' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldAntigen() {
    return [
      '#type' => 'textfield',
      '#title' => t('Antigen'),
      '#default_value' => $this->antigen,
      '#maxlength' => 210,
    ];
  }

  /**
   * Return an associative array with form control data for 'Registry ID' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldRegistryId() {
    return [
      '#type' => 'textfield',
      '#title' => t('Antibody Registry ID'),
      '#description' => 'Enter all Antibody Registry IDs (separated by comma) from antibodyregistry.org.',
      '#default_value' => $this->registryId,
      '#maxlength' => 100,
      '#attributes' => ['placeholder' => t('e.g.: AB_398040')],
    ];
  }

  /**
   * Return an associative array with form control data for 'Name' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldName() {
    return [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $this->name,
      '#attributes' => ['placeholder' => t('e.g.') . ': PP1'],
      '#maxlength' => 100,
    ];
  }

  /**
   * Return an associative array with form control data for 'LabID' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldLabId() {
    return [
      '#type' => 'textfield',
      '#title' => t('LabID'),
      '#default_value' => $this->labId,
      '#attributes' => ['placeholder' => t('e.g.') . ': 42'],
      '#maxlength' => 10,
    ];
  }

  /**
   * Return an associative array with form control data for 'Alternative name'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldAltName() {
    return [
      '#type' => 'textfield',
      '#title' => t('Alternative Name'),
      '#default_value' => $this->alternativeName,
      '#attributes' => ['placeholder' => t('e.g.') . ': Protein phosphatase 1'],
      '#maxlength' => 100,
    ];
  }

  /**
   * Return an associative array with form control data for 'Tag' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldTag() {
    return [
      '#type' => 'textfield',
      '#title' => t('Tag / Fluorophore'),
      '#default_value' => $this->tag,
      '#description' => 'Information on a potential antibody tag for detection or purification',
      '#maxlength' => 100,
      '#autocomplete_path' => SFB_ANTIBODY_API_AUTOCOMPLETE_TAG,
      //TODO: placeholder mit example fehlt
    ];
  }

  /**
   * Return an associative array with form control data for 'Raised in species'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldRaisedInSpecies() {
    return [
      '#type' => 'textfield',
      '#title' => t('Raised in (species)'),
      '#default_value' => $this->raisedInSpecies,
      '#description' => 'Enter the species of the organism, where the current antibody was raised in.',
      '#maxlength' => 100,
      '#autocomplete_path' => SFB_ANTIBODY_API_AUTOCOMPLETE_SPECIES,
      '#attributes' => ['placeholder' => t('e.g.') . ': Mouse'],
    ];
  }

  /**
   * Returns an array with form field which is used to entering reacting
   * species.
   *
   * This fields uses select2-library which comes from sfb_commons module
   *
   * More about Drupal Form API:
   * https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7.x
   *
   * @see sfb_commons_add_select2()
   *
   * @return array
   */
  public function getFormFieldReactsWith() {

    // add select2 to library from sfb_commons module
    sfb_commons_add_select2(SFB_ANTIBODY_API_SPECIES, 'reacts-with',
      'formatSpecies', 'formatSpeciesSelection', 2, 'e.g. Mouse, Human, Pig');

    $reacts_with_species_options = [];
    $reacts_with_species_options_selected = [];
    foreach ($this->getReactsWith() as $species) {
      $reacts_with_species_options[] = [$species->getName() => $species->getName()];
      $reacts_with_species_options_selected[] = $species->getName();
    }

    return [
      '#type' => 'select',
      '#title' => t('Antibody reacts with (species)'),
      '#attributes' => ['class' => ['reacts-with']],
      '#options' => $reacts_with_species_options,
      '#default_value' => $reacts_with_species_options_selected,
      '#validated' => 'true',
      '#multiple' => 'true',
    ];
  }

  /**
   * Return an associative array with form control data for 'Isotype' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldIsotype() {
    return [
      '#type' => 'textfield',
      '#title' => t('Isotype'),
      '#default_value' => $this->isotype,
      '#description' => '',
      '#maxlength' => 100,
      '#attributes' => ['placeholder' => t('e.g.') . ': IgG2b'],
    ];
  }

  /**
   * Return an associative array with form control data for 'Excitation max'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldExcitationMax() {
    return [
      '#type' => 'textfield',
      //'#element_validate' => array('sfb_antibody_secondary_validate'), //TODO: muss noch implementiert werden, da die zielfunktion sowohl in new, als auch in edit verwendet wird
      '#title' => t('Excitation Max. (in nm)'),
      '#default_value' => $this->excitationMax,
      '#description' => 'Enter the maximum excitation in nanometers of the current antibody.',
      '#maxlength' => 10,
    ];
  }

  /**
   * Return an associative array with form control data for 'Emission max'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldEmissionMax() {
    return [
      '#type' => 'textfield',
      //'#element_validate' => array('sfb_antibody_secondary_validate'),//TODO: muss noch implementiert werden, da die zielfunktion sowohl in new, als auch in edit verwendet wird
      '#title' => t('Emission Max. (in nm)'),
      '#default_value' => $this->emissionMax,
      '#description' => 'Enter the maximum emission in nanometers of the current antibody.',
      '#maxlength' => 10,
    ];
  }

  /**
   * Return an associative array with form control data for 'Clone' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldClone() {
    return [
      '#type' => 'textfield',
      '#title' => t('Clone'),
      '#default_value' => $this->clone,
      '#description' => '',
      '#maxlength' => 100,
    ];
  }

  /**
   * Return an associative array with form control data for 'Clonality' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldClonality() {
    return [
      '#type' => 'select',
      '#title' => t('Clonality'),
      #'#required' => TRUE,
      '#default_value' => $this->clonality,
      '#description' => 'Select applicable clonality.',
      '#options' => [
        'unknown' => 'unknown',
        'polyclonal' => 'polyclonal',
        'monoclonal' => 'monoclonal',
      ],
    ];
  }

  /**
   * Return an associative array with form control data for 'Catalog no' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldCatalogNo() {
    return [
      '#type' => 'textfield',
      '#title' => t('Catalog No.'),
      '#default_value' => $this->catalogNo,
      '#attributes' => ['placeholder' => t('e.g.') . ': A-21050'],
      '#maxlength' => 120,
    ];
  }

  /**
   * Return an associative array with form control data for 'Lot no' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldLotNo() {
    return [
      '#type' => 'textfield',
      '#title' => t('Lot No.'),
      '#default_value' => $this->lotNo,
      '#maxlength' => 120,
    ];
  }

  /**
   * Return an associative array with form control data for 'Description' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldDescription() {
    return [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $this->description,
    ];
  }

  /**
   * Return an associative array with form control data for 'Localization'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldLocalization() {
    return [
      '#type' => 'textarea',
      '#title' => t('Localization'),
      '#default_value' => $this->localization,
    ];
  }

  /**
   * Return an associative array with form control data for 'Storage
   * instruction' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldStorageInstruction() {
    return [
      '#type' => 'textarea',
      '#title' => t('Storage Instruction'),
      '#default_value' => $this->storageInstruction,
    ];
  }

  /**
   * Drupal form field: ReceiptDate (Date)
   *
   * @return array
   */
  public function getFormFieldReceiptDate() {

    // receiptDate uses mysql date format YYYY-MM-DD and it needs to be
    // converted to Drupal date field format which consists of an array with
    // three fields: year, month and day

    // prepare data for drupals date field
    $date_value = $this->receiptDate;
    if (!empty($this->receiptDate)) {
      $date_value = [
        'year' => explode('-', $this->receiptDate)[0],
        'month' => explode('-', $this->receiptDate)[1],
        'day' => explode('-', $this->receiptDate)[2],
      ];
    }

    return [
      '#type' => 'date',
      '#title' => t('Receipt Date'),
      '#default_value' => $date_value,
    ];
  }

  /**
   * Drupal form field: PreparationDate (Date)
   *
   * @return array
   */
  public function getFormFieldPreparationDateNew() {

    // since preparationDate uses default mysql date format YYYY-MM-DD, it needs
    // to be converted to Drupal date field format
    $date_value = $this->preparationDate;
    if (!empty($date_value)) {
      $date_value = [
        'year' => explode('-', $this->preparationDate)[0],
        'month' => intval(explode('-', $this->preparationDate)[1]),
        'day' => intval(explode('-', $this->preparationDate)[2]),
      ];
    }

    return [
      '#type' => 'date',
      '#title' => t('Preparation Date'),
      '#default_value' => $date_value,
    ];
  }

  /**
   * Return an associative array with form control data for 'Crafted by' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldCraftedBy() {
    return [
      '#type' => 'select',
      '#title' => t('Crafted by'),
      '#description' => 'Select the producer of this antibody.',
      '#default_value' => $this->craftedBy,
      '#options' => [
        'unknown' => 'unknown',
        'self-made' => 'self-made',
        'company' => 'company',
      ],
    ];
  }

  /**
   * Return an associative array with form control data for 'Demasking' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldDemasking() {
    return [
      '#type' => 'select',
      '#title' => t('Demasking'),
      '#default_value' => $this->demasking,
      '#options' => [
        'unknown' => 'unknown',
        'yes' => 'yes',
        'no' => 'no',
      ],
    ];
  }

  /**
   * Return an associative array with form control data for 'Company' field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldCompany() {
    return [
      '#type' => 'textfield',
      '#title' => t('Company / Manufacturer'),
      '#default_value' => $this->company,
      '#attributes' => ['placeholder' => t('e.g.') . ': Life Technologies'],
      '#description' => 'Enter company name or abbreviation which crafted the antibody.',
      '#maxlength' => 100,
      '#autocomplete_path' => SFB_ANTIBODY_API_AUTOCOMPLETE_COMPANY,
    ];
  }

  /**
   * Return an associative array with form control data for 'Antibodypedia URL'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldAntibodypediaUrl() {
    return [
      '#type' => 'textfield',
      '#title' => t('Antibodypedia URL'),
      '#maxlength' => 120,
      '#default_value' => $this->antibodypediaUrl,
      '#attributes' => ['placeholder' => t('e.g.') . ': http://www.antibodypedia.com/gene/3872/PPP1CA/antibody/12100/sc-7482'],
    ];
  }

  /**
   * Return an associative array with form control data for 'Working group'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldWorkingGroup() {
    global $user;

    $userc = UsersRepository::findByUid($user->uid);
    $workingGroups = $userc->getUserWorkingGroups();

    $workingGroupsSelect = [];
    foreach ($workingGroups as $workingGroup) {
      $workingGroupsSelect[$workingGroup->getId()] = $workingGroup->getName();
    }

    $default_value = $this->workingGroupId;

    //TODO: fall einbauen, dass der benutzer zu keiner arbeitsgruppe gehört

    return [
      '#type' => 'select',
      '#title' => t('Research Group'),
      '#required' => TRUE,
      '#options' => $workingGroupsSelect,
      '#default_value' => $default_value,
      '#description' => t('Select research group owning this antibody.'),
    ];
  }

  /**
   * Return an associative array with form control data for 'Sharing level'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldSharingLevel() {
    $allSharingLevels = AntibodySharingLevel::getAsArray();

    if (empty($this->sharingLevel)) {
      $this->sharingLevel = variable_get(SFB_ANTIBODY_CONFIG_DEFAULT_SHARING_LEVEL,
        AntibodySharingLevel::GROUP_LEVEL);
    }

    return [
      '#type' => 'select',
      '#title' => t('Sharing level'),
      '#description' => 'Change the visibility (sharing) level of this antibody.',
      '#suffix' => '
        <small>
        <strong>Group:</strong> Only members of your research group can see the current antibody.<br />
        <strong>Site:</strong> All users of the Research Data Platform are allowed to see the current antibody.<br />
        <strong>Public:</strong> Everyone (also anonymous user) is allowed to see the current antibody.<br />
        </small>',
      '#default_value' => $this->sharingLevel,
      '#required' => TRUE,
      '#options' => $allSharingLevels,
    ];
  }

  /**
   * Return an associative array with form control data for 'Add Image Button'
   * field.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldImageAdd() {
    return [
      '#type' => 'submit',
      '#value' => t('Add'),
      '#name' => 'AddImageButton',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'image-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => ['class' => ['btn-primary btn-sm'], ],
    ];
  }

  /* ------------------------------ Field views ----------------------------- */

  /**
   * Returns a database ID of this antibody.
   *
   * @return string
   */
  public function getElementId() {
    // If ID padding is used, then remove superfluous '0' characters.
    return str_pad($this->id,
      variable_get(SFB_ANTIBODY_CONFIG_PID_ID_PADDING, 4), '0', STR_PAD_LEFT);
  }

  /**
   * Returns the PID of this antibody.
   *
   * @return string
   */
  public function getElementPID() {

    $pid_id = $this->id;

    // if padding > 0, then pad a id string to given length
    if (variable_get(SFB_ANTIBODY_CONFIG_PID_ID_PADDING, 4) > 0) {
      $pid_id = str_pad($pid_id,
        variable_get(SFB_ANTIBODY_CONFIG_PID_ID_PADDING, 4), 0, STR_PAD_LEFT);
    }

    $pattern = '';
    if ($this->getType() == AntibodyType::PRIMARY) {
      $pattern = variable_get(SFB_ANTIBODY_CONFIG_PID_PRIMARY_PATTERN,
        '{type}-{PID}');
    }
    else {
      if ($this->getType() == AntibodyType::SECONDARY) {
        $pattern = variable_get(SFB_ANTIBODY_CONFIG_PID_SECONDARY_PATTERN,
          '{type}-{PID}');
      }
    }

    $replaced = preg_replace(['/{type}/', '/{PID}/'],
      [$this->getType(), $pid_id], $pattern);

    return $replaced;
  }

  /**
   * Returns the EPIC PID of this antibody
   *
   * @return string
   */
  public function getElementEPICPID() {
    return variable_get(SFB_ANTIBODY_CONFIG_PID_EPIC_URL,
        '') . $this->getElementPID();
  }

  /**
   * Returns the type of this antibody
   *
   * @return string
   * @see AntibodyType
   */
  public function getElementType() {
    if ($this->type == AntibodyType::PRIMARY) {
      return 'Primary';
    }
    if ($this->type == AntibodyType::SECONDARY) {
      return 'Secondary';
    }
  }

  /**
   * Returns the value of 'Antigen symbol' field.
   * Return value might be HTML formatted.
   *
   * @return string
   */
  public function getElementAntigenSymbol() {
    return $this->antigenSymbol;
  }

  /**
   * Returns html formatted mean quality of this application.
   *
   * @param $applicationAbbr
   *
   * @return string
   */
  public function getElementApplicationQuality($applicationAbbr) {
    $applicationQuality = $this->getApplicationQuality($applicationAbbr);

    return theme('sfb_antibody_quality', ['quality' => $applicationQuality]);
  }

  /**
   * Returns html foramtted string with overall quality of this antibody
   *
   * @return string
   */
  public function getElementOverallQuality() {
    $quality = $this->getOverallQuality();
    return theme('sfb_antibody_quality',
      ['quality' => $quality, 'quality_string' => TRUE]);
  }

  /**
   * Returns html formatted string with working group name.
   *
   * @return string
   */
  public function getElementWorkingGroup() {
    $workingGroup = $this->getWorkingGroup();
    if ($workingGroup->isEmpty()) {
      return '-';
    }
    return $workingGroup->getName();
  }

  /**
   * Returns html formatted stirng with working group icon.
   *
   * @return string
   */
  public function getElementWorkingGroupIcon() {
    $workingGroup = $this->getWorkingGroup();
    $wgName = $workingGroup->getName();

    if ($this->sharingLevel == AntibodySharingLevel::PUBLIC_LEVEL) {

      if (user_is_logged_in()) {

        $userc = User::getCurrent();

        if ($userc->isMemberOfWorkingGroup($workingGroup)) {
          return '<img src="' . base_path()
            . drupal_get_path('module', 'sfb_commons')
            . '/resources/ab_public_own20x20.png" alt="" 
            data-toggle="tooltip" title="' . $wgName . '" />';
        }
      }

      return '<img src="' . base_path()
        . drupal_get_path('module', 'sfb_commons')
        . '/resources/ab_public20x20.png" alt="" data-toggle="tooltip" 
        title="' . $wgName . '" />';
    }

    return '<img src="' . $workingGroup->getIconPath()
      . '" alt="" data-toggle="tooltip" title="' . $wgName . '" />';
  }

  /**
   * Returns html formatted string with the name of the sharing level.
   *
   * @return string
   */
  public function getElementSharingLevel() {
    $level = 'Unknown';

    switch ($this->sharingLevel) {
      case AntibodySharingLevel::PRIVATE_LEVEL:
        $level = 'Private';
        break;
      case AntibodySharingLevel::GROUP_LEVEL:
        $level = 'Group';
        break;
      case AntibodySharingLevel::SITE_LEVEL:
        $level = 'Site';
        break;
      case AntibodySharingLevel::PUBLIC_LEVEL:
        $level = 'Public';
        break;
    }

    return $level;
  }

  /**
   * Returns html formatted value of the 'Name' field.
   *
   * @return string
   */
  public function getElementName() {
    return $this->name;
  }

  /**
   * Returns html formatted value of the 'Alternative name' field.
   *
   * @return string
   */
  public function getElementAlternativeName() {
    return $this->alternativeName;
  }

  /**
   * Returns html formatted value of the 'Clonality' field.
   *
   * @return string
   */
  public function getElementClonality() {
    return $this->clonality;
  }

  /**
   * Returns html formatted value of the 'Antigen' field.
   *
   * @return string
   */
  public function getElementAntigen() {
    return $this->antigen;
  }

  /**
   * Returns html formatted value of the 'Description' field.
   *
   * @return string
   */
  public function getElementDescription() {
    return $this->description;
  }

  /**
   * Returns html formatted value of the 'Catalog no.' field.
   *
   * @return string
   */
  public function getElementCatalogNo() {
    return $this->catalogNo;
  }

  /**
   * Returns html formatted value of the 'Lot no.' field.
   *
   * @return string
   */
  public function getElementLotNo() {
    return $this->lotNo;
  }

  /**
   * Returns html formatted value of the 'Clone' field.
   *
   * @return string
   */
  public function getElementClone() {
    return $this->clone;
  }

  /**
   * Returns html formatted value of the 'Tag' field.
   *
   * @return string
   */
  public function getElementTag() {
    return $this->tag;
  }

  /**
   * Returns HTML-formatted string with species reacting with this antibody.
   *
   * @return string HTML-formatted string
   */
  public function getElementReactsWith() {
    $output = '';

    $species = $this->getReactsWith();

    for ($i = 0; $i < count($species); $i++) {
      $output .= $species[$i]->getName();
      if ($i != count($species) - 1) {
        $output .= ', ';
      }
    }

    return $output;
  }

  /**
   * Returns html formatted value of the 'Raised in' field.
   *
   * @return string
   */
  public function getElementRaisedIn() {
    return $this->raisedInSpecies;
  }

  /**
   * Returns html formatted value of the 'Emission max' field.
   *
   * @return string
   */
  public function getElementEmissionMax() {
    return $this->emissionMax;
  }

  /**
   * Returns html formatted value of the 'Excitation max' field.
   *
   * @return string
   */
  public function getElementExcitationMax() {
    return $this->excitationMax;
  }

  /**
   * Returns html formatted value of the 'Demasking' field.
   *
   * @return string
   */
  public function getElementDemasking() {
    return $this->demasking;
  }

  /**
   * Returns html formatted (user) name of the creator of this antibody.
   *
   * @return string
   */
  public function getElementCreatedBy() {

    $userc = UsersRepository::findByUid($this->createdBy);

    if ($userc->isEmpty()) {
      return "unknown";
    }

    return $userc->getFullname(TRUE);
  }

  /**
   * Returns html formatted (user) name who last modified this antibody.
   *
   * @return string
   */
  public function getElementLastModified() {
    $userc = UsersRepository::findByUid($this->lastModified);

    if ($userc->isEmpty()) {
      return "unknown";
    }

    return $userc->getFullname(TRUE);
  }

  /**
   * Returns html formatted value of the 'Storage isntruction' field.
   *
   * @return string
   */
  public function getElementStorageInstruction() {
    return $this->storageInstruction;
  }

  /**
   * Returns html formatted value of the 'Localization' field.
   *
   * @return string
   */
  public function getElementLocalization() {
    return $this->localization;
  }

  /**
   * Returns html formatted value of the 'Isotype' field.
   *
   * @return string
   */
  public function getElementIsotype() {
    return $this->isotype;
  }

  /**
   * Returns html formatted value of the 'Receipt date' field.
   *
   * @return string
   */
  public function getElementReceiptDate() {
    return $this->receiptDate;
  }

  /**
   * Returns html formatted value of the 'Preparation date' field.
   *
   * @return string
   */
  public function getElementPreparationDate() {
    return $this->preparationDate;
  }

  /**
   * Returns html formatted value of the 'Company' field.
   *
   * @return string
   */
  public function getElementCompany() {
    return $this->company;
  }

  /**
   * Returns html formatted value of the 'Registry ID' field.
   *
   * @return string
   */
  public function getElementRegistryId($suppressHTML = FALSE) {
    $output = '';

    if (empty(trim($this->registryId))) {
      return $output;
    }

    if ($suppressHTML) {
      $output = $this->registryId;
    }
    else {
      $registry_ids = explode(',', str_replace(";", ",", $this->registryId));

      for ($i = 0; $i < count($registry_ids); $i++) {
        $output .= '<a href="' . variable_get(SFB_ANTIBODY_CONFIG_ANTIBODYREGISTRY_URL,
            'http://antibodyregistry.org/search?q=') . trim($registry_ids[$i]) . '">' . $registry_ids[$i] . '</a>';
        if ($i + 1 < count($registry_ids)) {
          $output .= ', ';
        }
      }
    }


    return $output;
  }

  /**
   * Returns html formatted value of the 'Crafted by' field.
   *
   * @return string
   */
  public function getElementCraftedBy() {
    return $this->craftedBy;
  }

  /**
   * Returns html formatted link to the antibodypedia.
   *
   * @return string
   */
  public function getElementAntibodypediaUrl() {
    if (empty($this->antibodypediaUrl)) {
      return '-';
    }

    return l($this->antibodypediaUrl, $this->antibodypediaUrl);
  }

  /**
   * Returns html formatted link to the HGNC.
   *
   * @return string
   */
  public function getElementHGNCUrl() {
    if (empty($this->antigenSymbol)) {
      return '-';
    }

    return l('http://www.genenames.org/cgi-bin/gene_symbol_report?match=' . $this->getAntigenSymbol(),
      'http://www.genenames.org/cgi-bin/gene_symbol_report?match=' . $this->getAntigenSymbol());
  }

  public function getHGNCUrl() {
    if (empty($this->antigenSymbol)) {
      return '-';
    }

    return 'http://www.genenames.org/cgi-bin/gene_symbol_report?match=' . $this->getAntigenSymbol();
  }

  /**
   * Returns html formatted link to the wikipedia.
   *
   * @return string
   */
  public function getElementWikipediaUrl() {
    if (empty($this->antigenSymbol)) {
      return '-';
    }

    return l('http://en.wikipedia.org/wiki/' . $this->antigenSymbol,
      'http://en.wikipedia.org/wiki/' . $this->antigenSymbol);
  }

  public function getWikipediaUrl() {
    if (empty($this->antigenSymbol)) {
      return '-';
    }

    return'http://en.wikipedia.org/wiki/' . $this->antigenSymbol;
  }

  /**
   * Returns html formatted link to the Uniprot.
   *
   * @return string
   */
  public function getElementUniprotUrl() {
    if (empty($this->antigenSymbol)) {
      return '-';
    }

    return l('http://www.uniprot.org/uniprot/?query=' . $this->antigenSymbol,
      'http://www.uniprot.org/uniprot/?query=' . $this->antigenSymbol);
  }

  public function getUniprotUrl() {
    if (empty($this->antigenSymbol)) {
      return '-';
    }

    return 'http://www.uniprot.org/uniprot/?query=' . $this->antigenSymbol;
  }

  /**
   * Returns html formatted link to the Gene Cards.
   *
   * @return string
   */
  public function getElementGeneCardsUrl() {
    if (empty($this->antigenSymbol)) {
      return '-';
    }

    return l('http://www.genecards.org/cgi-bin/carddisp.pl?gene=' . $this->antigenSymbol,
      'http://www.genecards.org/cgi-bin/carddisp.pl?gene=' . $this->antigenSymbol);
  }

  public function getGeneCardsUrl() {
    if (empty($this->antigenSymbol)) {
      return '-';
    }

    return 'http://www.genecards.org/cgi-bin/carddisp.pl?gene=' . $this->antigenSymbol;
  }

  public function getElementAntibodyRegistryUrls() {
    $output = '';

    if (empty(trim($this->registryId))) {
      return '-';
    }

    $registry_ids = explode(',', str_replace(";", ",", $this->registryId));

    for ($i = 0; $i < count($registry_ids); $i++) {
      $output .= '<a href="' . variable_get(SFB_ANTIBODY_CONFIG_ANTIBODYREGISTRY_URL,
          'http://antibodyregistry.org/search?q=') . trim($registry_ids[$i]) . '">' . variable_get(SFB_ANTIBODY_CONFIG_ANTIBODYREGISTRY_URL,
          'http://antibodyregistry.org/search?q=') . trim($registry_ids[$i]) . '</a>';
      if ($i + 1 < count($registry_ids)) {
        $output .= '<br /> ';
      }
    }

    return $output;
  }

  public function getAntibodyRegistryUrls() {
    $output = '';

    if (empty(trim($this->registryId))) {
      return '-';
    }

    $registry_ids = explode(',', str_replace(";", ",", $this->registryId));

    for ($i = 0; $i < count($registry_ids); $i++) {
      $output .= variable_get(SFB_ANTIBODY_CONFIG_ANTIBODYREGISTRY_URL,
          'http://antibodyregistry.org/search?q=') . trim($registry_ids[$i]);
    }

    return $output;
  }

  /* --------------------------- Other functions ---------------------------- */

  /**
   * Saves the data of this antibody to the database.
   */
  public function save() {
    $nid = AntibodiesRepository::save($this);
    if ($this->isEmpty()) {
      $this->id = $nid;
    }

    // get all species reacting with this antibody
    // it is important, that species are load at this point, because the array
    // with 'reacts with' species might have not been initiated yet.
    $currentSpecies = $this->getReactsWith();

    // remove all current species/tags reacting with this antibody from db
    AntibodySpeciesRepository::deleteSpeciesReactingWithAntibody($this->id);

    // store species reacting with this antibody
    foreach ($currentSpecies as $species) {
      // save species to get species_id
      $species->save();
      // now save the relationship:    antibody <- reacts-with -> species
      AntibodySpeciesRepository::saveSpeciesReactingWithAntibody($this->id,
        $species->getId());
    }

  }

  /**
   * Checks whether this antibody is empty.
   * If an antibody is empty, that means, that it has not yet been stored into
   * database.
   *
   * @return bool
   */
  public function isEmpty() {
    if ($this->id == Antibody::EMPTY_ANTIBODY_ID) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Checks the access or action permission to/of this antibody.
   *
   * @see AntibodyShareLevel
   * @see AntibodyAction
   *
   * @param AntibodyAction $action
   *
   * @return bool
   */
  public function userHasPermissionTo($action) {
    global $user;
    $userc = UsersRepository::findByUid($user->uid);

    if ($action == AntibodyAction::READ) {

      if ($this->getSharingLevel() == AntibodySharingLevel::PUBLIC_LEVEL) {
        return TRUE;
      }

      if (user_is_anonymous()) {
        return FALSE;
      }

      if (user_access('administer site configuration')){
        return TRUE;
      }

      if ($this->getSharingLevel() == AntibodySharingLevel::SITE_LEVEL) {
        return TRUE;
      }

      $userc = UsersRepository::findByUid($user->uid);

      if ($userc->isEmpty()) {
        return FALSE;
      }

      if ($this->getSharingLevel() == AntibodySharingLevel::GROUP_LEVEL) {
        foreach ($userc->getUserWorkingGroups() as $wg) {
          if ($this->workingGroupId == $wg->getId()) {
            return TRUE;
          }
        }
      }

      return FALSE;
    }
    else {
      if ($action == AntibodyAction::WRITE) {

        // following conditions must be met to edit/write the antibody:
        // - user must be logged in
        // - user must have antibody registrar permission
        // - user must be a member of the research group owning this antibody

        if (!user_is_anonymous() && user_access(SFB_ANTIBODY_PERMISSION_REGISTRAR)
          && $this->userHasPermissionTo(AntibodyAction::READ)) {

          $userc = UsersRepository::findByUid($user->uid);

          foreach ($userc->getUserWorkingGroups() as $wg) {
            if ($this->workingGroupId == $wg->getId()) {
              return TRUE;
            }
          }

          if (user_access('administer site configuration')){
            return TRUE;
          }

          return FALSE;
        }
        return FALSE;

      }
      else {
        if ($action == AntibodyAction::EXTEND) {
          return $this->userHasPermissionTo(AntibodyAction::READ);
        }
      }
    }

    return FALSE;
  }

  /* -------------------------- Static functions ---------------------------- */

  /**
   * Returns the database ID of an antibody by extracting the id from pid
   * string.
   *
   * If an ID cannot be found by regular expression then null will be returned.
   *
   * @param $pid
   *
   * @return mixed|null
   */
  public static function extractIDFromPID($pid) {

    if (empty($pid)) {
      return NULL;
    }

    // Read the regular expression for id extraction. This value can be set in
    // the administration panel.
    // If the value has not been set yet, than return given pid.
    $regex = variable_get(SFB_ANTIBODY_CONFIG_PID_REGEX, '');
    if (empty($regex)) {
      return $pid;
    }

    //
    // Parse the PID by using regular expression.
    //

    $match = [];
    if (preg_match($regex, $pid, $match)) {

      $id_index = variable_get(SFB_ANTIBODY_CONFIG_PID_REGEX_ID_INDEX, 0);

      //
      // Since the return value of preg_match function ($match) can contain
      // more than one return values, the administrator can set more than
      // one value for getting an ID.
      // This is useful if you plan to support more than one antibody PID pattern.
      //
      foreach (explode(',', $id_index) as $index) {
        if (count($match) == $index + 1 && isset($match[$index])) {
          if ($match[$index] > 0) {
            return $match[$index];
          }
        }
      }

    }
    else {
      return NULL;
    }

    return NULL;

  }
}

/**
 * Class AntibodyType
 *
 * Enumerator for the types of the antibody.
 */
class AntibodyType {

  const PRIMARY = 'primary';

  const SECONDARY = 'secondary';
}

/**
 * Class AntibodySharingLevel
 *
 * Defines possible access levels of an antibody.
 *
 */
class AntibodySharingLevel {

  const PRIVATE_LEVEL = 10;

  const GROUP_LEVEL = 20;

  const SITE_LEVEL = 30;

  const PUBLIC_LEVEL = 90;

  public static function getAsArray() {
    return [
      #10 => 'Private',
      20 => 'Group',
      30 => 'Site',
      90 => 'Public',
    ];
  }

  /**
   * Returns the human readable name of AntibodySharingLevel.
   *
   * @param $sharing_level
   * @param bool $toLower
   *
   * @return string
   */
  public static function getName($sharing_level, $toLower = FALSE) {
    $sharing_level_name = '';

    switch ($sharing_level) {
      case AntibodySharingLevel::PRIVATE_LEVEL:
        $sharing_level_name = 'Private';
        break;
      case AntibodySharingLevel::GROUP_LEVEL:
        $sharing_level_name = 'Group';
        break;
      case AntibodySharingLevel::SITE_LEVEL:
        $sharing_level_name = 'Site';
        break;
      case AntibodySharingLevel::PUBLIC_LEVEL:
        $sharing_level_name = 'Public';
        break;
      default:
        $sharing_level_name = 'Unknown';
    }

    if ($toLower) {
      return strtolower($sharing_level_name);
    }

    return $sharing_level_name;
  }

  /* ------------------------------ Form fields ----------------------------- */
  public static function getFormFieldSharingLevel() {
    $sharing_levels = AntibodySharingLevel::getAsArray();
    return [
      '#type' => 'select',
      '#title' => t('Sharing Level'),
      '#description' => 'Select visibility of new antibody.',
      '#suffix' => 'Group: Only members of the same group as the creator can see the current antibody. 
      <br>Site: All users of the site are allowed to see the current antibody. <br>Public: Everyone is allowed to see the current antibody.',
      '#default_value' => 20,
      '#required' => TRUE,
      '#options' => $sharing_levels,
    ];
  }

  /**
   * Returns a database condition for access-safe Antibody table quering.
   *
   * @return \DatabaseCondition
   */
  public static function getDatabaseCondition() {
    $db_or = db_or();

    // public level
    $db_or->condition('sharing_level', AntibodySharingLevel::PUBLIC_LEVEL, '=');

    // site level
    if (user_is_anonymous()) {
      return $db_or;
    }

    $db_or->condition('sharing_level', AntibodySharingLevel::SITE_LEVEL, '=');

    // group level
    global $user;
    $userc = UsersRepository::findByUid($user->uid);
    foreach ($userc->getUserWorkingGroups() as $wg) {
      $db_and = db_and();
      $db_and->condition('sharing_level', AntibodySharingLevel::GROUP_LEVEL,
        '=');
      $db_and->condition('working_group_id', $wg->getId(), '=');
      $db_or->condition($db_and);
    }

    // private level
    //TODO: private level implementieren

    return $db_or;
  }

}

class AntibodyAction {

  const READ = 'read';

  const WRITE = 'write';

  const EXTEND = 'extend';
}
