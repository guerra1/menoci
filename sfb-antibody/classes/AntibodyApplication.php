<?php

class AntibodyApplication {

  /**
   * @var string
   */
  private $name;

  /**
   * @var string
   */
  private $abbreviation = null;

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getAbbreviation() {
    return $this->abbreviation;
  }

  /**
   * @param string $abbreviation
   */
  public function setAbbreviation($abbreviation) {
    $this->abbreviation = $abbreviation;
  }

  public function isEmpty() {
    if($this->abbreviation == null)
      return true;
    return false;
  }

  public function save() {
    AntibodyApplicationsRepository::save($this);
  }

}

class AntibodyApplicationsRepository {
  static $tableName = 'antibody_application';
  static $databaseFields = array('id', 'name', 'abbreviation');

  /**
   * @param $result
   * @return \AntibodyApplication
   */
  public static function databaseResultsToAntibodyApplication($result) {

    $application = new AntibodyApplication();

    if(empty($result)) {
      return $application;
    }

    $application->setName($result->name);
    $application->setAbbreviation($result->abbreviation);

    return $application;
  }

  /**
   * @param $results
   * @return \AntibodyApplication[]
   */
  public static function databaseResultsToAntibodyApplications($results) {
    $applications = array();
    foreach($results as $result)
      $applications[] = self::databaseResultsToAntibodyApplication($result);

    return $applications;
  }

  /**
   * @param AntibodyApplication $application
   */
  public static function save($application) {
    db_merge(self::$tableName)->key(array('abbreviation' => $application->getAbbreviation()))
      ->fields(array(
        'name' => $application->getName(),
        'abbreviation' => $application->getAbbreviation(),
      ))->execute();

    return true;
  }

  public static function findByAbbreviation($abbreviation) {
    $result = db_select(self::$tableName, 'aa')
      ->condition('abbreviation',$abbreviation, '=')
      ->fields('aa', self::$databaseFields)
      ->range(0,1)
      ->execute()
      ->fetch();

    return self::databaseResultsToAntibodyApplication($result);
  }

  /**
   * @param $like
   * @return \AntibodyApplication[]
   */
  public static function findLikeByAbbreviationOrByName($like){
    $db_or = db_or();
    $db_or->condition('abbreviation', db_like($like).'%','LIKE');
    $db_or->condition('name', db_like($like).'%','LIKE');

    $result = db_select(self::$tableName, 'aa')
      ->condition($db_or)
      ->fields('aa', self::$databaseFields)
      ->range(0,10)
      ->execute();

    return self::databaseResultsToAntibodyApplications($result);
  }

  public static function findAll() {
    $result = db_select(self::$tableName, 'aa')
      ->fields('aa', self::$databaseFields)
      ->execute();

    return self::databaseResultsToAntibodyApplications($result);
  }

}

abstract class AntibodyApplicationQuality {
  const Excellent = 5;
  const Good = 4;
  const Average = 3;
  const Fair = 2;
  const Poor = 1;
  const Unknown = 0;
}