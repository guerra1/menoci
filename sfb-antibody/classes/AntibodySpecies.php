<?php

class AntibodySpecies {
  const EMPTY_ANTIBODY_SPECIES_ID = -1;

  /**
   * @var int
   */
  private $id = AntibodySpecies::EMPTY_ANTIBODY_SPECIES_ID;

  /**
   * @var string
   */
  private $name;

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return bool
   */
  public function isEmpty() {
    if($this->id == self::EMPTY_ANTIBODY_SPECIES_ID)
      return true;
    return false;
  }

  public function save() {
    $id = AntibodySpeciesRepository::save($this);

    if($this->isEmpty())
      $this->id = $id;
  }

}

class AntibodySpeciesRepository
{
  /**
   * Name of the table containing antibody information.
   *
   * @var string
   */
  static $tableName = 'antibody_species';

  /**
   * Array with database fields.
   *
   * @var array
   */
  static $databaseFields = array(
    'id',
    'name',
  );

  /**
   * Reads antibody database result and creates new Antibody object.
   *
   * @see Antibody
   *
   * @param $result
   * @return \AntibodySpecies
   */
  public static function databaseResultsToAntibodySpecies($result)
  {
    $species = new AntibodySpecies();

    if (empty($result)) {
      return $species;
    }
    $species->setId($result->id);
    $species->setName($result->name);

    return $species;
  }

  /**
   * @param $results
   * @return \AntibodySpecies[]
   */
  public static function databaseResultsToAntibodySpeciesmultiple($results)
  {
    $species = array();
    foreach ($results as $result)
      $species[] = self::databaseResultsToAntibodySpecies($result);

    return $species;
  }

  /**
   * Stores the species data into database.
   *
   * If you want to update some species use @see AntibodySpecies->update() instead.
   *
   * @param AntibodySpecies $species
   * @return An ID of stored AntibodySpecies
   */
  public static function save($species)
  {
    db_merge(self::$tableName)->key(array('name' => $species->getName()))
      ->fields(array(
        'name' => $species->getName(),
      ))->execute();

    return self::findByName($species->getName())->getId();
  }

  /**
   * Updates the species data
   *
   * @param $species \AntibodySpecies
   * @return int
   */
  public static function update($species) {
    db_merge(self::$tableName)->key(array('id' => $species->getId()))
      ->fields(array(
        'id' => $species->getId(),
        'name' => $species->getName(),
      ))->execute();

    return self::findByName($species->getName())->getId();
  }

  /**
   * @param $speciesId int
   * @return \AntibodySpecies
   */
  public static function findById($speciesId) {
    $result = db_select(self::$tableName, 'asp')
      ->condition('id', $speciesId, '=')
      ->fields('asp', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToAntibodySpecies($result);
  }

  /**
   * @param $species
   * @return \AntibodySpecies
   */
  public static function findByName($species)
  {
    $result = db_select(self::$tableName, 'asp')
      ->condition('name', $species, '=')
      ->fields('asp', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToAntibodySpecies($result);
  }

  /**
   * @param $results
   * @return AntibodySpecies[]
   */
  public static function findByNames($results)
  {
    $species = array();
    foreach ($results as $result)
      $species[] = self::findByName($result);

    return $species;
  }

  /**
   * Returns an array with AntibodySpecies matching condition defined by
   * $condition
   *
   * @param $condition Countable|QueryConditionInterface
   * @return \AntibodySpecies[]
   */
  public static function findBy($condition) {
    $results = db_select('antibody_species', 'asp')
      ->condition($condition)
      ->fields('asp')
      ->execute();

    return self::databaseResultsToAntibodySpeciesmultiple($results);
  }

  public static function findAll() {
    $results = db_select('antibody_species', 'asp')
      ->fields('asp')
      ->execute();

    return self::databaseResultsToAntibodySpeciesmultiple($results);
  }

  /**
   * Returns all species (AntibodySpecies objects) reacting with the
   * given antibody.
   *
   * @param $antibodyId An ID of the antibody
   * @return \AntibodySpecies[]
   */
  public static function findSpeciesReactingWithAntibody($antibodyId) {

    $rwselect = db_select('antibody_antibody_reacts_with_species', 'aarws')
      ->condition('antibody_id', $antibodyId, '=');

    $rwselect->join('antibody_species', 's', 'aarws.species_id = s.id');
    $rwselect = $rwselect->fields('s', array('name', 'id'));

    $rwselect = $rwselect->execute();

    return self::databaseResultsToAntibodySpeciesmultiple($rwselect);
  }

  /**
   * Deletes all species reacting with given antibody
   *
   * @param $antibodyId An ID of the antibody
   * @return \DatabaseStatementInterface
   */
  public static function deleteSpeciesReactingWithAntibody($antibodyId) {

    $num_deleted = db_delete('antibody_antibody_reacts_with_species')
      ->condition('antibody_id', $antibodyId, '=')
      ->execute();

    return $num_deleted;

  }

  /**
   * Stores the relationships between antibody with given ID and reacting species.
   *
   * @param $antibodyId An ID of the antibody
   * @param $speciesId An ID of the species
   * @return bool
   */
  public static function saveSpeciesReactingWithAntibody($antibodyId, $speciesId) {

    if($antibodyId == Antibody::EMPTY_ANTIBODY_ID || $speciesId == AntibodySpecies::EMPTY_ANTIBODY_SPECIES_ID)
      return false;

    db_merge('antibody_antibody_reacts_with_species')
      ->key(
        array(
          'antibody_id' => $antibodyId,
          'species_id' => $speciesId,
        ))
      ->fields(array(
        'antibody_id' => $antibodyId,
        'species_id' => $speciesId,
      ))
      ->execute();

    return true;
  }

  /**
   * @param $antibodyId
   * @param $speciesId
   * @return bool
   */
  public static function saveSpeciesRaisedInAntibody($antibodyId, $speciesId) {

    if($antibodyId == Antibody::EMPTY_ANTIBODY_ID || $speciesId == AntibodySpecies::EMPTY_ANTIBODY_SPECIES_ID)
      return false;

    db_merge('antibody_antibody_raised_in_species')
      ->key(
        array(
          'antibody_id' => $antibodyId,
          'species_id' => $speciesId,
        ))
      ->fields(array(
        'antibody_id' => $antibodyId,
        'species_id' => $speciesId,
      ))
      ->execute();

    return true;
  }



}