<?php

/**
 * @file
 * Antibodies database layer.
 *
 * @author Luca Freckmann (luca.freckmann@med.uni-goettingen.de)
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 */
class AntibodiesRepository {

  /**
   * Name of the table containing antibody information.
   *
   * @var string
   */
  static $tableName = 'antibody_antibody2';

  /**
   * Array with database fields.
   *
   * @var array
   */
  static $databaseFields = array(
    'id',
    'type',
    'antigen_symbol',
    'locked',
    'working_group_id',
    'sharing_level',
    'name',
    'alt_name',
    'clonality',
    'antigen',
    'description',
    'catalog_no',
    'lot_no',
    'storage_instruction',
    'localization',
    'receipt_date',
    'company',
    'crafted_by',
    'isotype',
    'raised_in_species',
    'tag',
    'excitation_max',
    'emission_max',
    'clone',
    'preparation_date',
    'created_by',
    'last_modified_by',
    'registry_id',
    'demasking',
    'antibodypedia_url',
    'export',
    'ready_for_submission',
    'created_date',
    'lab_id',
  );

  /**
   * Reads antibody database result and creates new Antibody object.
   *
   * @see Antibody
   *
   * @param $result
   * @return \Antibody
   */
  public static function databaseResultsToAntibody($result) {
    $antibody = new Antibody();

    if(empty($result)) {
      return $antibody;
    }

    // set variables
    $antibody->setId($result->id);
    $antibody->setType($result->type);
    $antibody->setAntigenSymbol($result->antigen_symbol);
    $antibody->setName($result->name);
    $antibody->setAlternativeName($result->alt_name);
    $antibody->setLocked($result->locked);
    $antibody->setClone($result->clone);
    $antibody->setWorkingGroupId($result->working_group_id);
    $antibody->setSharingLevel($result->sharing_level);
    $antibody->setClonality($result->clonality);
    $antibody->setAntigen($result->antigen);
    $antibody->setDescription($result->description);
    $antibody->setCatalogNo($result->catalog_no);
    $antibody->setLotNo($result->lot_no);
    $antibody->setStorageInstruction($result->storage_instruction);
    $antibody->setLocalization($result->localization);
    $antibody->setReceiptDate($result->receipt_date);
    $antibody->setCompany($result->company);
    $antibody->setCraftedBy($result->crafted_by);
    $antibody->setRaisedInSpecies($result->raised_in_species);
    $antibody->setIsotype($result->isotype);
    $antibody->setTag($result->tag);
    $antibody->setPreparationDate($result->preparation_date);
    $antibody->setCreatedBy($result->created_by);
    $antibody->setLastModified($result->last_modified_by);
    $antibody->setRegistryId($result->registry_id);
    $antibody->setDemasking($result->demasking);
    $antibody->setAntibodypediaUrl($result->antibodypedia_url);
    $antibody->setCreatedDate($result->created_date);
    $antibody->setEmissionMax($result->emission_max);
    $antibody->setExcitationMax($result->excitation_max);
    $antibody->setLabId($result->lab_id);
    #$antibody->setExport($result->export);
    #$antibody->setReadyForSubmission($result->ready_for_submission);

    return $antibody;
  }

  /**
   * Reads antibody database results and create an array with Antibody objects.
   *
   * @param $results
   * @return \Antibody[]
   */
  public static function databaseResultsToAntibodies($results) {
    $antibodies = array();
    foreach($results as $result)
      $antibodies[] = self::databaseResultsToAntibody($result);

    return $antibodies;
  }

  /**
   * Stores antibody data into database.
   *
   * @param Antibody $antibody
   * @return \DatabaseStatementInterface|int
   * @throws \InvalidMergeQueryException
   */
  public static function save($antibody) {
    if($antibody->isEmpty()) {
      $nid = db_insert(self::$tableName)
        ->fields(array(
          'type' => $antibody->getType(),
          'registry_id' => $antibody->getRegistryId(),
          'antigen_symbol' => $antibody->getAntigenSymbol(),
          'working_group_id' => $antibody->getWorkingGroupId(),
          'sharing_level' => $antibody->getSharingLevel(),
          'name' => $antibody->getName(),
          'alt_name' => $antibody->getAlternativeName(),
          'clonality' => $antibody->getClonality(),
          'antigen' => $antibody->getAntigen(),
          'description' => $antibody->getDescription(),
          'catalog_no' => $antibody->getCatalogNo(),
          'lot_no' => $antibody->getLotNo(),
          'storage_instruction' => $antibody->getStorageInstruction(),
          'localization' => $antibody->getLocalization(),
          'receipt_date' => $antibody->getReceiptDate(),
          'company' => $antibody->getCompany(),
          'crafted_by' => $antibody->getCraftedBy(),
          'raised_in_species' => $antibody->getRaisedInSpecies(),
          'tag' => $antibody->getTag(),
          'isotype' => $antibody->getIsotype(),
          'created_by' => $antibody->getCreatedBy(),
          'last_modified_by' => $antibody->getLastModified(),
          'demasking' => $antibody->getDemasking(),
          'antibodypedia_url' => $antibody->getAntibodypediaUrl(),
          'clone' => $antibody->getClone(),
          'preparation_date' => $antibody->getPreparationDate(),
          'created_date' => $antibody->getCreatedDate(),
          'excitation_max' => $antibody->getExcitationMax(),
          'emission_max' => $antibody->getEmissionMax(),
          'lab_id' => $antibody->getLabId(),
        ))
        ->execute();

      self::saveImages($antibody->getImages(), $nid);
      return $nid;
    } else {
      $num_updated = db_update(self::$tableName)
        ->fields(array(
          'type' => $antibody->getType(),
          'registry_id' => $antibody->getRegistryId(),
          'antigen_symbol' => $antibody->getAntigenSymbol(),
          'working_group_id' => $antibody->getWorkingGroupId(),
          'sharing_level' => $antibody->getSharingLevel(),
          'name' => $antibody->getName(),
          'alt_name' => $antibody->getAlternativeName(),
          'clonality' => $antibody->getClonality(),
          'antigen' => $antibody->getAntigen(),
          'description' => $antibody->getDescription(),
          'catalog_no' => $antibody->getCatalogNo(),
          'lot_no' => $antibody->getLotNo(),
          'storage_instruction' => $antibody->getStorageInstruction(),
          'localization' => $antibody->getLocalization(),
          'receipt_date' => $antibody->getReceiptDate(),
          'company' => $antibody->getCompany(),
          'crafted_by' => $antibody->getCraftedBy(),
          'raised_in_species' => $antibody->getRaisedInSpecies(),
          'tag' => $antibody->getTag(),
          'isotype' => $antibody->getIsotype(),
          'created_by' => $antibody->getCreatedBy(),
          'last_modified_by' => $antibody->getLastModified(),
          'demasking' => $antibody->getDemasking(),
          'antibodypedia_url' => $antibody->getAntibodypediaUrl(),
          'clone' => $antibody->getClone(),
          'preparation_date' => $antibody->getPreparationDate(),
          'created_date' => $antibody->getCreatedDate(),
          'excitation_max' => $antibody->getExcitationMax(),
          'emission_max' => $antibody->getEmissionMax(),
          'lab_id' => $antibody->getLabId(),
        ))
        ->condition('id',$antibody->getId(), '=')
        ->execute();

      self::saveImages($antibody->getImages(), $antibody->getId());
      return -1;
    }
  }

  /**
   * @param \AntibodyImage[] $images
   * @param int $id
   * @throws \InvalidMergeQueryException
   */
  static function saveImages($images, $id) {
    if ($images) {
      foreach ($images as $image) {
        $image->setAbappId($id);
        $image->save();
      }
    }
  }

  /**
   * Search for an antibody with given antibody id.
   *
   * @param $id
   * @return \Antibody
   */
  public static function findById($id) {
    $result = db_select(self::$tableName,'a')
      ->condition('id', $id, '=')
      ->fields('a', self::$databaseFields)
      ->range(0,1)
      ->execute()
      ->fetch();

    return self::databaseResultsToAntibody($result);
  }

  /**
   * Returns an Antibody of given type and id.
   *
   * @see AntibodyType
   *
   * @param $id
   *   ID of the antibody
   * @param AntibodyType $type
   *   Type of the antibody
   * @return \Antibody
   *   New instance of Antibody class with data from database
   */
  public static function findByIdAndType($id, $type) {
    $result = db_select(self::$tableName,'a')
      ->condition('id', $id, '=')
      ->condition('type', $type, '=')
      ->fields('a', self::$databaseFields)
      ->range(0,1)
      ->execute()
      ->fetch();

    return self::databaseResultsToAntibody($result);
  }

  /**
   * Returns all antibodies of a given type.
   *
   * @see AntibodyType
   *
   * @param $type
   * @return \Antibody[]
   */
  public static function findByType($type) {

    // sharing level condition
    $sl_condition = AntibodySharingLevel::getDatabaseCondition();

    $results = db_select(self::$tableName, 'a')
      ->condition('type', $type, '=')
      ->condition($sl_condition)
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToAntibodies($results);
  }

  /**
   * @return \Antibody[]
   */
  public static function findAll() {
    $results = db_select(self::$tableName, 'a')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToAntibodies($results);
  }

  /**
   * @return int
   */
  public static function getAntibodiesNumber($type = null) {

    if($type === null) {
      $results = db_select(self::$tableName, 'a')
        ->fields('a', self::$databaseFields)
        ->execute();
    } else {
      $results = db_select(self::$tableName, 'a')
        ->condition('type', $type, '=')
        ->fields('a', self::$databaseFields)
        ->execute();
    }

    return $results->rowCount();
  }

  /**
   * Returns all antibody of a given type. Ordered by 'created date'.
   *
   * @param $type
   * @param $limit
   * @return \Antibody[]
   */
  public static function findByTypeOrderByIDAndLimit($type, $limit) {

    // sharing level condition
    $sl_condition = AntibodySharingLevel::getDatabaseCondition();

    $results = db_select(self::$tableName, 'a')
      ->condition('type', $type, '=')
      ->condition($sl_condition)
      ->fields('a', self::$databaseFields)
      ->range(0,$limit)
      ->orderBy('id', 'DESC')
      ->execute();

    return self::databaseResultsToAntibodies($results);
  }

  /**
   * Returns all antibodies of a given type with ordering and pager default.
   *
   * @param $type
   * @param $header
   * @return \Antibody[]
   */
  public static function findByTypeUseTableSortAndUsePagerDefault($type, $header, $limit = 5) {

    // sharing level condition
    $sl_condition = AntibodySharingLevel::getDatabaseCondition();

    $results = db_select(self::$tableName, 'a')
      ->extend('TableSort')
      ->fields('a', self::$databaseFields)
      ->condition('type', $type, '=')
      ->condition($sl_condition)
      ->extend('PagerDefault')
      ->limit($limit)
      ->orderByHeader($header)
      ->orderBy('id')
      ->execute()
    ;
    return self::databaseResultsToAntibodies($results);
  }

  /**
   * Executes search query on Antibody database with given condition.
   *
   * @param $condition Database condition
   * @return \Antibody[]
   */
  public static function findBy($condition) {
    // sharing level condition
    $sl_condition = AntibodySharingLevel::getDatabaseCondition();

    $results = db_select(self::$tableName, 'a')
      ->condition($condition)
      ->condition($sl_condition)
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToAntibodies($results);
  }

  /**
   * Returns tags stored in the Antibody table.
   *
   * @param $tagName
   * @return mixed
   */
  public static function findTags($tagName){
    $result = db_select(self::$tableName, 'a')
      ->condition('tag', db_like($tagName).'%','LIKE')
      ->fields('a', array('tag'))
      ->range(0,10)
      ->groupBy('tag')
      ->execute();

    return $result;
  }

  /**
   * Returns companies stored in the Antibody table.
   *
   * @param $companyName
   * @return mixed
   */
  public static function findCompanies($companyName) {

    $result = db_select(self::$tableName, 'a')
      ->condition('company', db_like($companyName).'%','LIKE')
      ->fields('a', array('company'))
      ->range(0,10)
      ->groupBy('company')
      ->execute();

    return $result;
  }

  /**
   *
   * @param $antibodyId
   * @return int
   */
  public static function incrementCounter($antibodyId) {

    if($antibodyId == Antibody::EMPTY_ANTIBODY_ID)
      return -1;

    // try to read the counter value from database. if there is no entry for
    // this antibody then set views and anonymous_views to default value.
    // In other case increment existing values and store it into database.
    $result = db_select('antibody_statistics', 's')
      ->condition('s.antibody', $antibodyId, '=')
      ->fields('s', array('views', 'anonymous_views'))
      ->range(0,1)
      ->execute();

    $views = 0;
    $anonymousViews = 0;

    if($result->rowCount() > 0) {
      $counters = $result->fetchObject();
      $views = $counters->views;
      $anonymousViews = $counters->anonymous_views;
    }

    $views++;

    if(user_is_anonymous())
      $anonymousViews++;

    db_merge('antibody_statistics')
      ->key(array('antibody' => $antibodyId))
      ->fields(array(
        'antibody' => $antibodyId,
        'views' => $views,
        'anonymous_views' => $anonymousViews,
      ))->execute();

    return $views;
  }

  /**
   * @param $antibodyId
   * @return array
   */
  public static function getCounter($antibodyId) {

    if($antibodyId == Antibody::EMPTY_ANTIBODY_ID)
      return array('views' => -1, 'anonymous_views' => -1);

    $result = db_select('antibody_statistics', 's')
      ->condition('s.antibody', $antibodyId, '=')
      ->fields('s', array('views', 'anonymous_views'))
      ->range(0,1)
      ->execute();

    $views = 0;
    $anonymousViews = 0;

    if($result->rowCount() > 0) {
      $counters = $result->fetchObject();
      $views = $counters->views;
      $anonymousViews = $counters->anonymous_views;
    }

    return array('views' => $views, 'anonymous_views' => $anonymousViews);
  }

  /**
   * @param $antibodyId
   * @return integer
   */
  public static function getViewsNumber($antibodyId) {
    return self::getCounter($antibodyId)['views'];
  }

  /**
   * @param $antibodyId
   * @return integer
   */
  public static function getAnonymousViewsNumber($antibodyId) {
    return self::getCounter($antibodyId)['anonymous_views'];
  }
}
