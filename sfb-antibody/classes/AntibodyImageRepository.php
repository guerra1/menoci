<?php

/**
 * Class AntibodyImageRepository
 */
class AntibodyImageRepository {

  /**
   * @var string
   *   Name of the table containing images.
   */
  static $tableName = 'antibody_images';

  /**
   * @var array
   *   Array with database fields.
   */
  static $databaseFields = [
    'id',
    'fid',
    'ab_app_id',
    'description',
    'comment',
    'uploader',
    'upload_date',
    'image_type'
  ];

  /**
   * Stores image data into the database.
   *
   * @param \AntibodyImage $image
   *   The image object to be saved into the database.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($image) {
    db_merge(self::$tableName)
      ->key(['id' => $image->getId()])
      ->fields([
        'fid' => $image->getFid(),
        'ab_app_id' => $image->getAbappId(),
        'description' => $image->getDescription(),
        'comment' => $image->getComment(),
        'uploader' => $image->getUploader(),
        'upload_date' => $image->getUploadDate(),
        'image_type' => $image->getImageType(),
      ])
      ->execute();
  }

  /**
   * Removes an image entry from the database.
   *
   * @param $img_id
   *   The image object id to be deleted.
   */
  public static function delete($img_id) {
    db_delete(self::$tableName)
      ->condition('id', $img_id, '=')
      ->execute();
  }

  /**
 * Return all image entries for an antibody ID.
 *
 * @param $ab_id
 *   ID of the antibody to be considered.
 *
 * @return \AntibodyImage[]
 *   All images for the antibody object.
 */
  public static function findByAntibodyId($ab_id) {
    $result = db_select(self::$tableName,'a')
      ->condition('ab_app_id', $ab_id, '=')
      ->condition('image_type', 'ab', '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToImages($result);
  }

  /**
   * Return all image entries for an application ID.
   *
   * @param $app_id
   *   ID of the application to be considered.
   *
   * @return \AntibodyImage[]
   *   All images for the antibody object.
   */
  public static function findByApplicationId($app_id) {
    $result = db_select(self::$tableName,'a')
      ->condition('ab_app_id', $app_id, '=')
      ->condition('image_type', 'app', '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToImages($result);
  }

  /**
   * Returns all images to a database result.
   *
   * @param $results
   *   Database results.
   *
   * @return array
   *   Antibody image objects.
   */
  public static function databaseResultsToImages($results) {
    $images = [];
    foreach($results as $result)
      $images[] = self::databaseResultsToImage($result);

    return $images;
  }

  /**
   * Returns single antibody image objects
   *
   * @param $result
   *   Database results.
   * @return \AntibodyImage
   *   Antibody image object.
   */
  public static function databaseResultsToImage($result) {
    $image = new AntibodyImage();

    if(empty($result)) {
      return $image;
    }
    $image->setId($result->id);
    $image->setFid($result->fid);
    $image->setAbappId($result->ab_app_id);
    $image->setDescription($result->description);
    $image->setComment($result->comment);
    $image->setUploader($result->uploader);
    $image->setUploadDate($result->upload_date);
    $image->setImageType($result->image_type);

    return $image;
  }
}
