<?php

class AntibodyAntibodyApplication {

  const EMPTY_ANTIBODY_ANTIBODY_APPLICATION_ID = -1;

  /**
   * @var int
   */
  private $id = AntibodyAntibodyApplication::EMPTY_ANTIBODY_ANTIBODY_APPLICATION_ID;

  /**
   * @var int
   */
  private $antibodyId;

  /**
   * @var string
   */
  private $applicationAbbr;

  /**
   * @var int
   */
  private $workingGroupId;

  /**
   * @var string
   */
  private $quality;

  /**
   * @var string
   */
  private $comment;

  /**
   * @var string
   */
  private $dilution = NULL;

  /**
   * @var string
   */
  private $concentrationRaw = NULL;
  /**
   * @var double
   */
  private $concentrationValue = NULL;

  /**
   * @var string
   */
  private $concentrationDimension = NULL;

  /**
   * @var null
   */
  private $wbBand = NULL;

  /**
   * @var string
   */
  private $createdBy = NULL;

  /**
   * @var string
   */
  private $lastModified = NULL;

  /**
   * @var AntibodyImage[]
   */
  private $images;

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getAntibodyId() {
    return $this->antibodyId;
  }

  /**
   * @param int $antibodyId
   */
  public function setAntibodyId($antibodyId) {
    $this->antibodyId = $antibodyId;
  }

  public function getAntibody() {
    return AntibodiesRepository::findById($this->antibodyId);
  }

  /**
   * @return int
   */
  public function getApplicationAbbr() {
    return $this->applicationAbbr;
  }

  /**
   * @return \AntibodyApplication
   */
  public function getApplication() {
    return AntibodyApplicationsRepository::findByAbbreviation($this->applicationAbbr);
  }

  /**
   * @param int $applicationAbbr
   */
  public function setApplicationAbbr($applicationAbbr) {
    $this->applicationAbbr = $applicationAbbr;
  }

  /**
   * @return mixed
   */
  public function getDilution(){
    return $this->dilution;
  }

  public function getWorkingGroup() {
    return WorkingGroupRepository::findById($this->workingGroupId);
  }
  /**
   * @param $dilution
   */
  public function setDilution($dilution){
    $this->dilution = $dilution;
  }

  /**
   * @return mixed
   */
  public function getWbBand(){
    return $this->wbBand;
  }

  /**
   * @param $wb_band
   */
  public function setWbBand($wbBand){
    $this->wbBand = $wbBand;
  }

  /**
   * @param $created_by
   */
  public function setCreatedBy($createdBy){
    $this->createdBy = $createdBy;
  }

  /**
   * @return string
   */
  public function getCreatedBy(){
    return $this->createdBy;
  }

  /**
   * @param $last_modified
   */
  public function setLastModified($lastModified){
    $this->lastModified = $lastModified;
  }

  /**
   * @return string
   */
  public function getLastModified(){
    return $this->lastModified;
  }

  /**
   * @return mixed
   */
  public function getQuality(){
    return $this->quality;
  }

  /**
   * @param $quality
   */
  public function setQuality($quality){
    $this->quality = $quality;
  }

  /**
   * @return mixed
   */
  public function getComment(){
    return $this->comment;
  }

  /**
   * @param $comment
   */
  public function setComment($comment){
    $this->comment = $comment;
  }

  /**
   * @return mixed
   */
  public function getConcentrationRaw(){
    return $this->concentrationRaw;
  }

  /**
   * @param $concentration_raw
   */
  public function setConcentrationRaw($concentrationRaw){
    $this->concentrationRaw = $concentrationRaw;
  }

  /**
   * @return mixed
   */
  public function getConcentrationValue(){
    return $this->concentrationValue;
  }

  /**
   * @param $concentration_value
   */
  public function setConcentrationValue($concentrationValue){
    $this->concentrationValue = $concentrationValue;
  }

  /**
   * @return mixed
   */
  public function getConcentrationDimension(){
    return $this->concentrationDimension;
  }

  /**
   * @param $concentration_dimension
   */
  public function setConcentrationDimension($concentrationDimension){
    $this->concentrationDimension = $concentrationDimension;
  }

  /**
   * @return int
   */
  public function getWorkingGroupId() {
    return $this->workingGroupId;
  }

  /**
   * @param int $workingGroupId
   */
  public function setWorkingGroupId($workingGroupId) {
    $this->workingGroupId = $workingGroupId;
  }
  /**
   * @return bool
   */
  public function isEmpty() {
    if($this->id == AntibodyAntibodyApplication::EMPTY_ANTIBODY_ANTIBODY_APPLICATION_ID)
      return true;
    return false;
  }

  /**
   * @return bool
   */
  public function save() {
    AntibodyAntibodyApplicationsRepository::save($this);
    return true;
  }

  /**
   * @return AntibodyImage[]
   */
  public function getImages()
  {
    return $this->images;
  }

  /**
   * @param AntibodyImage[] $images
   */
  public function setImages($images)
  {
    $this->images = $images;
  }

  /* ------------------------------ Form fields ----------------------------- */

  public function getFormFieldWorkingGroup() {
    global $user;

    $userc = UsersRepository::findByUid($user->uid);
    $workingGroups = $userc->getUserWorkingGroups();

    $workingGroupsSelect = array();
    foreach($workingGroups as $workingGroup) {
      $workingGroupsSelect[$workingGroup->getId()] = $workingGroup->getName();
    }

    $default_value = $this->workingGroupId;

    //TODO: fall einbauen, dass der benutzer zu keiner arbeitsgruppe gehört

    return array(
      '#type' => 'select',
      '#title' => t('Research Group'),
      '#options' => $workingGroupsSelect,
      '#default_value' => $default_value,
      '#description' => t('Select research group owning this antibody.'),
    );
  }

  public function getFormFieldApplication() {
    return array(
      '#type' => 'textfield',
      '#title' => t('Application'),
      '#size' => 15,
      '#maxlength' => 15,
      '#default_value' => $this->applicationAbbr,
      '#autocomplete_path' => SFB_ANTIBODY_API_AUTOCOMPLETE_APPLICATION,
      '#required' => TRUE,
      '#ajax' => array(
        'callback' => 'sfb_antibody_ajax_textfield_callback',
        'wrapper' => 'textfield',
        'effect' => 'fade',
        'method' => 'replace',
        'event' => 'change',
      ),
    );

  }

  public function getFormFieldQuality() {

    $quality_select = array(
      AntibodyApplicationQuality::Unknown => 'Not set',
      AntibodyApplicationQuality::Poor => 'Poor (1 star)',
      AntibodyApplicationQuality::Fair => 'Fair (2 stars)',
      AntibodyApplicationQuality::Average => 'Average (3 stars)',
      AntibodyApplicationQuality::Good => 'Good (4 stars)',
      AntibodyApplicationQuality::Excellent => 'Excellent (5 stars)',
    );

    return array(
      '#type' => 'select',
      '#title' => t('Quality'),
      '#options' => $quality_select,
      '#default_value' => $this->quality,
      '#description' => t(''),
    );
  }

  public function getFormFieldComments() {
    return array(
      '#title' => t('Comments'),
      '#type' => 'textarea',
      '#default_value' => $this->comment,
      '#description' => t('Free text to explain the "Quality" in other words and/or more detail, may also be used for other comments regarding the antibody'),
    );
  }

  public function getFormFieldDilution() {
    return array(
      '#type' => 'textfield',
      '#title' => t('Dilution'),
      '#description' => t('Usually a dilution factor, should be based on "Concentration", should indicate the value in combination with the technique if several techniques are assigned to "Application"'),
      '#size' => 40,
      '#default_value' => $this->dilution,
      '#maxlength' => 40,
    );
  }

  public function getFormFieldConcentrationRaw() {
    return array(
      '#type' => 'textfield',
      '#title' => t('Concentration'),
      '#description' => t('Please only use the English notation, where a dot is used as a decimal seperator'),
      '#size' => 20,
      '#default_value' => $this->concentrationRaw,
      '#maxlength' => 45,
    );
  }
  public function getFormFieldWbBand(){
    return array(
      '#type' => 'textfield',
      '#title' => t('WB Band'),
      '#description' => t(''),
      '#maxlength' => 45,
      '#states' => array(
        "visible" => array(
          "input[name='application_abbr']" => array("checked" => TRUE)),
      ),
      '#prefix' => '<div id="textfield">',
      '#suffix' => '</div>',
      '#default_value' => $this->wbBand,
    );
  }

  public function getFormFieldConcentrationDimension(){
    return array(
      '#type' => 'select',
      '#title' => t('Dimension of concentration'),
      '#default_value' => $this->concentrationDimension,
      '#options' => array(
        'unknown' => 'unknown',
        'µg/ml' => 'µg/ml',
        'mg/ml' => 'mg/ml'
      ),
    );
  }


  /**
   * Return an associative array with form control data for 'Add Image Button'
   * field in Applcation form.
   *
   * @see drupal_get_form()
   *
   * @return array
   */
  public function getFormFieldImageAdd() {
    return [
      '#type' => 'submit',
      '#value' => t('Add'),
      '#name' => 'AddImageButton',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'image-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => ['class' => ['btn-primary btn-sm'], ],
    ];
  }

  /* -------------------------- Field views (Elements) ---------------------- */

  /**
   * HTML code with working group icon
   *
   * @return string
   */
  public function getElementWorkingGroupIcon() {

    $workingGroup = $this->getWorkingGroup();
    $wgName = $workingGroup->getName();

    return '<img src="' . $workingGroup->getIconPath() . '" alt="" data-toggle="tooltip" title="' . $wgName . '" />';
  }

  public function getElementWorkingGroup() {
    $workingGroup = $this->getWorkingGroup();
    if($workingGroup->isEmpty())
      return 'unknown';
    else
      return $workingGroup->getName();
  }

  public function getElementEditButton() {

    global $user;
    $userc = UsersRepository::findByUid($user->uid);

    //TODO: benötigt benutzer auch Manage rechte?
    if($userc->isMemberOfWorkingGroup($this->getWorkingGroup())) {
      $antibody = $this->getAntibody();

      $edit_url = SFB_ANTIBODY_URL_PRIMARY_APPLICATION_EDIT;
      if($antibody->getType() == AntibodyType::SECONDARY)
        $edit_url = SFB_ANTIBODY_URL_SECONDARY_APPLICATION_EDIT;

      return '<a href="'.sfb_antibody_url($edit_url, $antibody->getElementPID(), $this->id).'" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>';
    }

    return '';
  }

  public function getElementConcentration() {
    if(!empty($this->concentrationDimension) && $this->concentrationDimension != 'unknown') {
      return $this->concentrationRaw.' '.$this->concentrationDimension;
    }
    else
      return $this->concentrationRaw;
  }

  /* ----------------------------- Other functions -------------------------- */
  public function userHasPermissionTo($action) {

    //TODO: implement
  }

}

class AntibodyAntibodyApplicationsRepository {

  static $tableName = 'antibody_antibody_application';
  static $databaseFields = array(
    'id',
    'antibody_id',
    'application',
    'working_group',
    'quality',
    'comment',
    'dilution',
    'concentration_raw',
    'wb_band',
    'created_by',
    'last_modified',
    'concentration_dimension'
  );

  /**
   * @param $result
   * @return \AntibodyAntibodyApplication
   */
  public static function databaseResultsToAntibodyAntibodyApplication($result) {
    $application = new AntibodyAntibodyApplication();

    if(empty($result))
      return $application;

    $application->setId($result->id);
    $application->setAntibodyId($result->antibody_id);
    $application->setApplicationAbbr($result->application);
    $application->setWorkingGroupId($result->working_group);
    $application->setQuality($result->quality);
    $application->setComment($result->comment);
    $application->setDilution($result->dilution);
    $application->setConcentrationRaw($result->concentration_raw);
    $application->setConcentrationDimension($result->concentration_dimension);
    $application->setWbBand($result->wb_band);
    $application->setCreatedBy($result->created_by);
    $application->setLastModified($result->last_modified);

    return $application;
  }

  /**
   * @param $results
   * @return \AntibodyAntibodyApplication[]
   */
  public static function databaseResultsToAntibodyAntibodyApplications($results) {
    $applications = array();
    foreach($results as $result)
      $applications[] = self::databaseResultsToAntibodyAntibodyApplication($result);

    return $applications;
  }

  public static function findById($id) {
    $result = db_select(self::$tableName,'aaa')
      ->condition('id', $id, '=')
      ->fields('aaa', self::$databaseFields)
      ->range(0,1)
      ->execute()
      ->fetch();

    return self::databaseResultsToAntibodyAntibodyApplication($result);
  }

  public static function findByAntibodyId($antibodyId) {
    $results = db_select(self::$tableName, 'aap')
      ->condition('antibody_id', $antibodyId, '=')
      ->fields('aap', self::$databaseFields)
      ->execute();

    return self::databaseResultsToAntibodyAntibodyApplications($results);
  }

  public static function findByAntibodyIdGroupByApplication($antibodyId) {
    $results = db_select(self::$tableName, 'aap')
      ->condition('antibody_id', $antibodyId, '=')
      ->fields('aap', self::$databaseFields)
      ->groupBy('application')
      ->execute();

    return self::databaseResultsToAntibodyAntibodyApplications($results);
  }

  public static function findByAntibodyIdAndByApplication($antibodyId, $applicationAbbr) {
    $results = db_select(self::$tableName, 'aap')
      ->condition('antibody_id', $antibodyId, '=')
      ->condition('application', $applicationAbbr, '=')
      ->fields('aap', self::$databaseFields)
      ->execute();

    return self::databaseResultsToAntibodyAntibodyApplications($results);
  }

  /**
   * @param $abbreviation
   * @return mixed
   */

  public static function findByAbbreviation($abbreviation) {
    $result = db_select(self::$tableName, 'aa')
      ->condition('application',$abbreviation, '=')
      ->fields('aa', self::$databaseFields)
      ->range(0,1)
      ->execute()
      ->fetch();

    return self::databaseResultsToAntibodyAntibodyApplication($result);
  }


  /**
   * @param $antibodyId
   * @param $applicationAbbr
   * @param $workingGroupId
   * @return \AntibodyAntibodyApplication
   */
  public static function findByAntibodyIdAndByApplicationAbbreviationAndByWorkingGroupId($antibodyId, $applicationAbbr, $workingGroupId) {
    $results = db_select(self::$tableName, 'aap')
      ->condition('antibody_id', $antibodyId, '=')
      ->condition('application', $applicationAbbr, '=')
      ->condition('working_group', $workingGroupId, '=')
      ->fields('aap', self::$databaseFields)
      ->range(0,1)
      ->execute()
      ->fetch();
    return self::databaseResultsToAntibodyAntibodyApplication($results);
  }

  /**
   * @param AntibodyAntibodyApplication $antibodyApplication
   * @return bool
   */
  public static function save($antibodyApplication) {

    $id = null;
    if($antibodyApplication->getId() != AntibodyAntibodyApplication::EMPTY_ANTIBODY_ANTIBODY_APPLICATION_ID)
      $id = $antibodyApplication->getId();

    $dilution = null;
    if(!empty($antibodyApplication->getDilution()))
      $dilution = $antibodyApplication->getDilution();

    $concentrationRaw = null;
    if(!empty($antibodyApplication->getConcentrationRaw()))
      $concentrationRaw = $antibodyApplication->getConcentrationRaw();

    db_merge(self::$tableName)
      ->key(array('id' => $id))
      ->fields(array(
        'antibody_id' => $antibodyApplication->getAntibodyId(),
        'application' => $antibodyApplication->getApplicationAbbr(),
        'working_group' => $antibodyApplication->getWorkingGroupId(),
        'quality' => $antibodyApplication->getQuality(),
        'comment' => $antibodyApplication->getComment(),
        'dilution' => $dilution,
        'concentration_raw' => $concentrationRaw,
        'concentration_value' => $antibodyApplication->getConcentrationValue(),
        'concentration_dimension' => $antibodyApplication->getConcentrationDimension(),
        'wb_band' => $antibodyApplication->getWbBand(),
        'created_by' => $antibodyApplication->getCreatedBy(),
        'last_modified' => $antibodyApplication->getLastModified(),
      ))
      ->execute();

    self::saveImages($antibodyApplication->getImages(), AntibodyAntibodyApplicationsRepository::findByAntibodyIdAndByApplication($antibodyApplication->getAntibodyId() ,$antibodyApplication->getApplicationAbbr())[0]->getId());
    return true;
  }

  /**
   * @param \AntibodyImage[] $images
   * @param int $id
   * @throws \InvalidMergeQueryException
   */
  static function saveImages($images, $id) {
    if ($images) {
      foreach ($images as $image) {
        $image->setAbappId($id);
        $image->save();
      }
    }
  }
}
