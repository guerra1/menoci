CREATE TABLE `mouselines_laboratory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbreviation` varchar(45) DEFAULT NULL,
  `ilar_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

CREATE TABLE `mouselines_mouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(45) DEFAULT NULL,
  `mouseline_id` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `sex` varchar(45) DEFAULT NULL,
  `date_of_birth` varchar(45) DEFAULT NULL,
  `date_of_death` varchar(45) DEFAULT NULL,
  `workinggroup_id` varchar(45) DEFAULT NULL,
  `sharing_level` varchar(45) DEFAULT NULL,
  `comments` varchar(45) DEFAULT NULL,
  `created_date` varchar(45) DEFAULT NULL,
  `last_change_date` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mouselines_mouseline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(45) DEFAULT NULL,
  `standard_name` varchar(255) DEFAULT NULL,
  `mpdurl` varchar(250) DEFAULT NULL,
  `strain` varchar(45) DEFAULT NULL,
  `mother_line_id` varchar(45) DEFAULT NULL,
  `father_line_id` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `host_name` varchar(255) DEFAULT NULL,
  `host_abbreviation` varchar(255) DEFAULT NULL,
  `donor_name` varchar(255) DEFAULT NULL,
  `donor_abbreviation` varchar(255) DEFAULT NULL,
  `mutation` varchar(45) DEFAULT NULL,
  `mutated_gene` varchar(255) DEFAULT NULL,
  `mutated_gene_symbol` varchar(45) DEFAULT NULL,
  `mutated_gene_details` varchar(255) DEFAULT NULL,
  `ncbi_gene_url` varchar(250) DEFAULT NULL,
  `lab_id` int(11) DEFAULT NULL,
  `workinggroup_id` varchar(45) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `last_change_date` varchar(45) DEFAULT NULL,
  `comments` varchar(45) DEFAULT NULL,
  `sharing_level` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

CREATE TABLE `mouselines_strain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `abbreviation` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
