<?php
/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 10.01.2017
 * Time: 09:30
 */
function sfb_mouseline_default() {

  // Set the title for this page
  drupal_set_title('Mouseline Catalogue');

  $output = '';

  // print a message about number of mouse lines registered in mouseline catalogue
  $mouselines_number = MouselineRepository::getMouselinesNumber(); //todo!
  $output .= 'The catalogue currently contains '.$mouselines_number.' mouselines.';

  //
  // prepare and print the mouseline table
  //

  // table header
  // columns with 'field' definition can be sorted
  $header = array(
      array('data' => t('AG')),
      array('data' => t('PID')),
      array('data' => t('Name')),
      array('data' => t('Type')),
      array('data' => t('Laboratory')),
      array('data' => t('Created')),
      array('data' => t('Comments')),
      array('data' => t('add Mice')),
  );

  // get all mouselines from database
  // use table sort and pager function, not 100% implemented yet
  $mouselines = MouselineRepository::findByTypeUseTableSortAndUsePagerDefault(
    $header,
    variable_get(SFB_MOUSELINE_CONFIG_OVERVIEW_NO_OF_MOUSELINES, 25));


  // create table rows
  $rows = array();

  // each table row is a table row
  foreach($mouselines as $mouseline) {
    // fill table rows with mouseline data
    if($mouseline->userHasPermissionTo(MouselineAction::WRITE)) {
      $rows[] = array(
        // field: research group
          $mouseline->getElementWorkingGroupIcon(),
        // field: mouse line pid and link to mouse line view
          l($mouseline->getElementPID(), sfb_mouseline_url(SFB_MOUSELINE_URL_VIEW_MOUSELINE, $mouseline->getElementPID())),
        // field: name
          $mouseline->getName(),
        //field: breeding
          $mouseline->getType(),
        // field: origin lab
        MouselineLaboratoryRepository::findById($mouseline->getLaboratoryId())->getName(),
        // field: data creation date
          $mouseline->getCreatedDate(),
        // field: comments
          $mouseline->getComments(),
          '<a href="'. sfb_mouseline_url(SFB_MOUSELINE_URL_ADD_MOUSE, $mouseline->getElementPID()) .'" class="btn btn-primary btn-xs" >
          <span class="glyphicon glyphicon-plus"></span>Mice</a>'
      );
    }else{
      $rows[] = array(
        // field: research group
          $mouseline->getElementWorkingGroupIcon(),
        // field: mouse line pid and link to mouse line view
          l($mouseline->getElementPID(), sfb_mouseline_url(SFB_MOUSELINE_URL_VIEW_MOUSELINE, $mouseline->getElementPID())),
        // field: name
          $mouseline->getName(),
        //field: breeding
          $mouseline->getType(),
        // field: lab
          MouselineLaboratoryRepository::findById($mouseline->getLaboratoryId())->getName(),
        // field: data creation date
          $mouseline->getCreatedDate(),
        // field: comments
          $mouseline->getComments(),
          ' ',
       );
    }
  }

  $output .=
    theme(
      'table',
      array(
        'header' => $header,
        'rows' => $rows)
    ).theme(
      'pager',
      array('tags' => array())
    );

  if(user_access(SFB_MOUSELINE_PERMISSION_REGISTRAR))
    $output .= '<a class="btn btn-primary" href="'. sfb_mouseline_url(SFB_MOUSELINE_URL_NEW_MOUSELINE) .'">Register new mouseline</a>';
  $output .= ' <a class="btn btn-default" href="'. sfb_mouseline_url(SFB_MOUSELINE_URL_HELP) .'">View help page</a>';

  // print main table and pager
  return $output;
}