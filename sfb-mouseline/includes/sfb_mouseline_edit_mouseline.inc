<?php
/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 09.01.2017
 * Time: 12:48
 */
/**
 * Form for editing mouse lines. Structure depends on mouse line's type.
 *
 * @param $form
 * @param $form_state
 * @param $mouseline_pid
 * @return mixed
 */
function sfb_mouseline_edit_mouseline($form, &$form_state, $mouseline_pid){
  //save Mouseline id and pid to form, makes sure it stays available
  $form_state['mouseline_id'] = Mouseline::extractIDFromPID($mouseline_pid);
  $form_state['mouseline_pid'] = $mouseline_pid;

  $mouseline = MouselineRepository::findByid($form_state['mouseline_id']);

  //in case you are editing an unspecified mouseline, which may be created by choosing unknown parents in hybrid lines, here you can choose its actual type
  if($mouseline->getType() == MouselineType::UNSPECIFIED){
    //$form_state['values']['mouseline_type'] = '';
    $form['fieldset-type'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fa fa-info"></span> ' . t('Select the type of mouseline you want to register'),
      '#description' => '',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['fieldset-type']['mouseline_type'] = $mouseline->getFormFieldType();
    $form['fieldset-type']['mouseline_type']['#ajax'] = array(
      'callback' => 'ajax_mouseline_new_mouseline_markup_callback',
      'wrapper' => 'markup-div',
    );

    $form['markup_fieldset'] = array(
      //'#title' => t("Description"),
      '#prefix' => '<div id="markup-div">',
      '#suffix' => '</div>',
      '#type' => 'fieldset',
    );
    //gives the description to the selected breeding type
    if(isset($form_state['values']['mouseline_type']))
      $form['markup_fieldset']["description"] = MouselineType::getDescription($form_state['values']['mouseline_type']);
    else
      $form['markup_fieldset']["description"] = MouselineType::getDescription($mouseline->getType());

    $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('sfb_mouseline_edit_mouseline_cancel'),
    '#validate' => array(),
    );

    $form['next'] = array(
      '#type' => 'submit',
      '#submit' => array('sfb_mouseline_edit_mouseline_next'),
      '#value' => t('Next <small>(Specify details of your mouseline)</small> &raquo;'),  );

    return $form;
  }
  //if mouseline new: create new lab and strain instances to generate the form fields
  if(empty($mouseline->getLaboratoryId())) {
    $laboratory = new MouselineLaboratory();
  } else{
    $laboratory = MouselineLaboratoryRepository::findById($mouseline->getLaboratoryId());
  }
  if(empty($mouseline->getStrainId())) {
    $strain = new MouselineStrain();
  } else{
    $strain = MouselineStrainRepository::findById($mouseline->getStrainId());
  }

  //build field set for type and name
  $form['fieldset-type'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('General information'),
    '#description' => '',
    '#collapsible' => False,
  );
  $form['fieldset-type']['mouseline_name'] = $mouseline->getFormFieldName();
  $form['fieldset-type']['mouseline_type'] = $mouseline->getFormFieldType();

    //field set for details
   $form['fieldset-details'] = array(
       '#type' => 'fieldset',
       '#title' => '<span class="fa fa-info"></span> ' . t('Current mouseline\'s details'),
       '#description' => '',
       '#collapsible' => False,
   );

  //details depend on breeding type.
  switch($mouseline->getType()){
    case MouselineType::INBRED:
      $form['fieldset-details']['strain_name'] = $strain->getFormFieldName();
      $form['fieldset-details']['strain_abbreviation'] = $strain->getFormFieldAbbreviation();
      $form['fieldset-details']['laboratory_name'] = $laboratory->getFormFieldName();
      $form['fieldset-details']['laboratory_abbreviation'] = $laboratory->getFormFieldAbbreviation();
      $form['fieldset-details']['laboratory_ilar_url'] = $laboratory->getFormFieldIlarURL();

      break;
    case MouselineType::SUBSTRAIN:
      $form['fieldset-details']['strain_name'] = $strain->getFormFieldName();
      $form['fieldset-details']['strain_abbreviation'] = $strain->getFormFieldAbbreviation();
      $form['fieldset-details']['laboratory_name'] = $laboratory->getFormFieldName();
      $form['fieldset-details']['laboratory_abbreviation'] = $laboratory->getFormFieldAbbreviation();
      $form['fieldset-details']['laboratory_ilar_url'] = $laboratory->getFormFieldIlarURL();

      break;
    case MouselineType::HYBRID:
      $form['fieldset-details-mother'] = array(
        '#type' => 'fieldset',
        '#title' => '<span class="fa fa-info"></span> ' . t('Mother\'s Details'),
        '#description' => '',
        '#collapsible' => true,
        '#collapsed' => false,
      );
      $form['fieldset-details-mother']['line_mother'] = $mouseline->getFormFieldNameSearch('mother');
      $form['fieldset-details-mother']['data_mother'] = Array (
        '#type' => 'markup',
        '#markup' => '<div id="mother">'.t("").'</div>',
        '#prefix' => '<div id="mouseline_mother_details_div">',
        '#suffix' => '</div>',
      );
      $form['fieldset-details-father'] = array(
        '#type' => 'fieldset',
        '#title' => '<span class="fa fa-info"></span> ' . t('Father\'s Details'),
        '#description' => '',
        '#collapsible' => true,
        '#collapsed' => false,
      );
      $form['fieldset-details-father']['line_father'] = $mouseline->getFormFieldNameSearch('father');
      $form['fieldset-details-father']['data_father'] = Array (
          '#type' => 'markup',
          '#markup' => '<div id="father">'.t("").'</div>',
          '#prefix' => '<div id="mouseline_father_details_div">',
          '#suffix' => '</div>',
      );
      $form['fieldset-details']['laboratory_name'] = $laboratory->getFormFieldName();
      $form['fieldset-details']['laboratory_abbreviation'] = $laboratory->getFormFieldAbbreviation();
      $form['fieldset-details']['laboratory_ilar_url'] = $laboratory->getFormFieldIlarURL();
      break;
    case MouselineType::MIX_INBRED:
      $form['fieldset-parental-strains'] = array(
        '#type' => 'fieldset',
        '#title' => '<span class="fa fa-info"></span> ' . t('Parental strains\' details'),
        '#description' => '',
        '#collapsible' => False,
      );
      $form['fieldset-parental-strains']['host_name'] = $mouseline->getFormfieldHostName();
      $form['fieldset-parental-strains']['host_abbreviation'] = $mouseline->getFormfieldHostAbbr();
      $form['fieldset-parental-strains']['donor_name'] = $mouseline->getFormfieldDonorName();
      $form['fieldset-parental-strains']['donor_abbreviation'] = $mouseline->getFormfieldDonorAbbr();

      $form['fieldset-details']['laboratory_name'] = $laboratory->getFormFieldName();
      $form['fieldset-details']['laboratory_abbreviation'] = $laboratory->getFormFieldAbbreviation();
      $form['fieldset-details']['laboratory_ilar_url'] = $laboratory->getFormFieldIlarURL();
      break;
    case MouselineType::CONGENIC:
      $form['fieldset-parental-strains'] = array(
        '#type' => 'fieldset',
        '#title' => '<span class="fa fa-info"></span> ' . t('Parental strains\' details'),
        '#description' => '',
        '#collapsible' => False,
      );
      $form['fieldset-parental-strains']['host_name'] = $mouseline->getFormfieldHostName();
      $form['fieldset-parental-strains']['host_abbreviation'] = $mouseline->getFormfieldHostAbbr();
      $form['fieldset-parental-strains']['donor_name'] = $mouseline->getFormfieldDonorName();
      $form['fieldset-parental-strains']['donor_abbreviation'] = $mouseline->getFormfieldDonorAbbr();

      $form['fieldset-details']['laboratory_name'] = $laboratory->getFormFieldName();
      $form['fieldset-details']['laboratory_abbreviation'] = $laboratory->getFormFieldAbbreviation();
      $form['fieldset-details']['laboratory_ilar_url'] = $laboratory->getFormFieldIlarURL();
      break;
    case MouselineType::COISOGENIC:
      $form['fieldset-details']['strain_name'] = $strain->getFormFieldName();
      $form['fieldset-details']['strain_abbreviation'] = $strain->getFormFieldAbbreviation();
      $form['fieldset-details']['laboratory_name'] = $laboratory->getFormFieldName();
      $form['fieldset-details']['laboratory_abbreviation'] = $laboratory->getFormFieldAbbreviation();
      $form['fieldset-details']['laboratory_ilar_url'] = $laboratory->getFormFieldIlarURL();
      break;
  }
  //MPD LINK:
  $form['fieldset-details']['mpd_url'] = $mouseline->getFormfieldMPDURL();

  //field set for mutation
  //if (($mouseline->getType() == MouselineType::MIX_INBRED) || ($mouseline->getType() == MouselineType::CONGENIC) || ($mouseline->getType() == MouselineType::COISOGENIC)) {
    $form['fieldset-mutation'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fa fa-info"></span> ' . t('Genetic Mutations'),
      '#description' => '',
      '#collapsible' => False,
    );
    $form['fieldset-mutation']['mutation_type'] = $mouseline->getFormfieldMutation();
    //testing for Congenic if "Cg" should be included in Nomenclature
    if ($mouseline->getType() == MouselineType::CONGENIC){
      $form['fieldset-mutation']['complex'] = $mouseline->getFormfieldCongenicComplex();
    }
    $form['fieldset-mutation']['mutation_gene'] = $mouseline->getFormfieldMutatedGene();
    $form['fieldset-mutation']['mutation_gene_symbol'] = $mouseline->getFormfieldMutatedgeneSymbol();
    $form['fieldset-mutation']['mutation_gene_url'] = $mouseline->getFormfieldMutatedgeneNcbiURL();
    $form['fieldset-mutation']['mutation_gene_details'] = $mouseline->getFormfieldMutatedGeneDetails();
  //}
  //field set for common information
  $form['fieldset-organisational'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Organisational Information'),
    '#description' => '',
    '#collapsible' => False,
  );
  $form['fieldset-organisational']['mouseline_comments'] = $mouseline->getFormFieldComments();
  $form['fieldset-organisational']['working_group_id'] = $mouseline->getFormFieldWorkingGroup();
  $form['fieldset-organisational']['sharing_level'] =  $mouseline->getFormFieldSharingLevel();

/* TODO: linking with publications from this end of the rdp, deactivated right now. further functions needed in publication registry
  $form['fieldset-linking'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Organisational Information'),
    '#description' => '',
    '#collapsible' => False,
  );

  $rdp_linking = RDPLinking::getLinkingByName('sfb_literature', 'pubreg_articles');
  $form['fieldset-linking']['mouseline_test'] =  $rdp_linking->getEditForm('mouselines_mouseline', $mouseline->getId());
*/

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('sfb_mouseline_edit_mouseline_cancel'),
    '#validate' => array(),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save mouseline'),
    '#submit' => array('sfb_mouseline_edit_mouseline_save'),
  );

  return $form;
}


/**
 * save entry after hitting the submit button
 *
 * @param $form
 * @param $form_state
 */
function sfb_mouseline_edit_mouseline_save($form, &$form_state)
{
  $mouseline = MouselineRepository::findByid($form_state['mouseline_id']);

  //save general information which is important for all types
  $mouseline->setComments($form_state['values']['mouseline_comments']);
  $mouseline->setWorkingGroupId($form_state['values']['working_group_id']);
  $mouseline->setSharingLevel($form_state['values']['sharing_level']);$laboratory = new MouselineLaboratory();
  $laboratory->setName($form_state['values']['laboratory_name']);
  $laboratory->setAbbreviation($form_state['values']['laboratory_abbreviation']);
  $laboratory->setIlarURL($form_state['values']['laboratory_ilar_url']);
  $laboratory->save();
  //look lab up in DB to fetch the newly given id
  //$id_lab = new MouselineLaboratory();
  $id_lab = MouselineLaboratoryRepository::findByName($laboratory->getName())->getId();
  //$lab_id = $id_lab->getId();

  //now save and add in the gathered ids of strain and lab
  $mouseline->setLaboratoryId($id_lab);

  $mouseline->setMpdURL($form_state['values']['mpd_url']);
  $mouseline->setMutationType($form_state['values']['mutation_type']);

  if(!($mouseline->getMutationType() == MouselineMutation::WILDTYPE)) {
    $mouseline->setMutatedGene($form_state['values']['mutation_gene']);
    $mouseline->setMutatedGeneSymbol($form_state['values']['mutation_gene_symbol']);
    $mouseline->setNcbiGeneURL($form_state['values']['mutation_gene_url']);
    $mouseline->setMutatedGeneDetails($form_state['values']['mutation_gene_details']);
  }


  if ($mouseline->getType() == MouselineType::HYBRID) {
    $mother = explode(", PID: ", $form_state['values']['line_mother']);
    $father = explode(", PID: ", $form_state['values']['line_father']);
    $standard_name = MouselineType::buildNameHybrid($mother[0], $father[0], $form_state['values']['laboratory_abbreviation']);
    $standard_name = MouselineMutation::buildMutationName($mouseline, $standard_name,$form_state['values']['mutation_type'],
      $form_state['values']['mutation_gene_symbol'], $form_state['values']['laboratory_abbreviation']);
    $mouseline->setName($standard_name);
    $pidMother = explode(", PID: ", $form_state['values']['line_mother']);
    $pidFather = explode(", PID: ", $form_state['values']['line_father']);
    if(!empty($pidMother[1])) {
      $mouseline->setMotherLineId(Mouseline::extractIDFromPID($pidMother[1]));
    } else {
      //unknown mouselines that are not yet present in the DB have to be created
      $newstrain = new Mouseline;
      $newstrain->setName( '"'.$form_state['values']['line_mother'].'"');
      $newstrain->setType(MouselineType::UNSPECIFIED);
      //inherit the hybrid mouse line's security parameters, so that it can be seen in the overview and edited afterwards
      $newstrain->setWorkingGroupId($form_state['values']['working_group_id']);
      $newstrain->setSharingLevel($form_state['values']['sharing_level']);
      $nid = $newstrain->save();
      $mouseline->setMotherLineId($nid);
    }
    if(!empty($pidFather[1])){
      $mouseline->setFatherLineId(Mouseline::extractIDFromPID($pidFather[1]));
    } else {
      $newstrain = new Mouseline;
      $newstrain->setName( '"'.$form_state['values']['line_father'].'"');
      $newstrain->setType(MouselineType::UNSPECIFIED);
      $newstrain->setWorkingGroupId($form_state['values']['working_group_id']);
      $newstrain->setSharingLevel($form_state['values']['sharing_level']);
      $nid = $newstrain->save();
      $mouseline->setFatherLineId($nid);
    }
    $mouseline->setType($form_state['values']['mouseline_type']);
  } else if ($mouseline->getType() == MouselineType::MIX_INBRED) {
      $mouseline->setHostName($form_state['values']['host_name']);
      $mouseline->setHostAbbr($form_state['values']['host_abbreviation']);
      $mouseline->setDonorName($form_state['values']['donor_name']);
      $mouseline->setDonorAbbr($form_state['values']['donor_abbreviation']);
      $standard_name = MouselineType::buildNameMixInbred($form_state['values']['host_abbreviation'], $form_state['values']['donor_abbreviation']);
      $standard_name = MouselineMutation::buildMutationName($mouseline, $standard_name, $form_state['values']['mutation_type'],
        $form_state['values']['mutation_gene_symbol'], $form_state['values']['laboratory_abbreviation']);
      $mouseline->setName($standard_name);
      $mouseline->setType($form_state['values']['mouseline_type']);
  } else if ($mouseline->getType() == MouselineType::CONGENIC) {
      $host_name = $form_state['values']['host_name'];
      $host_abbr = $form_state['values']['host_abbreviation'];
      $donor_name = $form_state['values']['donor_name'];
      $donor_abbr = $form_state['values']['donor_abbreviation'];
      $mouseline->setHostName($host_name);
      $mouseline->setHostAbbr($host_abbr);
      $mouseline->setDonorName($donor_name);
      $mouseline->setDonorAbbr($donor_abbr);
      //if complex genetic origin (Cg)
      if ($form_state['values']['complex'] == 'yes'){
        $standard_name = MouselineType::buildNameCongenic($form_state['values']['host_abbreviation'], 'Cg');
      }
      else {
        $standard_name = MouselineType::buildNameCongenic($form_state['values']['host_abbreviation'], $form_state['values']['donor_abbreviation']);
      }
      $standard_name = MouselineMutation::buildMutationName($mouseline, $standard_name,$form_state['values']['mutation_type'],
        $form_state['values']['mutation_gene_symbol'], $form_state['values']['laboratory_abbreviation']);
      $mouseline->setName($standard_name);
      $mouseline->setType($form_state['values']['mouseline_type']);
  } else {
    $strain = new MouselineStrain();
    $strain->setName($form_state['values']['strain_name']);
    $strain->setAbbreviation($form_state['values']['strain_abbreviation']);
    $strain->save();
    //look strain up in DB to fetch the newly given id
    $id_strain = MouselineStrainRepository::findByName($strain->getName());
    $strain_id = $id_strain->getId();
    $mouseline->setStrainId($strain_id);
    if(empty($form_state['values']['strain_abbreviation'])) {
      $strain = '<I>"'.$form_state['values']['strain_name'].'"</I>';
    } else{
      $strain = $form_state['values']['strain_abbreviation'];
    }
    if ($mouseline->getType() == MouselineType::INBRED) {
      $standard_name = MouselineType::buildNameInbred($form_state['values']['strain_name'], $form_state['values']['laboratory_abbreviation']);
    } elseif ($mouseline->getType() == MouselineType::SUBSTRAIN){
      $standard_name = MouselineType::buildNameSubstrain($form_state['values']['strain_name'], $form_state['values']['laboratory_abbreviation']);
    } elseif ($mouseline->getType() == MouselineType::COISOGENIC){
      //full strain name for building the coisogenic name
      $standard_name = MouselineType::buildNameCoisogenic($form_state['values']['strain_name']);
    } else {
      $standard_name = "n/a";
    }
    $standard_name = MouselineMutation::buildMutationName($mouseline, $standard_name,$form_state['values']['mutation_type'],
      $form_state['values']['mutation_gene_symbol'], $form_state['values']['laboratory_abbreviation']);
    $mouseline->setName($standard_name);
    //Inbred Strains = both parents are from the same line as their child
    $mouseline->setMotherLineId($mouseline->getId());
    $mouseline->setFatherLineId($mouseline->getId());
  }
  $mouseline->save();

  drupal_set_message('Saved new mouseline: '.$mouseline->getName());
  $form_state['redirect'] = sfb_mouseline_url(SFB_MOUSELINE_URL_VIEW_MOUSELINE, $form_state['mouseline_pid']);

}

/**
 * discard changes
 *
 * @param $form
 * @param $form_state
 */
function sfb_mouseline_edit_mouseline_cancel($form, &$form_state) {
  $form_state['redirect'] = SFB_MOUSELINE_URL_PAGE_DEFAULT;
}

function sfb_mouseline_edit_mouseline_next($form, &$form_state) {
  $form_state['type'] = $form_state['values']['mouseline_type'];
  if($form_state['type']!=MouselineType::UNSPECIFIED){
    $mouseline = MouselineRepository::findById($form_state['mouseline_id']);
    $mouseline->setType($form_state['type']);
    $mouseline->save();
    $form_state['rebuild'] = TRUE;  //drupal_set_message($nid);
  } else {
    drupal_set_message("Please specify the line's type.", $type= 'error');
  }


}


/**
 * AJAX Callbacks
 */

/**
 * called after info is inserted into strain abbreviation, lab abbreviation, mother/father line.
 * generates standardised name and enters it into name field
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function ajax_mouseline_name_callback($form, &$form_state)
{

  switch($form_state['values']['mouseline_type']) {
    case MouselineType::INBRED:
      $standard_name = MouselineType::buildNameInbred(
        $form_state['values']['strain_name'],
      $form_state['values']['laboratory_abbreviation']);
        break;
    case MouselineType::HYBRID:
      $mother = explode(", PID: ", $form_state['values']['line_mother']);
      $father = explode(", PID: ", $form_state['values']['line_father']);
      $standard_name = MouselineType::buildNameHybrid($mother[0], $father[0], $form_state['values']['laboratory_abbreviation']);
        break;
    case MouselineType::SUBSTRAIN:
      $standard_name = MouselineType::buildNameSubstrain(
        $form_state['values']['strain_name'],
      $form_state['values']['laboratory_abbreviation']);
        break;
    case MouselineType::MIX_INBRED:
      $standard_name = MouselineType::buildNameMixInbred($form_state['values']['host_abbreviation'], $form_state['values']['donor_abbreviation']);
      break;
    case MouselineType::CONGENIC:
      $host_abbr = $form_state['values']['host_abbreviation'];
      $donor_abbr = $form_state['values']['donor_abbreviation'];
      $standard_name = MouselineType::buildNameCongenic($form_state['values']['host_abbreviation'], $form_state['values']['donor_abbreviation']);
      break;
    case MouselineType::COISOGENIC:
      $standard_name = MouselineType::buildNameCoisogenic($form_state['values']['strain_name']);
      break;
    default:
      $standard_name = 'Error: unknown mouseline type';
  }
  $mouseline = MouselineRepository::findById($form_state['mouseline_id']);
  //add Mutation information to the Name
  $standard_name = MouselineMutation::buildMutationName($mouseline, $standard_name,$form_state['values']['mutation_type'],
    $form_state['values']['mutation_gene_symbol'], $form_state['values']['laboratory_abbreviation']);
  $temp = new Mouseline();
  //rebuild type field set
  $form['fieldset-type']['mouseline_name'] = $temp->getFormFieldName();
  $form['fieldset-type']['mouseline_type']['#value'] = $form_state['values']['mouseline_type'];
  $form['fieldset-type']['mouseline_type']['#attributes']['disabled'] = TRUE;
  $form['fieldset-type']['mouseline_name']['#attributes']['disabled'] = TRUE;
  $form['fieldset-type']['mouseline_name']['#value'] = $standard_name;
  return $form['fieldset-type']['mouseline_name'];
}

/**
 * called after mother line is selected. gives short overview of selected line.
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function ajax_mouseline_mother_details_callback($form, &$form_state)
{
  $pid = explode(", PID: ", $form_state['values']['line_mother']);
  $mother = MouselineRepository::findById(Mouseline::extractIDFromPID($pid[1]));
  //build array with commands that are to be executed in return
  $commands[] = ajax_command_html('#mother', t($mother->getInformationField()));
  $commands[] = ajax_command_replace('#mouseline-name-div', render(ajax_mouseline_name_callback($form, $form_state)));
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * called after father line is selected. gives short overview of selected line.
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function ajax_mouseline_father_details_callback($form, &$form_state)
{
  $pid = explode(", PID: ", $form_state['values']['line_father']);
  $father = MouselineRepository::findById(Mouseline::extractIDFromPID($pid[1]));
  //build array with commands that are to be executed in return
  $commands[] = ajax_command_html('#father', t($father->getInformationField()));
  $commands[] = ajax_command_replace('#mouseline-name-div', render(ajax_mouseline_name_callback($form, $form_state)));
  return array('#type' => 'ajax', '#commands' => $commands);
}

//TODO Following Code does not work, name is not generated. Abbreviation is added correctly, though
/*
function ajax_mouseline_lab_abb_callback($form, &$form_state)
{
  $lab = MouselineLaboratory::findByName($form_state['values']['laboratory_name']);
  //build array with commands that are to be executed in return
  $form['fieldset-details']['laboratory_abbreviation']['#value'] = $lab->getAbbreviation();
  $commands[] = ajax_command_replace('#lab', render($form['fieldset-details']['laboratory_abbreviation']));
  $commands[] = ajax_command_replace('#mouseline-name-div', render(ajax_mouseline_name_callback($form, $form_state)));
  return array('#type' => 'ajax', '#commands' => $commands);
}*/

/**
 * AJAX Callback, renew description
 */
function ajax_mouseline_new_mouseline_markup_callback($form, $form_state) {
  return $form['markup_fieldset'];
}
