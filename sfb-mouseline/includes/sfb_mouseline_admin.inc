<?php
/**
 * Created by PhpStorm.
 * User: agrim
 * Date: 14.01.2017
 * Time: 18:09
 */
/**
 * this form is responsible for displaying the admin's menu and configuration possibilities
 *
 * @return mixed
 */
function sfb_mouseline_admin() {
  $form = array();

  $form['fieldset-defaults'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fa fa-info"></span> ' . t('Defaults'),
      '#description' => '',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

  $form['fieldset-defaults']['ac_config_datasheet_public'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow download of PDF datasheets for anonymous users'),
      '#default_value' => variable_get(SFB_MOUSELINE_CONFIG_DATASHEET_PUBLIC, 0),
      '#description' => '',
  );

/*
  $form['fieldset-defaults'][SFB_MOUSELINE_CONFIG_DEFAULT_SHARING_LEVEL] = array(
      '#type' => 'select',
      '#title' => t('Default sharing level for new mouselines'),
      '#options' => MouselineSharingLevel::getAsArray(),
      '#default_value' => variable_get(SFB_MOUSELINE_CONFIG_DEFAULT_SHARING_LEVEL, MouselineSharingLevel::GROUP_LEVEL),
      '#description' => 'Change default sharing level for new mouselines',
  );*/

  $form['fieldset-defaults'][SFB_MOUSELINE_CONFIG_OVERVIEW_NO_OF_MOUSELINES] = array(
      '#type' => 'textfield',
      '#title' => t('Number of mouselines to be shown on primary and secondary overview pages'),
      '#default_value' => variable_get(SFB_MOUSELINE_CONFIG_OVERVIEW_NO_OF_MOUSELINES, 25),
      '#description' => 'Number of mouselines to be shown on primary and secondary overview pages',
      '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['fieldset-pid'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fa fa-info"></span> ' . t('Persistent Identifier Settings'),
      '#description' => '',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

  $form['fieldset-pid'][SFB_MOUSELINE_CONFIG_PID_REGEX] = array(
      '#type' => 'textfield',
      '#title' => t('Regular expression returning the PID'),
      '#description' => 'Regular expression which returns the PID from given pid',
    '#default_value' => variable_get(SFB_MOUSELINE_CONFIG_PID_REGEX,
      '^(?:umg-mlc-)(mouse|mouseline)(?:-)(\d{4})$/i'),
  );

  $form['fieldset-pid'][SFB_MOUSELINE_CONFIG_PID_REGEX_ID_INDEX] = array(
    '#type' => 'textfield',
    '#title' => t('Regex ID index'),
    '#description' => '',
    '#default_value' => variable_get(SFB_MOUSELINE_CONFIG_PID_REGEX_ID_INDEX, 2)
  );

  $form['fieldset-pid'][SFB_MOUSELINE_CONFIG_PID_PATTERN] = array(
      '#type' => 'textfield',
      '#title' => t('PID Pattern'),
      '#description' => 'String containing {type} and {pid} with pattern for creating PIDs.',
      '#default_value' => variable_get(SFB_MOUSELINE_CONFIG_PID_PATTERN, 'umg-mlc-{type}-{pid}')
  );

  $form['fieldset-pid'][SFB_MOUSELINE_CONFIG_PID_ID_PADDING] = array(
      '#type' => 'textfield',
      '#title' => t('ID padding'),
      '#description' => 'If you wish to use IDs in PID with fixed width, then enter the width ( greater than 0) of the PID ID.',
      '#default_value' => variable_get(SFB_MOUSELINE_CONFIG_PID_ID_PADDING, 4),
      '#element_validate' => array('element_validate_integer'),
  );
/*
  $form['fieldset-pid'][SFB_MOUSELINE_CONFIG_PID_EPIC_URL] = array(
      '#type' => 'textfield',
      '#title' => t('EPIC PID url prefix'),
      '#description' => '', //TODO: description
      '#default_value' => variable_get(SFB_MOUSELINE_CONFIG_PID_EPIC_URL, '')
  );*/


  return system_settings_form($form);
}