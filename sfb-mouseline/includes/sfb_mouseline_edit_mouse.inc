<?php
/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 11.01.2017
 * Time: 09:58
 */
function sfb_mouseline_edit_mouse($form, &$form_state, $mouseline_PID, $mouse_PID){

  //extract mouseline id from pid
    $mouseline_id = Mouseline::extractIDFromPID($mouseline_PID);
    if($mouseline_id == null) {
        watchdog('sfb_mouseline', 'Wrong PID used: '.$mouseline_PID);
        drupal_not_found();
        exit();
    }

    //extract mouse id from pid
    $mouse_id = MouselineMouse::extractIDFromPID($mouse_PID);
    if($mouse_id == null) {
        watchdog('sfb_mouseline_mouse', 'Wrong PID used: '.$mouse_PID);
        drupal_not_found();
        exit();
    }

  $form_state['mouse_id'] = $mouse_id;
  $form_state['mouseline_id'] = $mouseline_id;

  $mouse = MouselineMouseRepository::findById($form_state['mouse_id']);
  //
  // prepare new mouse form
  //

  $form['fieldset-general'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('General Information'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['fieldset-general']['mouse_name'] = $mouse->getFormFieldName();
  $form['fieldset-general']['mouse_sex'] = $mouse->getFormFieldSex();
  $form['fieldset-general']['mouse_date_of_birth'] = $mouse->getFormFieldDateOfBirth();
  $form['fieldset-general']['mouse_date_of_death'] = $mouse->getFormFieldDateOfDeath();
  $form['fieldset-general']['sharing_level'] = $mouse->getFormFieldSharingLevel();
  $form['fieldset-general']['comments'] = $mouse->getFormFieldComments();


  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('sfb_mouseline_edit_mouse_cancel'),
    '#validate' => array(),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['data'] = array(
    '#type' => 'submit',
    '#value' => t('Add Research Data'),
    '#submit' => array('sfb_mouseline_edit_mouse_add_mouse_data'),
  );

  return $form;
}

function sfb_mouseline_edit_mouse_submit($form, &$form_state) {
  $mouse = new MouselineMouse();
  $mouse->setId($form_state['mouse_id']);
  $mouse->setName($form_state['values']['mouse_name']);
  $mouse->setSex($form_state['values']['mouse_sex']);
  $mouse->setMouselineId($form_state['mouseline_id']);
  $mouse->setDateOfBirth($form_state['values']['mouse_date_of_birth']);
  $mouse->setDateOfDeath($form_state['values']['mouse_date_of_death']);
  $mouse->setSharingLevel($form_state['values']['sharing_level']);
  $mouse->setComments($form_state['values']['comments']);
  $mouse->setCreatedDate(date('Y-m-d H:i:s'));
  $mouse->save();

  $form_state['redirect'] = sfb_mouseline_url(SFB_MOUSELINE_URL_VIEW_MOUSELINE, arg(1));
}

function sfb_mouseline_edit_mouse_cancel($form, &$form_state) {
  $form_state['redirect'] = SFB_MOUSELINE_URL_PAGE_DEFAULT; //TODO: redirect auf übersicht ALL
}

function sfb_mouseline_edit_mouse_add_mouse_data($form, &$form_state){
  //TODO here it will be possible to add research data
}