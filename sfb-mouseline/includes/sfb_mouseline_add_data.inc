<?php
/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 02.03.2017
 * Time: 10:51
 */

function sfb_mouseline_add_data($form, &$form_state, $mouseline_PID)
{
  $form_state['mouseline_id'] = Mouseline::extractIDFromPID($mouseline_PID);
  $form_state['mouseline_pid'] = $mouseline_PID;
  $mouseline = MouselineRepository::findById(Mouseline::extractIDFromPID($mouseline_PID));

  /*TODO: Originally a select was planned to choose, what kind of data is to be added or changed; did not work
   * $form['fieldset-datatype'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Data Type'),
    '#description' => '',
  );

  $form['fieldset-datatype']['data_type'] = $mouseline->getFormFieldDataType();*/

  $form['fieldset-antibody'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Add Antibodies'),
    '#description' => '',
    '#access' => true,

    '#collapsible' =>true,
    //'#collapsed' => true,
  );
  $rdp_linking = RDPLinking::getLinkingByName('sfb_antibody', 'antibody_antibody2');
  $table = $rdp_linking->getViewData('mouselines_mouseline', $form_state['mouseline_id']);
  $form['fieldset-antibody']['ab_items'] = Array(
    '#markup' => $table,
  );
  $form['fieldset-antibody']['antibody_edit'] =  $rdp_linking->getEditForm('mouselines_mouseline', $form_state['mouseline_id']);

  $form['fieldset-alm'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Add Adv. Light Microscopy Data'),
    '#description' => '',
    '#access' => true,

    '#collapsible' =>true,
    '#collapsed' => true,
  );

  $form['fieldset-echo'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Add Echocardiographic Data'),
    '#description' => '',
    '#access' => true,

    '#collapsible' =>true,
    '#collapsed' => true,
  );


  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#submit' => array('sfb_mouseline_add_data_back'),
    '#validate' => array(),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes and return'),
    '#submit' => array('sfb_mouseline_add_data_save'),
  );

  return $form;
}

function sfb_mouseline_add_data_save($form, &$form_state)
{
  $rdp_linking = RDPLinking::getLinkingByName('sfb_antibody', 'antibody_antibody2');
  $rdp_linking->processSubmittedForm('mouselines_mouseline', $form_state['mouseline_id'], $form_state['values']['antibody_edit']);
  $form_state['redirect'] = sfb_mouseline_url(SFB_MOUSELINE_URL_VIEW_MOUSELINE, $form_state['mouseline_pid']);

}

function sfb_mouseline_add_data_back($form, &$form_state){
  $form_state['redirect'] = sfb_mouseline_url(SFB_MOUSELINE_URL_VIEW_MOUSELINE, $form_state['mouseline_pid']);
}
//ajax callbacks

function ajax_data_selection_callback($form, &$form_state){
  $mouseline = MouselineRepository::findById($form_state['mouseline_id']);
  if(true){
    $form['fieldset-selection']['access'] = true;
  }
  return $form['fieldset-selection'];
}