<?php

/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 10.01.2017
 * Time: 16:11
 */
class MouselineMouseRepository
{
  /**
   * Name of the table containing mouse information.
   *
   * @var string
   */
  static $tableName = 'mouselines_mouse';

  /**
   * Array with database fields.
   *
   * @var array
   */
  static $databaseFields = array(
    'id',
    'mouseline_id',
    'name',
    'sex',
    'date_of_birth',
    'date_of_death',
    'workinggroup_id',
    'sharing_level',
    'comments',
    'created_date',
    'last_change_date',
  );

  /**
   * Stores mouse data into database.
   *
   * @param MouselineMouse $mouse
   * @return \DatabaseStatementInterface|int
   */
  public static function save($mouse)
  {
    if($mouse->isEmpty()) {
      $nid = db_insert(self::$tableName)
        ->fields(array(
          //'id' => $mouseline->getId(),
          'mouseline_id' => $mouse->getMouselineId(),
          'name' => $mouse->getName(),
          'sex' => $mouse->getSex(),
          'date_of_birth' => $mouse->getDateOfBirth(),
          'date_of_death' => $mouse->getDateOfDeath(),
          'workinggroup_id' => $mouse->getWorkingGroupId(),
          'sharing_level' => $mouse->getSharingLevel(),
          'created_date' => $mouse->getCreatedDate(),
          'last_change_date' => date('Y-m-d'),
          'comments' => $mouse->getComments(),
        ))
        ->execute();
      return $nid;
    } else {
      $num_updated = db_update(self::$tableName)
        ->fields(array(
          //'id' => $mouseline->getId(),
          'mouseline_id' => $mouse->getMouselineId(),
          'name' => $mouse->getName(),
          'sex' => $mouse->getSex(),
          'date_of_birth' => $mouse->getDateOfBirth(),
          'date_of_death' => $mouse->getDateOfDeath(),
          //'workinggroup_id' => $mouse->getWorkingGroupId(),
          'sharing_level' => $mouse->getSharingLevel(),
          'created_date' => $mouse->getCreatedDate(),
          'last_change_date' => date('Y-m-d'),
          'comments' => $mouse->getComments(),
        ))
        ->condition('id',$mouse->getId(), '=')
        ->execute();
      return -1;

    }
  }

  /**
   * Reads mouse database result and creates a Mouse object.
   *
   * @param $result
   * @return \MouselineMouse
   */
  public static function databaseResultsToMouse($result)
  {
    $mouse = new MouselineMouse();

    if (empty($result)) {
      return $mouse;
    }
    // set variables
    $mouse->setId($result->id);
    $mouse->setMouselineId($result->mouseline_id);
    $mouse->setName($result->name);
    $mouse->setSex($result->sex);
    $mouse->setDateOfBirth($result->date_of_birth);
    $mouse->setDateOfDeath($result->date_of_death);
    $mouse->setWorkingGroupId($result->workinggroup_id);
    $mouse->setSharingLevel($result->sharing_level);
    $mouse->setCreatedDate($result->created_date);
    $mouse->setLastChangeDate($result->last_change_date);
    $mouse->setComments($result->comments);

    return $mouse;
  }

  /**
   * Reads mouse database results and create an array with Mouse objects.
   *
   * @param $results
   * @return \MouselineMouse[]
   */
  public static function databaseResultsToMice($results) {
    $mice = array();
    foreach($results as $result)
      $mice[] = self::databaseResultsToMouse($result);

    return $mice;
  }

  /**
   * looks up mice via given mouse line id. returns an array containing all mice belonging to given mouse line
   *
   * @param $mouseline_id
   * @return MouselineMouse[]
   */
  public static function findByMouselineId($mouseline_id)
  {
    // sharing level condition
    $sl_condition = MouselineSharingLevel::getDatabaseCondition();

    $result = db_select(self::$tableName, 'o')
      ->condition('mouseline_id', $mouseline_id, '=')
      ->condition($sl_condition)
      ->fields('o', self::$databaseFields)
      ->range(0, 99)
      ->execute();

    return self::databaseResultsToMice($result);
  }

  /**
   * finds mouse by id in db.
   *
   * @param $id
   * @return MouselineMouse
   */
  public static function findById($id)
  {
    $result = db_select(self::$tableName, 'm')
        ->condition('id', $id, '=')
        ->fields('m', self::$databaseFields)
        ->range(0, 1)
        ->execute()
        ->fetch();

    return self::databaseResultsToMouse($result);
  }

  /**
   * Executes search query on Mouse database with given condition.
   *
   * @param $condition Database condition
   * @return \MouselineMouse[]
   */
  public static function findBy($condition) {
    // sharing level condition
    $sl_condition = MouselineSharingLevel::getDatabaseCondition();

    $results = db_select(self::$tableName, 'm')
        ->condition($condition)
        ->condition($sl_condition)
        ->fields('m', self::$databaseFields)
        ->execute();

    return self::databaseResultsToMice($results);
  }

}