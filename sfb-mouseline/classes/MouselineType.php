<?php

/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 06.01.2017
 * Time: 15:16
 *
 * Here all currently available Types of Mouselines are deposited
 */
class MouselineType
{
  //standard value for all new mouseline objects.
  /**
   * unknown, unspecified, unidentified breeding
   */
  const UNSPECIFIED = 'unspecified';

  /**
   * 20+ generations of brotherxsister mating
   */
  const INBRED = 'Inbred';

  /**
   * derived from other line/strain, possible via genetic drift
   */
  const SUBSTRAIN = 'Substrain';

  /**
   * cross breeding between two lines
   */
  const HYBRID = 'Hybrid';

  /**
   * strains that are derived from two parental strains
   */
  const MIX_INBRED = 'Mixed inbred';

  /**
   * congenic strains are produced by repeated backcrosses to an inbred (background) strain, with selection for a particular marker from the donor strain
   */
  const CONGENIC = 'Congenic';

  /**
   * Coisogenic strains are inbred strains that differ at only a single locus through mutation occurring in that strain
   */
  const COISOGENIC = 'Coisogenic';

  //TODO add more
  //------------------------Descriptions for the different mouse line types------------------//
  /**
   * inbred description
   */
  const INBRED_DESCR =
      'An <strong>inbred strain</strong> is one that:<br>
  <li>is produced using at least 20 consecutive generations of sister x brother or parent x offspring matings, or;</li>
  <lI>is traceable to a single ancestral pair in the 20th or subsequent generation.</lI>';

  /**
   * substrain description
   */
  const SUBSTRAIN_DESCR = '<strong>Substrains</strong> are branches of an inbred strain that are either known or suspected to be genetically different from that inbred strain.
  They form under any of the following three conditions: <br>
  <li>branches of a strain are separated from the parent colony before the 40th generation of inbreeding; </li>
  <li>a branch of a strain is maintained separately from the parent colony for more than 20 generations of inbreeding (10 generations in the branch and 10 in the parent colony); and </li>
  <li>genetic differences from the parent colony are discovered. </li>';

  /**
   * hybrid description
   */
  const HYBRID_DESCR = 'F1 <strong>hybrid</strong> mice are produced by crossing mice of two different inbred strains in the same direction.
  Although they are heterozygous at all loci for which their parents have different alleles, they are similar to inbred strains in that they are genetically and phenotypically uniform.
  As long as the parental strains exist, F1 hybrids can be generated.';

  /**
   * mixed inbred description
   */
  const MIX_INBRED_DESCR = '<strong>Mixed inbred</strong> mice, i.e. incipient inbred strains are derived from up to three parental strains (one of which could be a gene-targeted ES cell line)';

  /**
   * congenic description
   */
  const CONGENIC_DESCR = '<strong>Congenic</strong> strains<br>
  <li>produced by repeated backcrosses to an inbred (background) strain, with selection for a particular marker from the donor strain</li>
  <li>a minimum of 10 backcross generations to the background strain have been made, counting the first hybrid or F1 generation as generation 1</li>';

  /**
   * coisogenic description
   */
  const COISOGENIC_DESCR = '<strong>Coisogenic</strong> mice<br>
  <li>inbred strains that differ at only a single locus through mutation occurring in that strain</li>
  <li>strains containing targeted mutations in ES cells that are then crossed to, and maintained, on the same inbred substrain from which the ES cells were derived</li>
  <li>chemically or radiation induced mutants on an inbred background</li>';

  //TODO: Add more

  /**
   * name generation rules per type
   */
  const INBRED_NAME =
    '<B>Nomenclature for inbred mouse lines:<br>
         <ul><font color = red>Strain Name</font><font color = orange>Lab Abbreviation</font></B> </ul>
    <I>Example: C57BL + Jackson Laboratory</I><br>
    <ul><b><font color = red>C57BL</font><font color = orange>J</font></b></ul>';

  const SUBSTRAIN_NAME =
    '<B>Nomenclature for substrains:<br>
         <ul><font color = red>Substrain name</font>/<font color = orange>Lab Abbreviation</font></B> </ul>
    <I>Example: C57BL/6 + Jackson Laboratory</I><br>
    <ul><b><font color = red>C57BL/6</font><font color = orange>J</font></b></ul>';

 const HYBRID_NAME =
    '<B>Nomenclature for hybrid mouse lines:<br>
         <ul>(<font color = red>Mother\'s Strain</font>x<font color = blue>Father\'s Strain</font>)F1</B> </ul>
    <I>Example: C57BL/6J + 129S1/SvImJ</I><br>
    <ul><b>(<font color = red>C57BL/6J</font>x<font color = blue>129S1/SvImJ</font>)F1</b></ul>
     <I>Example Abbreviation:</I><br>
    <ul><b><font color = red>B6</font><font color = blue>129S</font>F1</b></ul>';

 const MIX_INBRED_NAME =
    '<B>Nomenclature for mixed inbred strains:<br>
         <ul><font color = red>Host Strain Abbreviation</font>;<font color = orange>Donor Strain Abbreviation</font>-<font color = #00008b><i>Gene Symbol<sup>Mutation/Allele Details</sup></i></font></B> </ul>
    <I>Example: mixed strain derived from C57BL/6J and a 129 ES cell line carrying a targeted knockout of the Acvr2 gene</I><br>
    <ul><b><font color = red>B6</font>;<font color = orange>129</font>-<font color = #00008b><i>Acvr2<sup>tm1Sor</sup></i></font></B> </ul>';

  const CONGENIC_NAME =
    '<B>Nomenclature for congenic strains:<br>
         <ul><font color = red>Recipient Strain Abbreviation</font>.<font color = orange>Donor Strain Abbreviation</font>-<font color = #00008b><i>Gene Symbol<sup>Mutation/Allele Details</sup></i></font></B> </ul>
    <I>Example:<br>
    <ul>strain with the genetic background of C57BL/6 but which differs from that strain by the introduction of a differential allele (H2k) derived from strain AKR/J</I></ul>
    <ul><b><font color = red>B6</font>.<font color = orange>AKR</font>-<font color = #00008b><i>H2<sup>k</sup></i></font></B> </ul>';

  const COISOGENIC_NAME =
    '<B>Nomenclature for coisogenic strains:<br>
         <ul><font color = red>Strain Name</font>-<font color = #00008b><i>Gene Symbol<sup>Mutation/Allele Details</sup></i></font></B> </ul>
    <I>Example:<br>
    <ul>A targeted mutation of the Fyn gene was produced using the AB1 ES cell line derived from 129S7/SvEvBrd. Chimeric animals were mated to 129S7/SvEvBrd and the allele subsequently maintained on this coisogenic strain.</I><br></ul>
    <ul><b><font color = red>129S7/SvEvBrd</font>-<font color = #00008b><i>Fyn<sup>tm1Sor</sup></i></font></B> </ul>';

  /**
   * getter for the given type's description
   *
   * @param $type
   * @return array
   */
  public static function getDescription($type)
  {
    switch($type){
      case MouselineType::INBRED:
        $descr = MouselineType::INBRED_DESCR;
        break;
      case MouselineType::HYBRID:
        $descr = MouselineType::HYBRID_DESCR;
        break;
      case MouselineType::SUBSTRAIN:
        $descr = MouselineType::SUBSTRAIN_DESCR;
        break;
      case MouselineType::MIX_INBRED:
        $descr = MouselineType::MIX_INBRED_DESCR;
        break;
      case MouselineType::CONGENIC:
        $descr = MouselineType::CONGENIC_DESCR;
        break;
      case MouselineType::COISOGENIC:
        $descr = MouselineType::COISOGENIC_DESCR;
        break;
      default:
        $descr = 'Mouseline type not found';
    }
    return Array (
        '#type' => 'markup',
        '#markup' => '<div>'.t($descr).'</div>',
    );
  }

  /**
   * get array with all type, for example for usage in drop downs
   *
   * @return array
   */
  public static function getAllTypes(){
    return Array(MouselineType::INBRED, MouselineType::SUBSTRAIN, MouselineType::HYBRID, MouselineType::MIX_INBRED, MouselineType::CONGENIC, MouselineType::COISOGENIC, MouselineType::UNSPECIFIED);
  }

  /**
   *
   */
  public static function getNomenclatureName($type){
    switch($type){
      case MouselineType::INBRED:
        $descr = MouselineType::INBRED_NAME;
        break;
      case MouselineType::HYBRID:
        $descr = MouselineType::HYBRID_NAME;
        break;
      case MouselineType::SUBSTRAIN:
        $descr = MouselineType::SUBSTRAIN_NAME;
        break;
      case MouselineType::MIX_INBRED:
        $descr = MouselineType::MIX_INBRED_NAME;
        break;
      case MouselineType::CONGENIC:
        $descr = MouselineType::CONGENIC_NAME;
        break;
      case MouselineType::COISOGENIC:
        $descr = MouselineType::COISOGENIC_NAME;
        break;
      default:
        $descr = 'Mouseline type not found';
    }
    return Array (
      '#type' => 'markup',
      '#markup' => '<div>'.t($descr.'<small><i>Source and complete guide: <a href="http://www.informatics.jax.org/mgihome/nomen/strains.shtml" target="_blank">Guidelines for Nomenclature of Mouse and Rat Strains </a></i></small>').'</div>',
    );
  }

  //------------------------------ name generators ------------------------------//
  /**
   * builds the name of a hybrid line: (mother x father)
   *
   * @param $strain_mother
   * @param $strain_father
   * @return string
   */
  public static function  buildNameHybrid($strain_mother, $strain_father, $lab)
  {
    if(empty($strain_mother))
      $strain_mother = "?";
    if(empty($strain_father))
      $strain_father = "?";

    //fix all html <'s and >'s; Ajax tends to break html-commands by converting them into hardcoded symbols
    $strain_mother = str_replace("&amp;lt;", "<", $strain_mother);
    $strain_mother = str_replace("&amp;gt;", ">", $strain_mother);
    $strain_father = str_replace("&amp;lt;", "<", $strain_father);
    $strain_father = str_replace("&amp;gt;", ">", $strain_father);
     // return '(' . $strain_mother . ' x ' . $strain_father . ')';

    if (empty($lab))
      return $strain_mother . $strain_father . 'F1';
    else
      return $strain_mother . $strain_father . 'F1' . '/' . $lab;
    //return $strain_mother . $strain_father . 'F1';
  }

  /**
   * builds the name of an inbred line: strainlab
   *
   * @param $strain
   * @param $lab
   * @return string
   */
  public static function buildNameInbred($strain, $lab)
  {
      if (empty($lab))
          return $strain;
      else
          return $strain . $lab;
          //return $strain .'/'. $lab;
  }

  /**
   * builds the name of a substrain: strain/lab
   *
   * @param $strain
   * @param $lab
   * @return string
   */
  public static function buildNameSubstrain($strain, $lab){
      if (empty($lab))
          return $strain;
      // strain name already contains a '/'
      if (strpos($strain, '/') !== false){
        return $strain . $lab;
      }else
          return $strain .'/'. $lab;
  }

  /**
   * builds the name of a mixed inbred line: host;donor
   *
   * @param $host
   * @param $donor
   * @return string
   */
  public static function  buildNameMixInbred($host, $donor)
  {
    if(empty($host))
      $host = "?";
    if(empty($donor))
      $donor = "?";

    //fix all html <'s and >'s; Ajax tends to break html-commands by converting them into hardcoded symbols
    $host = str_replace("&amp;lt;", "<", $host);
    $host = str_replace("&amp;gt;", ">", $host);
    $donor = str_replace("&amp;lt;", "<", $donor);
    $donor = str_replace("&amp;gt;", ">", $donor);

   /* if (empty($lab))
      return $host . ';' . $donor;
    else
      return $host . ';' . $donor .'/'. $lab;*/
    return $host . ';' . $donor;
  }

  /**
   * builds the name of a congenic strain: host.donor
   *
   * @param $host
   * @param $donor
   * @return string
   */
  public static function  buildNameCongenic($host, $donor)
  {
    if(empty($host))
      $host = "?";
    if(empty($donor))
      $donor = "?";

    //fix all html <'s and >'s; Ajax tends to break html-commands by converting them into hardcoded symbols
    $host = str_replace("&amp;lt;", "<", $host);
    $host = str_replace("&amp;gt;", ">", $host);
    $donor = str_replace("&amp;lt;", "<", $donor);
    $donor = str_replace("&amp;gt;", ">", $donor);

    /*if (empty($lab))
      return $host . '.' . $donor;
    else
      return $host . '.' . $donor .'/'. $lab;*/
    return $host . '.' . $donor;
  }

  /**
   * builds the name of an coisogenic strain: strainlab
   *
   * @param $strain
   * @param $lab
   * @return string
   */
  public static function buildNameCoisogenic($strain)
  {
    /*if (empty($lab))
      return $strain;
    else
      return $strain .'/'. $lab;*/
    return $strain;
  }

}
