<?php

/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 06.01.2017
 * Time: 15:16
 */
class MouselineLaboratory
{
  /* ------------------------------ Attributes ----------------------------- */
  /**
   * Default id for new or yet unsaved lab objects.
   *
   * @var int
   */
  const EMPTY_LAB_ID = -1;

  /**
   * @var int
   */
  private $id = MouselineLaboratory::EMPTY_LAB_ID;
  /**
   * Laboratory's name
   *
   * @var string
   */
  private $name;
  /**
   * Laboratory's ILAR abbreviation
   *
   * @var string
   */
  private $abbreviation;

  /**
   * URL of Lab in ilar
   *
   * @var string
   */
  private $ilarURL;
  /* ------------------------------ Setters & Getters ----------------------------- */

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getAbbreviation()
  {
    return $this->abbreviation;
  }

  /**
   * @param string $abbreviation
   */
  public function setAbbreviation($abbreviation)
  {
    $this->abbreviation = $abbreviation;
  }

  /**
   * @return string
   */
  public function getIlarURL()
  {
    return $this->ilarURL;
  }

  /**
   * @param string $ilarURL
   */
  public function setIlarURL($ilarURL)
  {
    $this->ilarURL = $ilarURL;
  }
  /* ------------------------------ Formfields ----------------------------- */

  /**
   * returns form field for the Laboratory's name
   *
   * @return array
   */
  public function getFormFieldName()
  {
    return array(
      '#type' => 'textfield',
      '#title' => t('Laboratory name'),
      '#default_value' => $this->name,
      '#description' => 'Insert the origin lab\'s name here.',
      '#attributes' => array('placeholder' => t('e.g.') . ': Jackson Laboratory'),
      '#maxlength' => 100,
      '#autocomplete_path' => SFB_MOUSELINE_API_AUTOCOMPLETE_LABORATORY,
      '#ajax' => array(
          'callback' => 'ajax_mouseline_lab_abb_callback',
      ),
    );
  }

  /**
   * returns form field for the ILAR abbreviation
   *
   * @return array
   */
  public function getFormFieldAbbreviation()
  {
    return array(
      '#type' => 'textfield',
      '#title' => t('Laboratory Abbreviation'),
      '#default_value' => $this->abbreviation,
      '#description' => 'Insert the origin lab\'s standardized abbreviation here.',
      '#attributes' => array('placeholder' => t('e.g.') . ': J'),
      '#maxlength' => 100,
      '#ajax' => array(
          'callback' => 'ajax_mouseline_name_callback',
          'wrapper' => 'mouseline-name-div',
      ),
      '#prefix' => '<div id = "lab">',
      '#suffix' => '<small>For more information on lab abbreviations visit <a href="http://dels.nas.edu/global/ilar/Lab-Codes" target="_blank">
          the ILAR registry</a></small></div>'
    );
  }

  /**
   * returns form field for the ILAR url
   *
   * @return array
   */
  public function getFormFieldIlarURL()
  {
    return array(
      '#type' => 'textfield',
      '#title' => t('Laboratory Code URL in ILAR DB'),
      '#default_value' => $this->ilarURL,
      '#description' => 'Insert the lab code\'s URL here.',
      '#attributes' => array('placeholder' => t('e.g.') . ': http://ilarlabcode.nas.edu/search_codes_full.php?labcode_id=1100&user_id=10360'),
      '#maxlength' => 250,

    );
  }

  public function isEmpty() {
    if($this->id == MouselineLaboratory::EMPTY_LAB_ID) {
      return true;
    }
    return false;
  }


  /**
   * saves the lab , calls database function
   *
   * @return DatabaseStatementInterface|int
   */
  public function save() {
    $nid = MouselineLaboratoryRepository::findByName($this->getName());
    if($nid->isEmpty()) {
      $nid = MouselineLaboratoryRepository::save($this);
    }
    return $nid;
  }

}
