<?php

/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 05.04.2017
 * Time: 15:46
 */
class MouselineStrainRepository
{
  /* ------------------------------ DB functionality ----------------------------- */
  /**
   * strain db-table name
   *
   * @var string
   */
  static $tableName = 'mouselines_strain';

  /**
   * defines the table fields
   *
   * @var array
   */
  static $databaseFields = array(
    'id',
    'name',
    'abbreviation',
  );

  /**
   * saves the data set into the database
   */
  public static function save($strain){
    if($strain->isEmpty()) {
      $nid = db_insert(self::$tableName)
      ->fields(array(
        //'id' => $this->getId(),
        'name' => $strain->getName(),
        'abbreviation' => $strain->getAbbreviation(),
      ))
      ->execute();
      return $nid;
  } else {
      $num_updated = db_update(self::$tableName)
        ->fields(array(
          'name' => $strain->getName(),
          'abbreviation' => $strain->getAbbreviation(),
        ))
        ->condition('id',$strain->getId(), '=')
        ->execute();
      return -1;
    }
  }

  /**
   * Looks up strain with given id in the data base
   *
   * @param $id
   * @return MouselineStrain
   */
  public static function findById($id)
  {
    $result = db_select(self::$tableName, 's')
      ->condition('id', $id, '=')
      ->fields('s', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToStrain($result);
  }

  /**
   * Looks up strain with given name in the data base
   *
   * @param $name
   * @return MouselineStrain
   */
  public static function findByName($name)
  {
    $result = db_select(self::$tableName, 's')
      ->condition('name', $name, '=')
      ->fields('s', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToStrain($result);
  }

  /**
   * after db query is done, the results are translated into MouselineStrain object
   *
   * @param $result
   * @return MouselineStrain
   */
  public static function databaseResultsToStrain($result){
    $strain = new MouselineStrain();

    if(empty($result)) {
      return $strain;
    }
    // set variables
    $strain->setId($result->id);
    $strain->setName($result->name);
    $strain->setAbbreviation($result->abbreviation);

    return $strain;
  }

  /**
   * Executes search query on strain database with given condition.
   *
   * @param $condition Database condition
   * @return \MouselineStrain[]
   */
  public static function findBy($condition) {
    // sharing level condition

    $results = db_select(self::$tableName, 'm')
      ->condition($condition)
      ->fields('m', self::$databaseFields)
      ->execute();

    return self::databaseResultsToStrains($results);
  }

  /**
   * Reads strain database results and create an array with lab objects.
   *
   * @param $results
   * @return \MouselineStrain[]
   */
  public static function databaseResultsToStrains($results) {
    $labs = array();
    foreach($results as $result)
      $labs[] = self::databaseResultsToStrain($result);

    return $labs;
  }
}