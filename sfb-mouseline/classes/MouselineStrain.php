<?php

/**
 * Created by PhpStorm.
 * User: agrim
 * Date: 05.02.2017
 * Time: 10:15
 */
class MouselineStrain
{
  /* ------------------------------ Attributes ----------------------------- */
  /**
   * Default id for new or yet unsaved lab objects.
   *
   * @var int
   */
  const EMPTY_STRAIN_ID = -1;

  /**
   * @var int
   */
  private $id = MouselineStrain::EMPTY_STRAIN_ID;

  /**
   * complete strain name
   *
   * @var string
   */
  private $name;

  /**
   * common abbreviation, as it can be found in mouse phenome database
   *
   * @var string
   */
  private $abbreviation;

  /* ------------------------------ Setters & Getters ----------------------------- */

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getAbbreviation()
  {
    return $this->abbreviation;
  }

  /**
   * @param string $abbreviation
   */
  public function setAbbreviation($abbreviation)
  {
    $this->abbreviation = $abbreviation;
  }

  /* ------------------------------ Formfields ----------------------------- */

  /**
   * returns form field for the strain's name
   *
   * @return array
   */
  public function getFormFieldName()
  {
    return array(
        '#type' => 'textfield',
        '#title' => t('Strain\'s full name'),
        '#default_value' => $this->name,
        '#attributes' => array('placeholder' => t('e.g.') . ': C57BL/6J'),
        '#maxlength' => 100,
        '#suffix' => '<small>For more information on strain names visit the
        <a href="http://phenome.jax.org/strains" target="_blank">Mouse Phenome Database</a> or <a href="http://www.informatics.jax.org/mgihome/nomen/strains.shtml#hybrids" target="_blank">MGI approved abbreviations</a> for common mouse strains</small>',
        '#autocomplete_path' => SFB_MOUSELINE_API_AUTOCOMPLETE_STRAIN,
        '#required' => TRUE,
    );
  }

  /**
   * returns form field for the abbreviation
   *
   * @return array
   */
  public function getFormFieldAbbreviation()
  {
    return array(
        '#type' => 'textfield',
        '#title' => t('Strain\'s abbreviation'),
        '#default_value' => $this->abbreviation,
        '#attributes' => array('placeholder' => t('e.g.') . ': B6'),
        '#maxlength' => 100,
        '#ajax' => array(
            'callback' => 'ajax_mouseline_name_callback',
            'wrapper' => 'mouseline-name-div',
        ),
    );
  }


  public function isEmpty() {
    if($this->id == MouselineStrain::EMPTY_STRAIN_ID) {
      return true;
    }
    return false;
  }


  /**
   * saves the  strain, calls database function
   *
   * @return DatabaseStatementInterface|int
   */
  public function save() {
    $nid = MouselineStrainRepository::findByName($this->getName());
    if($nid->isEmpty()) {
      $nid = MouselineStrainRepository::save($this);
    }
    return $nid;
  }
}
