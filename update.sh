#!/bin/bash

declare -a modules=("sfb-commons" "sfb-literature" "rdp-wikidata" "sfb-antibody" "sfb-mouseline" "rdp-archive" "rdp-cellmodel")

for mod in "${modules[@]}"
do
   echo "Processing $mod"
   echo "Remove directory if exists..."
   rm -rf $mod
   # Clone repository
   git clone https://gitlab-pe.gwdg.de/research-data-platform/$mod.git
   # Remove .git directory
   rm -rf $mod/.git*
done

