<?php
/**
 * @file List subprojects.
 *
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 * @author Markus Suhr (markus.suhr@med.uni-goettingen.de)
 */

/**
 * @return string Drupal page content.
 */
function sfb_commons_config_subprojects() {

  // array used for table header
  $header = array(
    array('data' => t('ID')),
    array('data' => t('Abbreviation')),
    array('data' => t('Status')),
    array('data' => t('Title')),
    array('data' => t('Operation'))
  );

  $subprojects = SubprojectsRepository::findAll();

  // variable used for table rows
  $rows = array();

  //TODO: implement delete function (?)

  $status_options = Subproject::$status_options;

  // create table row for each subproject
  foreach($subprojects as $subproject) {
    $rows[] = array(
      $subproject->getId(),
      $subproject->getAbbreviation(),
      $status_options[$subproject->getStatus()],
      $subproject->getTitle(),
      '<a href="'.sfb_commons_url(SFB_COMMONS_URL_CONFIG_SUBPROJECTS_EDIT, $subproject->getId()).'">edit</a>',
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}