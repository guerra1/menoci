<?php

/**
 * @file
 * Shows configuration form with general settings for RDP modules
 *
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 *
 * @return mixed
 */
function sfb_commons_config_general() {
  $form = [];

  // form field for project title
  $form[SFB_COMMONS_CONFIG_VARIABLE_PROJECT_TITLE] = [
    '#type' => 'textfield',
    '#title' => t('Project name'),
    '#description' => t('Enter the name of your project which will be hosted by this Research Data Platform.'),
    '#default_value' => variable_get(SFB_COMMONS_CONFIG_VARIABLE_PROJECT_TITLE, SFB_COMMONS_CONFIG_PROJECTNAME),
  ];

  $form['theme'] = [
    '#type' => 'fieldset',
    '#title' => t('Theme settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['theme']['sfb_config_theme_logo'] = [
    '#type' => 'textfield',
    '#title' => t('Logo'),
    '#description' => t('Path to the logo'),
    '#default_value' => variable_get('sfb_config_theme_logo', 'logo.png'),
  ]; // TODO: diese variable wird noch nicht in theme verwendet

  $form[SFB_COMMONS_CONFIG_VAR_PROFILE_TYPE] = [
    '#type' => 'textfield',
    '#title' => t('Default profile type'),
    '#description' => t('Machine name of the default profile type (see: profile2 module), i.e. the profile that 
    includes users\' firstname, surname, and orcid for example.'),
    '#default_value' => variable_get(SFB_COMMONS_CONFIG_VAR_PROFILE_TYPE),
    '#required' => TRUE,
  ];

  $form[SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH] = [
    '#type' => 'textfield',
    '#title' => t('Resources directory path'),
    '#description' => t('NB: Path must be located within webserver 
    DOCUMENT_ROOT, system user running the webserver must have write access 
      to this directory.'),
    '#default_value' => variable_get(SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH),
    '#required' => TRUE,
  ];

  $form['#validate'][] = 'sfb_commons_config_general_validate';

  return system_settings_form($form);
}

/**
 * Validate config setting input.
 */
function sfb_commons_config_general_validate($form, &$form_state) {
  /**
   * Check if profile name exists
   */
  $profile_name = $form_state['values'][SFB_COMMONS_CONFIG_VAR_PROFILE_TYPE];
  $results = db_select('profile_type', 't')
    ->condition('t.type', $profile_name, '=')
    ->fields('t', ['type'])
    ->execute()->rowCount();
  if (!$results) {
    form_set_error(SFB_COMMONS_CONFIG_VAR_PROFILE_TYPE, t('Please insert a valid profile type.'));
  }

  // make sure leading and trailing slashes exist
  $path = $form_state['values'][SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH];
  if (substr($path, -1, 1) != '/') {
    $path = $path . '/';
  }
  if (substr($path, 0, 1) != '/') {
    $path = '/' . $path;
  }
  $form_state['values'][SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH] = $path;

  // test if new resources directory path is writeable by webserver
  $path = $_SERVER['DOCUMENT_ROOT'] . $path;
  if (!is_writeable($path)) {
    form_set_error(SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH,
      t('Path is not valid or webserver does not have write access.'));
  }
}