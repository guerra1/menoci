<?php
/**
 * @file Form and action handler to add subprojects.
 *
 * @author Markus Suhr (markus.suhr@med.uni-goettingen.de)
 */

/**
 * Form to edit subproject.
 *
 * @param int $subproject_id
 *
 * @return array Drupal form.
 */
function sfb_commons_config_subprojects_edit($form, &$form_state, $subproject_id) {

  // get subproject from database
  $subproject = SubprojectsRepository::findById($subproject_id);


  // if subproject is empty, than there is no database with given id
  if ($subproject->isEmpty()) {
    drupal_not_found();
    return;
  }

  $form = [];

  // pass subproject_id to submit action handler
  $form_state['subproject_id'] = $subproject_id;

  $form['abbreviation'] = [
    '#type' => 'textfield',
    '#title' => t('Abbreviation'),
    '#description' => t('Abbreviation for the subproject'),
    '#default_value' => $subproject->getAbbreviation(),
    '#required' => TRUE,
  ];

  $form['status'] = [
    '#type' => 'select',
    '#title' => t('Status'),
    '#description' => t('Status of the subproject'),
    '#options' => Subproject::$status_options,
    '#default_value' => $subproject->getStatus(),
    '#required' => TRUE,
  ];

  $form['title'] = [
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title of the subproject'),
    '#default_value' => $subproject->getTitle(),
    '#max_length' => 256,
    '#required' => TRUE,
  ];

  // submit button
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];
  return $form;
}

/**
 * Handle sfb_commons_config_subprojects_edit submit action.
 * Translate form field into Subproject object and save it to database.
 *
 * @param $form
 * @param $form_state
 */
function sfb_commons_config_subprojects_edit_submit($form, &$form_state) {
  // get subproject entry from database
  $subproject = SubprojectsRepository::findById($form_state['subproject_id']);

  // set class members with data from the form
  $subproject->setAbbreviation($form_state['values']['abbreviation']);
  $subproject->setStatus($form_state['values']['status']);
  $subproject->setTitle($form_state['values']['title']);

  // store the data into database
  if (!$subproject->save()) {
    drupal_set_message(t('There was a problem with the database. Form could not be stored!'), 'error');
    return;
  }

  // redirect to subprojects overview
  $form_state['redirect'] = SFB_COMMONS_URL_CONFIG_SUBPROJECTS;
  return;

}
