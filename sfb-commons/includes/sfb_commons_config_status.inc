<?php

//TODO: Comments
function sfb_commons_config_status() {

  // prepare table header
  $header = array(
    t('Module name'),
    t('Version'),
    t('Machine name'),
    t('Status'),
    t('Status messages'),
    t('Git revision'),
  );

  $rows = array();

  // invoke all module implementing rdp_config_check hook
  foreach (module_implements('rdp_config_check') as $module) {
    $result = module_invoke($module, 'rdp_config_check');

    // read module's info file
    $path = drupal_get_path('module', $module) . '/' . $module . '.info';
    $info = drupal_parse_info_file($path);

    // get module name
    $name = $module;
    if (isset($info['name'])) {
      $name = $info['name'];
    }

    // get module version
    $version = 'unknown';
    if (isset($info['version'])) {
      $version = $info['version'];
    }

    // get callee status
    $status = 'unknown';
    if (isset($result[$module . '_status'])) {
      switch ($result[$module . '_status']) {
        case RDP_MODULE_STATUS_OK :
          $status = '<span class="text-success">' . t('ok') . '</span>';
          break;
        case RDP_MODULE_STATUS_WARNING :
          $status = '<span class="text-warning">' . t('warning') . '</span>';
          break;
        case RDP_MODULE_STATUS_ERROR :
          $status = '<span class="text-danger"><strong>' . t('error') . '</strong></span>';
          break;
        default:
          $status = $result[$module . '_status'];
      }
    }

    // get callee messages
    $messages = '<ul>';
    if (isset($result[$module . '_messages'])) {
      foreach ($result[$module . '_messages'] as $msg) {
        $messages .= '<li>' . $msg . '</li>';
      }
    }
    $messages .= '</ul>';

    // git version if exists
    $git_commit = '';
    try {
      $git_commit .= sfb_commons_get_git_revision($module);
    } catch (Exception $e) {
      $git_commit = '-';
    }

    // prepare table rows
    $rows[] = array(
      $name,
      $version,
      $module,
      $status,
      $messages,
      $git_commit,
    );
  }

  return theme('table', array('header' => $header, 'rows' =>$rows));
}


