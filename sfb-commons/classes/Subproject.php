<?php

//TODO: comments

class Subproject {

  /**
   * @var array Allowed values for status field and the respective description
   */
  public static $status_options = ['E' => 'ended', 'C' => 'continued', 'N' => 'new'];

  /**
   * Array with database fields. Required by ALMMessagesRepository
   *
   * @var array
   */
  static $databaseFields = ['id', 'abbreviation', 'status', 'title'];

  const EMPTY_SUBPROJECT_ID = -1;

  /**
   * Database field (Primary key)
   * @var integer
   */
  private $id = self::EMPTY_SUBPROJECT_ID;

  /**
   * Database field (Unique)
   * @var string
   */
  private $abbreviation;

  /**
   * Database field
   * @var string
   */
  private $status;

  /**
   * Database field
   * @var string
   */
  private $title;

  /**
   * Checks if the Subproject instance is empty, i.e. not yet saved to database.
   *
   * @return bool True if empty, i.e. not save to database.
   */
  public function isEmpty() {
    if ($this->id == self::EMPTY_SUBPROJECT_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }


  /**
   * Save Subproject object to repository.
   *
   * @return bool True if saved successfully.
   */
  public function save()
  {
    $id = SubprojectsRepository::save($this);
    if($this->isEmpty() && $id){
      $this->id = $id;
      return TRUE;
    }
    else {
      return $id;
    }
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getAbbreviation() {
    return $this->abbreviation;
  }

  /**
   * @param string $abbreviation
   */
  public function setAbbreviation($abbreviation) {
    $this->abbreviation = $abbreviation;
  }

  /**
   * @return string
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param string $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @param string $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

}
