<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 14.11.17
 * Time: 13:02
 */

class UsersRepository {

  /**
   * @param $uid
   *
   * @return \User
   */
  public static function findByUid($uid) {
    $query = db_select('users', 'u');
    $result = $query->fields('u', ['uid', 'name', 'mail'])
      ->condition('u.uid', $uid, '=')
      ->range(0, 1)
      ->execute()
      ->fetchAssoc();

    $user = new User;
    $user->setUid($result['uid']);
    $user->setName($result['name']);
    $user->setMail($result['mail']);

    return $user;
  }

  public static function findByName($name) {
    $query = db_select('users', 'u');
    $result = $query->fields('u', ['uid', 'name', 'mail'])
      ->condition('u.name', $name, '=')
      ->range(0, 1)
      ->execute()
      ->fetchAssoc();

    $user = new User;
    $user->setUid($result['uid']);
    $user->setName($result['name']);
    $user->setMail($result['mail']);

    return $user;
  }

  /**
   * @return User[]
   */
  public static function findAll() {
    $users = [];

    $query = db_select('users', 'u');
    $results = $query->fields('u', ['uid', 'name', 'mail'])
      ->condition('u.uid', 0, '>')
      ->execute();

    foreach ($results as $result) {
      $nuser = new User();
      $nuser->setUid($result->uid);
      $nuser->setName($result->name);
      $nuser->setMail($result->mail);
      $users[] = $nuser;
    }

    return $users;
  }


}