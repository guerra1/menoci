<?php

class RDPStreamWrapper extends DrupalPublicStreamWrapper {

  public function getDirectoryPath() {
    // relative to www-root
    return $_SERVER['DOCUMENT_ROOT'] . variable_get(SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH, SFB_COMMONS_CONFIG_RESOURCES_PATH);
  }
}
