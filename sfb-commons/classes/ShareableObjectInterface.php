<?php
/**
 * @file Contains interface for classes describing data-objects within the
 *   Research Data Platform project that can be shared among users or publicly.
 */

/**
 * Interface ShareableObjectInterface
 */
interface ShareableObjectInterface {

  /**
   * @return bool TRUE if read permission is granted
   */
  public function checkReadPermission();

  /**
   * @return bool TRUE if write permission is granted
   */
  public function checkWritePermission();

  /**
   * @return int
   */
  public function getSharingLevel();

  /**
   * @return int
   */
  public function getOwnerId();

  /**
   * @return int
   */
  public function getWorkingGroupId();
}