<?php
/** @file ToDo: doc
 *
 */

/* ----------------------- HOOK RDP_ROLES_PERMISSIONS ----------------------- */

/**
 * Research Data Platform hook rdp_roles_permissions()
 *
 * This custom hook must return an associated array of roles (machine names as
 * keys) and an array of the role's associated permissions as values Custom
 * permissions must be defined through Drupal hook @see hook_permission()
 *
 * @return array
 *  Array of roles and associated permissions
 *  - module-custom-role: machine name of RDP custom user role
 *  - module-defined-permission: permission associated to the user role. Leave
 *   empty if no specific permissions are associated with role
 */
function hook_rdp_roles_permissions() {
  return [
    'module-custom-role' => [
      'module-defined-permission',
    ],
  ];
}

/**
 * Research Data Platform hook rdp_config_check()
 *
 * ToDo: hook demo, documentation
 *
 * @return
 */
function hook_rdp_config_check() {
  // ToDo: fill in hook demo
}

/* ---------------------------- RDP LINKING HOOKs --------------------------- */
/*
 * Each module supporting linking function must implement following hooks:
 *
 * hook_rdp_linking ToDo
 *    defines which entities (tables names) can be linked
 *
 * hook_rdp_linking_TABLENAME_view_get ToDo
 *    returns data (table) with linked items (can be themed)
 *
 * hook_rdp_linking_TABLENAME_edit_get ToDo
 *    returns form for linking items
 *
 * hook_rdp_linking_TABLENAME_edit_validate ToDo
 *    returns an array with validate result
 *
 * hook_rdp_linking_TABLENAME_edit_submit ToDo
 *    return an array with linked item ids, or null if an error occurred
 */