<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 28.07.18
 * Time: 19:08
 */

function rdp_external_id_config_title($class, $id) {

  switch ($class) {
    case ExternalIDSubject::class:
      return ExternalIDSubject::fetch($id)->getSubject();
      break;
    case ExternalIDType::class:
      $type = ExternalIDType::fetch($id);
      $label = $type->getLabel();
      return t($label);
      break;
    case ExternalID::class:
      return ExternalID::fetch($id)->getValue();
      break;
  }
}

function rdp_external_id_config() {

  $display = "<h2>Existing External IDs</h2>";

  $objects = GenericRepository::findAll(ExternalID::class);

  $display .= ExternalID::table($objects);

  return $display;
}

function rdp_external_id_config_list($class) {

  if (!is_subclass_of($class, GenericObject::class)) {
    drupal_not_found();
  }
  /**
   * @var \GenericObjectInterface $class
   */

  switch ($class) {
    case ExternalIDType::class:
      $display = "<h2>External ID Types</h2>";
      break;
    case ExternalIDSubject::class:
      $display = "<h2>External ID Subjects</h2>";
      break;
    case ExternalID::class:
    default:
      $display = "<h2>External Identifiers</h2>";
      break;
  }

  $objects = GenericRepository::findAll($class);

  if (count($objects)) {
    $display .= $class::table($objects);
  }

  return $display;

}

function rdp_external_id_config_create($form, $form_state, $class) {
  if (!is_subclass_of($class, GenericObject::class)) {
    drupal_not_found();
  }

  $instance = new $class();
  return $instance->getForm();
}

function rdp_external_id_config_create_validate($form, &$form_state) {
  $values = $form_state['values'];

  $class = $values['class'];

  GenericObjectForm::validate($class, $values);
}

function rdp_external_id_config_create_submit($form, &$form_state) {
  $values = $form_state['values'];

  $class = $values['class'];
  /** @var \GenericObjectInterface $class */

  $values_object = json_decode(json_encode($values));
  $instance = $class::translate_db_result($values_object);

  $instance->save();

  $form_state['redirect'] = $instance->url();
}

function rdp_external_id_config_edit($form, $form_state, $class, $id) {
  if (!is_subclass_of($class, GenericObject::class)) {
    drupal_not_found();
  }

  $instance = $class::fetch($id);
  return $instance->getForm('edit');
}

function rdp_external_id_config_edit_validate($form, &$form_state) {

  /**
   * ToDo: validation
   */
}

function rdp_external_id_config_edit_submit($form, &$form_state) {
  $values = $form_state['values'];

  $class = $values['class'];
  /** @var \GenericObjectInterface $class */

  $values_object = json_decode(json_encode($values));
  $instance = $class::translate_db_result($values_object);

  $instance->setId($values['id']);
  $instance->save();

  $form_state['redirect'] = $instance->url();
}

function rdp_external_id_config_view($class, $id) {
  if (!is_subclass_of($class, GenericObject::class)) {
    drupal_not_found();
  }
  /**
   * @var \GenericObjectInterface $class
   */

  if (!$object = GenericRepository::findById($id, $class)) {
    drupal_not_found();
  }
  /**
   * @var \GenericObjectInterface $object
   */

  return $object->displayPage();
}