<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 30.07.18
 * Time: 14:21
 */

/**
 * Class ExternalIDRepository
 */
abstract class ExternalIDRepository extends GenericRepository {

  public static function findById($id, $class = NULL) {
    $class = ExternalID::class;
    return parent::findById($id, $class);
  }

  public static function findAll($class = NULL) {
    $class = ExternalID::class;
    return parent::findAll($class);
  }

  public static function findByValue($value) {
    $result = parent::findByConditions(ExternalID::class,
      ['value' => $value]);
    if (count($result) == 1) {
      return array_pop($result);
    }
    else {
      return $result;
    }
  }

  /**
   * @param int|string $type
   * @param int|string $subject
   * @param int $subject_id
   *
   * @return array|mixed
   */
  public static function findByTypeSubjectId($type, $subject, $subject_id) {
    if (!is_int($type)) {
      $type_object = ExternalIDTypeRepository::findByLabel($type);
      if ($type_object) {
        $type = $type_object->getId();
      }
      else {
        return NULL;
      }
    }
    if (!is_int($subject)) {
      $subject_object = ExternalIDSubjectRepository::findBySubject($subject);
      if ($subject_object) {
        $subject = $subject_object->getId();
      }
      else {
        return NULL;
      }
    }
    $result = parent::findByConditions(ExternalID::class,
      [
        'type' => $type,
        'subject' => $subject,
        'subject_id' => $subject_id,
      ]);
    if (count($result) == 1) {
      return array_pop($result);
    }
    elseif (count($result) == 0) {
      return NULL;
    }
    else {
      return $result;
    }
  }

  /**
   * @param int|string $type The internal ID or label of an Ex-ID type
   * @param int|string $subject The internal ID or label of an Ex-ID subject
   *
   * @return ExternalID[] A list of objects of the requested type and subject
   */
  public static function findByTypeSubject($type, $subject) {
    if (!is_int($type)) {
      $type_object = ExternalIDTypeRepository::findByLabel($type);
      if ($type_object) {
        $type = $type_object->getId();
      }
      else {
        return NULL;
      }
    }
    if (!is_int($subject)) {
      $subject_object = ExternalIDSubjectRepository::findBySubject($subject);
      if ($subject_object) {
        $subject = $subject_object->getId();
      }
      else {
        return NULL;
      }
    }
    $result = parent::findByConditions(ExternalID::class,
      [
        'type' => $type,
        'subject' => $subject,
      ]);
    if (count($result) == 1) {
      return array_pop($result);
    }
    elseif (count($result) == 0) {
      return NULL;
    }
    else {
      return $result;
    }
  }

  public static function findBySubjectId($subject, $subject_id) {
    if (!is_int($subject)) {
      $subject_object = ExternalIDSubjectRepository::findBySubject($subject);
      if ($subject_object) {
        $subject = $subject_object->getId();
      }
      else {
        return NULL;
      }
    }
    $result = parent::findByConditions(ExternalID::class,
      [
        'subject' => $subject,
        'subject_id' => $subject_id,
      ]);
    if (count($result) == 1) {
      return array_pop($result);
    }
    elseif (count($result) == 0) {
      return NULL;
    }
    else {
      return $result;
    }
  }

  /**
   * @param int $id The ID of the instance to be deleted.
   *
   * @return bool TRUE on successful deletion.
   */
  public static function delete($id) {
    try {
      db_delete(ExternalID::getTableName())
        ->condition('id', $id)
        ->execute();
      return TRUE;
    } catch (Exception $e) {
      watchdog(__CLASS__, $e->getMessage(), WATCHDOG_ERROR);
      return FALSE;
    }
  }
}