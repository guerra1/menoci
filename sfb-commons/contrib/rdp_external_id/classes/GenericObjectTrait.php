<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 29.07.18
 * Time: 09:51
 */

trait GenericObjectTrait {

  private $id = self::EMPTY_ID;

  private $variables = [];

  public static function getTableName() {
    return self::$table_name;
  }

  public static function getTableFields() {
    return array_merge(['id'], array_keys(self::$attributes));
  }

  public static function getAttributes() {
    return self::$attributes;
  }

  public function getVars() {
    return $this->variables;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @param array $list Array of integers or objects, representing object IDs
   *   or the class instances to be display as a table.
   *
   * @return string
   */
  public static function table($list) {
    if (!count($list)) {
      return '';
    }

    /**
     * ToDo: create a way to define default sort_by parameter per Class
     *   instead of relying on the 'label' attribute. Maybe switch to JavaScript
     *   based front-end rendering of the table with pagination and sorting functionality etc.
     */
    if (array_key_exists('label', $list[0]->variables)) {
      usort($list, function ($a, $b) {
        return strcmp($a->variables['label'], $b->variables['label']);
      });
    }

    $header = self::tableHeader();
    $rows = [];

    $fetch = get_class($list[0]) == self::class ? FALSE : TRUE;

    foreach ($list as $element) {
      $object = $fetch ? self::fetch($element) : $element;
      $rows[] = $object->tableRow();
    }

    try {
      return theme('table', ['header' => $header, 'rows' => $rows]);
    } catch (Exception $exception) {
      // handle Exception
      watchdog_exception(__CLASS__, $exception);
      return '';
    }
  }


  /**
   * @param StdClass $object An object returned by a database query.
   *
   * @return \GenericObjectInterface
   */
  public static function translate_db_result($object) {
    $object_vars = get_object_vars($object);
    $object = new self();
    foreach ($object_vars as $key => $value) {
      if ($key == 'id') {
        $object->setId($value);
      }
      else {
        /**
         * Only if $key is specified as an attribute of the class.
         */
        if (array_key_exists($key, self::getAttributes())) {
          $object->set($key, $value);
        }
      }
    }
    return $object;
  }

  public function getForm($form_type = 'create') {

    $factory = new GenericObjectForm($this);

    return $factory->getForm($form_type);
  }

  protected function set($var, $value) {
    $this->variables[$var] = $value;
  }

  protected function get($var) {
    return array_key_exists($var,
      $this->variables) ? $this->variables[$var] : NULL;
  }

  public function isEmpty() {
    return $this->getId() == self::EMPTY_ID ? TRUE : FALSE;
  }

  public function save() {
    if ($id = GenericRepository::save($this)) {
      $this->setId($id);
    }
  }

  /**
   * @param int $id
   *
   * @return \GenericObjectInterface
   */
  public static function fetch($id) {
    if ($object = GenericRepository::findById($id, self::class)) {
      return $object;
    }
    else {
      return new self();
    }
  }
}
