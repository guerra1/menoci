<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 27.07.18
 * Time: 14:54
 */

abstract class GenericRepository {

  public static function save(GenericObject $object) {
    try {
      $id = $object->isEmpty() ? NULL : $object->getId();

      $update = db_merge($object::getTableName())
        ->key(['id' => $id])
        ->fields($object->getVars())
        ->execute();

      return is_null($id) ? Database::getConnection()->lastInsertId() : $id;
    } catch (Exception $exception) {
      // handle Exception
      watchdog_exception(__CLASS__, $exception);
      return FALSE;
    }
  }

  public static function findById($id, $class) {
    if (class_exists($class) && is_subclass_of($class, 'GenericObject')) {
      /**
       * @var \GenericObject $class
       */
      $table = $class::getTableName();

      try {
        $result = db_select($table, 't')
          ->condition('id', $id)
          ->fields('t', $class::getTableFields())
          ->execute()
          ->fetch();

        if ($result) {
          return $class::translate_db_result($result);
        }
      } catch (Exception $exception) {
        watchdog_exception(__CLASS__, $exception);
      }
    }
    return FALSE;
  }

  /**
   * @param string $class
   *
   * @return array
   */
  public static function findAll($class) {
    if (class_exists($class) && is_subclass_of($class, 'GenericObject')) {

      $table = $class::getTableName();

      try {
        $result = db_select($table, 't')
          ->fields('t', $class::getTableFields())
          ->execute()
          ->fetchAll();

        if (count($result)) {
          return self::translateResults($class, $result);
        }
      } catch (Exception $exception) {
        watchdog_exception(__CLASS__, $exception);
      }
    }
    return [];
  }

  private static function translateResults($class, $db_results) {
    $return_array = [];
    foreach ($db_results as $db_result) {
      $return_array[] = $class::translate_db_result($db_result);
    }

    return $return_array;
  }

  protected static function findByConditions($class, $conditions) {
    if (class_exists($class) && is_subclass_of($class, 'GenericObject')) {

      $table = $class::getTableName();

      try {
        $query = db_select($table, 't')
          ->fields('t', $class::getTableFields());

        $condition = db_and();
        foreach ($conditions as $field => $value) {
          if (db_field_exists($table, $field)) {
            $condition->condition($field, $value);
          }
        }

        $result = $query->condition($condition)
          ->execute()
          ->fetchAll();

        if (count($result)) {
          return self::translateResults($class, $result);
        }
      } catch (Exception $exception) {
        watchdog_exception(__CLASS__, $exception);
      }
    }
    return [];
  }
}