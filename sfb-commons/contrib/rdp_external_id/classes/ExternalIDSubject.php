<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 27.07.18
 * Time: 14:43
 */

class ExternalIDSubject extends GenericObject implements GenericObjectInterface {

  use GenericObjectTrait;

  private static $table_name = 'rdp_external_id_subject';

  private static $attributes = [
    'subject' => [
      'name' => 'External ID Subject',
      'class' => 'string',
      'required' => TRUE,
      'attributes' => [
        '#disabled' => TRUE,
      ],
    ],
    'description' => [
      'name' => 'Description',
      'class' => 'text',
      'required' => TRUE,
    ],
  ];

  public function setSubject($subject) {
    $this->set('subject', $subject);
  }

  public function getSubject() {
    return $this->get('subject');
  }

  public function setDescription($description) {
    $this->set('description', $description);
  }

  public function getDescription() {
    return $this->get('description');
  }

  protected static function tableHeader() {
    return [
      'Subject',
      'Description',
      '',
    ];
  }

  protected function tableRow() {
    return [
      $this->getSubject(),
      $this->getDescription(),
      l('edit', $this->url('edit')),
    ];
  }

  public function url($type = 'view') {
    switch ($type) {
      case 'edit':
        return str_replace('%', $this->getId(), RDP_EXTERNAL_ID_URL_CONFIG_SUBJECT_EDIT);
        break;
      case 'view':
      default:
        return RDP_EXTERNAL_ID_URL_CONFIG_SUBJECT_LIST;
        break;
    }
  }
}
