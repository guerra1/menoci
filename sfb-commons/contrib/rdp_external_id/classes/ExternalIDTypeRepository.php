<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 30.07.18
 * Time: 12:12
 */

abstract class ExternalIDTypeRepository extends GenericRepository {

  public static function findById($id, $class = NULL) {
    $class = ExternalIDType::class;
    return parent::findById($id, $class);
  }

  /**
   * @param null $class
   *
   * @return ExternalIDType[]
   */
  public static function findAll($class = NULL) {
    $class = ExternalIDType::class;
    return parent::findAll($class);
  }

  public static function findByLabel($label) {
    $result = parent::findByConditions(ExternalIDType::class,
      ['label' => $label]);
    if (count($result) == 1) {
      return array_pop($result);
    }
    else {
      return $result;
    }
  }
}