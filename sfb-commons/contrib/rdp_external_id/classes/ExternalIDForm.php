<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 02.08.18
 * Time: 13:10
 */

class ExternalIDForm {

  private $external_id;

  public function __construct(ExternalID $external_id) {
    $this->external_id = $external_id;
  }

  public function getAddIdSelectForm($subject_id, $type_list = []) {
    if (!$this->external_id->getSubject()) {
      // ERROR, subject must be set before ExternalIDForm instance is used
      watchdog(__CLASS__,
        "Error in function %f, ExternalIDSubject must be set before calling the function.",
        ['%f' => __FUNCTION__, WATCHDOG_ERROR]);
      return [];
    }

    $form = [];

    if (count($type_list) == 0) {
      /**
       * Allow all ExternalIDType
       */
      $types = ExternalIDTypeRepository::findAll();
    }
    else {
      // ToDo
    }

    /**
     * Build Type options array
     */
    $options = [];
    foreach ($types as $type) {
      $options[$type->getId()] = $type->getLabel();
    }

    $form['type'] = [
      '#type' => 'select',
      '#options' => $options,
      '#empty_option' => '-- Choose Identifier type --',
      '#required' => TRUE,
      '#prefix' => '<div class="form-group">',
      //      '#suffix' => '</div>',
    ];

    $form['value'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'Insert ID value',
        'size' => 20,
      ],
      //      '#prefix' => '<div class="form-group">',
      //      '#suffix' => '</div>',
    ];

    $form['subject'] = [
      '#type' => '#hidden',
      '#value' => $this->external_id->getSubject(),
      '#access' => FALSE,
    ];

    $form['subject_id'] = [
      '#type' => '#hidden',
      '#value' => $subject_id,
      '#access' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'save',
      //      '#prefix' => '<div class="form-group">',
      //      '#suffix' => '</div>',
    ];

    $form['#prefix'] = '<div class="form-inline form-group">';
    $form['#suffix'] = '</div>';

    return $form;

  }
}