<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 27.07.18
 * Time: 14:40
 */

/**
 * Class ExternalIDType
 */
class ExternalIDType extends GenericObject implements GenericObjectInterface {

  use GenericObjectTrait;

  private static $table_name = 'rdp_external_id_type';

  private static $attributes = [
    'label' => [
      'name' => 'Label',
      'class' => 'string',
      'required' => TRUE,
    ],
    'description' => [
      'name' => 'Description',
      'class' => 'text',
      'required' => TRUE,
    ],
    'validation_regex' => [
      'name' => 'Regular Expression for input validation',
      'class' => 'string',
    ],
    'resolve_url' => [
      'name' => 'Resolver URL',
      'class' => 'string',
      'description' => 'Prefix for input value to make ID a valid URL.',
      'maxlength' => 255,
    ],
    'unique_association' => [
      'name' => 'Unique Association',
      'class' => 'bool',
      'description' => 'Is the ID Type unique or could multiple IDs be associated with a single object?',
    ],
    'display_color_code' => [
      'name' => 'Color Code',
      'class' => 'string',
      'description' => 'HEX value for the color in which the ID will be displayed (without the leading \'#\').',
      'default_value' => '000000',
      'size' => 10,
      'maxlength' => 6,
    ],
  ];

  public function setLabel($label) {
    $this->set('label', $label);
  }

  public function getLabel() {
    return $this->get('label');
  }

  public function setDescription($description) {
    $this->set('description', $description);
  }

  public function getDescription() {
    return $this->get('description');
  }

  public function setValidationRegex($regex) {
    $this->set('validation_regex', $regex);
  }

  public function getValidationRegex() {
    return $this->get('validation_regex');
  }

  public function setResolveUrl($url) {
    $this->set('resolve_url', $url);
  }

  public function getResolveUrl() {
    return $this->get('resolve_url');
  }

  public function setDisplayColorCode($color) {
    $this->set('display_color_code', $color);
  }

  public function getDisplayColorCode() {
    return $this->get('display_color_code');
  }

  /**
   * @return boolean
   */
  public function is_unique() {
    return $this->get('unique_association');
  }

  protected static function tableHeader() {
    return [
      'Label',
      'Description',
      'Resolution URL',
      'Color',
      '',
    ];
  }

  protected function tableRow() {
    $label = l($this->getLabel(), $this->url());
    $edit_button = l('edit', $this->url('edit'));
    $style = "background-color: #" . $this->getDisplayColorCode() . "; 
        display: inline-block; 
        border-radius: 5px;
        color: white;
        padding-left: 4px;
        padding-right: 4px;
        font-weight: bolder;
        font-family: monospace, monospace;";
    $color_badge = '<span style="' . $style . '">' . $this->getDisplayColorCode() . '</span>';
    return [
      $label,
      $this->getDescription(),
      $this->getResolveUrl(),
      $color_badge,
      $edit_button,
    ];
  }

  public function url($type = 'view') {
    switch ($type) {
      case 'edit':
        return str_replace('%', $this->getId(),
          RDP_EXTERNAL_ID_URL_CONFIG_TYPE_EDIT);
        break;
      case 'view':
      default:
        return str_replace('%', $this->getId(),
          RDP_EXTERNAL_ID_URL_CONFIG_TYPE_VIEW);
    }
  }

  public function displayPage($classname = self::class) {
    return parent::displayPage($classname);
  }

  public function validate_regex($value) {
    $regex = $this->getValidationRegex();
    if (empty($regex)) {
      return TRUE;
    }
    else {
      if (preg_match($regex, $value)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
