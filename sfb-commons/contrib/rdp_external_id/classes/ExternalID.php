<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 27.07.18
 * Time: 14:06
 */

class ExternalID extends GenericObject implements GenericObjectInterface {

  use GenericObjectTrait;

  private static $table_name = 'rdp_external_id';

  private static $attributes = [
    'type' => [
      'class' => ExternalIDType::class,
      'name' => 'External ID Type',
      'required' => TRUE,
      'attributes' => [
        '#disabled' => TRUE,
      ],
    ],
    'subject' => [
      'class' => ExternalIDSubject::class,
      'name' => 'External ID Subject',
      'required' => TRUE,
      'attributes' => [
        '#disabled' => TRUE,
      ],
    ],
    'subject_id' => [
      'class' => 'integer',
      'name' => 'ID Subject Instance',
      'required' => TRUE,
      'attributes' => [
        '#disabled' => TRUE,
      ],
    ],
    'value' => [
      'class' => 'string',
      'name' => 'Value',
      'required' => TRUE,
      'attributes' => [
        '#maxlength' => 255,
      ],
    ],
  ];

  /**
   * @var \ExternalIDType $xid_type
   */
  private $xid_type = NULL;

  /**
   * @var \ExternalIDSubject $xid_subject
   */
  private $xid_subject = NULL;

  /**
   * @return int
   */
  public function getType() {
    return $this->get('type');
  }

  /**
   * @param int $type
   */
  public function setType($type) {
    $this->set('type', $type);
  }

  /**
   * @return string
   */
  public function getValue() {
    return $this->get('value');
  }

  /**
   * @param string $value
   */
  public function setValue($value) {
    $this->set('value', $value);
  }

  /**
   * @param \ExternalIDSubject $subject
   */
  public function setSubject($subject) {
    $this->set('subject', $subject);
  }

  /**
   * @return \ExternalIDSubject
   */
  public function getSubject() {
    return $this->get('subject');
  }

  public function getTypeLabel() {
    return $this->getTypeObject()->getLabel();
  }

  private function getTypeObject() {
    if (!$this->xid_type) {
      $this->xid_type = ExternalIDTypeRepository::findById($this->getType());
    }
    return $this->xid_type;
  }

  public function getSubjectLabel() {
    return $this->getSubjectObject()->getSubject();
  }

  public function setSubjectId($subject_id) {
    $this->set('subject_id', $subject_id);
  }

  private function getSubjectObject() {
    if (!$this->xid_subject) {
      $this->xid_subject = ExternalIDSubjectRepository::findById($this->getSubject());
    }
    return $this->xid_subject;
  }

  public function getSubjectId() {
    return $this->get('subject_id');
  }

  protected static function tableHeader() {
    return ['ID Type', 'Subject', 'Value', ''];
  }

  public function tableRow() {
    $type = $this->getTypeLabel();
    $subject = $this->getSubjectLabel();
    $value = $this->getValue();
    $edit = l('<span class="glyphicon glyphicon-edit"></span>',
      $this->url('edit'),
      ['html' => TRUE]);

    return [$type, $subject, $value, $edit];
  }

  public static function fetch($id) {
    return GenericRepository::findById($id, self::class);
  }

  public function url($type = 'view') {
    switch ($type) {
      case 'resolve':
        $resolver = $this->getTypeObject()->getResolveUrl();
        $value = $this->getValue();
        if (empty($resolver)) {
          // Add protocol to ensure correct external linking of generic Ex-IDs
          if(!strchr($value, "http://") && !strchr($value, "https://"))
          $resolver = 'http://';
        }
        return $resolver . $value;
      case 'edit':
        return str_replace('%', $this->getId(), RDP_EXTERNAL_ID_URL_EDIT);
        break;
      case 'view':
      default:
        return RDP_EXTERNAL_ID_URL_CONFIG;
    }
  }
}
