<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 29.07.18
 * Time: 09:46
 */

interface GenericObjectInterface {

  const EMPTY_ID = -1;

  public static function getAttributes();

  public static function getTableName();

  public static function getTableFields();

  public static function table($list);

  public static function translate_db_result($db_result_object);

  public function isEmpty();

  public function save();

  public function getVars();

  public function getId();

  public function setId($id);

  public function getForm($form_type);

  public function getFormField($field);

  public function displayPage();
}
