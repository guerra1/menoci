![menoci logo](menoci_logo.png "menoci")
## Lightweight Modular Research Data Management for Biomedical Projects

https://menoci.io

**This project is still in the process of refactoring.** Source 
code mentions "Research Data Platform" as well as "SFB" which 
are project labels from local instances run at the [University 
Medical Center Göttingen](http://umg.eu). 

### Original Repositories

This distribution repository comprises the following menoci modules:

* [Commons](https://gitlab.gwdg.de/research-data-platform/sfb-commons):
 shared libraries, group and subproject handling
* [Data Archive](https://gitlab.gwdg.de/research-data-platform/rdp-archive):
 store and share data packages (using 
[CDSTAR](https://cdstar.gwdg.de) as a backend storage service; 
https://gitlab.gwdg.de/cdstar/cdstar)
* [Literature](https://gitlab.gwdg.de/research-data-platform/sfb-literature): 
 publication list
* [Wikidata](https://gitlab.gwdg.de/research-data-platform/rdp-wikidata):
 optional extension of the publication list, 
 that cross-references Wikidata pages of registered articles
* [Antibody Catalogue](https://gitlab.gwdg.de/research-data-platform/sfb-antibody): 
 track antibodies used in the project
* [Mouse Line Catalogue](https://gitlab.gwdg.de/research-data-platform/sfb-mouseline):
 track mouse lines and specimen 
 used in the project

## Installation

For a quick exploration, build a Docker image from the shipped [Dockerfile](Dockerfile):
* `docker build -t local/menoci .`
* `docker run -p 80:80 --name menoci local/menoci`
* Open `localhost` in web browser, proceed with Drupal installation 
(choose *SQLite* database since no other database is available for 
this container)

More permanent installation should be done via `docker-compose.yml`:

```yaml
version: '2.0'

services:

  drupal:
    build: . # Build local image
    ports:
      - 80:80
    volumes:
      - sites:/var/www/html/sites

  database:
    image: mariadb
    volumes:
      - mysql:/var/lib/mysql
    environment:
      MYSQL_RANDOM_ROOT_PASSWORD: "yes"
      MYSQL_DATABASE: drupal
      MYSQL_USER: menoci
      MYSQL_PASSWORD: ChangeIt

volumes:
  sites:
  mysql:
```

### Configuration

Please refer to official configuration instructions at https://menoci.io

## Maintainers

This project is currently maintained by 
* [Linus Weber](https://orcid.org/0000-0001-7973-7491),
linus.weber@med.uni-goettingen.de,
University Medical Center Göttingen, [Department of Medical Informatics](https://medizininformatik.umg.eu/)
* [Christian Henke](https://orcid.org/0000-0002-4541-4018),
christian.henke@med.uni-goettingen.de,
University Medical Center Göttingen, [Department of Medical Informatics](https://medizininformatik.umg.eu/)
* [Luca Freckmann](https://orcid.org/0000-0002-8285-2586), 
luca.freckmann@med.uni-goettingen.de, 
University Medical Center Göttingen, [Department of Medical Informatics](https://medizininformatik.umg.eu/)
<!--
This project is currently maintained by 
* Luca Freckmann, luca.freckmann@med.uni-goettingen.de, 
[University Medical Center Göttingen](http://umg.eu), [Department of Medical Informatics](http://mi.umg.eu)
* Markus Suhr, markus.suhr@med.uni-goettingen.de, 
[University Medical Center Göttingen](http://umg.eu), [Department of Medical Informatics](http://mi.umg.eu)
-->
## Contribute 

1. Select one of the [original module repositories](#original-repositories)
2. Fork module repository
3. Make changes
4. Create pull request

If you encounter any problems, please contact the 
current maintainer ([see above](#maintainers))

## License

Copyright (C) 2012-2019 menoci contributors, see [AUTHORS.md](AUTHORS.md)

The menoci project source code is licensed 
under [GNU General Public License 3.0](https://spdx.org/licenses/GPL-3.0-or-later.html), see [license file](LICENSE)

The following third party libraries or content are part of the menoci projekt:
* Drupal, Copyright © All Drupal contributors, the GNU General Public License, version 2 or later, https://www.drupal.org/
* Bootstrap3, Copyright © 2011-2018 Twitter Inc. & The Bootstrap Authors, MIT License, https://getbootstrap.com/
* AdminLTE, Copyright © 2014-2022 ColorlibHQ, MIT License, https://adminlte.io/
* Uppy, Copyright © 2019 Transloadit, MIT License, https://uppy.io/
* PHPExcel, Copyright © 2006 - 2011 PHPExcel contributors, GNU Lesser General Public License, https://github.com/PHPOffice/PHPExcel
* CDStar, Copyright © 2016-2017 Marcel Hellkamp, Apache License 2.0, https://gitlab.gwdg.de/cdstar/cdstar
* Wikidata, Copyright © 2015-2019 Aleksandr Statciuk, MIT License, https://github.com/freearhey/wikidata
* FPDI, Copyright © 2007 - 2019 Setasign, MIT License, https://www.setasign.com/products/fpdi/about/
* TCPDF, Copyright © 2004-2019 – Nicola Asuni, GNU Lesser General Public License, https://tcpdf.org/
* Select2, Copyright © 2012-2017 Kevin Brown, Igor Vaynberg, and Select2 contributors, MIT License, https://select2.org/
* Plotly, Copyright © 2019 2019 Plotly, Inc., MIT License, https://plot.ly/
