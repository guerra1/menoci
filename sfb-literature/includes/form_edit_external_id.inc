<?php
/**
 * @file This file contains forms and handlers for editing External-ID items
 *   linked to a publication item
 */

/**
 * Returns a Drupal form for editing an External-ID linked to a publication.
 */
function sfb_literature_edit_external_id($form, &$form_state, $publication_id, $external_id) {

  $form_state['publication_id'] = $publication_id;
  $form_state['external_id'] = $external_id;

  $pub_ex_id = PublicationExternalIdentifier::fetch($external_id);

  $form = $pub_ex_id->getForm('edit');

  return $form;
}

/**
 * Cancel handler for @see sfb_literature_edit_external_id().
 */
function sfb_literature_edit_external_id_cancel($form, &$form_state) {
  $publication = $form_state['publication_id'];

  $form_state['redirect'] = Publication::url_by_id($publication);
}

/**
 * Validation for @see sfb_literature_edit_id().
 */
function sfb_literature_edit_external_id_validate($form, &$form_state) {
  /**
   * ToDo: resolve replication of function @see sfb_literature_edit_id_validate
   */
  $exid_type_id = $form_state['values']['exid_type'];

  $exid_type = ExternalIDTypeRepository::findById($exid_type_id);
  /** @var \ExternalIDType $exid_type */

  /**
   * Skip validation if no ExternalIDType is selected
   */
  if ($exid_type) {
    $value = $form_state['values']['exid_value'];

    if (!$exid_type->validate_regex($value)) {
      form_set_error('exid_value', 'Value does not match expected pattern of the identifier!');
    }

    if ($exid_type->is_unique()) {
      $existing_id = ExternalIDRepository::findByTypeSubjectId($exid_type->getLabel(),
        PublicationExternalIdentifier::EX_ID_SUBJECT, $form_state['publication_id']);
      if ($existing_id) {
        $external_id = $form_state['external_id'];
        // Do not set error when the existing item is edited
        if($existing_id->getId() !== $external_id){
          form_set_error('exid_type', 'Only one ID of this type may be added, this would be the second.');
        }
      }
    }
  }
}

/**
 * Submit handler for @see sfb_literature_edit_external_id().
 */
function sfb_literature_edit_external_id_submit($form, &$form_state) {

  $publication_id = $form_state['publication_id'];
  $external_id = $form_state['external_id'];

  $values = $form_state['values'];

  $ex_id = ExternalID::fetch($external_id);
  $ex_id->setValue($values['exid_value']);
  $ex_id->save();

  $pub_ex_id = PublicationExternalIdentifier::fetch($external_id);
  $pub_ex_id->setDescription($values['description']);
  $pub_ex_id->save();

  $form_state['redirect'] = Publication::url_by_id($publication_id);
}