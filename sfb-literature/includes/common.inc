<?php


module_load_include('inc', 'sfb_commons', 'utils');

define('SFB_LITERATURE_COMMONS', TRUE);

define('MAX_GEN_NAME_ITER', 42); // Randomly chosen small maximum number of iterations for random set-name generation

/**
 * Genrates a pseudo-random label folling the scheme
 * 'prefix_<label>{0,8}_random{8}' where
 * `prefix` is lowercase $prefix, `label` is lowercase $label truncated to 8
 * characters max, and `random` is a 8 character random hex string.
 *
 * @param string $prefix The prefix to prepend.
 * @param string $label The label.
 *
 * @return string The pseudo-unique name
 */
function createRandomizedIdLabel($prefix, $label) {
  // Generate pseudo-unique name 'prefix_<label>{0,8}_random{8}' where random is a 8 character random hex string.
  return sprintf('%s_%s_%s', strtolower($prefix), substr(strtolower($label), 0, 8), substr(uniqid(), 0, 8));
}

/**
 * Creates a fieldset containing a text for a given drupal form object.
 *
 * @param array $form The form to create the fieldset in.
 * @param string $title The text that is displayed as title of the fieldset.
 * @param string $text The text that is displayed in the fieldset itself.
 * @param string $setName The name of the fieldset, or NULL of it should be
 *   generated automatically. (Default NULL)
 *
 * @return string The name of the fieldset that has been created.
 */
function createTextSet(array &$form, $title, $text, $setName = NULL) {
  // Generate new name if needed
  if ($setName == NULL) {
    $i = 0;

    do {
      $setName = createRandomizedIdLabel('set', $title);
      // Fail if max. number is reached
      if ($i++ > MAX_GEN_NAME_ITER) {
        return FALSE;
      }
    } while (array_key_exists($setName, $form));
  }

  // Return false if set-name already exists
  if (array_key_exists($setName, $form)) {
    return FALSE;
  }

  $form[$setName] = [
    '#type' => 'fieldset',
    '#title' => $title,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form[$setName]['text'] = [
    '#type' => 'item',
    '#title' => $text,
  ];

  return $setName;
}

/**
 * Parse the name of a workinggroup.
 *
 * @param $s The string to match as a workinggrup.
 *
 * @return The name of the workinggroup, or NULL if invalid.
 */
function matchWorkingGroup($s) {
  // Match role name for working group shortname
  if (!preg_match('/^(?:registrar-publications|pi)-(ag_[^\\-_]+)$/i', $s, $match)) {
    return NULL;
  }

  //drupal_set_message(print_r($match, TRUE));
  return $match[1];
}

/**
 * Fetch the publication specified by $pubid and check if assigned workinggroup
 * is in $wg if $checkPrivileges evaluates to TRUE. drupal_access_denied() is
 * invoked on common errors.
 *
 * @param int $pubid The ID of the publication to fetch.
 * @param array $wgs An array of workinggroups.
 * @param boolean $checkPrivileges If set to TRUE, it is checked whether the
 *   publications workinggroup is in $wgs or not.
 *
 * @return \Publication | bool Returns the publication-object, or FALSE on
 *   common errors.
 */
function fetchPubAndCheck($pubid, $wgs, $checkPrivileges = TRUE) {
  try {
    $result = db_select('pubreg_articles', 'a')
      ->fields('a')
      ->condition('id', $pubid, '=')
      ->range(0, 1)
      ->execute();

    $cnt = $result->rowCount();

    if ($cnt != 1) {
      drupal_access_denied();
      return FALSE;
    }

    $rows = $result->fetchAll();
    $r = $rows[0];

    if ($checkPrivileges) {
      $wgs_db = explode (', ',$r->wg);
      $has_privileges = FALSE;
      foreach ($wgs_db as $wg) {
        if (in_array($wg, $wgs)) {
          $has_privileges = TRUE;
          break;
        }
      }
      if (!$has_privileges and !user_access('administer site configuration')) {
        drupal_access_denied();
        return FALSE;
      }
    }

    return $r;
  } catch (Exception $e) {
    drupal_set_message('A database error has occurred.', 'error');
    drupal_access_denied();
    return FALSE;
  }
}

/**
 * @param $wg
 * @param $wgs
 * @param $id
 * @param bool $showLocked
 * @param bool $hideOnNoCreatePerm
 * @param string $class
 *
 * @return string
 *
 * @deprecated Use method html_edit_link() of \Publication class object instead
 *
 */
function formatEditLink($wg, $wgs, $id, $showLocked = FALSE, $hideOnNoCreatePerm = TRUE, $class = '') {
  $publication = PublicationRepository::findById($id);
  return $publication->html_edit_link($class);
}

function AuthorListStr($authorsStr) {
  return rtrim(trim($authorsStr), ' ,;');
}


function LinkExtern($url, $title = NULL) {
  if ($title === NULL) {
    $title = $url;
  }

  $img = '<span class="glyphicon glyphicon-link"></span>&nbsp;';
  return sprintf('<a href="%s" target="_blank">%s%s</a>', StringEscapeHtml($url), $img, StringEscapeHtml($title));
}

/**
 * Gets the entry of the array that is associated to the key, or the default
 * value if there is no such entry in the array.
 *
 * @param array $arr The array to take values from.
 * @param string $key The key of the entry.
 * @param mixed $default The default value.
 *
 * @return mixed The array's entry associated with the key or the default value
 *   if there is no such entry in the array.
 */
function arrayValueOrDefault($arr, $key, $default) {
  if (array_key_exists($key, $arr)) {
    return $arr[$key];
  }

  return $default;
}

/**
 * @param array $fields
 * @param array $names
 * @param array $values
 * @param bool $tolerateNonexistantValues
 *
 * @return bool|int|string
 */
function formCheckFieldValueTypes($fields, $names, $values, $tolerateNonexistantValues = FALSE) {
  foreach ($fields as $key => $type) {
    // Handle missing fields
    if (!array_key_exists($key, $values)) {
      if ($tolerateNonexistantValues === TRUE) {
        continue;
      }

      return $key;
    }

    // Match value
    $value = $values[$key];

    // Handle types
    switch ($type) {
      case Publication::FORM_FIELD_TYPE_INTEGER:
        _formCheckFieldValueTypes_handler_integer($fields, $names, $values, $key, $value);
        break;
      case Publication::FORM_FIELD_TYPE_INTEGER:
        _formCheckFieldValueTypes_handler_text($fields, $names, $values, $key, $value);
        break;
      default:
        break;
    }
  }

  return TRUE;
}

function _formCheckFieldValueTypes_handler_integer($fields, $names, $values, $k, $v) {
  if (!preg_match('/^\s*\d*\s*$/i', $v)) {
    form_set_error($k, t('The value of the @fieldname field is invalid. It must be a number!',
      ['@fieldname' => arrayValueOrDefault($names, $k, $k)]));
  }
}

function _formCheckFieldValueTypes_handler_text($fields, $names, $values, $k, $v) {
  $lrs = article_get_length_restrictions();

  if (!array_key_exists($k, $lrs) || !is_int($lrs[$k])) {
    return;
  }

  $len = strlen($v);

  if ($len > $lrs[$k] - 1) {
    form_set_error($k,
      t('The value of the @fieldname field is invalid. It is too long, the maximum length is @maxlen characters but the value is @strlen characters long!',
        [
          '@fieldname' => arrayValueOrDefault($names, $k, $k),
          '@maxlen' => $lrs[$k] - 1,
          '@strlen' => $len,
        ]));
  }
}