<?php

module_load_include('inc', 'sfb_literature', 'includes/form_main');

function sfb_literature_search() {

  // save the search query so it can be put back into the search form
  $search_query_save = $_GET['search_query'];
  // the query string which is supposed to be changed
  $search_query = $search_query_save;
  $search_params = [];
  // look for quotation marks as they mark the beginning and end of a connected phrase
  if (preg_match_all('/"(.*?)"/i',$search_query, $matches)){
    // remove all matches from the search query which gets exploded later
    foreach ($matches[0] as $match) {
      $search_query = str_replace($match,'',$search_query);
    }
    // add all matches without the quotation marks into an array for later merging
    foreach ($matches[1] as $match) {
      $search_params [] = $match;
    }
  }

  // look for any duplicate whitespaces
  $search_query = preg_replace('/\s\s+/', ' ',$search_query);

  // look if the start or ending of the string is a whitespace
  if (substr($search_query,0,1) == ' ') {
    $search_query = substr($search_query,1);
  }
  if (substr($search_query,-1) == ' ') {
    $search_query = substr($search_query,0,-1);

  }

  // if the remaining search query is not empty, explode it
  $expl_query = NULL;
  if ($search_query !== '' and $search_query !== ' ') {
    $expl_query =  explode(' ', $search_query);
  }

  // if the remaining search query got exploded, merge the two arrays for searching in the db
  if ($expl_query !== NULL) {
    $search_params = array_merge($search_params,$expl_query);
  }

  if (empty($search_params)) {
    $search_params = [''];
  }

  // Get ordering of rows
  $defaultOrdering = [
    'column' => 'creation_time',
    'order' => 'DESC',
    'letter' => 'c',
  ]; // Default: Newest entry is on top
  $orderings = [
    't' => 'title',
    'a' => 'first_author',
    'j' => 'journal_title',
    'y' => 'publication_year',
    'c' => 'creation_time',
    'g' => 'wg',
  ];
  $ordering = getOrdering($orderings, $defaultOrdering);

  $structured_params = [
    'string' => [],
    'fields' => [],
  ];

  // use db_and as default condition
  $db_main_condition = db_and();
  // free text search conditions are queried by using 'OR' db condition
  $db_freetext_condition = db_or();

  for ($i = 0; $i < count($search_params); $i++) {
    $param = $search_params[$i];

    //
    // look for parameters
    //

    // brackets regex: (\(([^()]|(?R))*\))


    if (preg_match('/^(?:\[pmid\])=([^\\-_]+)$/i', $param, $match)) {
      $db_main_condition->condition('pmid', $match[1], '=');
      $structured_params['fields'][] = ['pmid' => $match[1]];

    }
    else {
      if (preg_match('/^(?:\[doi\])=([^\\-_]+)$/i', $param, $match)) {
        $db_main_condition->condition('doi', $match[1], '=');
        $structured_params['fields'][] = ['doi' => $match[1]];

      }
      else {
        if (preg_match('/^(?:\[publication_year\])=(.+)$/i', $param, $match)) {
          $db_main_condition->condition('publication_year', $match[1], '=');
          $structured_params['fields'][] = ['publication_year' => $match[1]];

        }
        else {
          if (preg_match('/^(?:\[title\])=([^\\-_]+)$/i', $param, $match)) {
            $db_main_condition->condition('title', $match[1], '=');
            $structured_params['fields'][] = ['title' => $match[1]];

          }
          else {
            if (preg_match('/^(?:\[journal_title\])=([^\\-_]+)$/i', $param, $match)) {
              $db_main_condition->condition('journal_title', $match[1], '=');
              $structured_params['fields'][] = ['journal_title' => $match[1]];

            }
            else {
              if (preg_match('/^(?:\[issn\])=([^\\-_]+)$/i', $param, $match)) {
                $db_main_condition->condition('issn', $match[1], '=');
                $structured_params['fields'][] = ['issn' => $match[1]];

              }
              else {
                if (preg_match('/^(?:\[essn\])=([^\\-_]+)$/i', $param, $match)) {
                  $db_main_condition->condition('essn', $match[1], '=');
                  $structured_params['fields'][] = ['essn' => $match[1]];

                }
                else {
                  if (preg_match('/^(?:\[authors\])=([^\\-_]+)$/i', $param, $match)) {
                    $db_main_condition->condition('author', $match[1], '=');
                    $structured_params['fields'][] = ['author' => $match[1]];

                  }
                  else {
                    if (preg_match('/^(?:\[research_group\])=([^\\-_]+)$/i', $param, $match)) {
                      $structured_params['fields'][] = ['research_group' => $match[1]];
                      //TODO: not yet implemented
                      $db_main_condition->condition('wg', $match[1], '=');
                      $structured_params['fields'][] = ['research_group' => $match[1]];

                    }
                    elseif (preg_match('/^(?:\[extra\])=([^\\-_]+)$/i', $param, $match)) {
                      $db_main_condition->condition('extra', $match[1], '=');
                      $structured_params['fields'][] = ['extra' => $match[1]];

                    }
                    elseif (preg_match('/^(?:\[subproject\])=([^\\-_]+)$/i', $param, $match)) {
                      $db_main_condition->condition('subproject', $match[1], '=');
                      $structured_params['fields'][] = ['subproject' => $match[1]];

                    }
                    #else if(preg_match('/^(?:\[publication_type\])=([^\\-_]+)$/i', $param, $match)) {
                    #   $db_main_condition->condition('publication_type',array_search($match[1], article_get_publication_types()),'=');
                    #  $structured_params['fields'][] = array ('publication_type' => $match[1]);
                    #}
                    else {

                      // free text search over following fields:
                      // - pmid
                      // - doi
                      // - publication year
                      // - issn
                      // - essn
                      // - author
                      $db_freetext_condition->condition('pmid', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('doi', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('publication_year', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('title', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('journal_title', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('issn', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('essn', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('authors', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('wg', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('extra', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('subproject', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('value', '%' . db_like($param) . '%', 'LIKE');
                      $db_freetext_condition->condition('description', '%' . db_like($param) . '%', 'LIKE');
                      $structured_params['string'][] = $param;

                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  $output = '';


  $btn_options = ['html' => TRUE, 'attributes' => ['class' => ['btn btn-default btn-sm']]];
  $btn_export_ris = l('<span class="glyphicon glyphicon-download-alt"></span> RIS-Export (Search)',
    SFB_LITERATURE_PAGE_EXPORT_MISC . 'ris', $btn_options);
  $btn_export_json = l('<span class="glyphicon glyphicon-download-alt"></span> JSON-Export (Search)',
    SFB_LITERATURE_PAGE_EXPORT_JSON, $btn_options);
  $btn_export_excel = l('<span class="glyphicon glyphicon-download-alt"></span> Excel-Export (Search)',
    SFB_LITERATURE_PAGE_EXPORT, $btn_options);

  $navbar_actions = '
    <nav class="navbar">
      <div class="container-fluid">
       <div class="navbar-header navbar-default">
        </div>
        <div class="collapse navbar-collapse" id="actionNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li>' . $btn_export_ris . '</li>
            <li>' . $btn_export_json . '</li>
            <li>' . $btn_export_excel . '</li>
          </ul>
        </div>
      </div>
    </nav>
';

  $output .= $navbar_actions;

  // print search box
  $search_form = drupal_get_form('sfb_literature_form_search', $search_query_save);
  $output .= drupal_render($search_form);

  // print explanation
  $output .= 'Searched for:';

  //print free text
  foreach ($structured_params['string'] as $search_str) {
    $output .= '<code>' . check_plain($search_str) . '</code>, ';
  }

  //print field parameters/*
  foreach ($structured_params['fields'] as $field) {
    foreach ($field as $key => $value) {
      $output .= ' <code>' . print_r($key, TRUE) . '=' . print_r($value, TRUE) . '</code> ';
    }
  }

  if (count($structured_params['string']) > 0) {
    $db_main_condition->condition($db_freetext_condition);
  }

  $articles = PublicationRepository::findBy($db_main_condition, $ordering);

  $output .= 'and found <strong>' . count($articles) . '</strong> matches. ';

  module_load_include('inc', 'sfb_literature', 'includes/session');
  if (empty($articles)) {
    lists_session("publications_filter", -2);
  }
  else {
    lists_session("publications_filter", $articles);
  }

  // create table rows
  $rows = [];
  $user = User::getCurrent();
  $user_wgs = $user->getUserWorkingGroups(TRUE);

  // each table row is a article
  foreach ($articles as $article) {

    // Construct table-rows
    // Make title link
    $link = url(sprintf('literature/publications/%s', StringEscapeHtml($article->id)));
    $linkpubtitle = sprintf('<a href="%s" style="color: #777; font-style: italic; letter-spacing: 0.45pt;">%s</a>', $link, t('Unnamed publication'));

    if (!IsNullOrWhitespace($article->title)) {
      $linkpubtitle = sprintf('<a href="%s">%s</a>', $link, StringEscapeHtml($article->title));
    }

    // get working group icon
    //TODO: wenn die Funktion wg_get_names an die Funktion aus Antibody-Module angepasst wird, dann muss auch folgender Code angepasst werden...
    $working_group_name = 'ag_unknown';
    if (!empty($article->wg)) {
      $working_group_name = $article->wg;
      $working_group_names = wg_get_names();
      if (isset($working_group_names[$working_group_name])) {
        $working_group_name = $working_group_names[$working_group_name];
      }
    }
    else {
      $article->wg = 'ag_unknown';
    }

    $group = WorkingGroupRepository::findByShortName($article->wg);

    $public_icon = '<img src="' . $group->getIconPath() .'" alt="" data-toggle="tooltip" title="' . $working_group_name . '" />';

    //add open access symbol to title
    $open_access_symbol = '';
    if ($article->open_access == 1) {
      $open_access_symbol = '
        <img src="' . base_path() . drupal_get_path('module', 'sfb_literature') . '/resources/open_access_16x25.png" alt="" data-toggle="tooltip" title="Open Access" style="float:right; margin: 5px;" />
        </a>
        ';
    }

    // Row content
    $rows[] = [
      $linkpubtitle . $open_access_symbol,
      _sfb_literature_search_mark_found_string($structured_params, StringEscapeHtml(AuthorListStr($article->authors))),
      _sfb_literature_search_mark_found_string($structured_params, StringEscapeHtml($article->journal_title)),
      _sfb_literature_search_mark_found_string($structured_params, StringEscapeHtml($article->publication_year)),
      GetDoiPmidHtml($article->doi, $article->pmid),
      formatEditLink($article->wg, $user_wgs, $article->id),
    ];

  }
  // Format Table
  // 'Title', 'Authors', 'Journal', 'Publication year', 'DOI', ''
  $header = [];
  $header[] = makeTitleOrderLink('Title', 't', VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING, $ordering);
  $header[] = makeTitleOrderLink('Authors', 'a', VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING, $ordering);
  $header[] = makeTitleOrderLink('Journal', 'j', VIEW_TABLE_COLUMN_ORDERING_DIRECTION_ASCENDING, $ordering);
  $header[] = makeTitleOrderLink('Publication year', 'y', VIEW_TABLE_COLUMN_ORDERING_DIRECTION_DESCENDING, $ordering);
  $header[] = '<div style="line-height: 1.1;display: inline-block; vertical-align: bottom;">DOI<br><div style="border-top: 1px dotted #ddd; margin-top: 3px; padding-top: 2px;">PMID</div></div>';
  $header[] = '';

  //print help
  #$output .= sfb_literature_search_help();
  // print main table and pager
  $output .= theme('table', ['header' => $header, 'rows' => $rows]);
  $output .= theme('pager');


  return $output;
}

function sfb_literature_search_help() {
  $output = '';

  $output .= 'In following you can find information to help you easily find articles in Published Data Catalogue';
  $output .= '<h2>Search parameters</h2>';

  $output .= '<h3>Free text search</h3>';


  return $output;
}

function _sfb_literature_search_mark_found_string($structured_params, $string) {

  //check free text
  foreach ($structured_params['string'] as $query) {
    $string = str_replace($query, '<span style="color: #c7254e; background-color: #f9f2f4">' . $query . '</span>', $string);
  }

  //TODO: farben für fields search

  return $string;
}
