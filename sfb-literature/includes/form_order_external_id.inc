<?php
/**
 * @file This file contains forms and handlers for ordering External-ID items
 *   from a publication item
 */

/**
 * Handles order callback for External Identifier linked to a publication.
 */
function sfb_literature_order_external_id($publication_id, $external_id, $up_down) {

  $pub_ex_id = PublicationExternalIdentifier::fetch($external_id);
  $pub_ex_id_swap = NULL;
  $pub_ex_id_order_position = $pub_ex_id->getOrderPosition();
  if ($pub_ex_id_order_position == NULL or $pub_ex_id_order_position == '') {
    $pub_ex_id_order_position = 1;
  }
  $ex_ids = ExternalIDRepository::findBySubjectId(1,$publication_id);
  if (!is_array ($ex_ids)) {
    $ex_ids = [$ex_ids];
  }
  $count_ex_id = count($ex_ids);
  if ($up_down == 'down' and $pub_ex_id_order_position < $count_ex_id) {
    $pub_ex_id->setOrderPosition($pub_ex_id_order_position+1);

    $pub_ex_id_swap = PublicationExternalIdentifier::fetchByOrderPosition($pub_ex_id_order_position+1,$publication_id);
    $pub_ex_id_swap->setOrderPosition($pub_ex_id_order_position);
    $pub_ex_id->save();
    $pub_ex_id_swap->save();

  }
  elseif ($up_down == 'up' and $pub_ex_id_order_position > 1) {
    echo ($pub_ex_id_order_position);
    $pub_ex_id->setOrderPosition($pub_ex_id_order_position-1);

    $pub_ex_id_swap = PublicationExternalIdentifier::fetchByOrderPosition($pub_ex_id_order_position-1,$publication_id);
    $pub_ex_id_swap->setOrderPosition($pub_ex_id_order_position);
    $pub_ex_id->save();
    $pub_ex_id_swap->save();
  }
  drupal_goto(Publication::url_by_id($publication_id), ['fragment' =>'ext_ids'] );
}