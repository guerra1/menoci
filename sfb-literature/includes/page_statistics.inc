<?php
/**
 * @file Build HTML markup for a literature module statistics page.
 */

/**
 * Build and return HTML markup for a literature module statistics
 */
function sfb_literature_statistics() {

  $html = sfb_literature_rdp_statistics();

  return $html;
}
