<?php
/**
 * @file This file contains forms and handlers for deleting External-ID items
 *   from a publication item
 */

/**
 * Handles delete callback for External Identifier linked to a publication.
 */
function sfb_literature_delete_external_id($publication_id, $external_id) {

  $pub_ex_id = PublicationExternalIdentifier::fetch($external_id);

  if ($pub_ex_id->delete()) {
    drupal_set_message("External Identifier deleted.");
  }
  else {
    drupal_set_message("External Identifier could not be deleted. 
      Please inform your system's administrator.", "error");
  }

  $pub_ex_id_position = $pub_ex_id->getOrderPosition();
  $ex_ids = ExternalIDRepository::findBySubjectId(1,$publication_id);
  if (!is_array ($ex_ids)) {
    $ex_ids = [$ex_ids];
  }
  $count_ex_id = count($ex_ids);
  for($i = $pub_ex_id_position+1; $i<=$count_ex_id+1;$i++){
    $pub_ex_id_after = PublicationExternalIdentifier::fetchByOrderPosition($i,$publication_id);
    $pub_ex_id_after->setOrderPosition($pub_ex_id_after->getOrderPosition()-1);
    $pub_ex_id_after->save();
  }

  drupal_goto(Publication::url_by_id($publication_id));
}