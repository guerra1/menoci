<?php
/**
 * @file This file contains forms and handlers for viewing specific publication
 *   entries in detail.
 */

/**
 * Implements publication view page (P2.Publication view)
 */
function sfb_literature_view($form, &$form_state, $publication_id) {

  $publication_object = PublicationRepository::findById($publication_id);

  /**
   * Check for the "accept" HTTP header and retrieve it
   */
  $headers = apache_request_headers();
  if (array_key_exists('accept', $headers)) {
    $accept = $headers['accept'];
  }
  elseif (array_key_exists('Accept', $headers)) {
    $accept = $headers['Accept'];
  }
  else {
    $accept = "*/*";
  }

  if ($accept == "application/json") {
    drupal_add_http_header('Content-Type', 'application/json');
    echo $publication_object->json();
    exit;
  }

  //check if publication exists, otherwise redirect to front page
  if (empty($publication_object)) {
    form_set_error('empty-response',
      'Publication is unavailable due to a wrong ID.');
    drupal_goto('literature');
    #$form_state['redirect'] = 'literature'; // why no work?
  }
  else {
    $form = $publication_object->display_page();
    return $form;
  }
}