<?php
/**
 * @file This file contains forms and handlers related to creation of
 *   publication entries.
 */

module_load_include('inc', 'sfb_commons', 'utils');
module_load_include('inc', 'sfb_literature', 'includes/common');
module_load_include('php', 'sfb_literature', 'classes/pubmedloader');
module_load_include('inc', 'sfb_literature', 'includes/article_def');
module_load_include('php', 'sfb_literature', 'classes/DoiLoader');

define('SESSION_VARS', 'sfb-literature-session-vars');
#define('SESSION_DOI_VARS', 'sfb-literature-session-doi-vars');

/**
 * Implements create new page (P3.Create new publication)
 *
 * @see sfb_literature_create_validate()
 * @see sfb_literature_create_submit()
 *
 */
function sfb_literature_create($form, &$form_state) {
  if (!defined('SFB_LITERATURE_COMMONS')) {
    drupal_set_message('Failed to load common.inc!', 'error');
    drupal_set_message(DRUPAL_ROOT . '/' . drupal_get_path('module', 'sfb-literature') . "/includes/common.inc", 'error');

    return;
  }

  createTextSet($form, 'Description',
    t('In this form you can enter properties of a new publication. After 
    the form is submitted a new item will be inserted in the literature database. 
    You can either import the values from PubMed using a PubMed-ID, or manually 
    fill in the data.'));

  $setimport = createTextSet($form, 'PubMed and CrossRef data-import',
    t('To import publication data from PubMed based on PubMed-ID just enter 
    the ID below or import from CrossRef based on DOI. The import data will replace 
    all entries made in the Fields-section below.'), 'pmimport');

  $form[$setimport]['import'] = [
    '#type' => 'textfield',
    '#title' => t('PubMed-ID or CrossRef DOI'),
  ];

  $form[$setimport]['import-submit'] = [
    '#type' => 'submit',
    '#value' => t('Import publication data'),
    '#submit' => ['sfb_literature_create_submit_import'],
    '#validate' => ['sfb_literature_create_validate_import'],
  ];

  $fields = Publication::get_form_fields();
  $names = Publication::get_field_names();

  $set = createTextSet($form, 'Fields',
    t('Enter the properties in fields below.'), 'pubdata');

  /**
   * Construct form fields
   */
  foreach ($fields as $key => $type) {

    // Add description text for "authors" field
    if ($key == 'authors') {
      $form[$set]['text-authors-format'] = [
        '#type' => 'item',
        '#markup' => '<hr><label><span style="font-weight: bold;">Names</span>
            &#x2014; The names are separated using commas. The surname is given 
            first, the firstname(s) are abbreviated by their initial letter and 
            placed after a space trailing the surname.<br>
            <span style="font-weight: bold;">Example:</span> 
            The authors <span style="font-style: italic;">
            Erika Mustermann-Schulz</span> and 
            <span style="font-style: italic;">John Richard Doe</span> 
            must be given as follows: <span style="font-style: italic;">M
            ustermann-Schulz E, Doe JR</span>.</label>',
      ];
    }

    // Handle non-textfield formfields
    if ($key == 'wg') {
      $form[$set][$key] = get_form_field_working_group();
    }
    elseif ($key == 'publication_type') {
      $form[$set][$key] = get_form_field_publication_type();
    }
    elseif ($key == 'open_access') {
      $form[$set][$key] = get_form_field_open_access();
    }
    elseif ($key == 'subproject') {
      $form[$set][$key] = get_form_field_subproject();

    }
    else {
      // By default, create textfield
      $form[$set][$key] = [
        '#type' => 'textfield',
        '#title' => t('@title', ['@title' => arrayValueOrDefault($names, $key, $key)]),
        '#maxlength' => NULL,
      ];
    }
  }



  /**
   * Pre-fill values from DOI/PubMed query
   */
  if (isset($_SESSION[SESSION_VARS]) && $_SESSION[SESSION_VARS]) {

    foreach ($fields as $key => $type) {
      if (array_key_exists($key, $_SESSION[SESSION_VARS])) {
        $form[$set][$key]['#default_value'] = $_SESSION[SESSION_VARS][$key];
      }
    }
  }

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => ['sfb_literature_create_submit_create'],
    '#validate' => ['sfb_literature_create_validate_create'],
  ];

  $form['cancel'] = [
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => ['sfb_literature_create_cancel'],
  ];

  // Invalidate pubmed and DOI import vars
  $_SESSION[SESSION_VARS] = NULL;

  return $form;
}

/**
 * Cancel handler for @see sfb_literature_create().
 */
function sfb_literature_create_cancel($form, &$form_state) {
  // Invalidate pubmed import vars
  $_SESSION[SESSION_VARS] = NULL;

  $form_state['redirect'] = 'literature';
}

/**
 * Create-Action validation function for @see sfb_literature_create().
 */
function sfb_literature_create_validate_create($form, &$form_state) {

  $pub_type = $form_state['values']['publication_type'];
  //check if publication type is empty, when no type is set
  if (empty($pub_type)) {
    form_set_error('publication_type', 'Please select a publication type for the publication.');
  }

  $wg = $form_state['values']['wg'];
  //check if working group field is empty by cylcing through each wg option
  $set_weg = false;
  foreach ($wg as $w) {
    if ($w !== 0){
      $set_weg = true;
    }
  }

  if (!$set_weg and !user_access('administer site configuration')) {
    form_set_error('wg', 'Please select at least one working group for the publication.');
  }

  /**
   * Test if DOI is already assigned to a registered publication.
   */
  if (array_key_exists('doi', $form_state['values'])) {
    $doi = $form_state['values']['doi'];
    if (!PublicationRepository::checkUniqueConstraint('doi', $doi)) {
      form_set_error('doi', t("A publication with the DOI %d is already registered.", ['%d' => $doi]));
    }
  }

  /**
   * Test if PMID is already assigned to a registered publication.
   */
  if (array_key_exists('pmid', $form_state['values'])) {
    $pmid = $form_state['values']['pmid'];
    if (!PublicationRepository::checkUniqueConstraint('pmid', $pmid)) {
      form_set_error('pmid', t("A publication with the PubMed ID %p is already registered.", ['%p' => $pmid]));
    }
  }

  $blank = TRUE;

  $fields = Publication::get_form_fields();
  $names = Publication::get_field_names();

  foreach ($fields as $k => $_) {
    $v = $form_state['values'][$k];

    if (is_array($v) or !IsNullOrWhitespace($v)) {
      $blank = FALSE;
      break;
    }
  }

  $missing = formCheckFieldValueTypes($fields, $names, $form_state['values']);

  if ($missing !== TRUE) {
    drupal_set_message(sprintf('The field %s is missing!', $missing), 'error');
  }

  if ($blank) {
    form_set_error('title', t('Enter the title of the publication.'));
  }
}

/**
 * Create-Action submit function for @see sfb_literature_create().
 */
function sfb_literature_create_submit_create($form, &$form_state) {
  // Invalidate pubmed import vars
  $_SESSION[SESSION_VARS] = NULL;

  $fields = Publication::get_form_fields();

  $data = [];

  foreach ($fields as $key => $_) {
    $value = '';
    if ($key == 'wg') {
      foreach ($form_state['values']['wg'] as $wg){

        if ($wg !== 0){
          $value .= $wg.', ';
        }
      }
      $value = substr($value,0,-2);

    }
    else if ($key == 'subproject') {
      foreach ($form_state['values']['subproject'] as $sp){

        if ($sp !== 0){
          $value .= $sp.', ';
        }
      }
      $value = substr($value,0,-2);

    }
    else {
      $value = $form_state['values'][$key];
    }

    if (!IsNullOrWhitespace($value)) {
      $data[$key] = trim($value);
    }
  }

  if (count($data) > 0) {
    $id = PublicationRepository::save($data, NULL);
    $form_state['redirect'] = sprintf('literature/publications/%s/', StringEscapeHtml($id));
  }
}

/**
 * Parse the PMID. A Pubmed link is parsed for a PMID if the value is no
 * numeric string.
 */
function parseImport(&$form_state) {
  $import = trim($form_state['values']['import']);

  try {
    if (!(preg_match('/^[0-9]+$/', $import)) && preg_match('/https?:\/\/(?:www\.ncbi\.nlm\.nih|pubmed)\.gov(?::\d+)?\/pubmed\/(\d+)\/?/i', $import, $m)) {
      $import = trim($m[1]);
    }
    else {
      if (!$import && preg_match('/\b(10[.][0-9]{4}(?:[.][0-9]+)*\S+)\b/', $import, $m)) {
        $import = trim($m[1]);
      }
    }
  } catch (Exception $e) {
    // Supress
  }

  return $import;
}

function sfb_literature_create_validate_import($form, &$form_state) {
  // Invalidate pubmed and doi import vars
  $_SESSION[SESSION_VARS] = NULL;

  $import_id = parseImport($form_state);

  if (empty($import_id)) {
    form_set_error('import', t('No valid import given.'));
  }
  elseif (!(preg_match('/^[0-9]+$/', $import_id)) && !(preg_match('/\b(10[.][0-9]{4}(?:[.][0-9]+)*\S+)\b/', $import_id, $m))) { #old regex /10.[0-9]{4}\/.+/
    form_set_error('import', t('Import input is not a PubMed-Id nor a DOI from CrossRef.' . $m[1]));
  }

}

function sfb_literature_create_submit_import($form, &$form_state) {
  // Invalidate pubmed import vars
  $_SESSION[SESSION_VARS] = NULL;


  $import_id = parseImport($form_state);
  if (preg_match('/^[0-9]+$/', $import_id)) {
    $loader = new PubmedLoader();
    $fields = $loader->loadPub($import_id);
  }
  else {
    $loader = new DoiLoader();
    $fields = $loader->loadPub($import_id);
  }
  #drupal_set_message(print_r($fields));
  if ($fields == NULL) {
    form_set_error('empty-response', 'Input cannot be resolved.');
  }

  if ($fields !== NULL) {
    if (array_key_exists('authors', $fields) && $fields['authors']) {
      $fields['authors'] = implode(', ', $fields['authors']);
    }
  }
  //load publication_type
  $_pubtypearr = Publication::publication_types();
  $_pubtypekey = array_search($fields['publication_type'], $_pubtypearr);
  $fields['publication_type'] = $_pubtypekey;

  // Store loaded pubmed info in session
  $_SESSION[SESSION_VARS] = $fields;
}
