<?php

function sfb_literature_export_misc() {
  if (arg(2) == 'ris') {
    sfb_literature_export_misc_ris();
    return NULL;
  }
  drupal_not_found();
  return NULL;
}

function sfb_literature_export_misc_ris () {
  $results = getPublications();

  module_load_include('inc', 'sfb_literature', 'includes/session');
  $publications_filter = lists_session("publications_filter");

  $ext = 'ris';

  if ($publications_filter == -1) {
    $fname = sprintf('%s_ris_export.%s', date('Y-m-d'), $ext);
  }
  else if ($publications_filter == -2) {
    $fname = sprintf('%s_ris_export_empty.%s', date('Y-m-d'), $ext);
    $results = [];
  }
  else {
    $fname = sprintf('%s_ris_export_filtered.%s', date('Y-m-d'), $ext);
    $results = $publications_filter;
  }

  $handle = fopen($fname, "w");
  foreach ($results as $result) {
    fwrite($handle,  'TY  - '.getRisType($result->publication_type)."\n");
    fwrite($handle,  'TI  - '.$result->title."\n");
    if ($result->doi) {
      fwrite($handle,  'DO  - '.$result->doi."\n");
    }
    if ($result->publication_year) {
      fwrite($handle,  'PY  - '.$result->publication_year."\n");
    }
    if ($result->journal_title) {
      fwrite($handle,  'CY  - '.$result->journal_title."\n");
    }
    if ($result->issn) {
      fwrite($handle,  'SN  - '.$result->issn."\n");
    }
    if ($result->url) {
      fwrite($handle,  'UR  - '.$result->url."\n");
    }
    if ($result->pages) {
      fwrite($handle,  'SP  - '.$result->pages."\n");
    }
    if ($result->volume) {
      fwrite($handle,  'VL  - '.$result->volume."\n");
    }
    $author_arr = explode(', ',$result->authors);
    if (!empty($author_arr)) {
      foreach ($author_arr as $author) {
        if (!empty($author)) {
          fwrite($handle,  'AU  - '.$author."\n");
        }
      }
    }
    fwrite($handle,  'ER  -'."\n\n");
  }
  fclose($handle);

  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename='.basename($fname));
  header('Expires: 0');
  header('Cache-Control: must-revalidate');
  header('Pragma: public');
  header('Content-Length: ' . filesize($fname));
  readfile($fname);
  exit;
}

function getRisType($rdp_type) {
  switch ($rdp_type) {
    case 'conference_abstract':
    case 'english_abstract':
      return 'ABST';
    case 'video_audio_media':
      return 'ADVS';
    case 'legal_case':
      return 'CASE';
    case 'clinical_conference':
    case 'consensus_development_conference':
    case 'consensus_development_conference_nih':
      return 'CPAPER';
    case 'dataset':
      return 'DATA';
    case 'dictionary':
      return 'DICT';
    case 'classical_article':
    case 'historical_article':
      return 'EJOUR';
    case 'government_publications':
    case 'research_support_nonus_gov_research_support':
    case 'research_support_us_gov':
      return 'GOVDOC';
    case 'introductory_journal_article':
    case 'journal_article':
      return 'JOUR';
    case 'legislation':
      return 'LEGAL';
    case 'newspaper_article':
      return 'NEWS';
    case 'case_report':
      return 'REPORT';
    default:
      return 'GEN';
  }
}

function sfb_literature_export_json () {
  $results = getPublications();

  module_load_include('inc', 'sfb_literature', 'includes/session');
  $publications_filter = lists_session("publications_filter");

  if ($publications_filter == -1) {
  }
  else if ($publications_filter == -2) {
    $results = [];
  }
  else {
    $results = $publications_filter;
  }

  return $results;
}

function getPublications() {
  $results = NULL;

  try {
    $q = db_select('pubreg_articles', 'a')
      ->fields('a')
      ->orderBy('id', 'ASC')
      ->execute();

    $results = $q->fetchAllAssoc('id');

  } catch(Exception $e) {
    drupal_set_message('An database error has occurred.', 'error');
    drupal_access_denied();
  }

  return $results;
}
