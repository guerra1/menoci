<?php
/**
 * @file This file contains forms and handlers for adding External-ID items to
 *   a publication item
 */

/**
 * ToDo: doc
 */
function sfb_literature_add_id($form, &$form_state, $publication_id) {

  $form_state['publication_id'] = $publication_id;

  $pub_ex_id = new PublicationExternalIdentifier();
  $form = $pub_ex_id->getForm();

  return $form;
}

/**
 * Cancel handler for @see sfb_literature_add_id().
 */
function sfb_literature_add_id_cancel($form, &$form_state) {
  $publication = $form_state['publication_id'];

  $form_state['redirect'] = Publication::url_by_id($publication);
}


/**
 * Validation for @see sfb_literature_add_id().
 */
function sfb_literature_add_id_validate($form, &$form_state) {
  /**
   * ToDo: resolve replication of function @see sfb_literature_edit_id_validate
   */
  $exid_type_id = $form_state['values']['exid_type'];

  $exid_type = ExternalIDTypeRepository::findById($exid_type_id);
  /** @var \ExternalIDType $exid_type */

  /**
   * Skip validation if no ExternalIDType is selected
   */
  if ($exid_type) {
    $value = $form_state['values']['exid_value'];

    if (!$exid_type->validate_regex($value)) {
      form_set_error('exid_value',
        'Value does not match expected pattern of the identifier!');
    }

    if ($exid_type->is_unique()) {
      $existing_ids = ExternalIDRepository::findByTypeSubjectId($exid_type->getLabel(),
        PublicationExternalIdentifier::EX_ID_SUBJECT,
        $form_state['publication_id']);
      if (!is_array($existing_ids) and $existing_ids !== NULL) {
        $existing_ids = [$existing_ids];
      }
      if (count($existing_ids)) {
        form_set_error('exid_type',
          'Only one ID of this type may be added, this would be the second.');
      }
    }
  }
}

/**
 * Submit handler for @see sfb_literature_add_id().
 */
function sfb_literature_add_id_submit($form, &$form_state) {

  $publication_id = $form_state['publication_id'];

  $values = $form_state['values'];

  $ex_id = new ExternalID();
  $ex_id->setSubject(ExternalIDSubjectRepository::findBySubject(PublicationExternalIdentifier::EX_ID_SUBJECT)
    ->getId());
  $ex_id->setType($values['exid_type']);
  $ex_id->setValue($values['exid_value']);
  $ex_id->setSubjectId($publication_id);
  $ex_id->save();

  $pub_ex_id = new PublicationExternalIdentifier();
  $pub_ex_id->setExternalId($ex_id->getId());
  $pub_ex_id->setDescription($values['description']);
  $pub_ex_id->save();

  /**
   * Cache last selected Ex-ID Type
   */
  global $user;
  $cache_id = "rdp_publication_exid_type_select_default_" . $user->uid;
  cache_set($cache_id, $values['exid_type']);

  $form_state['redirect'] = sprintf('literature/publications/%s/',
    StringEscapeHtml($publication_id));
}

/**
 * Ajax callback
 */
function sfb_literature_add_id_type_switch_ajax_callback(&$form, &$form_state) {

  $exid_type_id = $form_state['values']['exid_type'];
  $form['resolver_markup']['#markup'] = PublicationExternalIdentifier::render_external_id_type_info($exid_type_id);

  return $form['resolver_markup'];
}