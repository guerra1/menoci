<?php

module_load_include('inc', 'sfb_commons', 'utils');
module_load_include('inc', 'sfb_literature', 'includes/common');
module_load_include('inc', 'sfb_literature', 'includes/article_def');

define('PHP_EXCEL_PHP_PATH_REL', '../PHPExcel/Classes/PHPExcel.php');


/**
 * @file The literature export components.
 */

/**
 * The abstract base class for LiteratureExporters.
 */
abstract class LiteratureExporter {
	protected function __construct() {
	}

	/**
	 * The abstract base for Export-Impelementations.
	 *
	 * @param $literature The literature to export.
	 */
	abstract protected function DoExport($literature);

	/**
	 * The abstract base for Export instance-names.
	 *
	 * @return The name of the instance.
	 */
	abstract protected function DoGetName();

	/**
	 * Perform the export-operation on the given literature data.
	 *
	 * @param $literature The literature to export.
	 */
	public function Export($literature) {
		return $this->DoExport($literature);
	}

	/**
	 * Retrieves the name of the current Exporter instance.
	 *
	 * @return The name of the exporter.
	 */
	public function GetName() {
		return $this->DoGetName();
	}

	/**
	 * Retrieves the file extention of the current Exporter instance, or NULL if there is none.
	 *
	 * @return The file extention of the current Exporter instance, or NULL if there is none.
	 */
	public function GetFileExtention() {
		return NULL;
	}

	/**
	 * Retrieves the MIME-type of the current Exporter instance, or NULL if there is none.
	 */
	public function GetMimeType() {
		return NULL;
	}
}

/**
 * A class that implements literature export in Microsoft Excel format.
 */
class SpreadsheetExporter extends LiteratureExporter {
	/**
	 * An undefined Format.
	 */
	const FORMAT_UNDEFINED = 0x0000;

	/**
	 * Excel Binary Format. (*.xls)
	 */
	const FORMAT_BIFF = 0x0001;

	/**
	 * Office OpenXML Format. (*.xlsx)
	 */
	const FORMAT_OPENXML = 0x0002;

	/**
	 * OASIS OpenDocument Spreadsheet Format. (*.ods)
	 */
	const FORMAT_OASIS_OPENDOCUMENT = 0x0003;

	// Aliases for convenient usage
	const FORMAT_XLS  = self::FORMAT_BIFF;
	const FORMAT_XLSX = self::FORMAT_OPENXML;
	const FORMAT_ODS  = self::FORMAT_OASIS_OPENDOCUMENT;

	/**
	 * Instantiate the SpreadsheetExporter.
	 *
	 * @param $type The type of the export file. Can either be SpreadsheetExporter::FORMAT_BIFF for binary excel files (*.xls; Excel 2003 or older), SpreadsheetExporter::FORMAT_OPENXML for modern excel files in OpenXML format (*.xlsx; Excel 2007 or newer), SpreadsheetExporter::FORMAT_OASIS_OPENDOCUMENT for OASIS OpenDocument Speadsheet format (*.ods; LibreOffice).
	 */
	public function __construct($type = self::FORMAT_OPENXML) {
		parent::__construct();

		if($type != self::FORMAT_BIFF && $type != self::FORMAT_OPENXML && $type != self::FORMAT_OASIS_OPENDOCUMENT) {
			throw new Exception("Invalid Spreadsheet Format", 1);
		}

		$this->type = $type;
	}

	/**
	 * Creates a new Excel sheet.
	 *
	 * @return A newly instantiated PHPExcel object.
	 */
	protected function CreateSheet() {
		require_once(realpath(dirname(__FILE__) . '/' . PHP_EXCEL_PHP_PATH_REL));
		$excel = new PHPExcel();

		return $excel;
	}

	/**
	 * Sets the properties of the sheet, such as Creator, Title and Description.
	 *
	 * @param The Excel sheet to set the properties of.
	 */
	protected function SetSheetProperties($sheet) {
		$p = $sheet->getProperties();

		$date = (new DateTime())->format('Y-m-d H:i:s');

		$p->setCreator('SFB 1002 Literature Repository');
		$p->setTitle('Literature Export ' . $date);
		$p->setDescription('Export of the SFB 1002 Literature Repository. (' . $date .')');
	}

	/**
	 * Fills the cells of the Sheet with literature data.
	 *
	 * @param $sheet The sheet to add data to.
	 * @param $literature The literature data to export.
	 */
	protected function SheetAddData($sheet, $literature) {
    $fields = Publication::get_form_fields();
    $names = Publication::get_field_names();

    // Column header
    $sheet->getActiveSheet()->setCellValue('A1', "RDP-Page");

		$col = 'B';
		foreach($fields as $k => $_) {

      $n = arrayValueOrDefault($names, $k, $k);


      $sheet->getActiveSheet()->setCellValue(($col++) . '1', $n);
    }

		$sheet->getActiveSheet()->mergeCells($col.'1:Z1');
    $sheet->getActiveSheet()->setCellValue(($col) . '1', "External Resources");

    $style_header = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      ),
      'borders' => array(
        'bottom' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN
        )
      )

    );

    $sheet->getActiveSheet()->getStyle("A1:Z1")->applyFromArray($style_header);

		$rn = 2;

		if(count($literature) > 0) {
			// For each row in literature, handle publication_type, open_access and working_group separately
			foreach ($literature as $row) {
				// Output link to RDP page
        global $base_url;
        $resolve_url = $base_url.'/literature/publications/'.$row->id;
        $sheet->getActiveSheet()->getCell(('A').$rn)
          ->getHyperlink()
          ->setUrl($resolve_url)
          ->setTooltip($resolve_url);
        $sheet->getActiveSheet()->setCellValue(('A').$rn, 'Link');

			  // Output every field,
				$col = 'B';
				foreach ($fields as $k => $type) {
					if($k == 'publication_type'){
            $pub_types = Publication::publication_types();
					  $v = $pub_types[$row->publication_type];
          }else if($k == 'open_access'){
            if($row->open_access == 0)
              $v = 'No';
            else if($row->open_access == 1) {
              $v = 'Yes';
            } else{
              $v = 'Unknown';
            }
          }else if($k == 'wg' && $row->wg !== NULL){
            $wgnames = wg_get_names();
            $wg_array = explode(', ',$row->wg);
            $v = '';
            foreach ($wg_array as $wg) {
              $wgn = array_key_exists($wg, $wgnames) ? $wgnames[$wg] : sprintf('%s', $wg);
              $v .= StringEscapeHtml($wgn).', ';
            }
            $v = substr($v,0,-2);
          }else if($k == 'wg' && $row->wg == NULL){
					  $v = '<span style="font-style:italic; color:#777;">Unassigned</span>';
          }else{
					  $v = $row->$k;
          }

					$sheet->getActiveSheet()->setCellValue(($col++).$rn, $v);
				}
        // Get all external Ids
        $external_ids = PublicationExternalIdentifier::fetchAllByPublicationId($row->id);
				//Add External Ids to Excel sheet
        if ($external_ids and !is_array($external_ids)) {
          $external_ids = [$external_ids];
        }
        if($external_ids){
          foreach($external_ids as $external_id) {
            if (!($pub_ex_id = PublicationExternalIdentifier::fetch($external_id->getId()))->isEmpty()) {
              $type = $external_id->getTypeLabel();
              $value = $external_id->getValue();
              $desc = $pub_ex_id->getDescription();
              $resolve_url = $external_id->url('resolve');
              if (filter_var($resolve_url, FILTER_VALIDATE_URL) === FALSE and user_access('administer site configuration')) {
                $warning = 'The external resource <b>'.$desc.'</b> of <b>'.$row->title.'</b> could not be exported to Excel. <br>The given URL is invalid: <i>'.$resolve_url.'</i>';
                drupal_set_message($warning, 'warning');
                watchdog('sfb_literature', $warning);
              }
              else {
                $sheet->getActiveSheet()->getCell(($col).$rn)
                  ->getHyperlink()
                  ->setUrl($resolve_url)
                  ->setTooltip($value);
              }
              $sheet->getActiveSheet()->setCellValue(($col++).$rn, $type .": ".$desc);
            }
          }
        }
				$rn++;
			}

			// automatically set the size of the columns for better readability, but
      // limit column width to 100
      foreach(range('A','Z') as $columnID) {
        $sheet->getActiveSheet()->getColumnDimension($columnID)
          ->setAutoSize(true);
        $sheet->getActiveSheet()->calculateColumnWidths();
        $colwidth = $sheet->getActiveSheet()->getColumnDimension($columnID)->getWidth();
        if ($colwidth>100) {
          $sheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(false);
          $sheet->getActiveSheet()->getColumnDimension($columnID)->setWidth(100);
        }

      }

      // set the first row bold
      $from = "A1";
      $to = "U1";
      $sheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );
		}
	}

	protected function DoExport($literature) {
		$sheet = $this->CreateSheet();
		$this->SetSheetProperties($sheet);
		$this->SheetAddData($sheet, $literature);

		$k = 'Excel2007';

		switch ($this->type) {
			case self::FORMAT_BIFF:
				$k = 'Excel5';
				break;
			case self::FORMAT_OPENXML:
				$k = 'Excel2007';
				break;
			case self::FORMAT_OASIS_OPENDOCUMENT:
				$k = 'OOCalc';
				break;
		}

		// This is stupid because outputting directly won't enable us to set the Content-Length header D:
		$objWriter = PHPExcel_IOFactory::createWriter($sheet, $k);
		$objWriter->save('php://output');
	}

	protected function DoGetName() {
		switch ($this->type) {
			case self::FORMAT_BIFF:
				return 'Excel (Binary Compatible)';
			case self::FORMAT_OPENXML:
				return 'Excel (OpenXML)';
			case self::FORMAT_OASIS_OPENDOCUMENT:
				return 'LibreOffice Calc (OASIS OpenDocument)';
		}
	}

	public function GetFileExtention() {
		switch ($this->type) {
			case self::FORMAT_BIFF:
				return 'xls';
			case self::FORMAT_OPENXML:
				return 'xlsx';
			case self::FORMAT_OASIS_OPENDOCUMENT:
				return 'ods';
		}
	}

	public function GetMimeType() {
		switch ($this->type) {
			case self::FORMAT_BIFF:
				return 'application/vnd.ms-excel';
			case self::FORMAT_OPENXML:
				return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
			case self::FORMAT_OASIS_OPENDOCUMENT:
				return 'application/vnd.oasis.opendocument.spreadsheet';
		}
	}

}
