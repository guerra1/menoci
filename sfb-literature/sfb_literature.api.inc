<?php
/**
 * @file Contains custom Drupal hook definition
 */

/**
 * Hook allows other modules to inject content into RDP Publication module
 * display page by adding to the "rows" array that will be Drupal table themed.
 *
 * @param int $publication_id Internal ID of publication object.
 * @param array $rows Associative array for Drupal table theme rendering.
 *
 * @return array Modified rows array.
 */
function hook_rdp_publication_display_table_alter($publication_id, $rows) {
  foreach ($rows as $key => $value) {
    /**
     * $value contains an associative array with keys 'title', 'value'
     */
    $rows[$key]['value'] .= "<!-- Add HTML content to table row content field -->";
  }
  return $rows;
}

/**
 * Hook allows other modules to append json information into RDP Publication module about linked objects (e.g.
 * antibodies or labnotebooks) when the sfb-literature API is called to retrieve information about a specific
 * publication.
 *
 * @param int $publication_id Internal ID of the publication object.
 *
 * @return array containing data describing all objects that are linked to the publication.
 */
function hook_sfb_literature_json_get($publication_id) {

}