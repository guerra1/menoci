<?php
/**
 * Created by PhpStorm.
 * User: lucaf
 * Date: 15.12.2017
 * Time: 11:47
 */

class PublicationRepository {

  /**
   * Name of the table containing pubreg articles information.
   *
   * @var string
   */
  static $tableName = 'pubreg_articles';

  static $databaseFields = [
    'id',
    'pmid',
    'doi',
    'publication_year',
    'title',
    'journal_title',
    'issn',
    'essn',
    'url',
    'pages',
    'issue',
    'volume',
    'journal_abbr',
    'extra',
    'authors',
    'first_author',
    'last_author',
    'antibody_info',
    'gene_prot',
    'animals',
    'cell_lines',
    'links',
    'protocols',
    'wg',
    'open_access',
    'creation_time',
    'publication_type',
    'subproject',
  ];

  /**
   * Test if a value will satisfy a column's unique constraint on insert.
   *
   * @param string $field Database column to check
   * @param string $value Value to check
   *
   * @return bool Returns TRUE if value satisfies unique constraint, i.e. does
   *   not yet exist in the database
   */
  public static function checkUniqueConstraint($field, $value) {
    if (!db_field_exists(self::$tableName, $field)) {
      watchdog(__CLASS__, "Column %s does not exist in table %t",
        ['%s' => $field, '%t' => self::$tableName], WATCHDOG_ERROR);
      return FALSE;
    }
    else {
      try {
        $result = db_select(self::$tableName, 't')
          ->fields('t', [$field])
          ->distinct()
          ->execute()
          ->fetchAllAssoc($field);

        /**
         * Transformation for comparison: only use non-null values and
         * transform to uppercase characters
         */
        $unique_values = array_filter(array_keys($result));
        $unique_values = array_map("strtoupper", $unique_values);

        if (in_array(strtoupper($value), $unique_values)) {
          // value exists, unique constraint does not hold
          return FALSE;
        }
        else {
          // value does not exist in the database, unique constraint satisfied
          return TRUE;
        }
      } catch (Exception $exception) {
        // handle Exception
        watchdog_exception(__CLASS__, $exception);
        return FALSE;
      }
    }
  }

  /**
   * ToDo: doc
   *
   * @param      $data
   * @param      $publication
   * @param null $form_state pseudo-static Drupal $form_state array
   *
   * @return \DatabaseStatementInterface|int|void
   */
  public static function save($data, $publication, $form_state = NULL) {
    //save data into new table if publication is null or empty
    if (empty($publication)) {
      $txn = db_transaction();
      try {
        $q = db_insert('pubreg_articles');
        $id = $q->fields($data)->execute();

        drupal_set_message(t('The new publication has successfully been created!'));
        //drupal_set_message(sprintf('<a href="publications/%s/">View the publication!</a>', StringEscapeHtml($id)));

      } catch (Exception $e) {
        // Revert changes
        $txn->rollback();

        // Display error message
        drupal_set_message(t('An error has occurred while processing your publication. It was not created.') . $e->getMessage(),
          'error');

        $_SESSION[SESSION_PUBMED_VARS] = $data;
      }
      return $id;
    }
    //update existing table
    else {
      $txn = db_transaction();

      try {
        $n = db_update('pubreg_articles')
          ->fields($data)
          ->condition('id', $publication, '=')
          ->execute();

        if (module_exists("sfb_labnotebooks")) {
          sfb_labnotebooks_linking_update_database($form_state, 'linking_pubreg_labnotebook', 'pubreg_article',
            'labnotebook', $publication);
        }

        //
        // store linked antibodies
        //
        if (module_exists("sfb_antibody")) {
          $antibody_linking = RDPLinking::getLinkingByName('sfb_antibody', 'antibody_antibody2');
          $antibody_linking->processSubmittedForm('pubreg_articles', $publication, $form_state['values']['antibodies']);
          //sfb_antibody_linking_update_database($form_state, 'linking_pubreg_antibody', 'pubreg_article', 'antibody',  $publication);
        }
        if ($n != 1) {
          if ($n == 0) {
            $form_state['redirect'] = sprintf('literature/publications/%s/', StringEscapeHtml($publication));
          }
          else {
            drupal_set_message(t('Update failure!'), 'error');
          }

          return;
        }
        else {
          drupal_set_message(t('The publication has successfully been updated!'));
          //drupal_set_message(l(t('View the publication!'), sprintf('literature/publications/%s', StringEscapeHtml($publication))));

        }
      } catch (Exception $e) {
        // Revert changes
        $txn->rollback();

        // Display error message
        drupal_set_message(t('An error has occurred while processing your changes!'), 'error');
      }
    }
  }

  /**
   * Fetch an entry from database by ID.
   *
   * @param $id
   *
   * @return \Publication
   */
  public static function findById($id) {
    try {
      $query = db_select(self::$tableName, 't')
        ->fields('t', self::$databaseFields)
        ->condition('t.id', $id)
        ->range(0, 1);
      $result = $query->execute()
        ->fetch();

      if ($result) {
        return Publication::translate_db_result($result);
      }
    } catch (Exception $exception) {
      watchdog_exception(__CLASS__, $exception);
    }
  }

  /**
   * Fetch all entries from database.
   *
   * @return Publication
   */
  public static function findAll() {
    try {
      $query = db_select(self::$tableName, 't')
        ->fields('t', self::$databaseFields)
        ->execute();
      $result = $query->fetchAllAssoc('id');

    } catch (Exception $exception) {
      watchdog_exception(__CLASS__, $exception);
    }
    return $result;
  }

  /**
   * @return mixed
   */
  public static function findAllPublicationTypes() {
    try {
      return self::fetchDistinctValues('publication_type');
    } catch (Exception $exception) {
      watchdog_exception(__CLASS__, $exception);
    }
  }

  /**
   * @param $field
   *
   * @return mixed
   */
  private static function fetchDistinctValues($field) {
    if (db_field_exists(self::$tableName, $field)) {
      try {
        $query = db_select(self::$tableName, 't')
          ->distinct()
          ->fields('t', [$field])
          ->execute();
        $result = $query->fetchCol($field);
        return $result;
      } catch (Exception $exception) {
        watchdog_exception(__CLASS__, $exception);
      }
    }
    else {
      throw new InvalidArgumentException("Database field does not exist.");
    }
  }

  /**
   * Fetch an entry from database by ID with ordering.
   *
   * @param      $id
   * @param      $ordering
   * @param bool $pager
   *
   * @return mixed
   */
  public static function findByIdAndOrder($id, $ordering, $pager = FALSE) {
    try {
      if ($pager) {
        $query = db_select(self::$tableName, 't')
          ->extend('PagerDefault')
          ->fields('t', self::$databaseFields)
          ->condition('t.id', $id)
          ->orderBy($ordering['column'], $ordering['order'])
          ->range(0, 1);
      }
      else {
        $query = db_select(self::$tableName, 't')
          ->fields('t', self::$databaseFields)
          ->condition('t.id', $id)
          ->orderBy($ordering['column'], $ordering['order'])
          ->range(0, 1);
      }
      $result = $query->execute()
        ->fetch();

    } catch (Exception $exception) {
      watchdog_exception(__CLASS__, $exception);
    }
    return $result;
  }

  /**
   * default order is by creation_date latest first
   *
   * @param $ordering
   *
   * @return mixed
   */
  public static function findAllAndOrderByDefault($ordering, $pager) {
    try {
      // Query database
      if ($pager) {
        $q = db_select('pubreg_articles', 'a')
          ->extend('PagerDefault')
          ->fields('a', [
            'id',
            'title',
            'authors',
            'journal_title',
            'publication_year',
            'doi',
            'creation_time',
            'first_author',
            'wg',
            'pmid',
            'open_access',
          ])
          ->orderBy($ordering['column'], $ordering['order'])
          ->execute();
      }
      else {
        $q = db_select('pubreg_articles', 'a')
          ->fields('a', [
            'id',
            'title',
            'authors',
            'journal_title',
            'publication_year',
            'doi',
            'creation_time',
            'first_author',
            'wg',
            'pmid',
            'open_access',
          ])
          ->orderBy($ordering['column'], $ordering['order'])
          ->execute();
      }

      // Retrieve rows
      $result = $q->fetchAllAssoc('id');
    } catch (Exception $e) {
      drupal_set_message('A database error has occurred.', 'error');
      drupal_access_denied();
    }
    return $result;
  }

  /**
   * Executes search query on Literature database with given condition.
   *
   * @param $condition Database condition
   *
   * @return mixed
   */
  public static function findBy($condition, $ordering) {

    $query = db_select(self::$tableName, 'p');
    $query->leftJoin('rdp_external_id', 'r', 'r.subject_id = p.id');
    $query->leftJoin('rdp_publication_externalid', 'e', 'e.external_id = r.id');
    $result = $query->fields('p')
      ->condition($condition)
      ->fields('p', self::$databaseFields)
      ->orderBy($ordering['column'], $ordering['order'])
      ->execute();

    $r = $result->fetchAllAssoc('id');
    return self::databaseResultsToArticles($r);
  }

  /**
   * Reads literature database results and create an array with Antibody
   * objects.
   *
   * @param $results
   *
   * @return \pubreg_articles[]
   */
  public static function databaseResultsToArticles($results) {
    $articles = [];
    foreach ($results as $result) {
      $articles[] = $result;
    }

    return $articles;
  }

  public static function statistics($wg, $timelimit_end = 0) {
    $query = db_select(self::$tableName, 't');
    if ($wg == 'internal') {
      $query
        ->fields('t', ['open_access'])
        ->condition('wg', 'ag_zzexternal', '<>')
        ->groupBy('t.open_access')
        ->addExpression('COUNT(t.open_access)', 'n');
    }
    elseif ($wg == 'external') {
      $query
        ->fields('t', ['open_access'])
        ->condition('wg', 'ag_zzexternal', '=')
        ->groupBy('t.open_access')
        ->addExpression('COUNT(t.open_access)', 'n');
    }
    else {
      $query
        ->fields('t', ['open_access'])
        ->groupBy('t.open_access')
        ->addExpression('COUNT(t.open_access)', 'n');
    }
    if ($timelimit_end) {
      $query->condition('publication_year', $timelimit_end, '<=');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $open_access = [];
    $n = 0;
    foreach ($result as $row) {
      $row->open_access = PublicationForm::$options_open_access[$row->open_access];
      $open_access['x'][] = $row->open_access;
      $open_access['y'][] = $row->n;
      $n += $row->n;
    }
    $open_access['n'] = $n;

    $query = db_select(self::$tableName, 't');
    if ($wg == 'internal') {
      $query
        ->fields('t', ['journal_title'])
        ->condition('wg', 'ag_zzexternal', '<>')
        ->groupBy('t.journal_title')
        ->orderBy(2, 'DESC')
        ->addExpression('COUNT(t.journal_title)', 'n');

    }
    elseif ($wg == 'external') {
      $query
        ->fields('t', ['journal_title'])
        ->condition('wg', 'ag_zzexternal', '=')
        ->groupBy('t.journal_title')
        ->orderBy(2, 'DESC')
        ->addExpression('COUNT(t.journal_title)', 'n');
    }
    else {
      $query
        ->fields('t', ['journal_title'])
        ->groupBy('t.journal_title')
        ->orderBy(2, 'DESC')
        ->addExpression('COUNT(t.journal_title)', 'n');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $journals = [];
    // Define a dynamic offset: if more than 12 distinct journals are present in the
    // data, group journals with as many hits as the 12th place or less into "others"
    $rowcount = count($result);
    $others = 0;
    $threshold = $rowcount > 12 ? $result[12]->n : 1;
    foreach ($result as $row) {
      if ($row->n > $threshold) {
        $journals['title'][] = $row->journal_title;
        $journals['n'][] = $row->n;
      }
      else {
        $others += $row->n;
      }
    }
    if ($others) {
      $journals['title'][] = 'other journals';
      $journals['n'][] = $others;
    }

    $query = db_select(self::$tableName, 't');
    if ($wg == 'internal') {
      $query
        ->addField('t', 'publication_year', 'year');
      $query
        ->condition('wg', 'ag_zzexternal', '<>')
        ->groupBy('t.publication_year')
        ->orderBy('publication_year', 'ASC')
        ->addExpression('COUNT(t.publication_year)', 'n');
      $query
        ->addExpression('SUM(IF(open_access=1,1,0) AND IF(publication_year=t.publication_year,1,0))', 'n_oa');

    }
    elseif ($wg == 'external') {
      $query
        ->addField('t', 'publication_year', 'year');
      $query
        ->condition('wg', 'ag_zzexternal', '=')
        ->groupBy('t.publication_year')
        ->orderBy('publication_year', 'ASC')
        ->addExpression('COUNT(t.publication_year)', 'n');
      $query
        ->addExpression('SUM(IF(open_access=1,1,0) AND IF(publication_year=t.publication_year,1,0))', 'n_oa');
    }
    else {
      $query
        ->addField('t', 'publication_year', 'year');
      $query
        ->groupBy('t.publication_year')
        ->orderBy('publication_year', 'ASC')
        ->addExpression('COUNT(t.publication_year)', 'n');
      $query
        ->addExpression('SUM(IF(open_access=1,1,0) AND IF(publication_year=t.publication_year,1,0))', 'n_oa');
    }
    $data = $query->execute();
    $result = $data->fetchAll();

    $timeline = [];
    foreach ($result as $row) {
      $timeline['year'][] = $row->year;
      $timeline['number'][] = $row->n;
      $timeline['open_access'][] = $row->n_oa;
    }

    return [
      'open_access' => $open_access,
      'journals' => $journals,
      'timeline' => $timeline,
    ];
  }
}
