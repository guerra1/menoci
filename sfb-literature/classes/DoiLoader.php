<?php
/**
 * Created by PhpStorm.
 * User: freckmann8
 * Date: 15.02.2018
 * Time: 14:23
 */

module_load_include('inc', 'sfb_commons', 'utils');
module_load_include('inc', 'sfb_literature', 'includes/common');
module_load_include('inc', 'sfb_literature', 'includes/article_def');
module_load_include('inc', 'sfb_literature', 'includes/form_main');

class DoiLoader {
  protected $loadURL = 'http://api.crossref.org/works/%s';

  protected $loadCrossCheckURL = 'https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=doi:%s&format=json';

  protected $loadDataCiteURL = 'https://api.datacite.org/works/%s';

  protected $defaultLink = 'https://dx.doi.org/%s';

  /**
   * Name of the table containing pubreg articles information.
   *
   * @var string
   */
  static $tableName = 'pubreg_articles';

  /**
   * Formats the request uri based on pubmed-id.
   *
   * @param $doi The crossref-id
   *
   * @return string The formatted request uri.
   */
  protected function makeURL($URL, $doi) {
    return sprintf($URL, trim($doi));
    #return sprintf($this->loadURL, urlencode(trim($pmid)));
  }

  /**
   * Loads json representation of a pubmed entry based on DOI of the
   * entry.
   *
   * @param $doi The entries' crossref-id
   *
   * @return A JSON array containing the parsed api response.
   */
  protected function loadDoiJsonArray($URL, $doi) {
    $url = $this->makeURL($URL, $doi);
    $content = @file_get_contents($url);
    return json_decode($content, TRUE);
  }

  protected function extractDoi($rawDoi){
    preg_match('/\b(10[.][0-9]{4}(?:[.][0-9]+)*\S+)\b/',$rawDoi, $matches);
    $doi = $matches[0];
    return $doi;
  }

  /**
   * Parses a date string in format 'yyyy mmm dd', 'yyyy mmm', or 'yyyy' and
   * returns corresponding DateTime object with time set to 00:00:00 and Month
   * set to January if not defined and Day set to 01 if not defined.
   *
   * @param $s Date-String in format 'yyyy mmm dd' such as '1991 Apr 28', in
   *   format 'yyyy mmm' such as '1991 Apr', or in format 'yyyy' such as
   *   '1991'.
   *
   * @return DateTime object representing parsed date
   */
  protected function parseDateString($s) {
    $d = NULL;
    $s = sprintf('%s %s %s', $s[0], $s[1], $s[2]);

    // Parse 'yyyy mmm dd'
    try {
      $d = DateTime::createFromFormat('Y M d', $s);
    } catch (Exception $e) { /* silence */
    }

    if ($d == NULL) {
      return $d;
    }

    $d->setTime(0, 0, 0);
    return $d;
  }

  /**
   * Parse known fields in doi json entry object.
   *
   * @param Json entry
   *
   * @return Array of parsed publication parameters
   */
  protected function matchKnownFields($entry) {
    /*
     *  Mapping of fields:
     *
     *   Article Column   |  Json path description
     *  ------------------+------------------------------
     *  doi               > DOI
     *  publication_year  > created[date-parts]
     *  title             > title
     *  journal_title     > fulljournalname
     *  issn              > issn
     *  essn              >
     *  url               -
     *  pages             > pages
     *  issue             > issue
     *  volume            > volume
     *  journal_abbr      -
     *  authors           > author[*] name
     *  first_author      > sortfirstauthor
     *  last_author       > lastauthor
     *
     */

    $r = [];

    //find and parse date
    $_pubdatearr = $this->parseGracefully($entry['created'], 'date-parts');
    $_pubdate = $this->parseDateString($_pubdatearr[0]);
    $_pubyear = $_pubdatearr[0][0];

    //find and parse authors
    $_authorsnode = $this->parseGracefully($entry, 'author');
    $_authors = $this->parseAuthors($_authorsnode);

    //find and parse publication_type
    $_pubtype_raw = $this->parseGracefully($entry, 'type');
    $_pubtype = $this->parsePublicationType($_pubtype_raw);

    //find and parse url
    $url_raw = $this->parseAndDecodeXmlHtmlEntities($entry, 'URL');
    $url = str_replace('\"','', $url_raw); //delete all \ appearing in URL


    $r['doi'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'DOI');
    $r['publication_year'] = $_pubyear;
    $r['title'] = trim($this->parseAndDecodeXmlHtmlEntities($entry['title'], '0'));
    if(array_key_exists('container-title', $entry))
      $r['journal_title'] = $this->parseAndDecodeXmlHtmlEntities($entry['container-title'], '0');
    if(array_key_exists('ISSN', $entry))
      $r['issn'] = $this->parseAndDecodeXmlHtmlEntities($entry['ISSN'], '0');
    #$r['essn'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'essn'); //not available?
    $r['pages'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'page');
    $r['issue'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'issue');
    $r['publication_type'] = $_pubtype;
    $r['volume'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'volume');
    $r['authors'] = $_authors;
    $r['first_author'] = $_authors[0];
    if($_authors !== NULL)
      $r['last_author'] = array_pop($_authors);
    $r['url'] = $url;
    $r['journal_abbr'] = $this->parseAndDecodeXmlHtmlEntities($entry['short-container-title'], '0');

    return $r;
  }

  /**
   * Bring author names from CrossRef in the PubMed format
   * "surname" + " " + "first letter of prenames"
   *
   * @param $rawPreName
   * @param $surName
   *
   * @return string
   */
  protected function convertNamesAuthor($rawPreName, $surName){
    $preNames = explode(" ", $rawPreName);
    $abbrName = '';
    foreach ($preNames as $preName){
      $abbr = substr($preName, 0, 1);
      $abbrName .= $abbr;
    }
    return $surName ." " . $abbrName;
  }

  /**
   * Parse known fields in doi json entry object for crosscheck parameters.
   *
   * @param $entry json
   *
   * @return array of parsed parameters
   */
  protected function matchKnownFieldsCrossCheck($knownFields, $additionalFields) {
    /*
     * Mapping of fields:
     *
     *   Article Column   |  Json path description
     *  ------------------+------------------------------
     *  pubmed id           pmid
     *  open access         isOpenAccess
     *  journal abbr        journalTitle
     *  ISSN                journalIssn
     */
    if ($additionalFields['isOpenAccess'] == 'Y') {
      $openAccess = 1;
    }
    else {
      if ($additionalFields['isOpenAccess'] == 'N') {
        $openAccess = 0;
      }
      else //unknown
      {
        $openAccess = 2;
      }
    }
    if (array_key_exists('pmid', $additionalFields)) {
      $knownFields['pmid'] = $additionalFields['pmid'];
    }
    $knownFields['open_access'] = $openAccess;
    //sometimes journalTitle is the abbreviation of the title
    if (array_key_exists('journalTitle', $additionalFields))
      if((strcmp($knownFields['journal_abbr'], $additionalFields['journalTitle']) < 0)){
        $knownFields['journal_title'] = $additionalFields['journalTitle'];
      }
    #drupal_set_message(print_r($additionalFields, TRUE));
    if(!array_key_exists('issn', $knownFields) or empty($knownFields['issn'])){
      if(array_key_exists('journalIssn', $additionalFields) && !empty($additionalFields['journalIssn']))
        $knownFields['issn'] = $additionalFields['journalIssn'];
    }

    return $knownFields;
  }

  /**Parse known fields in doi json entry object for crosscheck parameters.
   * @param $dataCiteFields
   */
  protected function matchKnownFieldsDataCite($dataCiteFields){
    /*
     * Mapping of fields:
     *
     *   Article Column   |  Json path description
     *  ------------------+------------------------------
     *  doi                 doi
     *  author              author[{given, family},{...},...]
     *  article title       title
     *  journal title       container-title
     *  publication type    resource-type-subtype
     *  publishing date     published (year)
     *  url                 url
     */

    $r = [];

        //find and parse date
        $_pubyear = (int) $this->parseGracefully($dataCiteFields,'published');

        //find and parse authors
        $_authorsnode = $this->parseGracefully($dataCiteFields, 'author');
        $_authors = $this->parseAuthors($_authorsnode);

        //find and parse publication type
        $pubtype_raw = $this->parseGracefully($dataCiteFields, 'resource-type-subtype');
        $pubtype = $this->parsePublicationType($pubtype_raw);

        #drupal_set_message(print_r($_authors,true));

        $r['doi'] = $this->parseAndDecodeXmlHtmlEntities($dataCiteFields, 'doi');
        $r['publication_year'] = $_pubyear;
        $r['title'] = trim($this->parseAndDecodeXmlHtmlEntities($dataCiteFields, 'title'));
        $r['journal_title'] = $this->parseAndDecodeXmlHtmlEntities($dataCiteFields, 'publisher');
        if(array_key_exists('container-title', $dataCiteFields))
          $r['journal_title'] = $this->parseAndDecodeXmlHtmlEntities($dataCiteFields, 'container-title');
        $r['authors'] = $_authors;
        $r['first_author'] = $_authors[0];
        if($_authors !== NULL)
          $r['last_author'] = array_pop($_authors);
        $r['url'] = $this->parseAndDecodeXmlHtmlEntities($dataCiteFields, 'url');
        $r['publication_type'] = $pubtype;
        #drupal_set_message(print_r($r, true));

    return $r;
  }

  /**
   * Load a doi entry based on doi of the entry and return parsed
   * response data.
   *
   * @param $doi The entries' doi-id
   *
   * @return array A representation of key parameters of the entry
   */
  public function loadPub($doi) {
    $doi = sprintf('%s', $doi); // Convert doi to string


    // 1) Load api response json-object
    $json = $this->loadDoiJsonArray($this->loadURL, $doi);
    // load chrosscheck api response and datacite response
    $extractedDoi = $this->extractDoi($doi);
    $jsonCrossCheck = $this->loadDoiJsonArray($this->loadCrossCheckURL, $extractedDoi);
    $jsonDataCite = $this->loadDoiJsonArray($this->loadDataCiteURL, $extractedDoi);

    // 2) Parse result
    $r = $this->parseGracefully($json, 'message');
    $resCrossCheck = $this->parseGracefully($this->parseGracefully($jsonCrossCheck, 'resultList'), 'result');
    $resDataCite = $this->parseGracefully($this->parseGracefully($jsonDataCite, 'data'), 'attributes');


    // Return null if response is invalid or erroneous
    if ($r === NULL || $json['status'] !== 'ok') {
      //if result is empty or erroneous try datacite.org
      if ($resDataCite === NULL) {
        return NULL;
      }
    }


    if($r !== NULL){
      $f = $this->matchKnownFields($r);
    }else{
      $f = $this->matchKnownFieldsDataCite($resDataCite);
    }


    // Replace url with generic link if response empty

    if (!array_key_exists('url', $f) || IsNullOrWhitespace($f['url'])) {
      $f['url'] = sprintf($this->defaultLink, urlencode($doi));
    }
    // add additional fields from crosscheck json if available
    if($resCrossCheck !== NULL and !empty($resCrossCheck))
      //only match with CrossRef data
      #todo implement crosscheck for dataCite
      if($json !== NULL){
        $f = $this->matchKnownFieldsCrossCheck($f, $resCrossCheck[0]);
      }

    #drupal_set_message(print_r($f,true));

    return $f;
  }

  /**
   * this function takes an array of authors and extracts the pre and sur names
   * then it will convert the names like the function 'convertNamesAuthor' describes
   *
   * @param $_authorsnode
   *
   * @return array|null
   */
  protected function parseAuthors($_authorsnode) {
    if ($_authorsnode !== NULL) {
      $_authors = [];
      foreach ($_authorsnode as $node) {
        if(array_key_exists('given', $node) && array_key_exists('family', $node)){
          $prename = $this->parseAndDecodeXmlHtmlEntities($node, 'given') . ' '; // first name
          $surname = $this->parseAndDecodeXmlHtmlEntities($node, 'family'); // last name
          if ($prename !== NULL and $surname !== NULL) {
            $type = 'Author';
          }
          // Append author
          if ($type == 'Author') {
            $_authors[] = $this->convertNamesAuthor($prename, $surname);
          }
        }
      }
    }
    else {
      $_authors = NULL;

    }
    return $_authors;
  }

  /**
   * takes raw publication type and converts it to a defined form
   * @param $_pubtype_raw
   *
   * @return string
   */
  protected function parsePublicationType($_pubtype_raw){
    //convert string
    if ($_pubtype_raw !== NULL) {
      $_pubtype_trimmed = trim($_pubtype_raw);
      $_pubtype_lower_case = strtolower($_pubtype_trimmed);
      $_pubtype_key = str_replace('-', '_', $_pubtype_lower_case);
      $_pubtype_key = str_replace(' ', '_', $_pubtype_key);
    }
    else {
      $_pubtype_key = 'unknown';
    }
    $_pubtype_array = Publication::publication_types();

    if(array_key_exists($_pubtype_key, $_pubtype_array))
      $_pubtype = $_pubtype_array[$_pubtype_key];
    else
      $_pubtype = 'unknown';

    return $_pubtype;
  }

  /**
   * Gets value of $key in $jsonArr if exists, else return null.
   *
   * @param $jsonArr The associative json array
   * @param $key The key to look up
   *
   * @return The associated value of $key in $jsonArr or NULL if $jsonArr is
   *   NULL or $key is inexistent in $jsonArr.
   */
  protected function parseGracefully($jsonArr, $key) {
    // if array is null return null
    if ($jsonArr === NULL) {
      return NULL;
    }

    // Return null if key is not existing
    if (!array_key_exists($key, $jsonArr)) {
      return NULL;
    }

    // Return value
    return $jsonArr[$key];
  }

  /**
   * Searches for a node where sub-element $key has specified value $value in
   * $jsonArr json array.
   *
   * @param $jsonArr A json array consisting of nodes to search.
   * @param $key The key of the node's field
   * @param $value The value of the node's field
   *
   * @return The node if found or NULL otherwise.
   */
  protected function findNodeGracefully($jsonArr, $key, $value) {
    // if array is null return null
    if ($jsonArr === NULL) {
      return NULL;
    }

    // Find node where $node[$key] == $value
    foreach ($jsonArr as $node) {
      $v = $this->parseGracefully($node, $key);

      if ($v == $value) {
        return $node;
      }
    }

    return NULL;
  }

  protected function parseAndDecodeXmlHtmlEntities($arr, $key) {
    $n = $this->parseGracefully($arr, $key);

    if ($n === NULL) {
      return $n;
    }

    return html_entity_decode($n, ENT_COMPAT, 'UTF-8');
  }



}