<?php

module_load_include('inc', 'sfb_commons', 'utils');
module_load_include('inc', 'sfb_literature', 'includes/common');
module_load_include('inc', 'sfb_literature', 'includes/article_def');
module_load_include('inc', 'sfb_literature', 'includes/form_main');


/**
 * The class provides methods to load pubmed articles given a PubMed-ID value
 * and parse specific fields of pubmed-api-response.
 */
class PubmedLoader {

  protected $loadURL = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?retmode=json&db=pubmed&id=%s';

  protected $defaultLink = 'https://www.ncbi.nlm.nih.gov/pubmed/?term=%s%%5Buid%%5D';


  /**
   * Formats the request uri based on pubmed-id.
   *
   * @param $pmid The pubmed-id
   *
   * @return The formatted request uri.
   */
  protected function makeURL($pmid) {
    return sprintf($this->loadURL, urlencode(trim($pmid)));
    return sprintf($this->loadURL, urlencode(trim($pmid)));
  }

  /**
   * Loads json representation of a pubmed entry based on pubmed-id of the
   * entry.
   *
   * @param $pmid The entries' pubmed-id
   *
   * @return A JSON array containing the parsed api response.
   */
  protected function loadPubmedJsonArray($pmid) {
    $url = $this->makeURL($pmid);
    $content = file_get_contents($url);
    return json_decode($content, TRUE);
  }

  /**
   * Parses a date string in format 'yyyy mmm dd', 'yyyy mmm', or 'yyyy' and
   * returns corresponding DateTime object with time set to 00:00:00 and Month
   * set to January if not defined and Day set to 01 if not defined.
   *
   * @param $s Date-String in format 'yyyy mmm dd' such as '1991 Apr 28', in
   *   format 'yyyy mmm' such as '1991 Apr', or in format 'yyyy' such as
   *   '1991'.
   *
   * @return DateTime object representing parsed date
   */
  protected function parseDateString($s) {
    $d = NULL;

    if (!preg_match('/^\\s*(\\d{4})(?:\\s*([a-z]{3})(?:\\s*(\\d{1,2}))?)?\\s*$/i', $s, $m)) {
      return NULL;
    }

    $s = sprintf('%s %s %s', $m[1], isset($m[2]) ? $m[2] : 'Jan', isset($m[3]) ? $m[3] : '01');

    // Parse 'yyyy mmm dd'
    try {
      $d = DateTime::createFromFormat('Y M d', $s);
    } catch (Exception $e) { /* silence */
    }

    if ($d == NULL) {
      return $d;
    }

    $d->setTime(0, 0, 0);
    return $d;
  }

  /**
   * Parse known fields in pubmed json entry object.
   *
   * @param Json entry
   *
   * @return Array of parsed publication parameters
   */
  protected function matchKnownFields($entry) {
    /*
     *  Mapping of fields:
     *
     *   Article Column   |  Json path description
     *  ------------------+------------------------------
     *  doi               > articleids[>idtype=doi] value
     *  publication_year  > pubdate
     *  title             > title
     *  journal_title     > fulljournalname
     *  issn              > issn
     *  essn              > essn
     *  url               -
     *  pages             > pages
     *  issue             > issue
     *  volume            > volume
     *  journal_abbr      -
     *  authors           > authors[*] name
     *  first_author      > sortfirstauthor
     *  last_author       > lastauthor
     *
     */

    $r = [];

    $_doinode = $this->findNodeGracefully($this->parseGracefully($entry, 'articleids'), 'idtype', 'doi'); // Find 'doi'-node in 'articleids'
    $_pubdate = $this->parseAndDecodeXmlHtmlEntities($entry, 'pubdate');
    $_pubdate = $_pubdate != NULL ? $this->parseDateString($_pubdate) : NULL;
    $_pubyear = $_pubdate != NULL ? $_pubdate->format('Y') : NULL;

    $_authorsnode = $this->parseGracefully($entry, 'authors');
    if ($_authorsnode !== NULL) {
      $_authors = [];
      foreach ($_authorsnode as $node) {
        $name = $this->parseAndDecodeXmlHtmlEntities($node, 'name');
        $type = $this->parseGracefully($node, 'authtype');

        // Append author
        if ($type == 'Author') {
          $_authors[] = $name;
        }
      }
    }
    else {
      $_authors = NULL;
    }
    //find and parse publication_type
    $_pubtypearr = $this->parseGracefully($entry, 'pubtype');
    if($_pubtypearr[0] !== NULL)
        $_pubtype = $_pubtypearr[0];
    else
        $_pubtype = '';

    $r['pmid'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'uid');
    $r['doi'] = $this->parseAndDecodeXmlHtmlEntities($_doinode, 'value');
    $r['publication_year'] = $_pubyear;
    $r['title'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'title');
    $r['journal_title'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'fulljournalname');
    $r['issn'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'issn');
    $r['essn'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'essn');
    $r['pages'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'pages');
    $r['issue'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'issue');
    $r['publication_type'] = $_pubtype;
    $r['volume'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'volume');
    $r['authors'] = $_authors;
    $r['first_author'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'sortfirstauthor');
    $r['last_author'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'lastauthor');
    $r['url'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'availablefromurl');
    $r['journal_abbr'] = $this->parseAndDecodeXmlHtmlEntities($entry, 'source');

    return $r;
  }

  /**
   * Load a pubmed entry based on pubmed-id of the entry and return parsed
   * response data.
   *
   * @param $pmid The entries' pubmed-id
   *
   * @return A representation of key parameters of the entry
   */
  public function loadPub($pmid) {
    $pmid = sprintf('%s', $pmid); // Convert pmid to string

    // 1) Load api response json-object
    $json = $this->loadPubmedJsonArray($pmid);

    // 2) Parse result
    $r = $this->parseGracefully($json['result'], $pmid);

    // Return null if response is invalid or erroneous
    if ($r === NULL || array_key_exists('error', $r)) {
      return NULL;
    }

    $f = $this->matchKnownFields($r);

    // Replace url with generic link if response empty
    if (!array_key_exists('url', $f) || IsNullOrWhitespace($f['url'])) {
      $f['url'] = sprintf($this->defaultLink, urlencode($pmid));
    }

    return $f;
  }

  /**
   * Gets value of $key in $jsonArr if exists, else return null.
   *
   * @param $jsonArr The associative json array
   * @param $key The key to look up
   *
   * @return The associated value of $key in $jsonArr or NULL if $jsonArr is
   *   NULL or $key is inexistent in $jsonArr.
   */
  protected function parseGracefully($jsonArr, $key) {
    // if array is null return null
    if ($jsonArr === NULL) {
      return NULL;
    }

    // Return null if key is not existing
    if (!array_key_exists($key, $jsonArr)) {
      return NULL;
    }

    // Return value
    return $jsonArr[$key];
  }

  /**
   * Searches for a node where sub-element $key has specified value $value in
   * $jsonArr json array.
   *
   * @param $jsonArr A json array consisting of nodes to search.
   * @param $key The key of the node's field
   * @param $value The value of the node's field
   *
   * @return The node if found or NULL otherwise.
   */
  protected function findNodeGracefully($jsonArr, $key, $value) {
    // if array is null return null
    if ($jsonArr === NULL) {
      return NULL;
    }

    // Find node where $node[$key] == $value
    foreach ($jsonArr as $node) {
      $v = $this->parseGracefully($node, $key);

      if ($v == $value) {
        return $node;
      }
    }

    return NULL;
  }

  protected function parseAndDecodeXmlHtmlEntities($arr, $key) {
    $n = $this->parseGracefully($arr, $key);

    if ($n === NULL) {
      return $n;
    }

    return html_entity_decode($n, ENT_COMPAT, 'UTF-8');
  }
}