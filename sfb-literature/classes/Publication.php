<?php

class Publication {

  const FORM_FIELD_TYPE_TEXT = 0x0001;

  const FORM_FIELD_TYPE_INTEGER = 0x0002;

  const FORM_FIELD_TYPE_SELECT = 0x0003;

  const PERMISSION_EDIT = "publication_permission_edit";

  /**
   * @var array Associative array for generic handling of object variables.
   */
  private $variables = [];

  /**
   * @return array Associative array of form fields with according max_length
   *   values.
   */
  public static function get_field_length_restrictions() {
    $restrictions = [];
    foreach (self::attributes() as $key => $values) {
      if (array_key_exists('max_length', $values)) {
        $restrictions[$key] = $values['max_length'];
      }
    }
    return $restrictions;
  }

  /**
   * @return array An array keyed by the database column names, containing
   *   elements for Drupal form generation
   */
  private static function attributes() {
    return [
      'wg' => [
        'type' => 'checkboxes',
        'name' => 'Working Groups',
      ],
      'subproject' => [
        'type' => self::FORM_FIELD_TYPE_SELECT,
        'name' => 'Subproject',
      ],
      'open_access' => [
        'type' => self::FORM_FIELD_TYPE_SELECT,
        'name' => 'Open Access',
      ],
      'publication_type' => [
        'type' => self::FORM_FIELD_TYPE_SELECT,
        'name' => 'Publication Type',
      ],
      'pmid' => [
        'type' => self::FORM_FIELD_TYPE_INTEGER,
        'name' => 'PMID',
      ],
      'doi' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'DOI',
        'max_length' => '128',
      ],
      'publication_year' => [
        'type' => self::FORM_FIELD_TYPE_INTEGER,
        'name' => 'Publication Year',
      ],
      'title' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'Title',
      ],
      'journal_title' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'Journal',
      ],
      'issn' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'ISSN',
        'max_length' => '128',
      ],
      'essn' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'ESSN',
        'max_length' => '128',
      ],
      'url' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'URL',
      ],
      'pages' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'Pages',
        'max_length' => '128',
      ],
      'issue' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'Issue',
      ],
      'volume' => [
        'type' => self::FORM_FIELD_TYPE_INTEGER,
        'name' => 'Volume',
      ],
      'journal_abbr' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'Journal Abbreviation',
        'max_length' => '128',
      ],
      'extra' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'Extra',
      ],
      'authors' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'Authors',
      ],
      'first_author' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'First Author',
        'max_length' => '128',
      ],
      'last_author' => [
        'type' => self::FORM_FIELD_TYPE_TEXT,
        'name' => 'Last Author',
        'max_length' => '128',
      ],
    ];
  }

  /**
   * @param StdClass $object An object returned by a database query.
   *
   * @return \Publication
   */
  public static function translate_db_result($object) {
    $publication = new self();
    $object_vars = get_object_vars($object);
    foreach ($object_vars as $key => $value) {
      $publication->set($key, $value);
    }
    return $publication;
  }

  private function set($var, $value) {
    $this->variables[$var] = $value;
  }

  /**
   * Returns a relative location for the specified \Publication instance based
   * on the type of URL requested through the parameter.
   *
   * @param string $type
   *
   * @return string
   */
  public static function url_by_id($publication_id, $type = 'view') {
    $pub = PublicationRepository::findById($publication_id);
    return $pub->url($type);
  }

  /**
   * Returns a relative location based on the type of URL requested through the
   * parameter.
   *
   * @param string $type
   *
   * @return string
   */
  public function url($type = 'view') {
    switch ($type) {
      case "add_external_id":
        return str_replace('%', $this->getId(), SFB_LITERATURE_PAGE_ADD_EXTERNAL_ID);
        break;
      case "edit":
        return str_replace('%', $this->getId(), SFB_LITERATURE_PAGE_EDIT);
        break;
      case "view":
      default:
        break;
    }
    return str_replace('%', $this->getId(), SFB_LITERATURE_PAGE_VIEW);
  }

  /**
   * @param $authors
   *
   * @return string A string containing a list of authors shortened to contain
   *   first, second, and last author if original list contains more than 6.
   */
  public static function renderShortAuthorList($authors) {
    $authors_array = explode(',', $authors);
    if (count($authors_array) > 6) {
      $list = $authors_array[0] . ', '
        . $authors_array[1] . ', (...), '
        . array_pop($authors_array);
      return $list;
    }
    else {
      return $authors;
    }
  }

  public function getId() {
    return $this->get('id');
  }

  public function getDOI() {
    return $this->get('doi');
  }

  private function get($var) {
    return array_key_exists($var, $this->variables) ? $this->variables[$var] : NULL;
  }

  public function getFirstAuthor() {
    return $this->get('first_author');
  }

  /**
   * @return mixed
   */
  public function getPublicationType() {
    return $this->get('publication_type');
  }

  /**
   * @return mixed
   */
  public function getOpenAccess() {
    return $this->get('open_access');
  }

  /**
   * @return mixed
   */
  public function getPMID() {
    return $this->get('pmid');
  }


  /**
   * Check if the current user has access to this entity.
   *
   * @return bool
   */
  public function check_user_access() {
    $user = User::getCurrent();
    $user_wgs = $user->getUserWorkingGroups();

    /**
     * User has access.
     */
    if (in_array($this->get('wg'), $user_wgs)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Renders a link to edit the publication item if current user is permitted to edit.
   *
   * @param string $class CSS class to add to the link element.
   *
   * @return string Rendered HTML element
   */
  public function html_edit_link($class = '') {
    if ($this->check_permission(Publication::PERMISSION_EDIT)) {

      $css_class = !empty($class) ? [$class] : [];

      $link = l('<span class="glyphicon glyphicon-edit"></span>&nbsp;Edit',
        $this->url("edit"),
        ['html' => TRUE, 'attributes' => ['class' => $css_class, 'style' => ['white-space: nowrap;']]]);

      return $link;
    }
    else {
      return '';
    }
  }

  public function display_page() {
    /**
     * Load literature module common functions.
     */
    module_load_include('inc', 'sfb_literature', 'includes/common');

    $user = User::getCurrent();
    $user_wgs = $user->getUserWorkingGroups(TRUE);

    $publication_id = $this->get('id');

    $form = [];

    $form['back'] = [
      '#type' => 'link',
      '#title' => html_entity_decode('&#8617;', 0, 'UTF-8') .
        t(' Back to all publications'),
      '#href' => url('../literature'),
      // Nobody known why the parent directory is required
      '#attributes' => ['style' => 'display: block; margin-bottom: 10px;'],
    ];

    $editLink = $this->html_edit_link("btn btn-primary btn-sm");

    $set = createTextSet($form, 'Publication', $editLink, 'pubdata');

    $table = $this->display_table();

    $block_external_identifiers = PublicationExternalIdentifier::display_block($publication_id, TRUE, TRUE);

    $linked_antibodies = '';
    $linked_labnotebooks = '';
    try {
      // get linked antibodies
      $rdp_linking = RDPLinking::getLinkingByName('sfb_antibody', 'antibody_antibody2');
      if ($rdp_linking != NULL) {
        $icon_path = $GLOBALS['base_url'] . '/'
          . drupal_get_path('module', 'sfb_antibody') . '/icon32.png';
        $linked_antibodies = '<h4><img src="' . $icon_path . '" alt=""
                                    class="img-rounded" style="max-width: 32px;">
                                    Linked Antibodies</h4>';
        $antibodies_data_table = $rdp_linking->getViewData('pubreg_articles', $publication_id, TRUE);
        if(!empty($antibodies_data_table['table']['rows'])) { // if there are ABs
          $antibodies_data_table['table']['rows'] = Publication::sortLinkedABs($antibodies_data_table['table']['rows']);
          $linked_antibodies .= theme('table', [
            'header' => $antibodies_data_table['table']['header'],
            'rows' => $antibodies_data_table['table']['rows'],
          ]);
        }
        if (!$antibodies_data_table) {
          $linked_antibodies = NULL;
        }
      }

      // get linked lab notebooks
      $linked_labnotebooks = module_invoke('sfb_labnotebooks',
        'linking_get_items_by_publication', $publication_id, ['themed' => TRUE]);

    } catch (Exception $exception) {
      watchdog_exception(__CLASS__, $exception);
    }

    $semantic_markup = '<script type="application/ld+json">' . $this->semantic_markup() . '</script>';

    $form[$set]['table'] = [
      '#markup' => $table
        . $block_external_identifiers
        . $linked_labnotebooks
        . $linked_antibodies
        . $semantic_markup
    ];

    $form['edit_link']['#markup'] = $editLink;

    return $form;
  }

  private function display_table() {

    module_load_include('inc', 'sfb_commons', 'utils');

    $rows = [];

    foreach (self::get_form_fields() as $key => $type) {
      $value = array_key_exists($key, $attributes = $this->variables) ? $attributes[$key] : '';
      $publication_types = self::publication_types();
      $wg_shortname = $this->get('wg');

      switch ($key) {
        case 'doi':
          if (strlen($value) > 0) {
            $value = self::formatDOILink($value);
          }
          break;
        case 'wg':
          if ($wg_shortname !== '' and $wg_shortname !== NULL) {
            $value = '';
            $wgs = explode(', ', $wg_shortname);
            foreach ($wgs as $wg){
              $wg_object = WorkingGroupRepository::findByShortName($wg);
              $working_group_name = $wg_object->getName();

              $public_icon_path = WorkingGroupRepository::findByShortName($wg)
                ->getIconPath(WorkingGroup::ICON_SIZE_SMALL);
              $icon_element = [
                'path' => $public_icon_path,
                'title' => $working_group_name,
                'attributes' => [],
              ];
              $public_icon = theme_image($icon_element);
              $value .= $public_icon.' '.$working_group_name. ', ';
            }
            $value = substr($value,0,-2);
          }
          elseif ($this->get('wg') == NULL) {
            $value = '<span style="font-style:italic; color:#777;">Unassigned</span>';
          }
          break;
        case 'open_access':
          if ($this->get('open_access') == 0) {
            $value = 'No';
          }
          else {
            $path = base_path() . drupal_get_path('module', 'sfb_literature')
              . '/resources/open_access.svg';
            $icon = [
              'path' => $path,
              'height' => '18px',
              'title' => 'Open Access',
              'alt' => 'Open Access',
              'attributes' => [
                'style' => "vertical-align: top;",
              ],
            ];
            $icon = theme_image($icon) . '&nbsp;';
            $value = $this->get('open_access') == 1 ? $icon . 'Yes' : 'Unknown';
          }
          break;
        case 'publication_type';
          $value = $publication_types[$this->get('publication_type')];
          break;
        case 'authors':
          $value = StringEscapeHtml(AuthorListStr($value));
          break;
        case  'pmid':
          if (strlen($value) > 0) {
            $value = self::formatPMIDLink($value);
          }
          break;
        case 'url':
          if (preg_match('@^http(?:s)?://@i', $value)) {
            $value = LinkExtern($value);
          }
          else {
            $value = StringEscapeHtml($value);
          }
          break;
        case 'issue':
          if (empty($value) or $value == 'NULL') {
            $value = '';
          }
          break;
        case 'volume':
          if (empty($value)) {
            $value = '';
          }
          break;
        case 'subproject':
          if (empty($value)) {
            $value = '<span style="font-style:italic; color:#777;">None</span>';
          }
          break;
        default:
          $value = StringEscapeHtml($value);
          break;
      }

      $names = self::get_field_names();
      $rows[$key] = [
        'title' => sprintf('<span style="white-space: nowrap;">%s</span>', $names[$key]),
        'value' => $value,
      ];
    }

    $hook = 'rdp_publication_display_table_alter';

    foreach (module_implements($hook) as $module) {
      $rows = module_invoke($module, $hook, $this->getId(), $rows);
    }

    try {
      return theme('table', [
        'header' => ['Property', 'Value'],
        'rows' => $rows,
      ]);

    } catch (Exception $exception) {
      watchdog_exception(__CLASS__, $exception);
      return "";
    }

  }

  /**
   * List all fields and the according input form field type.
   *
   * @return array
   */
  public static function get_form_fields() {
    $fields = [];

    foreach (self::attributes() as $key => $value) {
      $fields[$key] = $value['type'];
    }

    return $fields;
  }
  /** sort antibodies so that all primaries are listed first then secondaries
   *
   * @param $abs array of linked antibodies
   * @return array */
  private static function sortLinkedABs($abs){
    $tmp_prim = array();
    $tmp_sec = array();
    foreach ($abs as $ab) {
      if ($ab[2] == 'primary')
        $tmp_prim [] = $ab;
      elseif ($ab[2] == 'secondary') $tmp_sec [] = $ab;
    }
    return array_merge($tmp_prim,$tmp_sec); }

  /**
   * @return array containing all publication types from pubmed
   */
  public static function publication_types() {
    //publication types
    $publication_types = [
      'adaptive_clinical_trial' => 'Adaptive Clinical Trial',    //pubmed
      'adresses' => 'Adresses', //pubmed
      'autobiography' => 'Autobiography', //pubmed
      'bibliography' => 'Bibliography', //pubmed
      'biography' => 'Biography',   //pubmed
      'case_report' => 'Case Report',   //pubmed
      'classical_article' => 'Classical Article',   //pubmed
      'clinical_conference' => 'Clinical Conference',   //pubmed
      'clinical_study' => 'Clinical Study',   //pubmed
      'clinical_trial' => 'Clinical Trial',   //pubmed
      'clinical_trial_phase_1' => 'Clinical Trial Phase I',   //pubmed
      'clinical_trial_phase_2' => 'Clinical Trial Phase II',   //pubmed
      'clinical_trial_phase_3' => 'Clinical Trial Phase III',   //pubmed
      'clinical_trial_phase_4' => 'Clinical Trial Phase IV',   //pubmed
      'clinical_trial_veterinary' => 'Clinical Trial, Veterinary',    //pubmed
      'collected_works' => 'Collected Works',   //pubmed
      'comparative_study' => 'Comparative Study',   //pubmed
      'congresses' => 'Congresses',   //pubmed
      'conference_abstract' => 'Conference Abstract', // datacite
      'consensus_development_conference' => 'Consensus Development Conference',   //pubmed
      'consensus_development_conference_nih' => 'Consensus Development Conference, NIH',   //pubmed
      'controlled_clinical_trial' => 'Controlled Clinical Trial',   //pubmed
      'dataset' => 'Dataset',   //pubmed
      'dictionary' => 'Dictionary',   //pubmed
      'directory' => 'Directory',   //pubmed
      'duplicate_publication' => 'Duplicate Publication',   //pubmed
      'editorial' => 'Editorial',   //pubmed
      'english_abstract' => 'English Abstract',   //pubmed
      'equivalence_trial' => 'Equivalence Trial',   //pubmed
      'evaluation_studies' => 'Evaluation Studies',   //pubmed
      'expression_of_concern' => 'Expression of Concern',   //pubmed
      'festschrift' => 'Festschrift',   //pubmed
      'government_publications' => 'Government Publications',   //pubmed
      'guideline' => 'Guideline',   //pubmed
      'historical_article' => 'Historical Article',   //pubmed
      'interactive_tutorial' => 'Interactive Tutorial',   //pubmed
      'interview' => 'Interview',   //pubmed
      'introductory_journal_article' => 'Introductory Journal Article',   //pubmed
      'journal_article' => 'Journal Article',   //pubmed & crossref
      'lectures' => 'Lectures',   //pubmed
      'legal_case' => 'Legal Case',   //pubmed
      'legislation' => 'Legislation',   //pubmed
      'letter' => 'Letter',   //pubmed
      'meta_analysis' => 'Meta-Analysis',   //pubmed
      'multicenter_study' => 'Multicenter Study',   //pubmed
      'news' => 'News',   //pubmed
      'newspaper_article' => 'Newspaper Article',   //pubmed
      'observational_study' => 'Obersational Study',   //pubmed
      'overall' => 'Overall',   //pubmed
      'patient_education_handout' => 'Patient Education Handout',   //pubmed
      'periodical_index' => 'Periodical Index',   //pubmed
      'personal_narratives' => 'Personal Narratives',   //pubmed
      'portraits' => 'Portraits',   //pubmed
      'practice_guidelines' => 'Practice Guidelines',   //pubmed
      'pragmatic_clinical_trial' => 'Pragmatic Clinical Trial',   //pubmed
      'publication_components' => 'Publication Components',   //pubmed
      'publication_formats' => 'Publication Formats',   //pubmed
      //'publication_type_category' => 'Publication Type Category',   //pubmed
      'randomized_controlled_trial' => 'Randomized Controlled Trial',   //pubmed
      'research_support_american_recovery_and_reinvestment_act' => 'Research Support, American Recovery and Reinvestment Act',   //pubmed
      'research_support_nih_extramural' => 'Research Support, N.I.H., Extramural',   //pubmed
      'research_support_nih_intramural' => 'Research Support, N.I.H., Intramural',   //pubmed
      'research_support_nonus_gov_research_support' => 'Research Support, Non-U.S. Gov\'t Research Support, U.S. Gov\'t, Non-P.H.S.',   //pubmed
      'research_support_us_gov' => 'Research Support, U.S. Gov\'t, P.H.S.',   //pubmed
      'review' => 'Review',   //pubmed
      'scientific_integrity_review' => 'Scientific Integrity Review',   //pubmed
      'study_characteristics' => 'Study Characteristics',   //pubmed
      'support_of_research' => 'Support of Research',   //pubmed
      'technical_support' => 'Technical Support',   //pubmed
      'twin_study' => 'Twin Study',   //pubmed
      'validation_studies' => 'Validation Studies',   //pubmed
      'video_audio_media' => 'Video-Audio Media',   //pubmed
      'webcasts' => 'Webcasts',   //pubmed
      'comment' => 'Comment',
      'meeting_report' => 'Meeting Report',
      'preprint' => 'Preprint',
      'correction' => 'Correction',
      'book_chapter' => 'Book Chapter',
    ];

    // sorts array contents alphanumerically (in place)
    asort($publication_types);

    // add "unknown" as first value in the array
    return array_merge(['unknown' => 'Unknown'], $publication_types);
  }

  /**
   * Create a HTML link resolving the DOI via dx.doi.org.
   *
   * @param string $doi The DOI to create a link of.
   *
   * @return string The HTML-formatted DOI-link.
   */
  public static function formatDOILink($doi) {
    return self::formatLink('DOI', $doi);
  }

  /**
   * Internal function to format ID-resolved external links.
   *
   * @param string $type Type of external link to format, i.e. "DOI", "PMID".
   * @param string $value The ID to resolve.
   *
   * @return string
   */
  private static function formatLink($type, $value) {

    switch ($type) {
      default:
      case 'DOI':
        $icon_name = 'DOI_logo.svg';
        $resolve_path = 'https://dx.doi.org/';
        $desc = 'DOI';
        break;
      case 'PMID':
        $icon_name = 'PubMed-Logo.svg';
        $resolve_path = 'https://www.ncbi.nlm.nih.gov/pubmed/';
        $desc = 'PubMed ID';
        break;
    }

    /**
     * Prepare icon
     */
    $path = base_path() . drupal_get_path('module', 'sfb_literature') . '/resources/' . $icon_name;
    $icon = [
      'path' => $path,
      'height' => '14px',
      'title' => $desc,
      'alt' => $desc,
      'attributes' => [
        'style' => "vertical-align: top;",
      ],
    ];
    $icon = theme_image($icon);

    /**
     * Assemble and return link
     */
    $l_display = $icon . '&nbsp;' . $value;
    $l_path = $resolve_path . $value;
    $l_attributes = ['html' => TRUE, 'attributes' => ['target' => '_blank']];
    return sprintf("<span style=\"white-space: nowrap\">%s</span>", l($l_display, $l_path, $l_attributes));
  }

  /**
   * Create a HTML link resolving the PMID.
   *
   * @param string $doi The PMID to create a link of.
   *
   * @return string The HTML-formatted PubMed-link.
   */
  public static function formatPMIDLink($pmid) {
    return self::formatLink('PMID', $pmid);
  }

  /**
   * List all fields and the according display label.
   *
   * @return array
   */
  public static function get_field_names() {
    $names = [];

    foreach (self::attributes() as $key => $value) {
      $names[$key] = $value['name'];
    }
    return $names;
  }

  /**
   * @url https://schema.org/ScholarlyArticle
   *
   * @return string JSON-LD formatted semantic representation of the
   *   publication item.
   */
  private function semantic_markup() {

    $factory = new PublicationMetadataFactory($this);
    $markup = $factory->getMarkup();

    return $markup;
  }

  public function __toString() {
    return json_encode($this->variables);
  }

  public function toStdClass() {
    $object = new StdClass();
    foreach ($this->variables as $key => $value) {
      $object->$key = $value;
    }
    return $object;
  }

  /**
   * Checks if the current (global) Drupal user has a certain permission
   * regarding the current @see \Publication instance.
   *
   * @param string $permission One of the "permission" constants defined by the
   *
   * @see \Publication class
   *
   * @return bool TRUE if permission is granted, FALSE otherwise.
   */
  public function check_permission($permission) {

    /**
     * Site admin should be granted all the permissions.
     */
    if (user_access("administer site configuration")) {
      return TRUE;
    }

    global $user;
    $rdp_user = UsersRepository::findByUid($user->uid);

    switch ($permission) {
      case self::PERMISSION_EDIT:
        $user_has_permission = user_access(SFB_LITERATURE_PERMISSION_CREATE);

        $publication_wg = $this->getWorkingGroup();
        $wgs = explode(', ',$publication_wg);
        $user_groups = $rdp_user->getUserWorkingGroups(TRUE);
        $user_is_group_member = FALSE;
        foreach ($wgs as $wg) {
          if (in_array($wg, $user_groups)){
            $user_is_group_member = TRUE;
          }
        }
        if ($user_has_permission && $user_is_group_member) {
          return TRUE;
        }
        break;
      default:
        return user_access(SFB_LITERATURE_PERMISSION_DEFAULT);
        break;
    }

    return FALSE;
  }

  /**
   * @return string The short name of @see \WorkingGroup associated with the
   *   publication item.
   */
  public function getWorkingGroup() {
    return $this->get('wg');
  }

  /**
   * @return mixed|null
   */
  public function getSubproject() {
    return $this->get('subproject');
  }

  /**
   * @return string
   */
  public function getPublicationYear() {
    return $this->get('publication_year');
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->get('title');
  }

  /**
   * @return string
   */
  public function getUrl() {
    return $this->get('url');
  }

  public function json() {
    return $this->semantic_markup();
  }

}
