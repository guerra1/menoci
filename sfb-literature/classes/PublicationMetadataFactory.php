<?php
/**
 * @file Translate Publication instances into linked data / semantic web
 *   representation.
 */

/**
 * Class PublicationMetadataFactory
 */
class PublicationMetadataFactory {

  const SCHEMA_JSONLD = "JSON Linked Data";

  /**
   * @var \Publication
   */
  private $publication;

  /**
   * @var string
   */
  private $schema;

  public function __construct(
    \Publication $publication,
    $schema = self::SCHEMA_JSONLD
  ) {
    $this->publication = $publication;
    $this->schema = $schema;
  }

  public function getMarkup() {

    $url = url($this->publication->url(), ['absolute' => TRUE]);
    $first_author = htmlspecialchars($this->publication->getFirstAuthor());
    $date = $this->publication->getPublicationYear();
    $doi = $this->publication->getDOI();
    $pmid = $this->publication->getPMID();
    $title = htmlspecialchars($this->publication->getTitle());

    /**
     * MeSH publication types are referenced, see https://www.nlm.nih.gov/mesh/pubtypes.html
     * Use the human-readable pub-type string instead of the escaped form stored in the database field
     */
    $type = Publication::publication_types()[$this->publication->getPublicationType()];

    /**
     * Explicitly mark Dataset type entries as https://schema.org/Dataset instances
     * for Google-Dataset search indexing
     */
    $schema_org_type = $type == 'Dataset' ? 'Dataset' : 'ScholarlyArticle';

    $publication_id = $this->publication->getId();


    $linked_objects_parts = module_invoke_all('literature_json_get', $publication_id);
    $linked_objects = json_encode($linked_objects_parts);

    $markup = /** @lang JSON */
      "{
  \"@context\": {
    \"@vocab\": \"https://schema.org/\",
    \"dc\": \"http://purl.org/dc/elements/1.1/\",
    \"pubtype\": \"https://www.nlm.nih.gov/mesh/pubtypes.html\"
    },
  \"@id\": \"$url\",
  \"@type\": \"$schema_org_type\",
  \"pubtype\":\"$type\",
  \"creator\": [
    {
      \"@type\": \"Person\",
      \"name\": \"$first_author\"
    }
  ],
  \"datePublished\": \"$date\",
  \"headline\": \"$title\",
  \"url\": \"$url\",
  \"sameAs\": \"http://dx.doi.org/$doi\",
  \"identifier\": [
    {
      \"@type\": \"PropertyValue\",
      \"name\": \"DOI\",
      \"value\": \"$doi\",
      \"sameAs\": \"http://dx.doi.org/$doi\"
    },
    {
      \"@type\": \"PropertyValue\",
      \"name\": \"PMID\",
      \"value\": \"$pmid\",
      \"sameAs\": \"https://www.ncbi.nlm.nih.gov/pubmed/$pmid\"
    } 
  ],
  \"dc:title\": \"$title\",
  \"dc:creator\": \"$first_author\",
  \"dc:relation\": $linked_objects
}";


    return $markup . "\n";
  }
}