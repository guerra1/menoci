<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function rdp_archive_install() {
  // do something
}

function rdp_archive_uninstall() {
  // do something
}

/**
 * Implements @see \hook_schema().
 */
function rdp_archive_schema() {
  $schema['rdp_archive'] = [
    'description' => t(''),
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique ID.',
      ],
      'name' => [
        'description' => 'Display name of the Archive.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ],
      'owner' => [
        'description' => "The {users}.uid of the associated user.",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
      ],
      'group_id' => [
        'description' => 'The internal ID of associated WorkingGroup object.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => FALSE,
        'default' => -1,
      ],
      'server' => [
        'description' => 'The internal ID of associated CdstarServer object.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'sharing_level' => [
        'description' => 'SharingLevel of the Archive.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'status' => [
        'description' => 'Status of the Archive.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'pid_service_id' => [
        'description' => 'The internal ID of associated PID Service.',
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
        'default' => -1,
      ],
    ],
    'primary key' => ['id'],
    'unique keys' => [
      'name' => ['name'],
    ],
    'foreign keys' => [
      'rdp_archive_fk_server' => [
        'table' => 'cdstar_server',
        'columns' => ['server' => 'id'],
      ],
      'rdp_archive_fk_owner' => [
        'table' => 'users',
        'columns' => ['owner' => 'uid'],
      ],
    ],
  ];

  $schema['rdp_archive_object'] = [
    'description' => t(''),
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique ID.',
      ],
      'archive_id' => [
        'description' => 'The internal ID of associated Archive object.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ],
      'cdstar_object_id' => [
        'description' => 'A CDSTAR Object ID.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
      ],
      'pid_id' => [
        'description' => 'The internal ID of associated PID object.',
        'type' => 'varchar',
        'length' => 127,
        'not null' => FALSE,
        'default' => '',
      ],
      'sharing_level' => [
        'description' => 'SharingLevel of the ArchiveObject.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'context' => [
        'description' => 'Integration context of the ArchiveObject.',
        'type' => 'varchar',
        'length' => 127,
        'not null' => TRUE,
        'default' => 'Generic Research Data Archive',
      ],
      'working_group_id' => [
        'description' => 'Internal ID of the associated Working Group.',
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
        'default' => NULL,
      ],
    ],
    'primary key' => ['id'],
    'foreign keys' => [
      'rdp_archive_object_fk_archive' => [
        'table' => 'rdp_archive',
        'columns' => ['archive_id' => 'id'],
      ],
      'rdp_archive_object_fk_cdstarobject' => [
        'table' => 'cdstar_object',
        'columns' => ['cdstar_object_id' => 'id'],
      ],
    ],
  ];

  return $schema;
}

/**
 * Adds pid_service_id field to database table rdp_archive.
 */
function rdp_archive_update_7001() {
  $table = 'rdp_archive';
  $field = 'pid_service_id';
  $spec = [
    'description' => 'The internal ID of associated PID Service.',
    'type' => 'int',
    'unsigned' => FALSE,
    'not null' => TRUE,
    'default' => -1,
  ];

  db_add_field($table, $field, $spec);

}

/**
 * Adds context field to database table rdp_archive_object.
 */
function rdp_archive_update_7002() {
  $table = 'rdp_archive_object';
  $field = 'context';

  $spec = [
    'description' => 'Integration context of the ArchiveObject.',
    'type' => 'varchar',
    'length' => 127,
    'not null' => TRUE,
    'default' => 'Generic Research Data Archive',
  ];
  /**
   * Add new database field.
   */
  db_add_field($table, $field, $spec);

  /**
   * Create initial content for existing database entries.
   *
   * Update rows where field is empty or null.
   */
  $db_or = db_or()
    ->isNull($field)
    ->condition($field, '');

  db_update($table)
    ->fields([$field => 'Generic Research Data Archive'])
    ->condition($db_or)
    ->execute();
}

/**
 * Update PID field of database table rdp_archive_object (change type from INT
 * to VARCHAR).
 */
function rdp_archive_update_7003() {
  $table = 'rdp_archive_object';
  $field = 'pid_id';

  $spec = [
    'description' => 'The internal ID of associated PID object.',
    'type' => 'varchar',
    'length' => 127,
    'not null' => FALSE,
    'default' => '',
  ];

  /**
   * Update database field.
   */
  db_change_field($table, $field, $field, $spec);
}

/**
 * Add working_group_id field to database table rdp_archive_object
 */
function rdp_archive_update_7004() {
  $table = 'rdp_archive_object';
  $field = 'working_group_id';

  $spec = [
    'description' => 'Internal ID of the associated Working Group.',
    'type' => 'int',
    'not null' => FALSE,
    'unsigned' => TRUE,
    'default' => NULL,
  ];

  /**
   * Add database field.
   */
  if (!db_field_exists($table, $field)) {
    db_add_field($table, $field, $spec);
  }
  else {
    db_change_field($table, $field, $field, $spec);
  }
}

