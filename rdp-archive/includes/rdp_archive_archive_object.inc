<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * ArchiveObject title callback
 */
function rdp_archive_archive_object_title($object_id) {
  $object = ArchiveObjectRepository::findById($object_id);
  $cdstarObject = CdstarObjectRepository::findById($object->getCdstarObjectId());
  $display_name = $cdstarObject->getDisplayName();

  return t("Object: !n", ['!n' => $display_name]);
}

/**
 *
 */
function rdp_archive_archive_object_view($object_id) {
  $object = ArchiveObjectRepository::findById($object_id);

  if ($object->isEmpty()) {
    drupal_not_found();
  }
  drupal_set_breadcrumb([l('Home', '/'), l("Research Data Archive", RDP_ARCHIVE_URL_ARCHIVE_DEFAULT)]);
  //rdp_archive_fix_archive_breadcrumb($object->getArchiveId());

  $renderer = new ArchiveObjectRenderer($object);

  return $renderer->displayPage();
}

/**
 * Drupal form to create a new object within an archive
 *
 * Implements @param $archive_id
 *
 * @return array Drupal form
 * @see \hook_form()
 *
 */
function rdp_archive_archive_object_create($form, &$form_state, $archive_id) {

  rdp_archive_fix_archive_breadcrumb($archive_id);

  $object = new ArchiveObject();
  $object->setArchiveId($archive_id);

  $form = $object->getForm();

  $form_state['archive_id'] = $archive_id;

  return $form;
}

/**
 * Drupal form handler.
 */
function rdp_archive_archive_object_create_submit($form, &$form_state) {

  $values = $form_state['values'];
  $archive = ArchiveRepository::findById($form_state['archive_id']);

  $archiveObject = new ArchiveObject();
  $archiveObject->setArchiveId($form_state['archive_id']);
  $archiveObject->setSharingLevel($values['sharing_level']);

  $cdstarObject = new CdstarObject();
  $cdstarObject->setDisplayName($values['display_name']);
  $cdstarObject->setServer($archive->getServer());
  $cdstarObject->generateBasicMetadata();
  $cdstarObject->save();

  // Upload file if a file was specified by user
  $upload_fieldname = 'file_upload';
  if ($_FILES['files']['error'][$upload_fieldname] !== UPLOAD_ERR_NO_FILE) {
    $cdstarObject->addFileFormAction($upload_fieldname);
    $cdstarObject->save();
  }

  $archiveObject->setCdstarObjectId($cdstarObject->getId());
  $archiveObject->save();

  $form_state['redirect'] = $archive->url();
}

/**
 * Drupal form to upload a zip/tar archive file and create a new \ArchiveObject
 *
 * @param $archive_id
 *
 * @return array Drupal form
 */
function rdp_archive_archive_object_create_upload($form, &$form_state, $archive_id) {

  rdp_archive_fix_archive_breadcrumb($archive_id);

  $object = new ArchiveObject();

  $form = $object->getForm(ArchiveObject::FORM_UPLOAD);

  $form_state['archive_id'] = $archive_id;

  return $form;
}


/**
 * Drupal form validation. Checks if indicated file is of supported MIME type.
 */
function rdp_archive_archive_object_create_upload_validate($form, &$form_state) {

  /**
   * Class method handles validation of file upload field.
   */
  ArchiveObject::formCreateUploadValidate();
}


/**
 * Drupal form handler.
 */
function rdp_archive_archive_object_create_upload_submit($form, &$form_state) {

  $archive_id = $form_state['archive_id'];
  $sharing_level = $form_state['values']['sharing_level'];

  $archiveObject = new ArchiveObject();
  $archiveObject->formCreateUploadSubmit($archive_id, $sharing_level);

  $archive = ArchiveRepository::findById($archiveObject->getArchiveId());

  $form_state['redirect'] = $archive->url();
}

/**
 * Drupal confirm form to delete ArchiveObject
 */
function rdp_archive_archive_object_delete($form = [], &$form_state, $id) {
  $object = ArchiveObjectRepository::findById($id);
  if ($object->isLocked()) {
    drupal_set_message("This dataset must not be deleted because it has been registered under a
     public Persistent Identifier.", 'warning');
    drupal_goto($object->url());
  }
  $archive = ArchiveRepository::findById($object->getArchiveId());
  $name = $object->getDisplayName();

  $form_state['object_id'] = $object->getId();
  return confirm_form($form, t("Delete the ArchiveObject %n?", ['%n' => $name]), $archive->url());
}

/**
 * Drupal form handler. Triggers ArchiveObject delete.
 */
function rdp_archive_archive_object_delete_submit($form, &$form_state) {
  $id = $form_state['object_id'];
  $object = ArchiveObjectRepository::findById($id);
  $archive = ArchiveRepository::findById($object->getArchiveId());

  $object->delete();

  $form_state['redirect'] = ($archive->url());
}

function rdp_archive_archive_object_addfile_form($form, &$form_state, $dataset_id) {
  $form = [];
  $form['file_fieldset'] = [
    '#type' => 'fieldset',
    '#title' => 'Add new file',
    '#collapsible' => TRUE,
    '#collapsed' => '#true',
  ];
  $form['file_fieldset'][] = CdstarObject::getFormAddFile('rdp_archive_archive_object_addfile_form_submit');
  $form_state['dataset_id'] = $dataset_id;

  return $form;
}

function rdp_archive_archive_object_addfile_form_submit($form, &$form_state) {
  $archiveObject = ArchiveObjectRepository::findById($form_state['dataset_id']);
  $object = $archiveObject->getCdstarObject();
  $object->addFileFormAction();
  $form_state['redirect'] = $archiveObject->url();
}

function rdp_archive_archive_object_pid($archive_object_id) {

  $archiveObject = ArchiveObjectRepository::findById($archive_object_id);

  $archive = ArchiveRepository::findById($archiveObject->getArchiveId());
  if (!$pid_service_id = $archive->getPidServiceId()) {
    $pid_service_id = variable_get(RDP_ARCHIVE_VAR_DEFAULT_PID_SERVICE, 0);
  }
  if (!$pid_service_id) {
    drupal_set_message("No service provider for PID registration has been 
    configured. Please inform your system administrator.");
    watchdog("RDP Archive", "No default PID service configured", [], WATCHDOG_CRITICAL);
    drupal_goto($archiveObject->url());
  }

  $archiveObject->registerPid();
  drupal_set_message("EPIC PID registered!");

  drupal_goto($archiveObject->url());
}
