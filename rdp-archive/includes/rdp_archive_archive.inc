<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Title callback function for single Archive display page.
 *
 * @return string
 */
function rdp_archive_archive_view_title($archive_id) {
  $archive = ArchiveRepository::findById($archive_id);
  $name = $archive->getName();

  return t("!n", ['!n' => $name]);
}


/**
 * Display single Archive information.
 *
 * @param $archive_id
 *
 * @return string
 */
function rdp_archive_archive_view($archive_id) {

  $user_current = User::getCurrent();
  $archive = ArchiveRepository::findById($archive_id);

  if ($user_current->getUid() == $archive->getOwner()) {
    $table = drupal_get_form('rdp_archive_archive_view_tableselect', $archive_id);
    $output = '<h2>Objects in the Archive</h2>' . drupal_render($table);
  }
  else {
    $archiveObjects = ArchiveObjectRepository::findAllByArchiveId($archive_id);
    $header = ArchiveObjectRenderer::tableHeader();
    $data = [];
    foreach ($archiveObjects as $archiveObject) {
      $renderer = new ArchiveObjectRenderer($archiveObject);
      $data[] = $renderer->tableRow();
    }
    $output = '<h2>Objects in the Archive</h2>';
    try {
      $output .= theme('table', ['header' => $header, 'rows' => $data]);
      $output .= theme('pager');
    } catch (Exception $e) {
      watchdog_exception('rdp_archive', $e);
    }
  }
  return $output;

}

function rdp_archive_archive_view_tableselect($form, &$form_state, $archive_id) {
  $archiveObjects = ArchiveObjectRepository::findAllByArchiveId($archive_id);

  $header = ArchiveObjectRenderer::tableHeader();
  $options = [];

  foreach ($archiveObjects as $archiveObject) {
    $renderer = new ArchiveObjectRenderer($archiveObject);
    $options[$archiveObject->getId()] = $renderer->tableRow();
  }

  $form['table'] = [
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
  ];

  $form['action'] = [
    '#type' => 'fieldset',
    '#title' => 'Actions',
    '#collapsible' => TRUE,
    '#attributes' => ['class' => ['form-inline']],
  ];

  $form['action']['action_select'] = [
    '#type' => 'select',
    '#options' => ['delete' => 'delete'],
    '#empty_option' => t("-- please select an action to perform --"),
    '#suffix' => '<larger><bold>&nbsp;all selected items.&nbsp;</bold></larger>',
  ];

  $form['action']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#attributes' => [
      'class' => ['btn-sm'],
      //'onclick' => 'if(!confirm(\'Warning: You are about to delete multiple Items! Continue?\')){return false;}',
    ],
  ];

  return $form;
}

function rdp_archive_archive_view_tableselect_submit($form, &$form_state) {
  $values = $form_state['values'];

  switch ($values['action_select']) {
    case 'delete':
      $delete_ids = array_filter($values['table']);
      foreach ($delete_ids as $id) {
        $archive_object = ArchiveObjectRepository::findById($id);
        $result = $archive_object->delete();
        if (!$result) {
          drupal_set_message("Failed to delete ArchiveObject, ID=$id.", "error");
        }
        else {
          drupal_set_message("Deleted ArchiveObject, ID=$id.");
        }
      }
      break;
    default:
      break;
  }
}

/**
 * Create new Archive object.
 *
 * Implements @return array
 *
 * @see \hook_form().
 *
 */
function rdp_archive_archive_create($form, &$form_state) {

  $archive = new Archive();

  return $archive->getForm();
}

/**
 * New Archive form submit action handling.
 */
function rdp_archive_archive_create_submit($form, &$form_state) {

  $values = $form_state['values'];
  $archive = new Archive();

  $archive->setName($values['name']);
  $archive->setOwner($values['owner']);
  $archive->setGroupId($values['group_id']);
  $archive->setServer($values['server']);
  $archive->setSharingLevel($values['sharing_level']);
  $archive->setStatus(Archive::ARCHIVE_DISABLED);
  $archive->setPidServiceId($values['pid_service_id']);
  $archive->save();

  //  $form_state['redirect'] = $archive->url();
  $form_state['redirect'] = RDP_ARCHIVE_URL_CONFIG_OVERVIEW;
}

/**
 * Edit Archive object.
 *
 * Implements @return array
 *
 * @see \hook_form().
 *
 */
function rdp_archive_archive_edit($form, &$form_state, $archive_id) {

  $archive = ArchiveRepository::findById($archive_id);
  $form_state['archive_id'] = $archive_id;

  return $archive->getForm("edit");
}

/**
 * Edit Archive form submit action handling.
 */
function rdp_archive_archive_edit_submit($form, &$form_state) {

  $values = $form_state['values'];
  $archive = ArchiveRepository::findById($form_state['archive_id']);

  $archive->setName($values['name']);
  $archive->setOwner($values['owner']);
  $archive->setGroupId($values['group_id']);
  $archive->setServer($values['server']);
  $archive->setSharingLevel($values['sharing_level']);
  $archive->setStatus($values['status']);
  $archive->setPidServiceId($values['pid_service_id']);
  $archive->save();

  //  $form_state['redirect'] = $archive->url();
  $form_state['redirect'] = RDP_ARCHIVE_URL_CONFIG;
}


/**
 * Archive forms' cancel action handling. Redirects to appropriate pages.
 */
function rdp_archive_cancel($form, &$form_state) {

  //  if(isset($form_state['archive_id'])){
  //    $archive = ArchiveRepository::findById($form_state['archive_id']);
  //    $form_state['redirect'] = $archive->url();
  //    $form_state['redirect'] = $archive->url();
  //  }
  //  else {
  $form_state['redirect'] = RDP_ARCHIVE_URL_CONFIG;
  //  }
}