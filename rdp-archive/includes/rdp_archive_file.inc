<?php

function rdp_archive_file_download($archive_object_id, $file_id) {

  $archiveObject = ArchiveObjectRepository::findById($archive_object_id);
  if (!$archiveObject->checkReadPermission()) {
    drupal_access_denied();
    exit();
  }

  $object = $archiveObject->getCdstarObject();

  /**
   * @var CdstarFile $cdstarfile
   */
  $cdstarfile = $object->getFiles()[$file_id];
  $cdstarfile->download();
}

function rdp_archive_file_delete($archive_object_id, $file_id) {
  $archiveObject = ArchiveObjectRepository::findById($archive_object_id);
  $object = $archiveObject->getCdstarObject();

  /* @var \CdstarFile $cdstarfile */
  $cdstarfile = $object->getFiles()[$file_id];

  if ($object->removeFile($cdstarfile)) {
    drupal_set_message(t('File :filename deleted successfully.',
      [':filename' => $cdstarfile->getFilename()]));
  }

  drupal_goto($archiveObject->url());
}
