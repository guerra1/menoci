<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

interface ArchiveObjectInterface {

  const EMPTY_ID = -1;

  const FORM_NEW = 'form_new';

  const FORM_UPLOAD = 'form_upload';

  /**
   * @return bool
   */
  public function isEmpty();

  /**
   * @return void
   */
  public function save();

  /**
   * @return void
   */
  public function delete();

  /**
   * @return int
   */
  public function getId();

  /**
   * @param int $id
   *
   * @return void
   */
  public function setId($id);

  /**
   * @return int
   */
  public function getArchiveId();

  /**
   * @param int $archive_id
   *
   * @return void
   */
  public function setArchiveId($archive_id);

  /**
   * @return int
   */
  public function getSharingLevel();

  /**
   * @param int $sharing_level
   *
   * @return void
   */
  public function setSharingLevel($sharing_level);

  /**
   * @return string
   */
  public function getPidId();

  /**
   * @param string $id
   *
   * @return void
   */
  public function setPidId($id);

  /**
   * @return string
   */
  public function getCdstarObjectId();

  /**
   * @param string $cdstar_object_id
   *
   * @return void
   */
  public function setCdstarObjectId($cdstar_object_id);

  /**
   * @return string
   */
  public function getContext();

  /**
   * @param string $context
   *
   * @return void
   */
  public function setContext($context);

  /**
   * @return int
   */
  public function getWorkingGroupID();

  /**
   * @param int $working_group_id
   *
   * @return void
   */
  public function setWorkingGroupID($working_group_id);

  /**
   * @param string $type
   *
   * @return array Drupal form array.
   */
  public function getForm($type);

  /**
   * @param string $type
   *
   * @return string
   */
  public function url($type);
}