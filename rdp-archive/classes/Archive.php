<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class Archive {

  const EMPTY_ID = -1;

  const ARCHIVE_ENABLED = 1;

  const ARCHIVE_DISABLED = 0;

  /**
   * Placeholder value for "personal" group, i.e. an Archive that is not
   * associated with any RDP WorkingGroup but that allows users to store
   * Objects in a personal space.
   */
  const ARCHIVE_GROUP_PERSONAL = -1;

  /**
   * @var int Archive ID. Set to EMPTY_ID constant for instances not saved in
   * database.
   */
  private $id = self::EMPTY_ID;

  /**
   * @var string
   * A human readable name for the Archive.
   */
  private $name;

  /**
   * @var int owner uid (Drupal user ID)
   */
  private $owner;

  /**
   * @var int owning group id (RDP WorkingGroup ID). "Personal" group by default
   */
  private $group_id = self::ARCHIVE_GROUP_PERSONAL;

  /**
   * @var int associated CdstarServer ID
   */
  private $server;

  /**
   * @var int SharingLevel
   */
  private $sharing_level = SharingLevel::PRIVATE_LEVEL;

  /**
   * @var int
   */
  private $status = self::ARCHIVE_DISABLED;

  private $pid_service_id;

  /**
   * Archive constructor.
   *
   * @param int $owner
   * @param int $server
   */
  public function __construct($server = CdstarServer::EMPTY_ID, $owner = -1) {

    // Get Drupal User object or default to current User
    if (!$user = user_load($owner)) {
      global $user;
    }
    $this->owner = $user->uid;

    // Get CdstarServer ID if given parameter resolves to a valid instance
    if ($server = CdstarServerRepository::findById($server)) {
      // returns empty CdstarServer instance if no match in repository
      if (!$server->isEmpty()) {
        $this->server = $server->getId();
      }
    }
  }

  /**
   * @return bool True if instance has not been saved to Repository/Database.
   */
  public function isEmpty() {
    if ($this->id === self::EMPTY_ID) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Build table headers compatible with Drupal table theme and
   *
   * @see \TableSort extension.
   *
   * @return array Table column headers for simple table display with Drupal
   *   table theme
   */
  public static function tableHeader() {
    return [
      ['data' => t('Archive'), 'field' => 'name'],
      ['data' => t('Server'), 'field' => 'server'],
      ['data' => t('Owner'), 'field' => 'owner'],
      //['data' => t('Group'), 'field' => 'group'],
      t('Group'),
      ['data' => t('Sharing Level'), 'field' => 'sharing_level'],
      ['data' => t('Status'), 'field' => 'status'],
      t('Options'),
    ];
  }

  /**
   * @return array human-readable data fields for simple table row display with
   *   Drupal table theme.
   */
  public function tableRow() {
    $serverlabel = CdstarServerRepository::findById($this->server)->getLabel();
    $ownerlabel = user_load($this->owner)->name;

    if ($this->group_id == self::ARCHIVE_GROUP_PERSONAL) {
      $grouplabel = 'Personal Archive';
    }
    else {
      $group = WorkingGroupRepository::findById($this->group_id);
      $grouplabel = '<img href="' . base_path() . '#" src="' . $group->getIconPath() . '">&nbsp;' .
        l($group->getName(), '#'); // ToDo: links in image and text!
    }

    $view_icon = l(CdstarResources::GLYPHICON_VIEW, $this->url(), ['html' => TRUE]);
    $edit_icon = '';
    if (user_access(RDP_ARCHIVE_PERMISSION_CONFIGURATION)) {
      $edit_icon = '&nbsp;' . l(CdstarResources::GLYPHICON_EDIT, $this->url("edit"), ['html' => TRUE]);
    }

    return [
      l($this->name, $this->url()),
      $serverlabel,
      $ownerlabel,
      $grouplabel,
      SharingLevel::getName($this->sharing_level),
      $this->status,
      $view_icon . $edit_icon,
    ];
  }

  /**
   * Save new or existing instance to repository.
   */
  public function save() {
    $id = ArchiveRepository::save($this);


    // Update id value if was empty before and repository saving was successful.
    if ($this->isEmpty() && $id) {
      $this->id = $id;
    }
  }

  /**
   * ToDo: implement & doc
   *
   * @param string $type
   *
   * @return array
   */
  public function getForm($type = "new") {
    $form = [];

    $form['name'] = $this->getFormField('name');
    $form['owner'] = $this->getFormField('owner');
    $form['group_id'] = $this->getFormField('group_id');
    $form['server'] = $this->getFormField('server');
    $form['pid_service_id'] = $this->getFormField('pid_service_id');

    $form['sharing'] = ['#type' => 'fieldset', '#title' => t("Sharing Level")];
    $form['sharing']['sharing_level'] = $this->getFormField('sharing_level');
    $form['sharing']['sharing_level']['#title'] = '';

    if ($type == 'edit') {
      $form['status'] = $this->getFormField('status');
      $form['status']['#prefix'] = '<div class="panel panel-primary"><div class="panel-body">';
      $form['status']['#suffix'] = '</div></div>';
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    $form['cancel'] = [
      '#type' => 'button',
      '#submit' => ['rdp_archive_cancel'],
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
      '#limit_validation_errors' => [],
      '#weight' => 20,
    ];

    return $form;
  }

  /**
   * ToDo: doc
   *
   * @param $fieldname
   *
   * @return array
   */
  private function getFormField($fieldname) {
    switch ($fieldname) {

      case 'name':
        $default = $this->name;
        return [
          '#type' => 'textfield',
          '#title' => t('Archive Name'),
          '#description' => t('Choose a display name for the Archive.'),
          '#default_value' => $default,
          '#required' => TRUE,
        ];

      case 'owner':
        $users = UsersRepository::findAll();
        $options = [];
        foreach ($users as $user) {
          $options[$user->getUid()] = $user->getName();
        }
        return [
          '#type' => 'select',
          '#title' => t('Owner'),
          '#description' => t('Appoint an owner for the Archive.'),
          '#options' => $options,
          '#default_value' => $this->owner,
          '#required' => TRUE,
        ];

      case 'group_id':
        $groups = WorkingGroupRepository::findAll();
        $options = [];
        $options[self::ARCHIVE_GROUP_PERSONAL] = 'Personal Archive';
        foreach ($groups as $group) {
          $options[$group->getId()] = $group->getName();
        }
        if (!empty($this->group_id)) {
          $default = $this->group_id;
        }
        return [
          '#type' => 'select',
          '#title' => t('Group'),
          '#description' => t('Which group does the Archive belong to? 
          Choose "Personal Archive" if only the owner should have access by 
          default.'),
          '#options' => $options,
          '#default_value' => $default,
          '#required' => TRUE,
        ];

      case 'server':
        $servers = CdstarServerRepository::findAll();
        $options = [];
        foreach ($servers as $server) {
          $options[$server->getId()] = $server->getLabel();
        }
        $field = [
          '#type' => 'select',
          '#title' => t('CDSTAR Server'),
          '#description' => t('Choose a CDSTAR storage service.'),
          '#options' => $options,
          '#default_value' => $this->server,
          '#required' => TRUE,
        ];

        return $field;

      case 'sharing_level':
        $field = SharingLevel::getFormField();
        $field['#default_value'] = $this->sharing_level;
        return $field;

      case 'status':
        $default = FALSE;
        if ($this->status == self::ARCHIVE_ENABLED) {
          $default = TRUE;
        }
        return [
          '#title' => t('Enable Archive'),
          '#description' => t('Allows users with according permission to create Objects within the Archive.'),
          '#type' => 'checkbox',
          '#default_value' => $default,
        ];

      case 'pid_service_id':
        if (module_exists('epic_pid')) {
          $services = EpicPidServiceRepository::findAll();
          $options = [];
          $options[EpicPidService::EMPTY_ID] = 'None';
          foreach ($services as $service) {
            $options[$service->getId()] = $service->getName();
          }
          $default = EpicPidService::EMPTY_ID;
          if (!empty($this->pid_service_id)) {
            $default = $this->pid_service_id;
          }
          return [
            '#type' => 'select',
            '#title' => t('Persistent Identifier Service'),
            '#description' => t('Choose a PID service to connect to this Archive.'),
            '#options' => $options,
            '#default_value' => $default,
            '#required' => TRUE,
          ];
        }
        else {
          return [];
        }
    }
  }


  /**
   * ToDo: doc
   */
  public function url($type = "view") {
    switch ($type) {
      case 'view':
        return RDP_ARCHIVE_URL_ARCHIVE_DEFAULT . '/' . $this->id;
      case 'edit':
        return RDP_ARCHIVE_URL_ARCHIVE_DEFAULT . '/' . $this->id . '/edit';
    }
  }

  public function isEnabled() {
    return $this->status == self::ARCHIVE_ENABLED ? TRUE : FALSE;
  }

  public function json() {
    return json_encode([
      'id' => $this->id,
      'name' => $this->name,
      'owner' => $this->owner,
      'group_id' => $this->group_id,
      'server' => $this->server,
      'sharing_level' => $this->sharing_level,
      'status' => $this->status,
    ]);
  }

  /****************************** GETTERS AND SETTERS *************************/

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return int
   */
  public function getOwner() {
    return $this->owner;
  }

  /**
   * @param int $owner
   */
  public function setOwner($owner) {
    $this->owner = $owner;
  }

  /**
   * @return int
   */
  public function getGroupId() {
    return $this->group_id;
  }

  /**
   * @param int $group_id
   */
  public function setGroupId($group_id) {
    $this->group_id = $group_id;
  }

  /**
   * @return int
   */
  public function getServer() {
    return $this->server;
  }

  /**
   * @param int $server
   */
  public function setServer($server) {
    $this->server = $server;
  }

  /**
   * @return int
   */
  public function getSharingLevel() {
    return $this->sharing_level;
  }

  /**
   * @param int $sharing_level
   */
  public function setSharingLevel($sharing_level) {
    // make sure parameter is a valid sharing level
    if (SharingLevel::isSharingLevel($sharing_level)) {
      $this->sharing_level = $sharing_level;
    }
  }

  /**
   * @return int
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param int $status
   */
  public function setStatus($status) {
    // make sure parameter is a valid status code
    if (in_array($status, [self::ARCHIVE_DISABLED, self::ARCHIVE_ENABLED])) {
      $this->status = $status;
    }
  }

  /**
   * @return mixed
   */
  public function getPidServiceId() {
    return $this->pid_service_id;
  }

  /**
   * @param mixed $pid_service_id
   */
  public function setPidServiceId($pid_service_id) {
    $this->pid_service_id = $pid_service_id;
  }
}
