<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class ArchiveObject implements ArchiveObjectInterface, ShareableObjectInterface {

  use ShareableObjectTrait;

  const CONTEXT_DEFAULT = 'Generic Research Data Archive';

  const FORM_FIELD_FILE_UPLOAD = 'file_upload';

  private $id = ArchiveObjectInterface::EMPTY_ID;

  private $archive_id = Archive::EMPTY_ID;

  private $cdstar_object_id = CdstarObject::EMPTY_ID;

  private $pid_id = PersistentIdentifierInterface::EMPTY_ID;

  private $sharing_level = SharingLevel::PRIVATE_LEVEL;

  private $context = self::CONTEXT_DEFAULT;

  private $working_group_id = WorkingGroup::EMPTY_GROUP_ID;

  /**
   * @var \CdstarObject
   */
  private $cdstar_object = NULL;

  /**
   * @var \Archive
   */
  private $archive = NULL;

  /**
   * @var \PersistentIdentifierInterface
   */
  private $pid = NULL;

  /**
   * Validates input for a file upload field that must contain ZIP/TAR archive.
   *
   * @param string $upload_fieldname
   *
   * @see \CdstarObject::formCreateUploadValidate()
   *
   */
  public static function formCreateUploadValidate($upload_fieldname = self::FORM_FIELD_FILE_UPLOAD) {

    CdstarObject::formCreateUploadValidate($upload_fieldname);
  }

  /**
   * Delete instance from repository.
   *
   * @return bool TRUE if successful, FALSE otherwise
   */
  public function delete() {
    return ArchiveObjectRepository::delete($this);
  }

  public function getDisplayName() {
    return $this->getCdstarObject()->getDisplayName();
  }

  /**
   * @return \CdstarObject
   */
  public function getCdstarObject() {
    if (is_null($this->cdstar_object)) {
      $this->cdstar_object = CdstarObjectRepository::findById($this->cdstar_object_id);
    }
    return $this->cdstar_object;
  }

  public function checkOwner() {
    $usr = User::getCurrent();
    if ($this->getOwnerId() == $usr->getUid()) {
      return TRUE;
    }
    return FALSE;
  }

  public function checkDeletePermission() {
    if ($this->checkOwner() && !$this->hasPid()) {
      return TRUE;
    }
    /**
     * Admin
     */
    if (user_access('administer site configuration')) {
      return TRUE;
    }
    return FALSE;
  }

  public function getForm($type = self::FORM_NEW) {
    $form = [];

    $object = $this->getCdstarObject();
    if ($type == self::FORM_NEW) {
      $form['display_name'] = $object->getFormField("display_name");
    }

    $form[self::FORM_FIELD_FILE_UPLOAD] = $this->getFormField(self::FORM_FIELD_FILE_UPLOAD);

    $form['sharing_level'] = $this->getFormField("sharing_level");

    if ($type == self::FORM_UPLOAD) {
      $form[self::FORM_FIELD_FILE_UPLOAD]['#title'] = t('Zip/Tar Archive File');
      $form[self::FORM_FIELD_FILE_UPLOAD]['#description'] = t('Choose a zip or tar archive
    file to upload and extract.');
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    if ($type == self::FORM_UPLOAD) {
      $form['submit']['#value'] = t('Upload file');
    }

    return $form;
  }

  private function getFormField($fieldname) {
    switch ($fieldname) {
      case "sharing_level":
        $field = SharingLevel::getFormField();
        $field['#default_value'] = $this->getArchive()->getSharingLevel();
        return $field;
      case self::FORM_FIELD_FILE_UPLOAD:
        $field = [
          '#title' => t('Upload file'),
          '#description' => t('Choose a file to upload.'),
          '#type' => 'file',
        ];
        return $field;
    }
  }

  private function getArchive() {
    if (is_null($this->archive)) {
      $this->archive = ArchiveRepository::findById($this->archive_id);
    }
    return $this->archive;
  }

  /**
   * @return int
   */
  public function getArchiveId() {
    return $this->archive_id;
  }

  /**
   * @param int $archive_id
   */
  public function setArchiveId($archive_id) {
    $this->archive_id = $archive_id;
  }

  /**
   * @return int
   */
  public function getSharingLevel() {
    return $this->sharing_level;
  }

  /**
   * @param int $sharing_level
   */
  public function setSharingLevel($sharing_level) {
    $this->sharing_level = $sharing_level;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getPidId() {
    return $this->pid_id;
  }

  /**
   * @param mixed $pid_id
   */
  public function setPidId($pid_id) {
    $this->pid_id = $pid_id;
  }

  /**
   * @return int
   */
  public function getCdstarObjectId() {
    return $this->cdstar_object_id;
  }

  /**
   * @param int $cdstar_object_id
   */
  public function setCdstarObjectId($cdstar_object_id) {
    $this->cdstar_object_id = $cdstar_object_id;
  }

  public function url($type = 'view') {
    $default = RDP_ARCHIVE_URL_ARCHIVE_DEFAULT . '/' . $this->archive_id . '/' . $this->id;
    switch ($type) {
      case 'delete':
        return $default . '/delete';
      case 'edit':
        return $default . '/edit';
      case 'register_pid':
        return $default . '/register_pid';
      case 'view':
      default :
        return $default;
    }
  }

  public static function url_by_id($id, $type = 'view') {
    $archiveObject = ArchiveObjectRepository::findById($id);
    return $archiveObject->url($type);
  }

  /**
   * @return string
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * @param string $context
   */
  public function setContext($context) {
    $this->context = $context;
  }

  /**
   * @return \DateTime Object creation timestamp.
   */
  public function getCreated() {
    return $this->getCdstarObject()->getCreated();
  }

  /**
   * @param \DateTime $dateTime
   */
  public function setCreated(DateTime $dateTime) {
    $this->getCdstarObject()->setCreated($dateTime);
  }

  /**
   * @return \DateTime Object modification timestamp.
   */
  public function getModified() {
    return $this->getCdstarObject()->getModified();
  }

  /**
   * @param \DateTime $dateTime
   */
  public function setModified(DateTime $dateTime) {
    $this->getCdstarObject()->setModified($dateTime);
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->getCdstarObject()->getDisplayName();
  }

  /**
   * @param string $name
   */
  public function setName(string $name) {
    $this->getCdstarObject()->setDisplayName($name);
  }

  private function getMetadataField($key) {
    $metadata = $this->getCdstarObject()->getMetadata();
    $metadata = json_decode($metadata, TRUE);
    if (array_key_exists($key, $metadata)) {
      return is_array($metadata[$key]) ? current($metadata[$key]) : $metadata[$key];
    }
    else {
      return '';
    }
  }

  private function setMetadataField($key, $value) {
    $metadata = $this->getCdstarObject()->getMetadata();
    $metadata = json_decode($metadata, TRUE);
    if (is_array($value)) {
      $value = (count($value) == 1) ? current($value) : $value;
    }
    $metadata[$key] = $value;
    $this->getCdstarObject()->setMetadata($metadata);
  }


  /**
   * @return string
   */
  public function getDescription() {
    return $this->getMetadataField('dc:description');
  }

  /**
   * @param string $description
   */
  public function setDescription(string $description): void {
    $this->setMetadataField('dc:description', $description);
  }

  /**
   * @return string
   */
  public function getSource() {
    return $this->getMetadataField('dc:source');
  }

  public function setSource($source) {
    $this->setMetadataField('dc:source', $source);
  }

  public function setSubject($subject) {
    $this->setMetadataField('dc:subject', $subject);
  }

  public function getSubject() {
    return $this->getMetadataField('dc:subject');
  }

  public function getRights() {
    return $this->getMetadataField('dc:rights');
  }

  public function setRights($rights) {
    $this->setMetadataField('dc:rights', $rights);
  }

  public function getCreator() {
    return $this->getMetadataField('dc:creator');
  }

  public function setCreator($creator) {
    $this->setMetadataField('dc:creator', $creator);
  }

  public function getPublisher() {
    return $this->getMetadataField('dc:publisher');
  }

  public function setPublisher($publisher) {
    $this->setMetadataField('dc:publisher', $publisher);
  }

  public function getContributor() {
    return $this->getMetadataField('dc:contributor');
  }

  public function setContributor($contributor) {
    $this->setMetadataField('dc:contributor', $contributor);
  }

  private function setIdentifier(string $identifier) {
    // CDSTAR does not allow complex metadata fields
    $this->setMetadataField('dc:identifier', $identifier);
  }

  public function getWorkingGroupId() {
    return $this->working_group_id ? $this->working_group_id : $this->getArchive()->getGroupId();
  }

  public function setWorkingGroupID($working_group_id) {
    $this->working_group_id = $working_group_id;
  }

  /**
   * @return int Object revision number.
   */
  public function getRevision() {
    return $this->getCdstarObject()->getRevision();
  }

  /**
   * @return int Number of files stored in object.
   */
  public function getFileCount() {
    return $this->getCdstarObject()->getFileCount();
  }

  /**
   *
   *
   * @param $archive_id
   * @param $sharing_level
   * @param $upload_fieldname
   */
  public function formCreateUploadSubmit(
    $archive_id,
    $sharing_level,
    $upload_fieldname = self::FORM_FIELD_FILE_UPLOAD
  ) {

    $archive = ArchiveRepository::findById($archive_id);

    $this->setArchiveId($archive_id);
    $this->setSharingLevel($sharing_level);

    $cdstarfile = new CdstarFile();
    $cdstarServer = CdstarServerRepository::findById($archive->getServer());

    // Pass temporarily-uploaded file data
    $cdstarfile->setFilename($_FILES['files']['name'][$upload_fieldname]);
    $cdstarfile->setContentType($_FILES['files']['type'][$upload_fieldname]);
    $cdstarfile->setTmpfile($_FILES['files']['tmp_name'][$upload_fieldname]);
    $cdstarObject = CdstarObjectRepository::createObjectFromArchive($cdstarServer, $cdstarfile);

    $cdstarObject->generateBasicMetadata();
    $cdstarObject->save();

    $this->setCdstarObjectId($cdstarObject->getId());
    $this->save();
  }

  /**
   * Save new or existing instance to repository.
   */
  public function save() {
    $this->setMetadataField('dc:type', 'Dataset');
    $this->getCdstarObject()->save();
    $id = ArchiveObjectRepository::save($this);

    // Update id value if was empty before and repository saving was successful.
    if ($this->isEmpty() && $id) {
      $this->id = $id;
    }
  }

  /**
   * @return bool True if instance has not been saved to Repository/Database.
   */
  public function isEmpty() {
    if ($this->id === self::EMPTY_ID) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @return bool TRUE if a persistent identifier has been assigned to the
   *   instance.
   */
  public function hasPid() {
    if (!empty($this->pid_id) && $this->pid_id != PersistentIdentifierInterface::EMPTY_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @return bool TRUE if write permission is granted
   */
  public function checkWritePermission() {
    $rdp_user = User::getCurrent();
    if ($this->getOwnerId() == $rdp_user->getUid()) {
      return TRUE;
    }

    // default to FALSE
    return FALSE;
  }

  /**
   * @return int
   */
  public function getOwnerId() {
    return $this->getArchive()->getOwner();
  }

  private function getPid() {
    if (is_null($this->pid)) {
      $this->pid = EpicPidRepository::findById($this->pid_id);
    }
    return $this->pid;
  }

  /**
   * @return bool TRUE if the dataset may not be altered anymore because a PID is registered.
   */
  public function isLocked() {
    return $this->hasPid();
  }

  /**
   * Register an EPIC Persistent Identifier for the dataset
   */
  public function registerPid() {
    $padding_length = variable_get(RDP_ARCHIVE_VAR_CONFIG_PID_PADDING);
    $nr = str_pad($this->getId(), $padding_length, '0', STR_PAD_LEFT);
    $pid_string = variable_get(RDP_ARCHIVE_VAR_CONFIG_PID_PREFIX) . $nr;

    $pid_service_id = variable_get(RDP_ARCHIVE_VAR_DEFAULT_PID_SERVICE);
    $pid = new EpicPid();
    $pid->setServiceId($pid_service_id);
    $fully_qualified_url = url($this->url(), ['absolute' => TRUE]);
    $pid->setTargetUrl($fully_qualified_url);
    $pid->setId($pid_string);
    $pid->save();

    $this->setIdentifier($pid->getUrl());
    $this->setPidId($pid->getId());
    $this->save();
  }
}
