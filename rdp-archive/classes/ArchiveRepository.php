<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class ArchiveRepository
 *
 * ToDo: doc
 *
 */
class ArchiveRepository {

  private static $tableName = 'rdp_archive';

  private static $databaseFields = [
    'id',
    'name',
    'owner',
    'group_id',
    'server',
    'sharing_level',
    'status',
    'pid_service_id',
  ];

  /**
   * ToDo: doc
   *
   * @param \Archive $archive
   *
   * @return bool|string
   */
  public static function save(Archive $archive) {
    // insert or update database data
    if (isset($archive)) {
      if ($archive->isEmpty()) {
        $id = NULL;
      }
      else {
        $id = $archive->getId();
      }

      try {
        db_merge(self::$tableName)
          ->key(['id' => $id])
          ->fields([
              'name' => $archive->getName(),
              'owner' => $archive->getOwner(),
              'group_id' => $archive->getGroupId(),
              'server' => $archive->getServer(),
              'sharing_level' => $archive->getSharingLevel(),
              'status' => $archive->getStatus(),
              'pid_service_id' => $archive->getPidServiceId(),
            ]
          )->execute();

        return Database::getConnection()->lastInsertId();

      } catch (Exception $e) {

        watchdog_exception('rdp_archive', $e);
        drupal_set_message($e->getMessage(), 'error');

        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * ToDo: doc
   *
   * @param $id
   *
   * @return \Archive
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('id', $id, '=')
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultToObject($result);
  }

  /**
   * Get database entries ordered by conditions specified through
   * Drupal's @return \Archive[] Array of Archive instances.
   *
   * @see TableSort extension.
   *
   */
  public static function findAllTableHeader($header) {
    $query = db_select(self::$tableName, 't')
      ->extend('TableSort')
      ->orderByHeader($header)
      // Extension: PagerDefault - use with theme('pager').
      ->extend('PagerDefault')
      ->limit(10);

    $results = $query
      ->fields('t', self::$databaseFields)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * ToDo: doc
   *
   * @return \Archive[]
   */
  public static function findAll() {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * ToDo: doc
   *
   * @param int $owner
   * @param string|int $status
   *
   * @return \Archive[]
   */
  public static function findAllByOwner($owner, $status = "any") {

    $conditions = [];
    $conditions[] = [
      'field' => 'owner',
      'value' => $owner,
      'operator' => '=',
    ];

    if ($status !== "any") {
      if (in_array($status, [
        Archive::ARCHIVE_ENABLED,
        Archive::ARCHIVE_DISABLED,
      ])) {
        $conditions[] = [
          'field' => 'status',
          'value' => $status,
          'operator' => '=',
        ];
      }
    }

    $results = self::findAllByConditions($conditions);

    return self::databaseResultsToObjects($results);
  }

  /**
   * ToDo: doc
   *
   * @param int $group_id
   * @param string|int $status
   *
   * @return \Archive[]
   */
  public static function findAllByGroupId($group_id, $status = "any") {

    $conditions = [];
    $conditions[] = [
      'field' => 'group_id',
      'value' => $group_id,
      'operator' => '=',
    ];

    if ($status !== "any") {
      if (in_array($status, [
        Archive::ARCHIVE_ENABLED,
        Archive::ARCHIVE_DISABLED,
      ])) {
        $conditions[] = [
          'field' => 'status',
          'value' => $status,
          'operator' => '=',
        ];
      }
    }

    $results = self::findAllByConditions($conditions);

    return self::databaseResultsToObjects($results);
  }

  /**
   * @param $uid User id
   *
   * @return \Archive[]
   */
  public static function findAllPersonalArchivesByUser($uid) {
    return self::findAllByOwnerAndGroupId($uid,
      Archive::ARCHIVE_GROUP_PERSONAL,
      Archive::ARCHIVE_ENABLED);
  }

  public static function userHasPersonalArchive($uid) {

    $db_and = db_and();
    $db_and->condition('owner', $uid);
    $db_and->condition('group_id', Archive::ARCHIVE_GROUP_PERSONAL);

    $result = db_select(self::$tableName)
      ->condition($db_and)
      ->countQuery()
      ->execute()
      ->fetchField();

    $count = (int) $result;
    return $count ? TRUE : FALSE;
  }

  /**
   * @param $owner
   * @param $group_id
   * @param string|int $status
   *
   * @return \Archive[]
   */
  public static function findAllByOwnerAndGroupId($owner, $group_id, $status = "any") {

    $conditions = [];
    $conditions[] = [
      'field' => 'owner',
      'value' => $owner,
      'operator' => '=',
    ];
    $conditions[] = [
      'field' => 'group_id',
      'value' => $group_id,
      'operator' => '=',
    ];

    if ($status !== "any") {
      if (in_array($status, [
        Archive::ARCHIVE_ENABLED,
        Archive::ARCHIVE_DISABLED,
      ])) {
        $conditions[] = [
          'field' => 'status',
          'value' => $status,
          'operator' => '=',
        ];
      }
    }

    $results = self::findAllByConditions($conditions);

    return self::databaseResultsToObjects($results);
  }

  /**
   * @param array $conditions
   */
  private static function findAllByConditions($conditions) {

    $db_and = db_and();

    foreach ($conditions as $condition) {
      $db_and->condition(
        $condition['field'],
        $condition['value'],
        $condition['operator']);
    }

    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition($db_and)
      ->execute()
      ->fetchAll();

    return $results;
  }

  /**
   * ToDo: doc
   *
   * @param $results
   *
   * @return \Archive[]
   */
  private static function databaseResultsToObjects($results) {
    $objects = [];
    foreach ($results as $result) {
      $objects[] = self::databaseResultToObject($result);
    }

    return $objects;
  }

  /**
   * ToDo: doc
   *
   * @param $result
   *
   * @return \Archive
   */
  private static function databaseResultToObject($result) {
    $archive = new Archive();

    if (empty($result)) {
      return $archive;
    }

    $archive->setId($result->id);
    $archive->setName($result->name);
    $archive->setOwner($result->owner);
    $archive->setGroupId($result->group_id);
    $archive->setServer($result->server);
    $archive->setSharingLevel($result->sharing_level);
    $archive->setStatus($result->status);
    $archive->setPidServiceId($result->pid_service_id);

    return $archive;
  }

  public static function findAllBySharingLevel($level) {
    $conditions = [];
    $conditions[] = [
      'field' => 'sharing_level',
      'value' => $level,
      'operator' => '=',
    ];
    $conditions[] = [
      'field' => 'status',
      'value' => Archive::ARCHIVE_ENABLED,
      'operator' => '=',
    ];

    $results = self::findAllByConditions($conditions);

    return self::databaseResultsToObjects($results);
  }

  /**
   * @param \User $user
   *
   * @return \Archive
   */
  public static function newPersonalArchive(User $user) {
    $archive = new Archive();
    $archive->setOwner($user->getUid());
    $archive->setGroupId(Archive::ARCHIVE_GROUP_PERSONAL);
    $archive->setSharingLevel(SharingLevel::PRIVATE_LEVEL);
    $fullname = trim($user->getFullname());
    $name = !empty($fullname) ? $fullname : $user->getName();
    $archive_name = $name . ' Personal Archive';
    $archive->setName($archive_name);

    // Get config variable for CDSTAR service
    $cdstar_id = variable_get(RDP_ARCHIVE_VAR_DEFAULT_CDSTAR);
    $archive->setServer($cdstar_id);

    // Get config variable for PID service
    $epic_id = variable_get(RDP_ARCHIVE_VAR_DEFAULT_PID_SERVICE);
    $archive->setPidServiceId($epic_id);

    // Get config for whether or not to active newly created archives
    $activate = variable_get(RDP_ARCHIVE_VAR_ACTIVATE_ARCHIVE_ON_USER_INSERT, FALSE);
    $archive->setStatus($activate ? Archive::ARCHIVE_ENABLED : Archive::ARCHIVE_DISABLED);

    $archive->save();
    return $archive;
  }

  /**
   * Return the ID of a users default personal archive.
   *
   * @param int $uid The user ID.
   *
   * @return int The ID of the default personal archive.
   * @throws \Exception Thrown when no personal archive is available.
   */
  public static function getDefaultPersonalArchiveID($uid) {
    $personal_archives = self::findAllPersonalArchivesByUser($uid);

    if (!count($personal_archives)) {
      /**
       * Switch to @see \NoUserArchiveFoundException after moving the class into rdp_archive module
       */
      throw new Exception("No default personal archive available for user-id $uid");
    }
    else {
      return $personal_archives[0]->getId();
    }
  }

}