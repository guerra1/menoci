<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

abstract class ArchiveResources extends CdstarResources {

  const GLYPHICON_CONFIG = '<span class="glyphicon glyphicon-cog" style="float: right;">
                <span class="sr-only">configure</span></span>';
}