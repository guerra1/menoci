<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class ArchiveObjectRenderer {

  /**
   * @var \ArchiveObject
   */
  private $archiveObject;

  private $cdstarObject;

  private $archive;

  /**
   * ArchiveObjectRenderer constructor.
   *
   * @param \ArchiveObject $archiveObject
   */
  public function __construct(ArchiveObject $archiveObject) {
    $this->archiveObject = $archiveObject;
    $this->cdstarObject = CdstarObjectRepository::findById($archiveObject->getCdstarObjectId());
    $this->archive = ArchiveRepository::findById($this->archiveObject->getArchiveId());
  }

  public static function tableHeader() {
    return [
      'Dataset Name',
      'Owner',
      'PID',
      'Group',
      'Sharing Level',
      'Options',
    ];
  }

  public function tableRow() {

    /**
     * Compose action links for "Options" column.
     */
    $options = '';

    // View
    $options .= l(ArchiveResources::GLYPHICON_VIEW,
      $this->archiveObject->url(), ['html' => TRUE]);

    if ($this->archiveObject->checkWritePermission()) {
      // Edit
      $options .= '&nbsp;' . l(ArchiveResources::GLYPHICON_EDIT,
          $this->archiveObject->url('edit'), ['html' => TRUE]);
    }

    // Delete
    if ($this->archiveObject->checkWritePermission()) {
      $options .= '&nbsp;' . l(ArchiveResources::GLYPHICON_DELETE,
          $this->archiveObject->url('delete'), ['html' => TRUE]);
    }

    $pid_display = $this->archiveObject->isLocked() ? self::render_pid($this->archiveObject->getPidId()) : '';

    return [
      // Name
      l($this->archiveObject->getDisplayName(), $this->archiveObject->url()),
      // Owner
      self::render_owner($this->archiveObject->getOwnerId()),
      // PID
      $pid_display,
      // Working Group
      self::render_working_group($this->archiveObject->getWorkingGroupId()),
      // Sharing Level
      SharingLevel::getHtmlInline($this->archiveObject->getSharingLevel()),
      // Options
      $options,
    ];
  }

  public static function render_pid($pid_id) {
    if ($pid_id == EpicPid::EMPTY_ID) {
      return "<i>not registered yet</i>";
    }
    else {
      $pid = EpicPidRepository::findById($pid_id);
      $span = "<span style='white-space: nowrap'>" . $pid->getIconHtmlLink()
        . l($pid->getPid(), $pid->getUrl(),
          ['html' => TRUE, 'external' => TRUE, 'attributes' => ['class' => ['badge btn-warning']]]) . "</span>";
      return $span;
    }
  }

  public static function render_owner(int $owner_id) {
    $owner = UsersRepository::findByUid($owner_id);
    return $owner->getFullNameWithOrcid();
  }

  private static function render_working_group(int $wg_id) {
    if ($wg_id == WorkingGroup::EMPTY_GROUP_ID) {
      return '';
    }
    $wg = WorkingGroupRepository::findById($wg_id);
    $icon_path = WorkingGroup::getGroupIconPath($wg->getShortName());
    $img = theme_image(['path' => $icon_path, 'title' => $wg->getName(), 'attributes' => []]);
    return $img . '&nbsp;' . $wg->getName();
  }

  /**
   * @return string
   * @deprecated
   *
   */
  public function viewPage() {

    return $this->displayPage();
  }

  public function displayPage() {

    $date_created = $this->archiveObject->getCreated()->format(RDP_ARCHIVE_DEFAULT_DATE_FORMAT);

    /**
     * Action buttons
     */
    $navbar_actions = self::action_navbar();

    $secondary_block_css_style = 'background-color: #f5f5f5; border-radius: 4px; padding: 16px;';

    $general_information = "<div><span class='badge' title='Publication date' 
        data-toggle='tooltip'>$date_created</span></div>";

    $general_information .= "<div><h3>"
      . $this->archiveObject->getDisplayName() . "</h3></div>";
    $general_information .= "<div><div style='margin: 64px 0;'><p><strong>Description:</strong></p>
        <p>" . $this->archiveObject->getDescription() . "</p></div></div>";

    $pid_info = '';
    if ($this->archiveObject->checkWritePermission()) {
      if ($this->archiveObject->isLocked()) {
        $pid_info = "<div class='col-md-4 label-warning' style='border-radius: 4px; padding: 16px; margin-bottom: 32px;'>
<span class='glyphicon glyphicon-lock'></span>&nbsp;PID registered, 
contents of this dataset may not be altered anymore</div>";
      }
      else {
        $pid_info = "<div class='col-md-4 label-info' style='border-radius: 4px; padding: 16px; margin-bottom: 32px;'>
<span class='glyphicon glyphicon-edit'></span>&nbsp;PID is not registered yet, 
contents of this dataset may be altered</div>";
      }
    }

    $meta_information = self::render_meta_information_block();

    $linked_publications = self::linkedPublicationsBlock();


    if ($this->archiveObject->checkReadPermission()) {
      $files_visible = self::listFiles();
      $files_hidden = '';
    }
    else {
      $files_visible = '';
      $files_hidden = '<div class="alert alert-warning"><strong>Access partially denied!</strong><br>
        Due to access restrictions set by the owner of the requested dataset, only metadata and contact information 
        can be provided. For full access to the available date, please contact the responsible persons listed on 
        this page.</div>';
    }

    $addfile_form = '';
    if ($this->archiveObject->checkWritePermission() && !$this->archiveObject->isLocked()) {
      $addfile_form = self::addFileForm();
    }

    $output = $navbar_actions
      . "<div class='container-fluid'>
            <div class='col-md-8'>"
      . $files_hidden
      . $general_information
      . $files_visible
      . $addfile_form
      . "</div>
            " . $pid_info . "
            <div class='col-md-4' 
            style='" . $secondary_block_css_style . "'>
            " . $meta_information . $linked_publications
      . "</div></div>";

    /**
     * Add JSON-LD markup
     */
    $output .= '<script type="application/ld+json">' . $this->render_meta_jsonld() . '</script>';

    return $output;
  }

  private function action_navbar() {

    $navbar_actions = '';
    if ($this->archiveObject->checkWritePermission() && !$this->archiveObject->hasPid()) {

      $buttons = [];

      $buttons[] = '<li>' . l('<span class="glyphicon glyphicon-edit"></span>&nbsp;Edit',
          $this->archiveObject->url('edit'), [
            'html' => TRUE,
            'attributes' => ['class' => ['btn btn-default']],
          ]) . '</li>';

      if ($this->archiveObject->checkOwner()) {
        $confirmation_dialogue = 'Registering the PID will permanently block changes to the uploaded files and metadata of this dataset. Please be sure that all information is correct!';
        $buttons[] = '<li>' . l('<span class="glyphicon glyphicon-lock"></span>&nbsp;Register PID',
            $this->archiveObject->url('register_pid'), [
              'html' => TRUE,
              'attributes' => [
                'class' => ['btn btn-default'],
                'onclick' => 'if(!confirm(\'' . $confirmation_dialogue . '\')){return false;}',
              ],
            ]) . '</li>';
      }
      if ($this->archiveObject->checkDeletePermission()) {
        $buttons[] = '<li>' . l('<span class="glyphicon glyphicon-trash"></span>&nbsp;Delete',
            $this->archiveObject->url('delete'), [
              'html' => TRUE,
              'attributes' => ['class' => ['btn btn-default']],
            ]) . '</li>';
      }

      $navbar_actions = '
    <nav class="navbar">
      <div class="container-fluid">
       <div class="navbar-header navbar-default">
          <button type="button" class="navbar-default navbar-toggle" data-toggle="collapse" data-target="#actionNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="actionNavbar">
          <ul class="nav navbar-nav navbar-right">
          ' . implode('', $buttons) . '
          </ul>
        </div>
      </div>
    </nav>
';
    }
    return $navbar_actions;
  }

  private function render_meta_information_block() {

    $metadata_vars = [
      'Creator' => self::render_orcid_links_in_string($this->archiveObject->getCreator()),
      'Date Created' => self::render_created($this->archiveObject->getCreated()),
      'Identifier' => self::render_pid($this->archiveObject->getPidId()),
      'Sharing Level' => SharingLevel::getHtmlInline($this->archiveObject->getSharingLevel()),
      'Working Group' => self::render_working_group($this->archiveObject->getWorkingGroupId()),
      'Contributor' => self::render_orcid_links_in_string($this->archiveObject->getContributor()),
      'Publisher' => self::render_orcid_links_in_string($this->archiveObject->getPublisher()),
      'Use and Access Rights' => $this->archiveObject->getRights(),
      'Subject (keywords)' => self::render_subject($this->archiveObject->getSubject()),
      'Source' => $this->archiveObject->getSource(),
    ];

    foreach ($metadata_vars as $key => $value) {
      if (!empty($value)) {
        $html[] = "<h6>$key</h6><p>$value</p>";
      }
    }
    return implode('', $html);
  }

  /**
   * Replaces all text occurrences of an ORCID with the iD icon
   *
   * @param string $input
   *
   * @return string
   */
  private static function render_orcid_links_in_string(string $input) {
    $icon = theme_image([
      'path' => base_path() . drupal_get_path('module', 'sfb_commons') . '/resources/orcid_icon.svg',
      'height' => '14px',
      'title' => 'ORCID Researcher ID',
      'attributes' => [],
    ]);

    $orcid_regex = '/\b((https:\/\/)|(http:\/\/))?(www\.)?(orcid.org\/)?\d{4}\-\d{4}\-\d{4}\-\d{4}\b(\/)?/';
    $matches = [];
    if (preg_match_all($orcid_regex, $input, $matches)) {
      $hits = $matches[0];
      $output = $input;
      $hits = array_unique($hits);
      foreach ($hits as $hit) {
        $replace = l($icon, self::harmonize_orcid($hit),
          ['html' => TRUE, 'attributes' => ['target' => '_blank']]);
        $output = str_replace($hit, $replace, $output);
      }
      return $output;
    }
    return $input;
  }

  public static function harmonize_orcid(?string $orcid) {
    $orcid = str_replace('https://', '', $orcid);
    $orcid = str_replace('http://', '', $orcid);
    $orcid = str_replace('www.', '', $orcid);
    $orcid = str_replace('orcid.org/', '', $orcid);
    return 'https://orcid.org/' . $orcid;
  }

  public static function render_created(DateTime $created) {
    return $created->format(RDP_ARCHIVE_DEFAULT_DATE_FORMAT);
  }

  private static function render_subject(string $subject) {
    $css = /** @lang CSS */
      'span.badge-keyword {
      background-color: #5bc0de;
      color: #ffffff;
      font-weight: bold;
      white-space: nowrap;
    }';
    drupal_add_css($css, 'inline');

    $keywords = explode(', ', $subject);
    foreach ($keywords as $key => $value) {
      $keywords[$key] = '<span class="badge badge-keyword"">' . $value . '</span>';
    }

    return count($keywords) ? implode(' ', $keywords) : '';
  }

  /**
   * ToDo
   *
   * @return string
   */
  public function linkedPublicationsBlock() {

    // ToDo: implement linking & display
    return '';

  }

  private function listFiles() {

    $object = $this->archiveObject->getCdstarObject();
    $files = $object->getFiles();
    $base_url = RDP_ARCHIVE_URL_ARCHIVE_DEFAULT . '/' . $this->archive->getId() . '/' . $this->archiveObject->getId() . '/';

    $files_html = '';
    foreach ($files as $file) {
      $files_html .= CdstarFileRenderer::display_tile($file, $base_url);
    }

    $block = '<div><h4>Files</h4>';
    $block .= $files_html . '</div>';

    return $block;
  }

  private function addFileForm() {
    $form = drupal_get_form('rdp_archive_archive_object_addfile_form', $this->archiveObject->getId());
    return drupal_render($form);
  }

  private function render_meta_jsonld() {

    $metadata = $this->archiveObject->getCdstarObject()->getMetadata();
    /* Remove parenthesis */
    $metadata = substr($metadata, 1, -1);
    $markup = /** @lang JSON */
      "{
        \"@context\": {
          \"@vocab\": \"https://schema.org/\",
          \"dc\": \"http://purl.org/dc/elements/1.1/\"
      },
  \"@type\": \"Dataset\",
  " . $metadata . "
      }";

    return $markup;
  }

  private function accessOwner() {
    return rdp_archive_archive_object_owner_access($this->archiveObject->getId());
  }

  private function owner() {
    return UsersRepository::findByUid($this->cdstarObject->getOwner())
      ->getName();
  }

}