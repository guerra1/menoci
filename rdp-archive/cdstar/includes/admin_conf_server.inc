<?php
/**
 * @file
 * Callbacks, forms and functions to manage CdstarServer instances in Drupal
 *   admin UI.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Displays CDSTAR_CONFIG_SERVER_DEFAULT path.
 * A list of configured servers, links to the respective paths
 * to edit, delete and add new server configurations.
 *
 * @return string Drupal page content
 */
function cdstar_admin_conf_server() {
  // Get existing servers
  $header = CdstarServer::tableHeader();
  $servers = CdstarServerRepository::findAllTableHeader($header);

  // Build table of configured servers
  $rows = [];
  foreach ($servers as $server) {
    $rows[] = $server->tableRow();
  }
  try {
    $output = theme('table', ['header' => $header, 'rows' => $rows]);
  } catch (Exception $e) {
    watchdog_exception('CDSTAR', $e);
  }

  return $output;
}

/**
 * Drupal Form to add new CdstarServer object
 *
 * @return array Drupal Form
 */
function cdstar_admin_conf_server_new($form, &$form_state) {
  $server = new CdstarServer();

  $method = isset($form_state['values']['auth_method']) ? $form_state['values']['auth_method'] : $server->getAuthMethod();
  $server->setAuthMethod($method);

  $form = $server->getForm('new');

  return $form;
}

/**
 * Ajax callback handler to switch between HTTP-Basic and JWT Authentication
 * fieldsets.
 */
function cdstar_admin_conf_server_authmethod_callback($form, $form_state) {

  return $form['auth'];
}

/**
 * Validate form for new or edited CdstarServer entry.
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_server_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (array_key_exists('auth_method', $values)
    && $values['auth_method'] == CdstarServer::AUTH_BASIC) {
    if (empty($values['username'])) {
      form_set_error('username', t('Username must not be empty for Basic HTTP Authentication method'));
    }
  }

  _validate_url_field("url", $values['url'], $form_state);
  if (array_key_exists($field = 'tus_endpoint_url', $values) && !empty($values[$field])) {
    _validate_url_field($field, $values[$field], $form_state);
  }
  // Check if server connection can be established
  if (!form_get_errors()) {

    // Get the existing instance if an "edit" form was submitted
    if (isset($form_state['server_id'])) {
      $server = CdstarServerRepository::findById($form_state['server_id']);
    }
    // if not, create new instance
    else {
      $server = new CdstarServer();
      $server->setAuthMethod($values['auth_method']);
    }

    // update runtime instance with new input from the form
    $server->setUrl($values['url']);
    // only update if a new password or secret was submitted
    if (!empty($values['password'])) {
      $server->setPassword($values['password']);
    }
    // only handle JWT-specific attributes if needed
    if ($server->getAuthMethod() == CdstarServer::AUTH_JWT) {
      if (!empty($values['jwt_iss'])) {
        $server->setJwtIss($values['jwt_iss']);
      }
      if (isset($values['jwt_sub'])) {
        $server->setJwtSub($values['jwt_sub']);
      }
    }
    // update username attribute, if auth method is not JWT-auth
    else {
      $server->setUsername($values['username']);
    }
    // test the connection
    $test_result = CdstarServerTestCase::testAuthentication($server);
    // display an error message, if test failed
    if (!$test_result) {
      form_set_error('auth', t('Authentication with the server could not be verified. 
      Please check the URL and authentication method parameters.'));
    }
  }
}

/**
 * Handles new CdstarServer object form submit action
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_server_new_submit($form, &$form_state) {
  $values = $form_state['values'];

  $server = new CdstarServer();

  $server->setName($values['name']);
  $server->setLabel($values['label']);
  $server->setUrl($values['url']);
  $server->setTusEndpointUrl($values['tus_endpoint_url']);
  $server->setAuthMethod($values['auth_method']);
  $server->setUsername($values['username']);
  $server->setPassword($values['password']);
  if (!empty($values['jwt_iss'])) {
    $server->setJwtIss($values['jwt_iss']);
  }
  if (isset($values['jwt_sub'])) {
    $server->setJwtSub($values['jwt_sub']);
  }

  $server->save();

  $form_state['redirect'] = CDSTAR_CONFIG_SERVER_DEFAULT;
}

/**
 * Drupal Form to add new CdstarServer object
 *
 * @param $form
 * @param $form_state
 * @param $server_id int CdstarServer object ID from path
 *
 * @return array Drupal Form
 */
function cdstar_admin_conf_server_edit($form, &$form_state, $server_id) {
  $server = CdstarServerRepository::findById($server_id);

  $form_state['server_id'] = $server_id;

  $form = $server->getForm('edit');

  $method = isset($form_state['values']['auth_method']) ? $form_state['values']['auth_method'] : $server->getAuthMethod();
  $server->setAuthMethod($method);

  return $form;
}


/**
 * Handles CdstarServer object edit form submit action
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_server_edit_submit($form, &$form_state) {
  $values = $form_state['values'];

  $server = CdstarServerRepository::findById($form_state['server_id']);

  $server->setName($values['name']);
  $server->setLabel($values['label']);
  $server->setUrl($values['url']);
  $server->setTusEndpointUrl($values['tus_endpoint_url']);
  $server->setUsername($values['username']);
  if (!empty($values['password'])) {
    $server->setPassword($values['password']);
  }
  if (!empty($values['jwt_iss'])) {
    $server->setJwtIss($values['jwt_iss']);
  }
  if (isset($values['jwt_sub'])) {
    $server->setJwtSub($values['jwt_sub']);
  }


  $server->save();

  $form_state['redirect'] = CDSTAR_CONFIG_SERVER_DEFAULT;
}

/**
 * Delete CdstarServer entry.
 * Implements @param $form
 *
 * @param $form_state
 * @param int $server_id The database ID of the entry..
 *
 * @return array
 * @see hook_form().
 *
 */
function cdstar_admin_conf_server_delete($form, &$form_state, $server_id) {
  if (count(CdstarObjectRepository::findByServerId($server_id)) > 0) {
    drupal_set_message(t('Cannot delete CDSTAR server configuration 
            because at least one object is stored with the given server.'), 'warning');
    drupal_goto(CDSTAR_CONFIG_SERVER_DEFAULT);
  }
  else {
    $form = [];
    $form_state['server_id'] = $server_id;

    $server = CdstarServerRepository::findById($server_id);
    $form['nb'] = [
      '#markup' => '<p>' . t("Delete CDSTAR Server :server permanently?",
          [':server' => $server->getName()]) . '</p>',
    ];

    $form['cancel'] = [
      '#type' => 'button',
      '#submit' => ['cdstar_admin_conf_server_cancel'],
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
      '#limit_validation_errors' => [],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Delete server'),
    ];

    $form['#submit'][] = 'cdstar_admin_conf_server_delete_submit';

    return $form;
  }
}


/**
 * Handles the delete form action, deletes \CdstarServer entry from repository.
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_server_delete_submit($form, &$form_state) {
  $server_id = $form_state['server_id'];
  $server = CdstarServerRepository::findById($server_id);
  if ($server->delete()) {
    drupal_set_message(t("Successfully deleted CDSTAR server configuration :server.",
      [':server' => $server->getName()]));
  }

  $form_state['redirect'] = CDSTAR_CONFIG_SERVER_DEFAULT;
}

/**
 * Handles CdstarServer form cancel action
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_server_cancel($form, &$form_state) {
  $form_state['redirect'] = CDSTAR_CONFIG_SERVER_DEFAULT;
}

function _validate_url_field($field, $value, &$form_state) {
  // Add (possibly) missing slash to URL
  if (substr($value, -1) != "/") {
    // update pointer
    $form_state['values'][$field] = $value . "/";
    // update local variable for further processing
    $value = $form_state['values'][$field];
  }
  if (!valid_url($value, TRUE)) {
    form_set_error($field, t('Please insert a valid URL like 
    for example: https://cdstar.gwdg.de/demo/v3/'));
  }
}