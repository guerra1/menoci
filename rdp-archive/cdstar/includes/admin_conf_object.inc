<?php
/**
 * @file
 * Callbacks, forms and functions to manage CdstarObject instances in Drupal
 *   admin UI.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 *
 */
function cdstar_admin_conf_object_view_title() {
  $object_id = arg(5);
  $display_name = CdstarObject::getObject($object_id)->getDisplayName();

  return $display_name;
}

/**
 * Displays CDSTAR_CONFIG_OBJECT_DEFAULT path.
 * A list of existing CdstarObject objects.
 *
 * @return string Drupal page content
 */
function cdstar_admin_conf_object() {

  $header = CdstarObject::tableHeader();
  $objects = CdstarObjectRepository::findAllTableHeader($header);

  $data = [];
  foreach ($objects as $object) {
    $data[] = $object->tableRow();
  }
  try {
    $output = theme('table', ['header' => $header, 'rows' => $data]);
    $output .= theme('pager');
  } catch (Exception $e) {
    watchdog_exception('CDSTAR', $e);
  }

  return $output;

}

function cdstar_admin_conf_object_view($object_id) {

  $object = CdstarObject::getObject($object_id);

  $header = CdstarObject::tableHeader();
  $data[] = $object->tableRow();
  try {
    $output = theme('table', ['header' => $header, 'rows' => $data]);
  } catch (Exception $e) {
    watchdog_exception('CDSTAR', $e);
  }

  $output .= '<h3>Metadata</h3><pre>' . $object->getMetadata() . '</pre>';
  $output .= '<h3>Permissions</h3><pre>' . $object->getPermissions() . '</pre>';
  $output .= '<h3>Files</h3>' . $object->listFiles();

  $form = drupal_get_form('cdstar_admin_conf_object_addfile_form', $object->getId());
  $output .= drupal_render($form);

  return $output;
}

function cdstar_admin_conf_object_new($form, &$form_state) {
  $form = [];

  $object = new CdstarObject();

  $form['display_name'] = $object->getFormField('display_name');
  $form['server'] = $object->getFormField('server');

  $form_state['owner'] = $object->getOwner();

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Create object'),
  ];

  $form['cancel'] = [
    '#type' => 'button',
    '#submit' => ['cdstar_admin_conf_object_cancel'],
    '#value' => t('Cancel'),
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => [],
  ];

  $form['#submit'][] = 'cdstar_admin_conf_object_new_submit';

  return $form;
}

function cdstar_admin_conf_object_new_submit($form, &$form_state) {
  $object = new CdstarObject();

  $values = $form_state['values'];

  $server_id = $values['server'];
  $object->setServer($server_id);

  $object->setOwner($form_state['owner']);
  $object->setDisplayName($values['display_name']);

  $object->generateBasicMetadata();
  $object->save();

  //  $form_state['redirect'] = CDSTAR_CONFIG_OBJECT_DEFAULT;
  $form_state['redirect'] = $object->url();
}

function cdstar_admin_conf_object_new_upload($form, &$form_state) {
  $form = [];

  $object = new CdstarObject();

  $form['server'] = $object->getFormField('server');

  $form_state['owner'] = $object->getOwner();

  $form['zip_archive'] = [
    '#type' => 'file',
    '#title' => 'Zip/Tar Archive File.',
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Upload and create Object'),
    '#required' => TRUE,
  ];


  $form['cancel'] = [
    '#type' => 'button',
    '#submit' => ['cdstar_admin_conf_object_cancel'],
    '#value' => t('Cancel'),
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => [],
  ];

  $form['#submit'][] = 'cdstar_admin_conf_object_new_upload_submit';
  $form['#validate'][] = 'cdstar_admin_conf_object_new_upload_validate';

  return $form;

}

function cdstar_admin_conf_object_new_upload_validate($form, &$form_state) {

  $upload_fieldname = 'zip_archive';

  CdstarObject::formCreateUploadValidate($upload_fieldname);
}

function cdstar_admin_conf_object_new_upload_submit($form, &$form_state) {

  $cdstarfile = new CdstarFile();
  $cdstarServer = CdstarServerRepository::findById($form_state['values']['server']);

  // Pass temporarily-uploaded file data
  $fieldname = 'zip_archive';
  $cdstarfile->setFilename($_FILES['files']['name'][$fieldname]);
  $cdstarfile->setContentType($_FILES['files']['type'][$fieldname]);
  $cdstarfile->setTmpfile($_FILES['files']['tmp_name'][$fieldname]);

  $object = CdstarObjectRepository::createObjectFromArchive($cdstarServer, $cdstarfile);

  $form_state['redirect'] = $object->url();

}

function cdstar_admin_conf_object_addfile_form($form, &$form_state, $object_id) {
  $form = CdstarObject::getFormAddFile();
  $form_state['object_id'] = $object_id;

  return $form;
}

function cdstar_object_addfile_form_submit($form, &$form_state) {
  $object = CdstarObject::getObject($form_state['object_id']);
  $object->addFileFormAction();
  $form_state['redirect'] = $object->url();
}

function cdstar_admin_conf_file_delete($object_id, $file_id) {
  $object = CdstarObject::getObject($object_id);

  /* @var \CdstarFile $cdstarfile */
  $cdstarfile = $object->getFiles()[$file_id];

  if ($object->removeFile($cdstarfile)) {
    drupal_set_message(t('File :filename deleted successfully.',
      [':filename' => $cdstarfile->getFilename()]));
  }

  drupal_goto($object->url());
}

function cdstar_admin_conf_object_delete($form, &$form_state, $object_id) {
  $form = [];

  $form_state['object_id'] = $object_id;
  $form['nb'] = [
    '#markup' => '<p>' . t("Delete Object :id <strong>and all associated files</strong> permanently?",
        [':id' => $object_id]) . '</p>',
  ];

  $form['cancel'] = [
    '#type' => 'button',
    '#submit' => ['cdstar_admin_conf_object_cancel'],
    '#value' => t('Cancel'),
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => [],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Delete object'),
  ];

  $form['#submit'][] = 'cdstar_admin_conf_object_delete_submit';

  return $form;
}

function cdstar_admin_conf_object_delete_submit($form, &$form_state) {
  $object = CdstarObject::getObject($form_state['object_id']);

  if (CdstarObjectRepository::delete($object)) {
    drupal_set_message(t('Deleted object :id successfully.', [':id' => $form_state['object_id']]));
  }

  $form_state['redirect'] = CDSTAR_CONFIG_OBJECT_DEFAULT;
}

function cdstar_admin_conf_file_download($object_id, $file_id) {
  if (!user_access(CDSTAR_PERMISSION_CONFIGURATION)) {
    drupal_access_denied();
  }

  $object = CdstarObject::getObject($object_id);

  /**
   * @var CdstarFile $cdstarfile
   */
  $cdstarfile = $object->getFiles()[$file_id];
  $cdstarfile->download();
}

/**
 * Handles CdstarServer form cancel action
 *
 * @param $form
 * @param $form_state
 */
function cdstar_admin_conf_object_cancel($form, &$form_state) {
  $form_state['redirect'] = CDSTAR_CONFIG_OBJECT_DEFAULT;
}