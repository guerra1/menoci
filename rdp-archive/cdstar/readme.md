# CDSTAR Drupal Module

Drupal module to provide abstraction layer of CDSTAR object storage services.

Developed at the University Medical Center Göttingen ([UMG](http://umg.eu)), 
Department of Medical Informatics ([MI](http://www.mi.med.uni-goettingen.de)).

Maintainer: Markus Suhr, 
[markus.suhr@med.uni-goettingen.de](mailto:markus.suhr@med.uni-goettingen.de)

## About

This module adds functionality to store files 
in a [CDSTAR object storage](https://cdstar.gwdg.de) 
service over a REST API (currently version v2).

Multiple CDSTAR server instances can be 
configured through an admin UI.

Objects stored in CDSTAR services can be 
displayed and managed in an admin UI.

Provides a Drupal development API to connect "File" 
form fields to the CDSTAR storage service.

Provides basic display and interaction (CRUD) methods for stored 
objects and files in Drupal pages.

## ToDo

* imput validations
* meta data concept
* object
  * ZIP file upload
  * edit object
  * add multiple files to object
* EPIC PIDs?
* Permission handling?

## Third-party libraries

* [Firebase PHP-JWT](https://github.com/firebase/php-jwt): 
Copyright (c) 2011 Neuman Vong
* [Uppy](https://uppy.io): Copyright (c) 2019 
Transloadit (https://transloadit.com), [MIT license](lib/uppy/LICENSE))
                         

## Funding

Work on this project was enabled through funding by the 
Bundesministerium für Bildung und Forschung (BMBF) via the 
German Center for Cardiovascular Research ([DZHK](https://dzhk.de)) and the 
German Research Foundation ([SFB1002](https://sfb1002.med.uni-goettingen.de))

## License and Copyright

This Drupal module is licensed under the [GPL Version 3.0](LICENSE).

Copyright lies with the authors of source code contributions as documented 
in the [GitLab project repository](https://gitlab.gwdg.de/msuhr1/rdp-cdstar).