<?php
/**
 * @file
 * Defines class CdstarServer.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarServer
 */
class CdstarServer {

  /**
   * Constant identifier for unsaved objects.
   */
  const EMPTY_ID = -1;

  /**
   * Constant string to identify HTTP Basic Authentication
   */
  const AUTH_BASIC = 'base';

  /**
   * Constant string to identify HTTP Authentication using JSON Web Token
   */
  const AUTH_JWT = 'jwt';

  /**
   * @var array Possible authentication methods
   */
  private static $auth_methods = [
    self::AUTH_BASIC => 'Basic HTTP Authentication',
    self::AUTH_JWT => 'JSON Web Token',
  ];

  /**
   * @var int
   */
  private $id = self::EMPTY_ID;

  /**
   * @var string
   */
  private $name;

  /**
   * @var string
   */
  private $label;

  /**
   * @var string
   */
  private $url;

  /**
   * @var string
   */
  private $tus_endpoint_url;

  /**
   * @var string
   */
  private $username;

  /**
   * @var string
   */
  private $password;

  /**
   * @var string issuer claim for JWT authentication method
   */
  private $jwt_iss;

  /**
   * @var string subject claim for JWT authentication method
   */
  private $jwt_sub;

  /**
   * @var string authentication method used for this server
   */
  private $auth_method = self::AUTH_BASIC;

  /**
   * Build table headers compatible with Drupal table theme and
   *
   * @return array Column headers for simple table display with Drupal table
   *   theme
   * @see \TableSort extension.
   *
   */
  public static function tableHeader() {
    return [
      ['data' => 'Server', 'field' => 'label'],
      ['data' => 'URL', 'field' => 'url'],
      ['data' => 'Authentication Method', 'field' => 'auth_method'],
      'Options',
    ];
  }

  /**
   * @return string JSON encoded array representation of the object.
   */
  public function __toString() {
    $json = [
      $this->name => [
        'id' => $this->id,
        'label' => $this->label,
        'url' => $this->url,
        'username' => $this->username,
        'password' => $this->password,
      ],
    ];
    return json_encode($json);
  }

  /**
   * @return array human-readable data fields
   * for simple table row display with Drupal table theme.
   */
  public function tableRow() {
    $edit_icon = l(CdstarResources::GLYPHICON_EDIT, $this->url('edit'),
      ['html' => TRUE]);
    $delete_icon = l(CdstarResources::GLYPHICON_DELETE, $this->url('delete'),
      ['html' => TRUE]);

    return [
      $this->label . ' (' . $this->name . ')',
      $this->url,
      self::$auth_methods[$this->auth_method],
      $edit_icon . '&nbsp;' . $delete_icon,
    ];
  }

  /**
   * Build and return different URL paths.
   *
   * @param string $route A string specifying which functional path is desired.
   *
   * @return string
   */
  public function url($route = 'edit') {
    switch ($route) {
      case 'edit':
        return CDSTAR_CONFIG_SERVER_DEFAULT . '/' . $this->id . '/edit';
      case 'delete':
        return CDSTAR_CONFIG_SERVER_DEFAULT . '/' . $this->id . '/delete';
    }
  }

  /**
   * Stores object in repository.
   */
  public function save() {
    $id = CdstarServerRepository::save($this);

    // Update id value if was empty before and repository saving was successful.
    if ($this->isEmpty() && $id) {
      $this->id = $id;
    }
  }

  /**
   * @return bool
   */
  public function isEmpty() {
    if ($this->id == self::EMPTY_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Deletes object from repository.
   *
   * @return bool
   */
  public function delete() {
    return CdstarServerRepository::delete($this);
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getLabel() {
    return $this->label;
  }

  /************************************* GETTERS AND SETTERS **************************************/

  /**
   * @param mixed $label
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * @return mixed
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @param mixed $url
   */
  public function setUrl($url) {
    $this->url = $url;
  }

  /**
   * @return mixed
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * @param mixed $username
   */
  public function setUsername($username) {
    $this->username = $username;
  }

  /**
   * @return mixed
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * @param mixed $password
   */
  public function setPassword($password) {
    $this->password = $password;
  }

  public function getForm($type) {
    $form = [];
    $display_new_form = $type == 'new' ? TRUE : FALSE;

    $form['name'] = $this->getFormField('name');
    $form['label'] = $this->getFormField('label');
    $form['url'] = $this->getFormField('url');
    $form['tus_endpoint_url'] = $this->getFormField('tus_endpoint_url');

    $form['auth'] = [
      '#type' => 'fieldset',
      '#title' => t('Authentication'),
      '#prefix' => '<div id="authmethod-wrapper">',
      '#suffix' => '</div>',
    ];

    if ($display_new_form) {
      $form['auth']['auth_method'] = $this->getFormField('auth_method');
    }
    else {
      $form['auth']['info']['#markup'] = '<p><strong>Method:</strong> '
        . self::$auth_methods[$this->getAuthMethod()] . '</p>';
    }

    $form['auth']['username'] = $this->getFormField('username');
    $form['auth']['password'] = $this->getFormField('password');

    if ($display_new_form) {
      $form['auth']['password']['#required'] = TRUE;
    }

    $form['auth']['JWT_header'] = [
      '#type' => 'markup',
      '#markup' => '<p>Insert claims for JWT Authentication:</p>',
    ];

    $form['auth']['jwt_iss'] = $this->getFormField('jwt_iss');
    $form['auth']['jwt_sub'] = $this->getFormField('jwt_sub');

    if ($method = $this->getAuthMethod() !== self::AUTH_JWT) {
      $form['auth']['JWT_header']['#access'] = FALSE;
      $form['auth']['jwt_iss']['#access'] = FALSE;
      $form['auth']['jwt_sub']['#access'] = FALSE;
    }
    elseif ($method !== self::AUTH_BASIC) {
      $form['auth']['username']['#access'] = FALSE;
    }

    if ($display_new_form) {
      $submit_label = t('Add new Server');
    }
    else {
      $submit_label = t('Update Server settings');
    }

    $form['submit']['#type'] = 'submit';
    $form['submit']['#value'] = $submit_label;
    $form['submit']['#validate'] = ['cdstar_admin_conf_server_validate'];

    $form['cancel'] = [
      '#type' => 'button',
      '#submit' => ['cdstar_admin_conf_server_cancel'],
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
      '#limit_validation_errors' => [],
    ];

    if ($this->hasObjects()) {
      $form['auth']['auth_method']['#disabled'] = TRUE;
      $form['auth']['jwt_iss']['#disabled'] = TRUE;
      $form['auth']['jwt_sub']['#disabled'] = TRUE;
    }

    if ($display_new_form) {
      $form['#submit'][] = 'cdstar_admin_conf_server_new_submit';
    }
    else {
      $form['#submit'][] = 'cdstar_admin_conf_server_edit_submit';
    }

    return $form;
  }

  /**
   * Generates the Drupal form field array for a specified class variable (the
   * parameter)
   *
   * @param $fieldname
   *
   * @return array Drupal form field
   */
  public function getFormField($fieldname) {
    switch ($fieldname) {
      case 'name':
        return [
          '#type' => 'textfield',
          '#title' => t('Server machine name'),
          '#description' => t('Enter the machine name of the new CDSTAR server.'),
          '#default_value' => $this->name,
          '#required' => TRUE,
          '#attributes' => ['placeholder' => 'gwdg_public_demo'],
        ];
      case 'label':
        return [
          '#type' => 'textfield',
          '#title' => t('Server label'),
          '#description' => t('Enter a human readable name for the server.'),
          '#default_value' => $this->label,
          '#required' => TRUE,
          '#attributes' => ['placeholder' => 'GWDG Public Demo Server, Vault: Demo'],
        ];
      case 'url':
        return [
          '#type' => 'textfield',
          '#title' => t('Server URL'),
          '#description' => t('Enter the base URL for API calls to the server, including vault name. For example: 
            "https://cdstar.gwdg.de/demo/v3/demo/" with the last "demo" as the vault.'),
          '#default_value' => $this->url,
          '#required' => TRUE,
          '#attributes' => ['placeholder' => 'https://demo.cdstar.gwdg.de/demo/v3/demo/'],
        ];
      case 'tus_endpoint_url':
        return [
          '#type' => 'textfield',
          '#title' => t('TUS Endpoint URL'),
          '#description' => t('Enter the URL for the TUS upload API. (If TUS is enabled for the CDSTAR server, 
            this is usually the same as the above Server URL up until before the "v3" part. For Example: 
            "https://demo.cdstar.gwdg.de/demo/tus/".)'),
          '#default_value' => $this->tus_endpoint_url,
          '#attributes' => ['placeholder' => 'https://demo.cdstar.gwdg.de/demo/tus/'],
        ];
      case 'username':
        $required = FALSE;
        if ($this->auth_method === self::AUTH_BASIC) {
          $required = TRUE;
        }
        return [
          '#type' => 'textfield',
          '#title' => t('Username'),
          '#description' => t('Enter a username for the connection to the 
            server. Required for Basic HTTP Authentication method.'),
          '#default_value' => $this->username,
          '#required' => $required,
        ];
      case 'password':
        return [
          '#type' => 'password',
          '#title' => t('Password or shared secret'),
          '#description' => t('Enter a password (Basic HTTP Authentication 
            method) or shared secret (JWT Authentication method) for the 
             connection to the server.'),
        ];
      case 'auth_method':
        return [
          '#type' => 'radios',
          '#title' => t('Authentication Method'),
          '#description' => t('Choose the method used to authenticate against this server'),
          '#options' => self::$auth_methods,
          '#default_value' => $this->getAuthMethod(),
          '#required' => TRUE,
          '#ajax' => [
            'callback' => 'cdstar_admin_conf_server_authmethod_callback',
            'wrapper' => 'authmethod-wrapper',
          ],
        ];
      case 'jwt_iss':
        return [
          '#type' => 'textfield',
          '#title' => t('ISS'),
          '#description' => t('Issuer string for JWT Authentication process.'),
          '#default_value' => $this->jwt_iss,
          '#required' => TRUE,
        ];
      case 'jwt_sub':
        return [
          '#type' => 'textfield',
          '#title' => t('SUB'),
          '#description' => t('Subject string for JWT Authentication process.'),
          '#default_value' => $this->jwt_sub,
          '#required' => FALSE,
        ];
    }
  }

  /**
   * @return mixed
   */
  public function getAuthMethod() {
    return $this->auth_method;
  }

  /**
   * @param mixed $auth_method
   */
  public function setAuthMethod($auth_method) {
    if (array_key_exists($auth_method, self::$auth_methods)) {
      $this->auth_method = $auth_method;
    }
  }

  /**
   * @return bool TRUE if any CdstarObjects are registered for this server
   */
  private function hasObjects() {
    return CdstarObjectRepository::serverHasObjects($this);
  }

  /**
   * @return mixed
   */
  public function getJwtIss() {
    return $this->jwt_iss;
  }

  /**
   * @param mixed $jwt_iss
   */
  public function setJwtIss($jwt_iss) {
    $this->jwt_iss = $jwt_iss;
  }

  /**
   * @return mixed
   */
  public function getJwtSub() {
    return $this->jwt_sub;
  }

  /**
   * @param mixed $jwt_sub
   */
  public function setJwtSub($jwt_sub) {
    $this->jwt_sub = $jwt_sub;
  }

  /**
   * @return bool TRUE if attribute $tus_endpoint_url is set
   */
  public function hasTusEndpoint() {
    return !empty($this->tus_endpoint_url) ? TRUE : FALSE;
  }

  /**
   * @param array $post_data An associative array of information about TUS-uploaded files: TUS upload URL (containing
   * temporary TUS id, filename, filetype.
   *
   * @return CdstarObject
   */
  public function createObjectFromTusUpload(array $post_data) {

    $cdstarObject = new CdstarObject();

    if (count($post_data) > 1) {
      $user_fullname = User::getCurrent()->getFullname();
      $date_string = ", " . date(DATE_W3C);
      $display_name = "Dataset, " . $user_fullname . $date_string;
      $cdstarObject->setDisplayName($display_name);
    }
    else {
      $cdstarObject->setDisplayName($post_data[0]['name']);
    }
    $cdstarObject->setServer($this->getId());
    $cdstarObject->generateBasicMetadata();
    $cdstarObject->save();

    foreach ($post_data as $file) {
      //extract tusID from uploadURL
      $upload_URL = $file['uploadUrl'];
      $url_array = explode('/', $upload_URL);
      $tus_ID = end($url_array);

      //extract name and type
      $filename = $file['name'];
      $mimetype = $file['type'];

      /**
       * Copy uploaded file into the CDSTAR package
       */
      $cdstarObject->addFileFromTus($tus_ID, $filename, $mimetype);
    }

    $cdstarObject->save();
    return $cdstarObject;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * Provide Uppy upload script within a form
   *
   * @param string $fieldname form array key of the upload form field
   * @param string $target URL target for redirect on successful upload
   *
   * @return array Drupal form element definition
   */
  public function getTusUploadForm($fieldname = 'tus_upload', $target = 'create', $allow_multiple_files = FALSE) {
    $tus_endpoint_url = $this->getTusEndpointUrl();
    $multi = $allow_multiple_files ? 'allowMultipleUploads: true' : 'restrictions: { maxNumberOfFiles: 1 }';
    $uppy_script = /** @lang JavaScript */
      'var uppy = Uppy.Core({ ' . $multi . ' })

        .use(Uppy.Dashboard, { 
            inline: true, 
            height: 350, 
            showLinkToFileUploadResult: false, 
            target: \'#drag-drop-area\'
        })
        
        .use(Uppy.Tus, {
          endpoint: \'' . $tus_endpoint_url . '\',
          resume: true,
          autoRetry: true,
          retryDelays: [0, 1000, 3000, 5000, 10000, 20000],
        })

      uppy.on(\'complete\', (result) => {
        //console.log(\'successful files:\', result.successful);
        if( result.failed.length == 0){
        //send request with payload
        post("' . $target . '", result.successful);
        }
      });
    
      function post(path, params, method=\'post\') {
      
        const form = document.createElement(\'form\');
        form.method = method;
        form.action = path;
        
        const files = [];
        
        params.forEach(extractFileInfo);
        
        function extractFileInfo(item, index){
            const file = {};
            file["uploadUrl"] = item.tus.uploadUrl;
            file["name"] = item.data.name;
            file["type"] = item.data.type;
            files.push( file);
        }
        
        const hiddenField = document.createElement(\'input\');
        hiddenField.type = \'hidden\';
        hiddenField.name = \'' . $fieldname . '_data\';
        hiddenField.value = JSON.stringify(files);
        form.appendChild(hiddenField);
      
        document.body.appendChild(form);
        form.submit();
      }';
    $form[$fieldname] = [
      '#type' => 'markup',
      '#markup' => '<div id="drag-drop-area"></div>',
      '#suffix' => '<script>' . $uppy_script . '</script>',
    ];

    return $form;
  }

  /**
   * @return string
   */
  public function getTusEndpointUrl(): string {
    return $this->tus_endpoint_url;
  }

  /**
   * @param string $tus_endpoint_url
   */
  public function setTusEndpointUrl(string $tus_endpoint_url): void {
    $this->tus_endpoint_url = $tus_endpoint_url;
  }
}