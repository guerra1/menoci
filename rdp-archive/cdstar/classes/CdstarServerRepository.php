<?php
/**
 * @file
 * Defines class CdstarServerRepository.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarServerRepository
 */
class CdstarServerRepository {

  static $tableName = 'cdstar_server';

  static $databaseFields = [
    'id',
    'name',
    'label',
    'url',
    'tus_endpoint_url',
    'username',
    'password',
    'auth_method',
    'jwt_iss',
    'jwt_sub',
  ];

  /**
   * Save CdstarServer instance to database.
   *
   * @param CdstarServer $server
   *
   * @return bool|int Server ID if successfully inserted, true if successfully
   *   updated, false otherwise.
   */
  public static function save(CdstarServer $server) {
    try {
      $fields = [
        'name' => $server->getName(),
        'label' => $server->getLabel(),
        'url' => $server->getUrl(),
        'tus_endpoint_url' => $server->getTusEndpointUrl(),
        'username' => $server->getUsername(),
        'password' => $server->getPassword(),
        'auth_method' => $server->getAuthMethod(),
        'jwt_iss' => $server->getJwtIss(),
        'jwt_sub' => $server->getJwtSub(),
      ];
      if ($server->isEmpty()) {
        $id = db_insert(self::$tableName)
          ->fields($fields)
          ->execute();
        return (int) $id;

      }
      else {
        $fields['id'] = $server->getId();
        db_update(self::$tableName)
          ->fields($fields)
          ->condition('id', $server->getId(), '=')
          ->execute();
        return TRUE;
      }
    } catch (Exception $e) {
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  /**
   * Translate database result to class instance.
   *
   * @param stdClass $result Generic database result object.
   *
   * @return CdstarServer
   */
  private static function databaseResultToObject($result) {
    $server = new CdstarServer();

    if (empty($result)) {
      return $server;
    }

    $server->setId($result->id);
    $server->setName($result->name);
    $server->setLabel($result->label);
    $server->setUrl($result->url);
    $server->setTusEndpointUrl($result->tus_endpoint_url);
    $server->setUsername($result->username);
    $server->setPassword($result->password);
    $server->setAuthMethod($result->auth_method);
    $server->setJwtIss($result->jwt_iss);
    $server->setJwtSub($result->jwt_sub);

    return $server;
  }

  /**
   * Translate multiple database results to class instances.
   *
   * @param stdClass[] $results Array of generic database result objects.
   *
   * @return CdstarServer[] Array of CdstarServer instances.
   */
  private static function databaseResultsToObjects($results) {
    $servers = [];
    foreach ($results as $result) {
      $servers[] = self::databaseResultToObject($result);
    }

    return $servers;
  }

  /**
   * Fetch instances from database by identifier.
   *
   * @param int $id CdstarServer identifier.
   *
   * @return CdstarServer
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('id', $id, '=')
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultToObject($result);

  }

  /**
   * Get database entries ordered by conditions specified through Drupal's
   * @see TableSort extension.
   *
   *   @return \CdstarServer
   */
  public static function findAllTableHeader($header) {
    $query = db_select(self::$tableName, 't')
      ->extend('TableSort')
      ->orderByHeader($header);

    $results = $query
      ->fields('t', self::$databaseFields)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);

  }

  /**
   * Fetch all CdstarServer instances from database.
   *
   * @return CdstarServer[]
   */
  public static function findAll() {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * Delete instance from database.
   *
   * @param CdstarServer $server
   *
   * @return bool True if deleted successfully.
   */
  public static function delete(CdstarServer $server) {
    try {
      db_delete(self::$tableName)
        ->condition('id', $server->getId())
        ->execute();

      return TRUE;

    } catch (Exception $e) {
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }
}
