<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */


define("EPIC_PID_URL_CONFIG_DEFAULT", "admin/config/rdp/epic-pid");
define("EPIC_PID_URL_CONFIG_PID_EDIT", EPIC_PID_URL_CONFIG_DEFAULT . "/%/edit");
define("EPIC_PID_URL_CONFIG_SERVICE_DEFAULT", "admin/config/rdp/pid-services");
define("EPIC_PID_URL_CONFIG_SERVICE_CREATE", EPIC_PID_URL_CONFIG_SERVICE_DEFAULT . "/new");
define("EPIC_PID_URL_CONFIG_SERVICE_VIEW", EPIC_PID_URL_CONFIG_SERVICE_DEFAULT . "/%");
define("EPIC_PID_URL_CONFIG_SERVICE_EDIT", EPIC_PID_URL_CONFIG_SERVICE_VIEW . "/edit");
define("EPIC_PID_URL_CONFIG_SERVICE_DELETE", EPIC_PID_URL_CONFIG_SERVICE_EDIT . "/delete");
define("EPIC_PID_URL_CONFIG_SERVICE_CLONE", EPIC_PID_URL_CONFIG_SERVICE_EDIT . "/clone");

define("EPIC_PID_PERMISSION_CONFIGURATION", "epic-pid configuration");

/**
 * Implements  @see hook_permission().
 */
function epic_pid_permission() {
  return [
    EPIC_PID_PERMISSION_CONFIGURATION => [
      'title' => t('EPIC PID configuration'),
      'description' => t('Configure EPIC-PID Connector module: Define new 
      PID services.'),
    ],
  ];
}

/**
 * Implements  @see hook_menu().
 */
function epic_pid_menu() {

  $items[EPIC_PID_URL_CONFIG_DEFAULT] = [
    'title' => 'EPIC PID Overview',
    'description' => 'Show an overview of actively registered Persistent Identifiers',
    'page callback' => 'epic_pid_config',
    'page arguments' => [],
    'access arguments' => [EPIC_PID_PERMISSION_CONFIGURATION],
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/epic_pid_config.inc',
  ];

  $items[EPIC_PID_URL_CONFIG_PID_EDIT] = [
    'title' => 'Edit PID',
    'description' => 'Edit PID.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['epic_pid_edit', 4],
    'access arguments' => [EPIC_PID_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/epic_pid_config.inc',
  ];

  $items[EPIC_PID_URL_CONFIG_SERVICE_DEFAULT] = [
    'title' => 'PID Services',
    'description' => 'Configure available PID Services.',
    'page callback' => 'epic_pid_services',
    'page arguments' => [],
    'access arguments' => [EPIC_PID_PERMISSION_CONFIGURATION],
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/epic_pid_service.inc',
  ];

  $items[EPIC_PID_URL_CONFIG_SERVICE_VIEW] = [
    'title' => 'View PID Service',
    'description' => 'Display PID Service details.',
    'page callback' => 'epic_pid_service_view',
    'page arguments' => [5],
    'access arguments' => [EPIC_PID_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/epic_pid_service.inc',
  ];

  $items[EPIC_PID_URL_CONFIG_SERVICE_CREATE] = [
    'title' => 'Add new PID Service',
    'description' => 'Add new PID Service.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['epic_pid_service_new'],
    'access arguments' => [EPIC_PID_PERMISSION_CONFIGURATION],
    'type' => MENU_LOCAL_ACTION,
    'file' => 'includes/epic_pid_service.inc',
  ];

  $items[EPIC_PID_URL_CONFIG_SERVICE_EDIT] = [
    'title' => 'Edit PID Service',
    'description' => 'Edit PID Service.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['epic_pid_service_edit', 4],
    'access arguments' => [EPIC_PID_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/epic_pid_service.inc',
  ];

  $items[EPIC_PID_URL_CONFIG_SERVICE_DELETE] = [
    'title' => 'Delete PID Service',
    'description' => 'Delete PID Service.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['epic_pid_service_delete', 4],
    'access arguments' => [EPIC_PID_PERMISSION_CONFIGURATION],
    'type' => MENU_LOCAL_ACTION,
    'file' => 'includes/epic_pid_service.inc',
  ];

  $items[EPIC_PID_URL_CONFIG_SERVICE_CLONE] = [
    'title' => 'Add new cloned PID Service',
    'description' => 'Add new cloned PID Service.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['epic_pid_service_clone', 4],
    'access arguments' => [EPIC_PID_PERMISSION_CONFIGURATION],
    'type' => MENU_LOCAL_ACTION,
    'file' => 'includes/epic_pid_service.inc',
  ];

  return $items;
}