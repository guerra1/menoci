<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 *
 */
function epic_pid_config() {

  $pids = EpicPidRepository::findAll();

  $header = ['PID', 'Service', 'Target'];

  $rows = [];

  foreach ($pids as $pid) {
    $rows[] = [
      // column PID
      $pid->getIconHtmlLink() . '&nbsp;' . $pid->getId(),
      // column Service
      EpicPidServiceRepository::findById($pid->getServiceId())->getName(),
      // column Target
      $link = $pid->getTargetUrl() . '&nbsp;' .
        l('<span class="glyphicon glyphicon-edit">
                <span class="sr-only">edit</span></span>',
          $pid->path(EpicPid::PATH_EDIT), ['html' => TRUE]),
    ];

  }

  try {
    return theme('table', ['header' => $header, 'rows' => $rows]);
  } catch (Exception $e) {
    watchdog_exception('epic-pid', $e);
  }
}

function epic_pid_edit($form, &$form_state, $pid_id) {
  $pid = EpicPidRepository::findById($pid_id);
  $form = $pid->getForm(EpicPid::FORM_EDIT);
  $form_state['id'] = $pid_id;

  return $form;
}

function epic_pid_edit_submit($form, &$form_state) {

  $pid_id = $form_state['id'];
  $pid = EpicPidRepository::findById($pid_id);

  $values = $form_state['values'];
  $pid->setTargetUrl($values['target_url']);
  $pid->save();

  $form_state['redirect'] = EPIC_PID_URL_CONFIG_DEFAULT;
}

function epic_pid_edit_cancel($form, &$form_state) {

  drupal_goto(EPIC_PID_URL_CONFIG_DEFAULT);
}