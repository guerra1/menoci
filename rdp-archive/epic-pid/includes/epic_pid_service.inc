<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function epic_pid_services() {

  $services = EpicPidServiceRepository::findAll();

  $output = "";

  foreach ($services as $service) {
    $output .= '<p>' . l("Edit: " . $service->getName(), $service->url("edit")) . '</p>';
  }

  return $output;
}

function epic_pid_service_view($service_id) {
  $service = EpicPidServiceRepository::findById($service_id);
  drupal_set_title($service->getName());

  $output = '';
  $output .= '<p>' . $service->json() . '</p>';

  $pids = $service->getAssociatedPids();
  if (count($pids) > 0) {

    $output .= '<h2>Associated Persitent Identifiers</h2>';
    foreach ($pids as $pid) {
      $output .= '<p>' . l($pid->getPid(), $pid->getUrl())
        . ' (Target: ' . $pid->getTargetUrl() . ')</p>';
    }
  }
  else {
    $output .= '<p>No Persistent Identifiers have been 
                generated using this service.</p>';
  }

  return $output;
}

function epic_pid_service_new($form, &$form_state) {
  $service = new EpicPidService();
  $form = $service->getForm();

  $form['#validate'][] = 'epic_pid_service_validate';

  return $form;
}

function epic_pid_service_new_submit($form, &$form_state) {
  $values = $form_state['values'];

  $service = new EpicPidService();
  $service->setServiceType($values['service_type']);
  $service->setName($values['name']);
  $service->setUrl($values['url']);
  $service->setServicePrefix($values['service_prefix']);
  $service->setUserpass(base64_encode($values['username'] . ':' . $values['password']));
  $service->setAuthType($values['auth_type']);
  $service->setMethod($values['method']);

  if ($service->getAuthType() == EpicPidService::AUTH_TYPE_BASIC) {
    $service->setUserpass(base64_encode($values['username'] . ':' . $values['password']));
  }
  elseif ($service->getAuthType() == EpicPidService::AUTH_TYPE_HANDLE_KEYS) {
    $service->setFilepathPrivateKey($values['filepath_private_key']);
    $service->setFilepathCertificate($values['filepath_certificate']);
  }
  if ($service->getMethod() !== EpicPidService::MANUAL) {
    // Pre- and suffix should not be set for manually generated PIDs
    $service->setPrefix($values['prefix']);
    $service->setSuffix($values['suffix']);
  }
  $service->setResolveUrl($values['resolve_url']);
  $service->setIsDummy($values['is_dummy']);
  $service->save();

  $form_state['redirect'] = EPIC_PID_URL_CONFIG_SERVICE_DEFAULT;

}

function epic_pid_service_edit($form, &$form_state, $service_id) {
  $service = EpicPidServiceRepository::findById($service_id);
  $form = $service->getForm("edit");

  $form_state['service_id'] = $service_id;

  $form['#validate'][] = 'epic_pid_service_validate';

  return $form;
}

function epic_pid_service_validate($form, &$form_state) {

  $values = $form_state['values'];

  // Add (possibly) missing slash to URL
  if (substr($values['url'], -1) != "/") {
    // update pointer
    $form_state['values']['url'] = $values['url'] . "/";
    // update local variable for further processing
    $values['url'] = $form_state['values']['url'];
  }

  if (!valid_url($values['url'], TRUE)) {
    form_set_error('url', t('Please insert a valid URL like 
    for example: https://foo.com/bar/'));
  }

  // Add (possibly) missing slash to Resolve-URL
  if (substr($values['resolve_url'], -1) != "/") {
    // update pointer
    $form_state['values']['resolve_url'] = $values['resolve_url'] . "/";
    // update local variable for further processing
    $values['resolve_url'] = $form_state['values']['resolve_url'];
  }

  if (!valid_url($values['resolve_url'], TRUE)) {
    form_set_error('resolve_url', t('Please insert a valid URL like 
    for example: http://hdl.handle.net/'));
  }

  // Test connection and authentication for the PID server
  if (!form_get_errors()) {

    // Get the existing instance if an "edit" form was submitted
    if (isset($form_state['service_id'])) {
      $service = EpicPidServiceRepository::findById($form_state['service_id']);
    }
    // if not, create new instance
    else {
      $service = new EpicPidService();
    }

    $service->setServiceType($values['service_type']);
    $service->setUrl($values['url']);
    $service->setServicePrefix($values['service_prefix']);
    $service->setAuthType($auth_type = $values['auth_type']);
    if ($auth_type == EpicPidService::AUTH_TYPE_BASIC) {
      if (!empty($values['username']) && !empty($values['password'])) {
        $service->setUserpass(base64_encode($values['username'] . ':' . $values['password']));
      }
    }
    elseif ($auth_type == EpicPidService::AUTH_TYPE_HANDLE_KEYS) {
      if (empty($values['filepath_certificate'])) {
        form_set_error('filepath_certificate', t("Certificate file system path required"));
      }
      elseif (empty($values['filepath_private_key'])) {
        form_set_error('filepath_private_key', t("Private Key file system path required"));
      }
      else {
        $service->setFilepathCertificate($values['filepath_certificate']);
        $service->setFilepathPrivateKey($values['filepath_private_key']);
      }
    }
    $service->setMethod($values['method']);
    $service->setIsDummy($values['is_dummy']);

    // test the connection
    $test_result = $service->testAuthentication();
    // display an error message, if test failed
    if ($test_result !== TRUE) {
      form_set_error('url', t('Authentication with the server could not be verified. 
      Please check the URL and authentication parameters.'));
      if ($auth_type == EpicPidService::AUTH_TYPE_BASIC) {
        form_set_error('username');
        form_set_error('password');
      }
      elseif ($auth_type == EpicPidService::AUTH_TYPE_HANDLE_KEYS) {
        form_set_error('filepath_private_key');
        form_set_error('filepath_certificate');
      }
    }
  }

}

function epic_pid_service_edit_submit($form, &$form_state) {
  $values = $form_state['values'];

  $service = EpicPidServiceRepository::findById($form_state['service_id']);

  $service->setServiceType($values['service_type']);
  $service->setName($values['name']);
  $service->setUrl($values['url']);
  $service->setServicePrefix($values['service_prefix']);
  $service->setAuthType($values['auth_type']);
  $service->setMethod($values['method']);

  if ($service->getAuthType() == EpicPidService::AUTH_TYPE_BASIC) {
    $service->setUserpass(base64_encode($values['username'] . ':' . $values['password']));
  }
  elseif ($service->getAuthType() == EpicPidService::AUTH_TYPE_HANDLE_KEYS) {
    $service->setFilepathPrivateKey($values['filepath_private_key']);
    $service->setFilepathCertificate($values['filepath_certificate']);
  }

  if ($service->getMethod() == EpicPidService::MANUAL) {
    // Clear pre- and suffix if method changed to manual PID generation
    $values['prefix'] = "";
    $values['suffix'] = "";
  }
  $service->setPrefix($values['prefix']);
  $service->setSuffix($values['suffix']);
  $service->setResolveUrl($values['resolve_url']);
  $service->setIsDummy($values['is_dummy']);
  $service->save();

  $form_state['redirect'] = EPIC_PID_URL_CONFIG_SERVICE_DEFAULT;
}

function epic_pid_service_delete($form, &$form_state, $service_id) {

  $service = EpicPidServiceRepository::findById($service_id);
  if ($service->delete()) {
    drupal_set_message(t('PID Service :name deleted successfully.',
      [':name' => $service->getName()]), 'success');
    drupal_goto(EPIC_PID_URL_CONFIG_SERVICE_DEFAULT);
  }
  drupal_goto($service->url("edit"));
}

function epic_pid_service_clone($form, &$form_state, $service_id) {
  $service = EpicPidServiceRepository::findById($service_id);
  $duplicate = $service->duplicate();
  drupal_set_message(t('PID Service :name cloned successfully.',
    [':name' => $duplicate->getName()]), 'success');
  drupal_goto(EPIC_PID_URL_CONFIG_SERVICE_DEFAULT);
}

function epic_pid_service_cancel($form, &$form_state) {
  $form_state['redirect'] = EPIC_PID_URL_CONFIG_SERVICE_DEFAULT;
}