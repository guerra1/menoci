<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class EpicApiV2
 */
class EpicApiV2 implements EpicPidApiInterface {

  private static $status;

  private static function init(EpicPidService $pidService) {
    $ch = curl_init();

    // Set the authentication options
    if ($pidService->getAuthType() == EpicPidService::AUTH_TYPE_HANDLE_KEYS) {
      /**
       * Handle.net Key/Certificate based authentication method
       *
       * @see https://userinfo.surfsara.nl/systems/epic-pid/usage/handle-http-json-rest-api-php
       */
      $certificateOnly = $pidService->getFilepathCertificate();
      $privateKey = $pidService->getFilepathPrivateKey();

      if (!file_exists($certificateOnly)) {
        throw new Exception('Missing cert: ' . $certificateOnly);
      }

      if (!file_exists($privateKey)) {
        throw new Exception('Missing key: ' . $privateKey);
      }

      curl_setopt($ch, CURLOPT_SSLCERT, $certificateOnly);
      curl_setopt($ch, CURLOPT_SSLKEY, $privateKey);
      //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Handle clientCert="true"']);

    }
    else {
      /**
       * HTTP BasicAuth method
       */
      curl_setopt($ch, CURLOPT_USERPWD, base64_decode($pidService->getUserpass()));
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }

    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);

    // Verbose for debugging
    curl_setopt($ch, CURLOPT_VERBOSE, TRUE);

    return $ch;
  }

  private static function execute($ch) {

    $output = curl_exec($ch);
    $info = curl_getinfo($ch);

    // Download the given URL, and return output

    if ($info['http_code'] == 200) {
      self::$status = "HANDLE EXISTS";
    }
    if ($info['http_code'] == 201) {
      self::$status = "PID CREATED";
    }
    if ($info['http_code'] == 204) {
      self::$status = "PID UPDATED";
    }
    if ($info['http_code'] == 404) {
      self::$status = "HANDLE DOESNT EXIST";
    }

    curl_close($ch);

    // ToDo: Error handling here

    return $output;
  }

  /**
   * @param \EpicPidService $epicPidService
   *
   * @return bool
   * @throws \Exception
   */
  public function testAuthentication(EpicPidService $epicPidService): bool {
    $ch = self::init($epicPidService);

    $url = $epicPidService->getServiceUrl();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    $result = curl_exec($ch);
    $info = curl_getinfo($ch);

    if ($info['http_code'] == 200) {
      return TRUE;
    }
    if ($info['http_code'] == 401) {
      return FALSE;
    }
    // default to FALSE
    return FALSE;
  }

  /**
   * @param \EpicPid $pid
   *
   * @return bool|string Newly created PID on success, FALSE otherwise
   * @throws \Exception
   */
  public function create(EpicPid $pid) {

    $pidService = EpicPidServiceRepository::findById($pid->getServiceId());

    if ($pidService->getMethod() == EpicPidService::MANUAL) {
      // "Manual" PID generation expects that the EpicPid instance's ID is
      // generated previously, which should trigger the update() method.
      drupal_set_message("PID could not be generated. Please contact 
        your technical support.", "error");
      watchdog("EPIC PID Module", 'Attempt to generate PID for 
        empty instance. Manual generation mode, PID Service %n, ID: %s',
        ['%n' => $pidService->getName(), '%s' => $pidService->getId()],
        WATCHDOG_ERROR);
      return FALSE;
    }

    // Get cURL resource
    $ch = self::init($pidService);

    $url = $pidService->getServiceUrl();

    // ToDo: implement other methods than auto-generate
    if ($pidService->getMethod() == EpicPidService::AUTO_GENERATE) {
      if (!empty($pidService->getPrefix())) {
        $url .= '?prefix=' . $pidService->getPrefix();
      }
      if (!empty($pidService->getSuffix())) {
        $url .= '&suffix=' . $pidService->getSuffix();
      }

      // set the POST Action
      curl_setopt($ch, CURLOPT_POST, TRUE);
    }


    //Set the url with the new name of the PID
    curl_setopt($ch, CURLOPT_URL, $url);

    $data = [
      [
        'type' => 'URL',
        'parsed_data' => $pid->getTargetUrl(),
      ],
    ];
    $update_json = json_encode($data);
    //Set the headers to complete the request
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
      'Content-Length: ' . strlen($update_json),
    ]);
    //SET the postfield data
    curl_setopt($ch, CURLOPT_POSTFIELDS, $update_json);

    $output = self::execute($ch);

    if (self::$status == "PID CREATED") {
      list($headers, $response) = explode("\r\n\r\n", $output, 2);
      // $headers now has a string of the HTTP headers
      // $response is the body of the HTTP response

      $headers = explode("\n", $headers);
      foreach ($headers as $header) {
        if (stripos($header, 'Location:') !== FALSE) {
          $location = $header;
        }
      }
    }

    if (isset($location)) {
      $result = str_replace("Location: ", "", $location);
      $id = str_replace($pidService->getServiceUrl(), "", $result);
      $id = str_replace("\r", "", $id);
      return $id;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param \EpicPid $pid
   *
   * @return bool TRUE if PID was updated successfully, false otherwise.
   * @throws \Exception
   */
  public function update(EpicPid $pid): bool {
    $pidService = EpicPidServiceRepository::findById($pid->getServiceId());
    // Get cURL resource
    $ch = self::init($pidService);

    // build the call URL (with full PID for update)
    $url = $pidService->getServiceUrl() . $pid->getId();
    curl_setopt($ch, CURLOPT_URL, $url);

    // set PUT Action
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

    $data = [
      [
        'type' => 'URL',
        'parsed_data' => $pid->getTargetUrl(),
      ],
    ];
    $update_json = json_encode($data);
    //Set the headers to complete the request
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
      'Content-Length: ' . strlen($update_json),
    ]);
    //Set the post field data
    curl_setopt($ch, CURLOPT_POSTFIELDS, $update_json);

    self::execute($ch);

    //drupal_set_message(self::$status);

    if (self::$status == "PID UPDATED" || self::$status == "PID CREATED") {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}