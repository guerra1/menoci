<?php
/**
 * @file
 * ToDo Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class EpicPidService
 */
class EpicPidService {

  const EMPTY_ID = -1;

  const AUTO_GENERATE = "auto";

  const MANUAL = "manual";

  const POOL_GENERATE = "pool";

  const POOL_CONSUME = "consumer";

  const AUTH_TYPE_BASIC = "HttpBasicAuth";

  const AUTH_TYPE_HANDLE_KEYS = "HandleKeyBasedAuth";

  private static $methods = [
    self::AUTO_GENERATE => "Automatic PID generator.",
    self::MANUAL => "Manually generate PIDs.",
    self::POOL_GENERATE => "PID pool generator.",
    self::POOL_CONSUME => "PID pool consumer.",
  ];

  private static $auth_types = [
    self::AUTH_TYPE_BASIC => "HTTP Basic Authentication (username/password)",
    self::AUTH_TYPE_HANDLE_KEYS => "Handle.net Key/Certificate based Authentication",
  ];

  private static $service_types = [
    DummyAPI::class => "Dummy",
    EpicApiV2::class => "EPIC PID API v2 (GWDG)",
    SurfsaraApi::class => "SURFsara API",
  ];

  private $id = self::EMPTY_ID;

  private $service_type = DummyAPI::class;

  /**
   * @return mixed
   */
  public function getServiceType() {
    return $this->service_type;
  }

  /**
   * @param mixed $service_type
   */
  public function setServiceType($service_type): void {
    $this->service_type = $service_type;
  }

  private $name;

  private $url;

  private $service_prefix;

  private $auth_type = self::AUTH_TYPE_BASIC;

  /**
   * @var string base64 encoded "USERNAME:PASSWORD" for HTTP Basic
   *   Authentication.
   */
  private $userpass;

  /**
   * @var string File system path to private key file for Handle.net Key-based authentication type
   */
  private $filepath_private_key = '';

  /**
   * @var string File system path to certificate file for Handle.net Key-based authentication type
   */
  private $filepath_certificate = '';

  private $method = self::AUTO_GENERATE;

  private $prefix;

  private $suffix;

  private $resolve_url = "http://hdl.handle.net/";

  private $is_dummy = FALSE;

  /**
   * @return array All allowed values for EpicPidService methods.
   */
  public static function getMethods() {
    return array_keys(self::$methods);
  }

  /**
   * @return string
   */
  public function getFilepathPrivateKey() {
    return $this->filepath_private_key;
  }

  /**
   * @param string $filepath_private_key
   */
  public function setFilepathPrivateKey($filepath_private_key) {
    $this->filepath_private_key = $filepath_private_key;
  }

  /**
   * @return string
   */
  public function getFilepathCertificate() {
    return $this->filepath_certificate;
  }

  /**
   * @param string $filepath_certificate
   */
  public function setFilepathCertificate($filepath_certificate) {
    $this->filepath_certificate = $filepath_certificate;
  }

  /**
   * @return string
   */
  public function getAuthType() {
    return $this->auth_type;
  }

  /**
   * @param string $auth_type
   */
  public function setAuthType($auth_type) {
    if (array_key_exists($auth_type, self::$auth_types)) {
      $this->auth_type = $auth_type;
    }
  }

  /**
   * @param bool $is_dummy
   */
  public function setIsDummy($is_dummy) {
    $this->is_dummy = $is_dummy;
  }

  public function url($type = "view") {
    switch ($type) {
      case "view":
        return str_replace('%', $this->getId(), EPIC_PID_URL_CONFIG_SERVICE_VIEW);
      case "edit":
        return str_replace('%', $this->getId(), EPIC_PID_URL_CONFIG_SERVICE_EDIT);
    }
  }

  /**
   * @return int
   */
  public
  function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public
  function setId(
    $id
  ) {
    $this->id = $id;
  }

  public function testAuthentication() {
    $api = $this->getAPI();

    return $api->testAuthentication($this);
  }

  /**
   * @return \EpicPidApiInterface
   */
  public function getAPI() {
    $class = $this->getServiceType();
    return new $class;
  }

  /**
   * @return bool
   */
  public function isDummy() {
    return $this->is_dummy;
  }

  public function getForm($type = "new") {
    $form = [];

    $form['service_type'] = $this->getFormField("service_type");
    $form['name'] = $this->getFormField("name");
    $form['url'] = $this->getFormField("url");
    $form['service_prefix'] = $this->getFormField("service_prefix");
    $form['auth_type'] = $this->getFormField('auth_type');
    $form['filepath_private_key'] = $this->getFormField('filepath_private_key');
    $form['filepath_certificate'] = $this->getFormField('filepath_certificate');
    $form['username'] = $this->getFormField("username");
    $form['password'] = $this->getFormField("password");
    $form['method'] = $this->getFormField("method");
    $form['prefix'] = $this->getFormField("prefix");
    $form['suffix'] = $this->getFormField("suffix");
    $form['resolve_url'] = $this->getFormField("resolve_url");
    $form['is_dummy'] = $this->getFormField("is_dummy");

    if ($type == 'edit') {
      $form['is_dummy']['#disabled'] = TRUE;
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    $form['cancel'] = [
      '#type' => 'button',
      '#submit' => ['epic_pid_service_cancel'],
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
      '#limit_validation_errors' => [],
    ];

    return $form;
  }

  public function getFormField($fieldname) {
    switch ($fieldname) {
      case 'service_type':
        $description = t('Which type of service provider shall be registered?');
        $attributes = [];
        if ($this->hasPids()) {
          $attributes['readonly'] = 'readonly';
          $description = t('Service type cannot be changed because 
          PIDs have been generated using this service.');
        }
        return [
          '#type' => 'select',
          '#title' => t('Service type'),
          '#description' => $description,
          '#options' => self::$service_types,
          '#default_value' => $this->getServiceType(),
          '#required' => TRUE,
          '#attributes' => $attributes,
        ];
        break;

      case 'name':
        return [
          '#type' => 'textfield',
          '#title' => t('PID Service name'),
          '#description' => t('Enter a unique name for the PID Service.'),
          '#default_value' => $this->name,
          '#required' => TRUE,
        ];
        break;

      case 'url':
        $description = t('Enter the base URL for API calls to the server.');
        $attributes = [];
        if ($this->hasPids()) {
          $attributes['readonly'] = 'readonly';
          $description = t('URL cannot be changed because PIDs have been 
          generated using this service.');

        }
        return [
          '#type' => 'textfield',
          '#title' => t('Server URL'),
          '#description' => $description,
          '#default_value' => $this->url,
          '#required' => TRUE,
          '#attributes' => $attributes,
        ];
        break;

      case 'service_prefix':
        $description = t('Enter the service-prefix for API calls to the server.');
        $attributes = [];
        if ($this->hasPids()) {
          $attributes['readonly'] = 'readonly';
          $description = t('Service-prefix cannot be changed because PIDs have been 
          generated using this service.');

        }
        return [
          '#type' => 'textfield',
          '#title' => t('Service prefix'),
          '#description' => $description,
          '#default_value' => $this->service_prefix,
          '#required' => TRUE,
          '#attributes' => $attributes,
        ];
        break;

      case 'filepath_private_key':
        return [
          '#type' => 'textfield',
          '#title' => t('File System Path to Private Key'),
          '#description' => t('File system path to private key (required Handle.net key/certificate authentication method)'),
          '#default_value' => $this->getFilepathPrivateKey(),
        ];
        break;

      case 'filepath_certificate':
        return [
          '#type' => 'textfield',
          '#title' => t('File System Path to Certificate'),
          '#description' => t('File system path to certificate (required Handle.net key/certificate authentication method)'),
          '#default_value' => $this->getFilepathCertificate(),
        ];
        break;

      case 'username':
        return [
          '#type' => 'textfield',
          '#title' => t('Username'),
          '#description' => t('Enter a username for the connection to the 
            server.'),
        ];
        break;

      case 'password':
        return [
          '#type' => 'password',
          '#title' => t('Password'),
          '#description' => t('Enter a password for Basic HTTP 
          Authentication to the server.'),
        ];
        break;

      case 'auth_type':
        $description = t('Which authentication method is required by the service provider?');
        $attributes = [];
        if ($this->hasPids()) {
          $attributes['readonly'] = 'readonly';
          $description = t('Authentication method cannot be changed because 
          PIDs have been generated using this service.');
        }
        return [
          '#type' => 'radios',
          '#title' => t('Authentication method'),
          '#description' => $description,
          '#options' => self::$auth_types,
          '#default_value' => $this->getAuthType(),
          '#required' => TRUE,
          '#attributes' => $attributes,
        ];
        break;

      case 'method':
        $description = t('Choose the method to generate PIDs.');
        $attributes = [];
        if ($this->hasPids()) {
          $attributes['readonly'] = 'readonly';
          $description = t('Generation method cannot be changed because 
          PIDs have been generated using this service.');
        }
        return [
          '#type' => 'radios',
          '#title' => t('PID generation method'),
          '#description' => $description,
          '#options' => self::$methods,
          '#default_value' => $this->getMethod(),
          '#required' => TRUE,
          '#attributes' => $attributes,
        ];
        break;

      case 'prefix':
        $description = t('Enter a prefix for generated Persistent Identitifiers.');
        $attributes = [];
        if ($this->hasPids()) {
          $attributes['readonly'] = 'readonly';
          $description = t('Prefix cannot be changed because PIDs have been 
          generated using this service.');
        }
        elseif ($this->method == self::MANUAL) {
          $attributes['readonly'] = 'readonly';
          $description = t('Prefix not available for manual PID generation mode.');
        }
        return [
          '#type' => 'textfield',
          '#title' => t('PID prefix'),
          '#description' => $description,
          '#default_value' => $this->prefix,
          '#attributes' => $attributes,
        ];
        break;

      case 'suffix':
        $description = t('Enter a suffix for generated Persistent Identitifiers.');
        $attributes = [];
        if ($this->hasPids()) {
          $attributes['readonly'] = 'readonly';
          $description = t('Suffix cannot be changed because PIDs have been 
          generated using this service.');
        }
        elseif ($this->method == self::MANUAL) {
          $attributes['readonly'] = 'readonly';
          $description = t('Prefix not available for manual PID generation mode.');
        }
        return [
          '#type' => 'textfield',
          '#title' => t('PID suffix'),
          '#description' => $description,
          '#default_value' => $this->suffix,
          '#attributes' => $attributes,
        ];
        break;

      case 'resolve_url':
        $description = t('Enter the resolver URL for PIDs defined by this service, 
        for example "hdl.handle.net".');
        $attributes = [];
        return [
          '#type' => 'textfield',
          '#title' => t('Resolver URL'),
          '#description' => $description,
          '#default_value' => $this->resolve_url,
          '#required' => TRUE,
          '#attributes' => $attributes,
        ];
        break;

      case 'is_dummy':
        return [
          '#type' => 'checkbox',
          '#title' => t("Dummy Service for Testing?"),
          '#description' => t("Is this service a dummy instance for 
                              development and testing"),
          '#default_value' => $this->isDummy(),
        ];
        break;
    }
  }

  /**
   * @return bool
   */
  public function hasPids() {
    $pids = EpicPidRepository::findAllByServiceId($this->id);

    if (count($pids) > 0) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @return string
   */
  public
  function getMethod() {
    return $this->method;
  }

  /**
   * @param string $method
   */
  public
  function setMethod(
    $method
  ) {
    $this->method = $method;
  }

  public function getSelectField() {
    $services = EpicPidServiceRepository::findAll();
    $options = [];
    foreach ($services as $service) {
      $options[$service->getId()] = $service->getName();
    }
    return [
      '#type' => 'select',
      '#title' => t('Persistent Identifier Service'),
      '#description' => t('Choose a Persistent Identifier service.'),
      '#options' => $options,
      '#default_value' => $this->getId(),
      '#empty_option' => 'None',
      '#required' => TRUE,
    ];
  }

  /**
   * @return mixed
   */
  public
  function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public
  function setName(
    $name
  ) {
    $this->name = $name;
  }

  public function duplicate() {
    $duplicate = clone($this);
    $duplicate->setId(self::EMPTY_ID);
    $duplicate->setName($this->name . ' (Copy)');
    $duplicate->save();
    return $duplicate;
  }

  public function save() {
    $id = EpicPidServiceRepository::save($this);

    if ($this->isEmpty() && $id != self::EMPTY_ID) {
      $this->id = $id;
    }
  }

  public
  function isEmpty() {
    if ($this->id == self::EMPTY_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function delete() {
    if ($this->hasPids()) {
      drupal_set_message(t('Cannot delete PID Service because at least one 
      PID has been registered.'), "error");
      return FALSE;
    }
    else {
      return EpicPidServiceRepository::delete($this);
    }
  }

  /**
   * @return \EpicPid[]
   */
  public function getAssociatedPids() {
    return EpicPidRepository::findAllByServiceId($this->id);
  }

  public function json() {
    return json_encode([
      $this->id,
      $this->name,
      $this->url,
      $this->service_prefix,
      $this->userpass,
      $this->method,
      $this->prefix,
      $this->suffix,
      $this->resolve_url,
      $this->is_dummy,
    ]);
  }

  public function getServiceUrl() {
    return $this->url . $this->service_prefix . '/';
  }

  /**
   * @return string
   */
  public
  function getUserpass() {
    return $this->userpass;
  }

  /**
   * @param string $userpass
   */
  public
  function setUserpass(
    $userpass
  ) {
    $this->userpass = $userpass;
  }

  /**
   * @return mixed
   */
  public
  function getPrefix() {
    return $this->prefix;
  }

  /**
   * @param mixed $prefix
   */
  public
  function setPrefix(
    $prefix
  ) {
    $this->prefix = $prefix;
  }

  /**
   * @return mixed
   */
  public
  function getSuffix() {
    return $this->suffix;
  }

  /**
   * @param mixed $suffix
   */
  public
  function setSuffix(
    $suffix
  ) {
    $this->suffix = $suffix;
  }

  /**
   * @return mixed
   */
  public
  function getUrl() {
    return $this->url;
  }

  /**
   * @param mixed $url
   */
  public
  function setUrl(
    $url
  ) {
    $this->url = $url;
  }

  /**
   * @return mixed
   */
  public function getServicePrefix() {
    return $this->service_prefix;
  }

  /**
   * @param mixed $service_prefix
   */
  public function setServicePrefix($service_prefix) {
    $this->service_prefix = $service_prefix;
  }

  /**
   * @return mixed
   */
  public function getResolveUrl() {
    return $this->resolve_url;
  }

  /**
   * @param mixed $resolve_url
   */
  public function setResolveUrl($resolve_url) {
    $this->resolve_url = $resolve_url;
  }

}