<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class DummyAPI implements EpicPidApiInterface {

  public function testAuthentication(EpicPidService $epicPidService): bool {
    /**
     * Successful connection test return boolean.
     */
    return TRUE;
  }

  public function create(EpicPid $epicPid) {

    $pidService = EpicPidServiceRepository::findById($epicPid->getServiceId());

    switch ($pidService->getMethod()) {
      case EpicPidService::MANUAL:
        /**
         * Manual PID generation should refer to update() method.
         */
        return $this->update($epicPid);
        break;
      case EpicPidService::AUTO_GENERATE:
        /**
         * Generate and return fake PID.
         */
        return $pidService->getPrefix() . md5(time()) . $pidService->getSuffix();
    }
  }

  public function update(EpicPid $epicPid): bool {
    /**
     * Successful update return boolean.
     */
    return TRUE;
  }

}