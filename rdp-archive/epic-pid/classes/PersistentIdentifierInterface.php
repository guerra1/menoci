<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Interface PersistentIdentifierInterface
 *
 * ToDo: doc
 */
interface PersistentIdentifierInterface {

  const EMPTY_ID = -1;

  /**
   * @return string The resolve-able URL representation of this PID.
   */
  public function getUrl();

  /**
   * @return string The PID with service-prefix but without resolver URL.
   */
  public function getPid();

  /**
   * @return mixed A Persistent Identifier or constant EMPTY_ID if not yet
   *   stored.
   */
  public function getId();

  /**
   * @return int Internal ID of the PID-issuing service.
   */
  public function getServiceId();

  /**
   * @return string The URL the Persistent Identifier points to.
   */
  public function getTargetUrl();

  /**
   * Save identifier with the associated PID service.
   */
  public function save();

}