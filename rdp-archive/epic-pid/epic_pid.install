<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Implements @see \hook_install()
 */
function epic_pid_install() {
  // ToDo: something
}

/**
 * Implements @see \hook_schema()
 */
function epic_pid_schema() {

  $schema['epic_pid'] = [
    'description' => 'Stores EPIC Persistent Identifiers data.',
    'fields' => [
      'id' => [
        'description' => 'The Persistent Identifier.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => -1,
      ],
      'service_id' => [
        'description' => 'Internal ID of the associated EPIC PID service.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'target_url' => [
        'description' => 'The PID target URL.',
        'type' => 'text',
        'not null' => TRUE,
      ],
      'status' => [
        'description' => 'The PIDs status.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['id'],
    'unique keys' => [
      'name' => ['id'],
    ],

  ];
  $schema['epic_pid_service'] = [
    'description' => 'Stores EpicPidService object data.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique server object ID.',
      ],
      'service_type' => [
        'description' => 'Type of the EPIC PID service.',
        'type' => 'varchar',
        'length' => 254,
        'not null' => TRUE,
        'default' => '',
      ],
      'name' => [
        'description' => 'The human-readable name of the object.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ],
      'url' => [
        'description' => 'The service URL for API calls.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'service_prefix' => [
        'description' => 'The service prefix for API calls.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'resolve_url' => [
        'description' => 'The resolver URL for PIDs.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'method' => [
        'description' => 'The method of PID / UUID generation.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ],
      'prefix' => [
        'description' => 'A prefix for PID/UUID generation.',
        'type' => 'varchar',
        'length' => 128,
      ],
      'suffix' => [
        'description' => 'A suffix for PID/UUID generation.',
        'type' => 'varchar',
        'length' => 128,
      ],
      'auth_type' => [
        'description' => 'Authentication type',
        'type' => 'varchar',
        'length' => 128,
      ],
      'userpass' => [
        'description' => 'HTTP Auth credentials for this service.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'filepath_private_key' => [
        'description' => 'Filesystem path to private key file',
        'type' => 'varchar',
        'length' => 254,
      ],
      'filepath_certificate' => [
        'description' => 'Filesystem path to certificate file',
        'type' => 'varchar',
        'length' => 254,
      ],
      'is_dummy' => [
        'description' => 'Boolean indicator whether service is a dummy 
                          instance for development and testing.',
        'type' => 'int',
        'length' => 1,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['id'],
    'unique keys' => [
      'name' => ['name'],
    ],
  ];


  return $schema;
}

/**
 * Adds new service_prefix field to table epic_pid_service.
 */
function epic_pid_update_7001() {

  $table = 'epic_pid_service';
  $field = 'service_prefix';
  $spec = [
    'description' => 'The service prefix for API calls.',
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
    'default' => '',
  ];

  db_add_field($table, $field, $spec);

}

/**
 * Adds new pid_handle field to table epic_pid_service.
 */
function epic_pid_update_7002() {

  $table = 'epic_pid_service';
  $field = 'resolve_url';
  $spec = [
    'description' => 'The resolver URL for PIDs.',
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
    'default' => '',
  ];

  db_add_field($table, $field, $spec);

}

/**
 * Adds new status field to table epic_pid.
 */
function epic_pid_update_7003() {

  $table = 'epic_pid';
  $field = 'status';
  $spec = [
    'description' => 'The PIDs status.',
    'type' => 'int',
    'not null' => TRUE,
    'unsigned' => TRUE,
    'default' => 0,
  ];

  db_add_field($table, $field, $spec);

}

/**
 * Adds new is_dummy field to table epic_pid.
 */
function epic_pid_update_7004() {

  $table = 'epic_pid_service';
  $field = 'is_dummy';
  $spec = [
    'description' => 'Boolean indicator whether service is a dummy 
                      instance for development and testing.',
    'type' => 'int',
    'length' => 1,
    'not null' => TRUE,
    'default' => 0,
  ];

  db_add_field($table, $field, $spec);
}

/**
 * Adds database fields for PID Service authentication type processing
 */
function epic_pid_update_7005() {

  $table = 'epic_pid_service';
  $fields = [
    'auth_type' => [
      'description' => 'Authentication type',
      'type' => 'varchar',
      'length' => 128,
    ],
    'filepath_private_key' => [
      'description' => 'Filesystem path to private key file',
      'type' => 'varchar',
      'length' => 254,
    ],
    'filepath_certificate' => [
      'description' => 'Filesystem path to certificate file',
      'type' => 'varchar',
      'length' => 254,
    ],
  ];
  foreach ($fields as $key => $spec) {
    if (!db_field_exists($table, $key)) {
      db_add_field($table, $key, $spec);
    }
  }
}

/**
 * Adds database field for PID Service type processing
 */
function epic_pid_update_7006() {

  $table = 'epic_pid_service';
  $fields = [
    'service_type' => [
      'description' => 'Type of the EPIC PID service.',
      'type' => 'varchar',
      'length' => 254,
      'not null' => TRUE,
      'default' => '',
    ],
  ];
  foreach ($fields as $key => $spec) {
    if (!db_field_exists($table, $key)) {
      db_add_field($table, $key, $spec);
    }
  }

  /**
   * Set defaults
   */
  db_query("UPDATE $table SET service_type = '" . DummyAPI::class . "' WHERE is_dummy = 1")->execute();
  db_query("UPDATE $table SET service_type = '" . EpicApiV2::class . "' WHERE service_type=''")->execute();
}